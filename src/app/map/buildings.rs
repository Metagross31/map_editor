use egui::Ui;
use sr_libs::utils::commands::{ParsingError, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::{AcquirableEntity, Param};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct Buildings(pub Vec<Building>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Building {
    pub b: AcquirableEntity,
    pub version_3_or_2: u8,
    // version_3, version_2 not yet supported
    pub entity_id: u32,
    pub team: u8,
}

impl Buildings {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Building::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for building in &self.0 {
            building.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Buildings {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Buildings {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Building {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut buildings = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (building, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} buildings, failed to parse the next Building, because: {e:?}", buildings.len())))?;
            buildings.push(building);
            loop_buf = buf;
        }
        Ok(buildings)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (two, buf) = read_u16(buf)?;
        if two != 2 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 2 found {two}, there should be only buildings!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (version_3_or_2, buf) = read_u8(buf)?;
        let (entity_id, buf) = read_u32(buf)?;
        let (team, buf) = read_u8(buf)?;
        let (zero, buf) = read_u32(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (Building 4)")});
        }
        let (zero, buf) = read_u8(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (Building 5)")});
        }
        let (one, buf) = read_u8(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (Building 6)")});
        }
        Ok((Self{ b, version_3_or_2, entity_id, team }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(2);
        self.b.write(buf);
        buf.put_u8(self.version_3_or_2);
        buf.put_u32_le(self.entity_id);
        buf.put_u8(self.team);
        buf.put_u32_le(0);
        buf.put_u8(0);
        buf.put_u8(1);
    }
    fn fix_bug(&mut self) {
        self.b.b.b.params.retain(|p| match p { Param::BUG(_) => false, _ => true });
    }
    fn is_not_openable_chest(&self) -> bool {
        self.entity_id == 2000314 && !self.b.b.b.params.iter().any(|p| match p { Param::Chest(_) => true, _ => false })
    }
}

impl Validate for Building {
     fn validate(&self, _map: &Map) -> IssueCounter {
         let mut i = IssueCounter::default();
         if self.b.b.b.params.iter().any(|p| match p { Param::BUG(_) => true, _ => false }) {
             i.add_bug("Parameter known to be causing game to crash (Nexus Portal".to_string());
         }
         if self.b.b.b.often_one != 1 {
             i.add_warning(format!("Unknown field that is often 1 is {} on building: {}", self.b.b.b.often_one, self.entity_id))
         }
         if self.version_3_or_2 != 3 {
             i.add_warning(format!("Version field expected to be 3, but is {} on building: {}", self.version_3_or_2, self.entity_id))
         }

         if self.is_not_openable_chest() {
             i.add_warning(format!("Chest {} can not be opened (not fixed automatically)", self.entity_id))
         }
         i
     }
 }

impl Fix for Building {
    fn fix(&mut self, _map: &Map) {
        self.fix_bug();
        //self.b.b.b.often_one = 1;
    }
}

impl Building {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because parsing some ID would be too much work :(
            ui.push_id(self as *mut _ as usize, |ui|{
                if self.is_not_openable_chest() {
                    let r = ui.button("make openable");
                    if r.clicked() {
                        // TODO how to get right ID?
                        self.b.b.b.params.push(Param::Chest(42));
                    }
                    r.on_hover_text("This is a chest, and can be made intractable");
                }
                let have_bug = self.b.b.b.params.iter().any(|p| match p { Param::BUG(_) => true, _ => false });
                if have_bug {
                    let r = ui.button("FIX BUG");
                    if r.clicked() {
                        self.fix_bug();
                    }
                    r.on_hover_text("Nexus portal (player card) is generating this bug");
                    ui.colored_label(ui.style().visuals.error_fg_color, format!("{self:?}")).on_hover_text("The param called bug appear when Portal nexus is placed on a map");
                } else {
                    if self.b.b.b.often_one != 1 {
                        let r = ui.button("FIX warning");
                        if r.clicked() {
                            self.b.b.b.often_one = 1;
                        }
                        r.on_hover_text("The field is usually one, otherwise game does not load it, but editor is OK with it");
                        ui.colored_label(ui.style().visuals.warn_fg_color, format!("{self:?}"));
                    } else {
                        ui.label(format!("{self:?}"));
                    }
                }
            });
        });
    }
}
