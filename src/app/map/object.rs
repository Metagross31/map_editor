use egui::Ui;
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use sr_libs::utils::commands::{ParsingError, read_u16, read_u32, read_u8};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::PositionedEntity;
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct Objects(pub Vec<Object>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Object {
    pub b: PositionedEntity,
    pub unknown_0: u32,
    pub unknown_1: u16,
    pub flag: u16,
    pub unknown_2: u32,
    pub unknown_4: u32,
    pub unknown_5: u32,
    pub unknown_6: u8,
    pub unknown_7: u32,
}

impl Objects {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Object::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for building in &self.0 {
            building.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Objects {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Objects {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Object {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut objects = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (obj, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} objects, failed to parse the next Objects, because: {e:?}", objects.len())))?;
            objects.push(obj);
            loop_buf = buf;
        }
        Ok(objects)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (tree, buf) = read_u16(buf)?;
        if tree != 3 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 3 found {tree}, there should be only objects!")});
        }
        let (b, buf) = PositionedEntity::read(buf)?;
        let (unknown_0, buf) = read_u32(buf)?;
        let (unknown_1, buf) = read_u16(buf)?;
        let (flag, buf) = read_u16(buf)?;
        let (unknown_2, buf) = read_u32(buf)?;
        let (unknown_4, buf) = read_u32(buf)?;
        let (unknown_5, buf) = read_u32(buf)?;
        let (unknown_6, buf) = read_u8(buf)?;
        let (unknown_7, buf) = read_u32(buf)?;
        Ok((Self{ b, unknown_0, unknown_1, flag, unknown_2, unknown_4, unknown_5, unknown_6, unknown_7 }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(3);
        self.b.write(buf);
        buf.put_u32_le(self.unknown_0);
        buf.put_u16_le(self.unknown_1);
        buf.put_u16_le(self.flag);
        buf.put_u32_le(self.unknown_2);
        buf.put_u32_le(self.unknown_4);
        buf.put_u32_le(self.unknown_5);
        buf.put_u8(self.unknown_6);
        buf.put_u32_le(self.unknown_7);
    }
}

impl Validate for Object {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for Object {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Object {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because parsing some ID would be too much work :(
            ui.push_id(self as *mut _ as usize, |ui|{
                ui.label(format!("{self:?}"));
            });
        });
    }
}

