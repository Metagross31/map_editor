use sr_libs::utils::commands::{encode_str, ParsingError, read_f32, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Entity {
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub enum Param {
    Tag(String),
    PKit(String),
    Chest(u32),
    Id(u32),
    BUG(Vec<u8>),
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct IdEntity {
    pub b: Entity,
    pub often_one: u8,
    pub params: Vec<Param>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Position {
    pub x: f32,
    pub z: f32,
    pub y: f32,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PositionedEntity {
    pub b: IdEntity,
    pub unknown: u8,
    pub collision_pos: Position,
    pub vis_pos: Position,
    pub often_zero: u32,
    pub rotation: f32, // in radians
    pub often_zero_2: u32,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct AcquirableEntity {
    pub b: PositionedEntity,
    pub no_player_acquire: bool,
}

impl Entity {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (zero, buf) = read_u16(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (entity 0)")});
        }
        let (zero, buf) = read_u32(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (entity 1)")});
        }
        let (tree, buf) = read_u8(buf)?;
        if tree != 3 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 3 found {tree} (entity 2)")});
        }
        Ok((Self{}, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(0);
        buf.put_u32_le(0);
        buf.put_u8(3);
    }
}

impl IdEntity {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (b, buf) = Entity::read(buf)?;
        let (often_one, buf) = read_u8(buf)?;
        let (count, buf) = read_u16(buf)?;
        let mut params = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (param, buf) = Param::read(loop_buf)?;
            params.push(param);
            loop_buf = buf;
        }
        Ok((Self{
            b,
            often_one,
            params,
        }, loop_buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        self.b.write(buf);
        buf.put_u8(self.often_one);
        buf.put_u16_le(self.params.len() as u16);
        for param in &self.params {
            param.write(buf);
        }
    }
}

impl Param {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (param_type, buf) = read_u16(buf)?;
        let (mostly_one, buf) = read_u8(buf)?;
        match mostly_one {
            1 => {}
            64 => {
                // TODO this might be wrong
                let (block, buf) = buf.split_at(26);
                return Ok((Self::BUG(block.to_vec()), buf))
            }
            _ => {
                return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 or 64 found {mostly_one} (Param 1)")});
            }
        }
        match param_type {
            1 => {
                let (tag, buf) = read_string(buf)?;
                Ok((Self::Tag(tag.to_string()), buf))
            }
            13 => {
                let (p_kit, buf) = read_string(buf)?;
                Ok((Self::PKit(p_kit.to_string()), buf))
            }
            26 => {
                let (chest, buf) = read_u32(buf)?;
                Ok((Self::Chest(chest), buf))
            }
            27 => {
                let (id, buf) = read_u32(buf)?;
                Ok((Self::Id(id), buf))
            }
            _ => {
                Err(ParsingError::UnexpectedData {internal_error: format!("{param_type} is unexpected param_type")})
            }
        }
    }
    pub fn write(&self, buf: &mut BytesMut) {
        match self {
            Param::Tag(tag) => {
                buf.put_u16_le(1);
                buf.put_u8(1);
                encode_str(buf, tag);
            }
            Param::PKit(p_kit) => {
                buf.put_u16_le(13);
                buf.put_u8(1);
                encode_str(buf, p_kit);
            }
            Param::Chest(chest) => {
                buf.put_u16_le(26);
                buf.put_u8(1);
                buf.put_u32_le(*chest);
            }
            Param::Id(id) => {
                buf.put_u16_le(27);
                buf.put_u8(1);
                buf.put_u32_le(*id);
            }
            Param::BUG(bug) => {
                buf.put_u16_le(1);
                buf.put_u8(64);
                buf.put_slice(bug);
            }
        }
    }
}

impl Position {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (x, buf) = read_f32(buf)?;
        let (z, buf) = read_f32(buf)?;
        let (y, buf) = read_f32(buf)?;
        Ok((Self{x, z, y}, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_f32_le(self.x);
        buf.put_f32_le(self.z);
        buf.put_f32_le(self.y);
    }
}

impl PositionedEntity {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (b, buf) = IdEntity::read(buf)?;
        let (unknown, buf) = read_u8(buf)?;
        let (collision_pos, buf) = Position::read(buf)?;
        let (vis_pos, buf) = Position::read(buf)?;
        let (often_zero, buf) = read_u32(buf)?;
        let (rotation, buf) = read_f32(buf)?;
        let (often_zero_2, buf) = read_u32(buf)?;
        let buf = if unknown == 2 {
            let (one, buf) = read_u32(buf)?;
            if one != 1 {
                return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PositionedEntity 6)")});
            }
            buf
        } else {
            buf
        };
        Ok((Self{
            b,
            unknown,
            collision_pos,
            vis_pos,
            often_zero,
            rotation,
            often_zero_2,
        }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        self.b.write(buf);
        buf.put_u8(self.unknown);
        self.collision_pos.write(buf);
        self.vis_pos.write(buf);
        buf.put_u32_le(self.often_zero);
        buf.put_f32_le(self.rotation);
        buf.put_u32_le(self.often_zero_2);
        if self.unknown == 2 {
            buf.put_u32_le(1);
        }
    }
}

impl AcquirableEntity {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (b, buf) = PositionedEntity::read(buf)?;
        let (no_player_acquire, buf) = read_u32(buf)?;
        let no_player_acquire = if no_player_acquire == 0 {
            false
        } else if no_player_acquire == 1 {
            true
        } else {
            return Err(ParsingError::UnexpectedData {internal_error: format!("AcquirableEntity no_player_acquire={no_player_acquire}")})
        };
        Ok((Self{ b, no_player_acquire }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        self.b.write(buf);
        buf.put_u32_le(if self.no_player_acquire { 1 } else { 0 });
    }
}
