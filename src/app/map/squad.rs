use egui::Ui;
use sr_libs::utils::commands::{ParsingError, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::PositionedEntity;
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct Squads(pub Vec<Squad>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Squad {
    pub b: PositionedEntity,
    pub entity_id: u32,
    pub team: u8,
}

impl Squads {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Squad::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for squad in &self.0 {
            squad.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Squads {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Squads {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Squad {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut squads = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (building, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} squads, failed to parse the next Squad, because: {e:?}", squads.len())))?;
            squads.push(building);
            loop_buf = buf;
        }
        Ok(squads)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (six, buf) = read_u16(buf)?;
        if six != 6 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 6 found {six}, there should be only squads!")});
        }
        let (b, buf) = PositionedEntity::read(buf)?;
        let (zero, buf) = read_u32(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (Squad 0)")});
        }
        let (tree, buf) = read_u8(buf)?;
        if tree != 3 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 3 found {tree} (Squad 1)")});
        }
        let (entity_id, buf) = read_u32(buf)?;
        let (team, buf) = read_u8(buf)?;
        Ok((Self{ b,  entity_id, team }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(6);
        self.b.write(buf);
        buf.put_u32_le(0);
        buf.put_u8(3);
        buf.put_u32_le(self.entity_id);
        buf.put_u8(self.team);
    }
}

impl Validate for Squad {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for Squad {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Squad {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
