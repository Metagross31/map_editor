
use egui::Ui;
use sr_libs::utils::commands::{encode_str, ParsingError, read_f32, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::{IdEntity, Position};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct AtmoZones{
    pub b: IdEntity,
    pub unknown_byte: u8,
    pub zones: Vec<AtmoZone>,
    pub unk_u16_1: u16,
    pub unk_u16_2: u16,
    pub y: u32,
    pub x: u32,
    pub data: Vec<u8>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct AtmoZone {
    u8_0: u8,
    s_0: String,
    u32_0: u32,
    pos: Position,
    u32_1: u32,
    f32_0: f32,
    f32_1: f32,
    inner_0: Vec<Inner0>,
    inner_1: Vec<Inner1>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Inner0 {
    str: String,
    u8_0: u8,
    f32_0: f32,
    f32_1: f32,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Inner1 {
    str: String,
    u8_0: u8,
    f32_0: f32,
    f32_1: f32,
    f32_2: f32,
}

impl AtmoZones {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (seventeen, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 17 failed, because: {e:?}")))?;
        if seventeen != 17 {
            return Err(MapError::MapFileParsing (format!("Expected 17 found {seventeen}, there should be only AtmoZone!")));
        }
        let (b, buf) = IdEntity::read(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing IdEntity failed, because: {e:?}")))?;
        let (four, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing x failed, because: {e:?}")))?;
        if four != 4 {
            return Err(MapError::MapFileParsing (format!("Expected 4 found {four}!")));
        }
        let (unknown_byte, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing unknown_byte failed, because: {e:?}")))?;
        let (count, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing zones count failed, because: {e:?}")))?;
        let mut zones = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (zone, buf) = AtmoZone::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing zone failed, while having {} zones already parsed, because: {e:?}", zones.len())))?;
            zones.push(zone);
            loop_buf = buf;
        }
        let (unk_u16_1, buf) = read_u16(loop_buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing unk_u16_1 failed, because: {e:?}")))?;
        let (unk_u16_2, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing unk_u16_2 failed, because: {e:?}")))?;
        let (x, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing x failed, because: {e:?}")))?;
        let (y, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing y failed, because: {e:?}")))?;
        let (data, buf) = buf.split_at((x*y) as usize);

        if !buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones): {buf:?}")))
        }

        Ok(Self{ b, unknown_byte, zones, unk_u16_1, unk_u16_2, y: x, x: y, data: data.to_vec() })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u16_le(17);
        self.b.write(&mut buf);
        buf.put_u8(4);
        buf.put_u8(self.unknown_byte);
        buf.put_u8(self.zones.len() as u8);
        for zone in &self.zones {
            zone.write(&mut buf);
        }
        buf.put_u16_le(self.unk_u16_1);
        buf.put_u16_le(self.unk_u16_2);
        buf.put_u32_le(self.y);
        buf.put_u32_le(self.x);
        buf.extend_from_slice(&self.data);
        buf.to_vec()
    }
}

impl Fix for AtmoZones {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for AtmoZones {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl AtmoZone {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (u8_0, buf) = read_u8(buf)?;
        let (s_0, buf) = read_string(buf)?;
        let (u32_0, buf) = read_u32(buf)?;
        let (pos, buf) = Position::read(buf)?;
        let (u32_1, buf) = read_u32(buf)?;
        let (f32_0, buf) = read_f32(buf)?;
        let (f32_1, buf) = read_f32(buf)?;
        let (count, buf) = read_u8(buf)?;
        let mut loop_buf = buf;
        let mut inner_0 = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let (str, buf) = read_string(loop_buf)?;
            let (u8_0, buf) = read_u8(buf)?;
            let (f32_0, buf) = read_f32(buf)?;
            let (f32_1, buf) = read_f32(buf)?;
            inner_0.push(Inner0{ str: str.to_string(), u8_0, f32_0, f32_1 });
            loop_buf = buf;
        }
        let (count, buf) = read_u8(loop_buf)?;
        let mut loop_buf = buf;
        let mut inner_1 = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let (str, buf) = read_string(loop_buf)?;
            let (u8_0, buf) = read_u8(buf)?;
            let (f32_0, buf) = read_f32(buf)?;
            let (f32_1, buf) = read_f32(buf)?;
            let (f32_2, buf) = read_f32(buf)?;
            inner_1.push(Inner1{ str: str.to_string(), u8_0, f32_0, f32_1, f32_2 });
            loop_buf = buf;
        }
        Ok((Self{ u8_0, s_0: s_0.to_string(), u32_0, pos, u32_1, f32_0, f32_1, inner_0, inner_1, }, loop_buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u8(self.u8_0);
        encode_str(buf, &self.s_0);
        buf.put_u32_le(self.u32_0);
        self.pos.write(buf);
        buf.put_u32_le(self.u32_1);
        buf.put_f32_le(self.f32_0);
        buf.put_f32_le(self.f32_1);
        buf.put_u8(self.inner_0.len() as u8);
        for i in self.inner_0.iter() {
            encode_str(buf, &i.str);
            buf.put_u8(i.u8_0);
            buf.put_f32_le(i.f32_0);
            buf.put_f32_le(i.f32_1);
        }
        buf.put_u8(self.inner_1.len() as u8);
        for i in self.inner_1.iter() {
            encode_str(buf, &i.str);
            buf.put_u8(i.u8_0);
            buf.put_f32_le(i.f32_0);
            buf.put_f32_le(i.f32_1);
            buf.put_f32_le(i.f32_2);
        }
    }
}

impl Validate for AtmoZone {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for AtmoZone {
    fn fix(&mut self, _map: &Map) {
    }
}

impl AtmoZones {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
