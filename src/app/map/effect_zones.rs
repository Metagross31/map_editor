
use egui::Ui;
use sr_libs::utils::commands::{read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::{IdEntity};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct EffectZones{
    pub b: IdEntity,
    pub unknown: u16,
    pub _height: u16,
    pub _width: u16,
    pub height: u32,
    pub width: u32,
    pub zones: Vec<u8>,
}

impl EffectZones {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (eighteen, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 18 failed, because: {e:?}")))?;
        if eighteen != 18 {
            return Err(MapError::MapFileParsing (format!("Expected 18 found {eighteen}, there should be only EffectZone!")));
        }
        let (b, buf) = IdEntity::read(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing IdEntity failed, because: {e:?}")))?;
        let (two, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 2 failed, because: {e:?}")))?;
        if two != 2 {
            return Err(MapError::MapFileParsing (format!("Expected 2 found {two}!")));
        }
        let (unknown, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing unknown failed, because: {e:?}")))?;
        let (_height, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing _height failed, because: {e:?}")))?;
        let (_width, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing _width failed, because: {e:?}")))?;
        let (height, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing height failed, because: {e:?}")))?;
        let (width, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing width failed, because: {e:?}")))?;

        let size = height as usize * width as usize;
        if size > buf.len() {
            return Err(MapError::MapFileParsing(format!("Parsing failed, because: not enough data to read {size} (w: {width} * h: {height})")))
        }
        let (zones, buf) = buf.split_at(size);
        if !buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones?): {buf:?}")))
        }

        Ok(Self{ b, unknown, _width, _height, width, height, zones: zones.to_vec() })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u16_le(18);
        self.b.write(&mut buf);
        buf.put_u8(2);
        buf.put_u16_le(self.unknown);
        buf.put_u16_le(self._width);
        buf.put_u16_le(self._height);
        buf.put_u32_le(self.width);
        buf.put_u32_le(self.height);
        buf.extend_from_slice(&self.zones);
        buf.to_vec()
    }
}

impl Fix for EffectZones {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for EffectZones {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl EffectZones {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
