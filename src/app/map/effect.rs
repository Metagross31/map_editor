use egui::Ui;
use sr_libs::utils::commands::{encode_str, ParsingError, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::{AcquirableEntity};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct Effects(pub Vec<Effect>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Effect {
    pub b: AcquirableEntity,
    pub path: String,
    pub unknown: [u32;4],
}

impl Effects {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Effect::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for effect in &self.0 {
            effect.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Effects {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Effects {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Effect {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut effects = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (effect, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} effects, failed to parse the next Effect, because: {e:?}", effects.len())))?;
            effects.push(effect);
            loop_buf = buf;
        }
        Ok(effects)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (twenty_eight, buf) = read_u16(buf)?;
        if twenty_eight != 28 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 28 found {twenty_eight}, there should be only starting points!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (three, buf) = read_u8(buf)?;
        if three != 3 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 3 found {three}, this is probably unsupported version!")});
        }
        let (path, buf) = read_string(buf)?;
        let (unk0, buf) = read_u32(buf)?;
        let (unk1, buf) = read_u32(buf)?;
        let (unk2, buf) = read_u32(buf)?;
        let (unk3, buf) = read_u32(buf)?;
        let unknown = [unk0, unk1, unk2, unk3];
        Ok((Self{ b, path: path.to_string(), unknown }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(28);
        self.b.write(buf);
        buf.put_u8(3);
        encode_str(buf, &self.path);
        for unk in self.unknown.iter().copied() {
            buf.put_u32_le(unk);
        }
    }
}

impl Validate for Effect {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for Effect {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Effect {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because parsing some ID would be too much work :(
            ui.push_id(self as *mut _ as usize, |ui|{
                if self.b.b.b.often_one != 1 {
                    let r = ui.button("FIX warning");
                    if r.clicked() {
                        self.b.b.b.often_one = 1;
                    }
                    r.on_hover_text("The field is usually one, otherwise game does not load it, but editor is OK with it");
                    ui.colored_label(ui.style().visuals.warn_fg_color, format!("{self:?}"));
                } else {
                    ui.label(format!("{self:?}"));
                }
            });
        });
    }
}
