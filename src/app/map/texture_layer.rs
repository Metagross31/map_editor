
use egui::Ui;
use sr_libs::utils::commands::{encode_str, read_string, read_u16, read_u32};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TextureLayers(pub Vec<TextureLayer>);
#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TextureLayer {
    pub name: String,
    pub enabled: u32,
}

impl TextureLayers {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (one, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 1 failed, because: {e:?}")))?;
        if one != 1 {
            return Err(MapError::MapFileParsing (format!("Expected 1 found {one}.")));
        }
        let (zero, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 0 failed, because: {e:?}")))?;
        if zero != 0 {
            return Err(MapError::MapFileParsing (format!("Expected 0 found {zero}.")));
        }
        let (count, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing count failed, because: {e:?}")))?;
        let mut layers = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _i in 0..count {
            let (name, buf) = read_string(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing name {_i} failed, because: {e:?}")))?;
            let (enabled, buf) = read_u32(buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing xml {_i} failed, because: {e:?}")))?;
            layers.push(TextureLayer{ name: name.to_string(), enabled });
            loop_buf = buf;
        }
        if !loop_buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones?): {buf:?}")))
        }

        Ok(Self(layers))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u16_le(1);
        buf.put_u16_le(0);
        buf.put_u32_le(self.0.len() as u32);
        for layer in self.0.iter() {
            encode_str(&mut buf, &layer.name);
            buf.put_u32_le(layer.enabled);
        }
        buf.to_vec()
    }
}

impl Fix for TextureLayers {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for TextureLayers {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl TextureLayers {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        for layer in self.0.iter_mut() {
            layer.show_mut(ui);
        }
    }
}

impl TextureLayer {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
