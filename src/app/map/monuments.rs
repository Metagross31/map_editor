use egui::Ui;
use sr_libs::utils::commands::{ParsingError, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::AcquirableEntity;
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct Monuments(pub Vec<Monument>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Monument {
    pub b: AcquirableEntity,
    pub entity_id: u32,
}

impl Monuments {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Monument::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for monument in &self.0 {
            monument.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Monuments {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Monuments {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Monument {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut monuments = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (monument, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} monuments, failed to parse the next Monument, because: {e:?}", monuments.len())))?;
            monuments.push(monument);
            loop_buf = buf;
        }
        Ok(monuments)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (tweny_seven, buf) = read_u16(buf)?;
        if tweny_seven != 27 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 27 found {tweny_seven}, there should be only monuments!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (entity_id, buf) = read_u32(buf)?;
        let (zero, buf) = read_u8(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (Monument 1)")});
        }
        let (two, buf) = read_u8(buf)?;
        if two != 2 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 2 found {two} (Monument 2)")});
        }
        let (zero, buf) = read_u32(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (Monument 3)")});
        }
        Ok((Self{ b, entity_id }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(27);
        self.b.write(buf);
        buf.put_u32_le(self.entity_id);
        buf.put_u8(0);
        buf.put_u8(2);
        buf.put_u32_le(0);
    }
}

impl Validate for Monument {
     fn validate(&self, _map: &Map) -> IssueCounter {
         IssueCounter::default()
     }
 }

impl Fix for Monument {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Monument {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
