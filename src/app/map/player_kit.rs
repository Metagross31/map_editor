use std::ops::Deref;
use egui::{DragValue, Ui, Widget};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use sr_libs::utils::commands::{encode_str, ParsingError, read_string, read_u16, read_u32, read_u8};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::Entity;
use crate::app::map::{Map, MapError, MapTable, TEAM_CONFIGURATIONS_TABLE_ID};
use crate::app::map::team_config::{TeamSetup};

#[derive(serde::Serialize)]
pub struct PlayerKits(pub Vec<PlayerKit>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PlayerKit {
    pub b: Entity,
    pub name: String,
    pub enum_affecting_what_fields_follow: u8,
    pub starting_power: u32,
    pub starting_void_power: u32,
}

impl PlayerKits {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(PlayerKit::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for e in &self.0 {
            e.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for PlayerKits {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for PlayerKits {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl PlayerKit {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut player_kits = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (obj, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} player_kits, failed to parse the next PlayerKits, because: {e:?}", player_kits.len())))?;
            player_kits.push(obj);
            loop_buf = buf;
        }
        Ok(player_kits)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (ten, buf) = read_u16(buf)?;
        if ten != 10 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 10 found {ten}, there should be only player_kits!")});
        }
        let (b, buf) = Entity::read(buf)?;
        let (one, buf) = read_u8(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PlayerKit 1)")});
        }
        let (one, buf) = read_u16(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PlayerKit 2)")});
        }
        let (one, buf) = read_u16(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PlayerKit 3)")});
        }
        let (one, buf) = read_u8(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PlayerKit 4)")});
        }
        let (name, buf) = read_string(buf)?;
        let (team, buf) = read_u8(buf)?;
        let (one, buf) = read_u32(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PlayerKit 4)")});
        }
        let (starting_power, buf) = read_u32(buf)?;
        let (starting_void_power, buf) = read_u32(buf)?;
        Ok((Self{ b, name: name.to_string(), enum_affecting_what_fields_follow: team, starting_power, starting_void_power }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(10);
        self.b.write(buf);
        buf.put_u8(1);
        buf.put_u16_le(1);
        buf.put_u16_le(1);
        buf.put_u8(1);
        encode_str(buf, &self.name);
        buf.put_u8(self.enum_affecting_what_fields_follow);
        buf.put_u32_le(1);
        buf.put_u32_le(self.starting_power);
        buf.put_u32_le(self.starting_void_power);
    }
}

impl Validate for PlayerKit {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut i = IssueCounter::default();
        let mut found = false;
        if let Some((t, _, _, _)) = map.map.get(&TEAM_CONFIGURATIONS_TABLE_ID) {
            match t.borrow().deref() {
                MapTable::TeamConfigurations(tc) => {
                    match &tc.configurations[0].setup {
                        TeamSetup::V4(ts) => {
                            for t in &ts.teams {
                                if t.team_id == self.enum_affecting_what_fields_follow {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        TeamSetup::V5(ts) => {
                            for t in &ts.teams {
                                if t.team_id == self.enum_affecting_what_fields_follow {
                                    found = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                _ => i.add_bug(format!("table 9000 is {:?}", t.borrow().name())),
            }
        } else {
            i.add_bug("No TeamConfigurations".to_string());
        }
        if !found {
            i.add_bug(format!("Team {} does not exist", self.enum_affecting_what_fields_follow));
        }
        i
    }
}

impl Fix for PlayerKit {
    fn fix(&mut self, _map: &Map) {
    }
}

impl PlayerKit {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because I need self inside
            ui.push_id(self as *mut _ as usize, |ui|{
                ui.horizontal(|ui|{
                    ui.label("name");
                    ui.text_edit_singleline(&mut self.name)
                });
                ui.horizontal(|ui|{
                    ui.label("team");
                    DragValue::new(&mut self.enum_affecting_what_fields_follow).clamp_range(1..=20).ui(ui);
                });
                ui.horizontal(|ui|{
                    ui.label("starting power");
                    DragValue::new(&mut self.starting_power).clamp_range(0..=100_000).ui(ui);
                });
                ui.horizontal(|ui|{
                    ui.label("starting void power");
                    DragValue::new(&mut self.starting_void_power).clamp_range(0..=100_000).ui(ui);
                });
            });
        });
    }
}

