use std::cell::RefCell;
use std::collections::BTreeMap;
use std::error::Error;
use std::path::Path;
use eframe::epaint::ColorImage;
use egui::Widget;
use image::{EncodableLayout, GrayImage, RgbImage};

use crate::app::map::{_1014_TABLE_ID, ATMO_ZONES_TABLE_ID,
                      CLIFF_TABLE_ID, EFFECT_ZONES_TABLE_ID, F8003_TABLE_ID, LIGHTINGS_TABLE_ID, Map, MapTable,
                      OtherFile, PKIT_START_INFO_TABLE_ID, PLAYER_KITS_TABLE_ID,
                      TEAM_CONFIGURATIONS_TABLE_ID, TERRAIN_TABLE_ID, TEXTURE_LAYERS_TABLE_ID};
use crate::app::map::_8003::Data8003;
use crate::app::map::atlas::Atlas;
use crate::app::map::atmo_zones::AtmoZones;
use crate::app::map::cliff::Cliffs;
use crate::app::map::effect_zones::EffectZones;
use crate::app::map::entity::{Entity, IdEntity};
use crate::app::map::pkit_info::PkitInfo;
use crate::app::map::player_kit::{PlayerKit, PlayerKits};
use crate::app::map::team_config::{PlayerType, PlayerV5, Relation, RelationType, TeamConfiguration, TeamConfigurations, TeamIdentifiers, TeamIdentifierV6, TeamRelations, TeamSetup, TeamSetupV5, TeamV5};
use crate::app::map::terrain::{Blocking, BlockingFlags, CGdCliffs, CGdWaterMap, CliffMask, Heights, MapVersion, Terrain, TerrainData, TextureMaskData, TextureMasks};
use crate::app::map::texture_layer::{TextureLayer, TextureLayers};
use crate::retained_image::RetainedImage;

pub struct EmptyMap{
    pub safe_sizes: bool,
    pub width: u16,
    pub height: u16,
    height_map: HeightMapChoice,
    pub done: bool,
}


enum HeightMapChoice {
    Flat,
    Image{rgb: RetainedImage, gray: GrayImage},
    Dialog(egui_file::FileDialog)
}



impl Default for EmptyMap {
    fn default() -> Self {
        Self{
            safe_sizes: true,
            width: 256,
            height: 256,
            height_map: HeightMapChoice::Flat,
            done: false,
        }
    }
}

const SAFE_SIZES : &[(u16, &str)] = &[(128, "128"), (256, "256"), (512, "512"), (1024, "1024"), (2048, "2048")];

impl EmptyMap {
    pub fn show(&mut self, ctx: &egui::Context) {
        egui::Window::new("Create empty map").show(ctx, |ui|{
            match &mut self.height_map {
                HeightMapChoice::Image { rgb, .. } => {
                    ui.label(format!("width: {}", self.width));
                    ui.label(format!("height: {}", self.height));
                    ui.label("height map").on_hover_ui(|ui|{
                        rgb.show_scaled(ui, 1.0);
                    });
                    if ui.button("remove height map").clicked() {
                        self.height_map = HeightMapChoice::Flat;
                    }
                }
                HeightMapChoice::Dialog(d) => {
                    d.show(ui.ctx());
                    if d.selected() {
                        fn load(path: &Path) -> Result<(RgbImage, GrayImage), Box<dyn Error>> {
                            let img = std::fs::read(path)?;
                            let img = image::load_from_memory(&img)?;
                            Ok((img.to_rgb8(), img.to_luma8()))
                        }
                        #[cfg(not(target_arch = "wasm32"))]
                            let img = load(d.path().unwrap());
                            match img {
                                Ok((rgb, gray)) => {
                                    if rgb.width() > u16::MAX as u32 || rgb.height() > u16::MAX as u32 {
                                        log::error!("Invalif image size: {}x{}", rgb.width(), rgb.height());
                                        return;
                                    }
                                    self.width = rgb.width() as u16;
                                    self.height = rgb.height() as u16;
                                    let rgb = ColorImage::from_rgb(
                                        [rgb.width() as usize, rgb.height() as usize],
                                        rgb.as_bytes());
                                    let rgb = RetainedImage::from_color_image("new height map", rgb);
                                    self.height_map = HeightMapChoice::Image{rgb, gray};
                                }
                                Err(e) => {
                                    log::error!("Loading height map failed: {e}");
                                }
                            }
                    }
                }
                HeightMapChoice::Flat => {
                    ui.checkbox(&mut self.safe_sizes, "safe sizes")
                        .on_hover_text("Using unsafe sizes, might result in this editor crashing, because not all parts support them.
EA's editot on the other hand seems to handle them OK (if it does not crash for other reasons).");
                    if self.safe_sizes {
                        egui::containers::ComboBox::new("width", "width")
                            .selected_text(SAFE_SIZES.iter().find(|(s, _)| *s == self.width).map(|&(_,t)|t).unwrap_or("unsafe"))
                            .show_ui(ui, |ui|{
                                for safe in SAFE_SIZES.iter() {
                                    ui.selectable_value(&mut self.width, safe.0, safe.1);
                                }
                            });
                        egui::containers::ComboBox::new("height", "height")
                            .selected_text(SAFE_SIZES.iter().find(|(s, _)| *s == self.height).map(|&(_,t)|t).unwrap_or("unsafe"))
                            .show_ui(ui, |ui|{
                                for safe in SAFE_SIZES.iter() {
                                    ui.selectable_value(&mut self.height, safe.0, safe.1);
                                }
                            });
                    } else {
                        ui.horizontal(|ui|{
                            ui.label("width");
                            egui::DragValue::new(&mut self.width).clamp_range(64..=16384).ui(ui);
                        });
                        ui.horizontal(|ui|{
                            ui.label("height");
                            egui::DragValue::new(&mut self.height).clamp_range(64..=16384).ui(ui);
                        });
                    }
                    if ui.button("load height map").clicked() {
                        let mut d = egui_file::FileDialog::open_file(None);
                        d.open();
                        self.height_map = HeightMapChoice::Dialog(d);
                    }
                },
            }


            if ui.button("create").clicked() {
                self.done = true;
            }
        });
    }
    pub fn create(&self) -> Map {

        let mut map = BTreeMap::new();
        map.insert(10000, {
            let data = vec![2,0,0,0,0,3,0,0,0,0];
            (RefCell::new(MapTable::Unknown(data.clone())), data, 1, 1)
        });
        map.insert(PKIT_START_INFO_TABLE_ID, {
            let table = MapTable::PkitInfo(PkitInfo{
                unknown: 1,
                maybe_strings: [String::new(), String::new(), String::new(), String::new(), String::new()],
                y: self.height,
                x: self.width,
                another_0: 0,
                something: vec![
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0xB3, 0x43, 0x33, 0x33, 0xB3, 0x43, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0xB3, 0x43, 0x33, 0x33, 0xB3, 0x43 ],
                pkit_starting_points: vec![
                    /*PkitStart{
                        name: "pk_kit1".to_string(),
                        y: 10.0,
                        x: 10.0,
                    }*/
                ],
                more_zeros: [0, 0],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(TEAM_CONFIGURATIONS_TABLE_ID, {
            let table = MapTable::TeamConfigurations(TeamConfigurations{
                configurations: vec![
                    TeamConfiguration{
                        setup: TeamSetup::V5(TeamSetupV5{
                            setup: "setup".to_string(),
                            teams: vec![
                                TeamV5{
                                    team_name: "tm_Neutral".to_string(),
                                    team_id: 1,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Animal".to_string(),
                                    team_id: 2,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Creep".to_string(),
                                    team_id: 3,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Team1".to_string(),
                                    team_id: 4,
                                    players: vec![
                                        PlayerV5{
                                            player_type: PlayerType::Human,
                                            player_tag: "pl_Player1".to_string(),
                                            kit_tag: "pk_kit1".to_string(),
                                        }
                                    ],
                                },
                                TeamV5{
                                    team_name: "tm_Enemy1".to_string(),
                                    team_id: 5,
                                    players: vec![
                                        PlayerV5{
                                            player_type: PlayerType::Monster,
                                            player_tag: "pl_Enemy1".to_string(),
                                            kit_tag: "pk_enemy1".to_string(),
                                        }
                                    ],
                                },
                            ],
                        }),
                        relation: TeamRelations{
                            team_relations: BTreeMap::from_iter([
                                (Relation::new(1, 1), RelationType::Friends),
                                (Relation::new(1, 2), RelationType::Neutral),
                                (Relation::new(1, 3), RelationType::Neutral),
                                (Relation::new(1, 4), RelationType::Neutral),
                                (Relation::new(1, 5), RelationType::Neutral),

                                (Relation::new(2, 1), RelationType::Neutral),
                                (Relation::new(2, 2), RelationType::Friends),
                                (Relation::new(2, 3), RelationType::Neutral),
                                (Relation::new(2, 4), RelationType::Neutral),
                                (Relation::new(2, 5), RelationType::Neutral),

                                (Relation::new(3, 1), RelationType::Neutral),
                                (Relation::new(3, 2), RelationType::Neutral),
                                (Relation::new(3, 3), RelationType::Friends),
                                (Relation::new(3, 4), RelationType::Neutral),
                                (Relation::new(3, 5), RelationType::Neutral),

                                (Relation::new(4, 1), RelationType::Neutral),
                                (Relation::new(4, 2), RelationType::Neutral),
                                (Relation::new(4, 3), RelationType::Neutral),
                                (Relation::new(4, 4), RelationType::Friends),
                                (Relation::new(4, 5), RelationType::Enemies),

                                (Relation::new(5, 1), RelationType::Neutral),
                                (Relation::new(5, 2), RelationType::Neutral),
                                (Relation::new(5, 3), RelationType::Neutral),
                                (Relation::new(5, 4), RelationType::Enemies),
                                (Relation::new(5, 5), RelationType::Friends),
                            ].into_iter()),
                            team_identifiers: TeamIdentifiers::V6(vec![
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_ANIMAL".to_string(),
                                    team_id: 2,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_CREEP".to_string(),
                                    team_id: 3,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_ENEMY1".to_string(),
                                    team_id: 5,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_NEUTRAL".to_string(),
                                    team_id: 1,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_TEAM1".to_string(),
                                    team_id: 4,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                            ]),
                        },
                    }
                ],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 3)
        });
        map.insert(F8003_TABLE_ID, {
            let table = MapTable::F8003(Data8003{
                something: vec![],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(CLIFF_TABLE_ID, {
            let table = MapTable::Cliffs(Cliffs(vec![]));
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(TEXTURE_LAYERS_TABLE_ID, {
            let table = MapTable::TextureLayers(TextureLayers(vec![
                TextureLayer{
                    name: "Layer".to_string(),
                    enabled: 1,
                }
            ]));
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(1035, {
            let data = vec![1,0,0,0,0,0,0,0,0];
            (RefCell::new(MapTable::Unknown(data.clone())), data, 1, 1)
        });/*
        map.insert(MONUMENTS_TABLE_ID, {
            let table = MapTable::Monuments(Monuments(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(POWER_SLOTS_TABLE_ID, {
            let table = MapTable::PowerSlots(PowerSlots(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(STARTING_POINTS_TABLE_ID, {
            let table = MapTable::StartingPoints(StartingPoints(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(BARRIER_SETS_TABLE_ID, {
            let table = MapTable::BarrierSets(BarrierSets(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(BARRIERS_TABLE_ID, {
            let table = MapTable::Barriers(Barriers(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });*/
        map.insert(LIGHTINGS_TABLE_ID, {
            let data = vec![1, 0, 0];
            let table = MapTable::Unknown(data.clone());
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(_1014_TABLE_ID, {
            let data = vec![6, 0, 0, 0, 0];
            let table = MapTable::Unknown(data.clone());
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(EFFECT_ZONES_TABLE_ID, {
            let table = MapTable::EffectZones(EffectZones{
                b: IdEntity {
                    b: Entity {},
                    often_one: 3,
                    params: vec![],
                },
                unknown: 0,
                _height: self.height,
                _width: self.width,
                height: self.height as u32,
                width: self.width as u32,
                zones: vec![0; self.width as usize * self.height as usize],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(ATMO_ZONES_TABLE_ID, {
            let table = MapTable::AtmoZones(AtmoZones{
                b: IdEntity {
                    b: Entity {},
                    often_one: 3,
                    params: vec![],
                },
                unknown_byte: 0,
                zones: vec![],
                unk_u16_1: self.height,
                unk_u16_2: self.width,
                y: self.height as u32,
                x: self.width as u32,
                data: vec![0; self.width as usize * self.height as usize],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });/*
        map.insert(1007, {
            let table = MapTable::Unknown(vec![]);
            (RefCell::new(table), vec![], 1, 2)
        });
        map.insert(BUILDINGS_TABLE_ID, {
            let table = MapTable::Buildings(Buildings(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 3)
        });
        map.insert(SQUADS_TABLE_ID, {
            let table = MapTable::Squads(Squads(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });*/
        map.insert(PLAYER_KITS_TABLE_ID, {
            let table = MapTable::PlayerKits(PlayerKits(vec![
                PlayerKit{
                    b: Entity {},
                    name: "pk_kit1".to_string(),
                    enum_affecting_what_fields_follow: 4,
                    starting_power: 200,
                    starting_void_power: 0,
                },
                PlayerKit{
                    b: Entity {},
                    name: "pk_enemy1".to_string(),
                    enum_affecting_what_fields_follow: 4,
                    starting_power: 200,
                    starting_void_power: 0,
                },
            ]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });/*
        map.insert(OBJECTS_TABLE_ID, {
            let table = MapTable::Objects(Objects(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });*/
        map.insert(TERRAIN_TABLE_ID, {
            let data_size = {
                if self.height <= 1 || self.width <= 1 {
                    unreachable!()
                }
                let mut y = (self.height as usize).next_power_of_two();
                let mut x = (self.width as usize).next_power_of_two();
                let mut data_size = x * y;
                while y != 0 || x != 0 {
                    data_size += y.max(1) * x.max(1);
                    y >>= 1;
                    x >>= 1;
                }
                data_size
            };

            let heights_map = {
                let heights = match &self.height_map {
                    HeightMapChoice::Image { gray, .. } => {
                        let mut heights = vec![0.0; data_size];
                        for (x, y, p) in gray.enumerate_pixels() {
                            let y = (self.height as usize - 1) - y as usize;
                            heights[(y * self.width as usize) + x as usize] = p.0[0] as f32;
                        }
                        let mut offset = self.height as usize * self.width as usize;
                        for i in 0..offset {
                            heights[i + offset] = heights[i];
                        }

                        for scales in [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024].windows(2) {
                            let scale = scales[0];
                            let next_scale = scales[1];
                            let size_y = self.height as usize / scale;
                            let size_x = self.width as usize / scale;
                            let next_size_y = self.height as usize / next_scale;
                            let next_size_x = self.width as usize / next_scale;
                            if next_size_y == 0 && next_size_x == 0 {
                                break;
                            }
                            let size_y = size_y.max(1);
                            let size_x = size_x.max(1);
                            let next_size_x = next_size_x.max(1);
                            let next_offset = offset + size_y * size_x;
                            for y in (0..size_y).step_by(2) {
                                for x in (0..size_x).step_by(2) {
                                    let average = [
                                        heights[(y * size_x) + x + offset],
                                        heights[(y * size_x) + x + 1 + offset],
                                        heights[(y * size_x) + size_x + x + offset],
                                        heights[(y * size_x) + size_x + x + 1 + offset],
                                    ].into_iter().sum::<f32>() / 4.;
                                    let i = (y / 2 * next_size_x) + x / 2 + next_offset;
                                    heights[i] = average;
                                }
                            }
                            offset += size_y * size_x;
                        }
                        heights
                    }
                    _ => {
                        vec![50.0; data_size]
                    }
                };
                Heights{
                    unk: Some(1),
                    iy: self.height as u32,
                    ix: self.width as u32,
                    fy: self.height as f32 * 1.4,
                    fx: self.width as f32 * 1.4,
                    heights,
                }
            };
            let aviatic_heights_map = heights_map.clone();
            let terrain = TerrainData{
                category: 3,
                map_layer: 1,
                aspects: vec![],
                terrain: MapVersion::Type23{
                    y: self.height,
                    x: self.width,
                    blocking: Blocking{
                        y: self.height as u32 * 2,
                        x: self.width as u32 * 2,
                        blocking: vec![
                            BlockingFlags::empty();
                            self.width as usize * 2 * self.height as usize * 2
                        ],
                    },
                    heights_map,
                    aviatic_heights_map,
                    texture_masks: TextureMasks{
                        atlas_col: "C:/Users/xxxku/Documents/BattleForge/editor_map/empty/terrain/atlas_col.dds".to_string(),
                        atlas_nor: "C:/Users/xxxku/Documents/BattleForge/editor_map/empty/terrain/atlas_nor.dds".to_string(),
                        f0_1: 1.0,
                        f1_0: 0.0,
                        f2_1: 1.0,
                        f3_0_75: 0.75,
                        masks: vec![
                            TextureMaskData{
                                terrain_textures_default_col: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_col.tga".to_string(),
                                terrain_textures_default_ble: "".to_string(),
                                terrain_textures_default_nor: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_nor.tga".to_string(),
                                terrain_textures_default_spec: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_spec.tga".to_string(),
                                y: self.height as u32,
                                x: self.width as u32,
                                data: vec![255; self.width as usize * self.height as usize],
                            },
                        ],
                        unks1: vec![],
                    },
                    water: CGdWaterMap{
                        y: self.height as u32,
                        x: self.width as u32,
                        data: vec![0; self.width as usize * self.height as usize],
                        unk1s: vec![],
                    },
                    cliffs: {
                        let size = self.height as usize * ((self.width as usize + 31) >> 5);
                        CGdCliffs{
                            bits_0: CliffMask {
                                y: self.height as u32,
                                x: self.width as u32,
                                data: vec![0; size],
                            },
                            bits_1: CliffMask {
                                y: self.height as u32,
                                x: self.width as u32,
                                data: vec![0; size],
                            },
                        }
                    },
                }
            };
            let gui_data = crate::app::map::terrain::GuiData::new(&terrain);
            let table = MapTable::Terrain(Terrain{
                terrain,
                gui_data,
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        let map = Map{
            map,
            entity_tags: Default::default(),
            map_name: "TODO name".to_string(),
            global_variables: None,
            script_list: None,
            script_groups: None,
            spawn_groups: None,
            scripts: vec![],
            other_files: vec![
                OtherFile{
                    relative_path: "ui\\DESCRIPTION.xml".to_string(),
                    content: DESCRIPTION.as_bytes().to_vec(),
                }
            ],
            atlas: Atlas {
                errors: vec![],
            },
        };

        map
    }
}


const DESCRIPTION: &str = r#"<mapdescription author="TODO your nickname" public="0">
    <DESCRIPTION language="en">
        <mapname>TODO EN name</mapname>
        <DESCRIPTION>TODO EN DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="de">
        <mapname>TODO DE name</mapname>
        <DESCRIPTION>TODO DE DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="fr">
        <mapname>TODO FR name</mapname>
        <DESCRIPTION>TODO FR DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="ru">
        <mapname>TODO RU name</mapname>
        <DESCRIPTION>TODO TU DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
</mapdescription>
"#;