use crate::app::map::generator::empty::EmptyMap;
use crate::app::map::Map;

#[allow(dead_code)]
mod ea;
mod empty;

pub enum  Generator{
    Empty(EmptyMap),
    EA(ea::Generator),
}


pub fn generate_map() -> Generator {
    //Generator::EA(ea::generate_map())
    Generator::Empty(EmptyMap::default())
}

impl Generator {
    pub fn show(&mut self, ctx: &egui::Context) {
        match self {
            Generator::EA(g) => {
                g.show(ctx)
            }
            Generator::Empty(e) => {
                e.show(ctx)
            }
        }
    }

    pub fn finish(&self) -> Option<Map> {
        match self {
            Generator::EA(_) => {
                None
            }
            Generator::Empty(empty) => {
                empty.done.then(|| empty.create())
            }
        }
    }
}
