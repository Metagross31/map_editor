use super::rng::Rng;
use log::info;
use std::collections::BTreeMap;
use egui::Vec2;

enum IsPvE {
    PvP = 0,
    PvE = 1,
}

#[derive(Copy, Clone, Debug)]
enum Preset {
    Greenland,
    Frostland,
    Bushland,
    Cactusland,
    Ghostland,
    Lavaland,
    Twilightland,
    Winterland,
    Jungleland,
    Luminousland,
    Fireland,
    Natureland,
}

struct PresetData {
    orb: u8,
    pglow: u8,
    pgmid: u8,
    pghigh: u8,
}

impl PresetData {
    pub fn new(preset: Preset) -> Self {
        match preset {
            Preset::Greenland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            Preset::Frostland => Self {
                orb: 16,
                pglow: 23,
                pgmid: 24,
                pghigh: 25,
            },
            Preset::Bushland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            Preset::Cactusland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            Preset::Ghostland => Self {
                orb: 30,
                pglow: 29,
                pgmid: 31,
                pghigh: 32,
            },
            Preset::Lavaland => Self {
                orb: 30,
                pglow: 29,
                pgmid: 31,
                pghigh: 32,
            },
            Preset::Twilightland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            Preset::Winterland => Self {
                orb: 16,
                pglow: 17,
                pgmid: 18,
                pghigh: 19,
            },
            Preset::Jungleland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            Preset::Luminousland => Self {
                orb: 4,
                pglow: 9,
                pgmid: 11,
                pghigh: 12,
            },
            // no idea what values these two should have!
            Preset::Fireland => Self {
                orb: 0,
                pglow: 0,
                pgmid: 0,
                pghigh: 0,
            },
            Preset::Natureland => Self {
                orb: 0,
                pglow: 0,
                pgmid: 0,
                pghigh: 0,
            },
        }
    }
}
struct ResourceDefinitions {
    preset_data: PresetData,
    //rng: Rng, // maybe unused? maybe from base class?
    resource_presets: Vec<()>, // TODO from xml
}

fn preset_from_seed(_seed: u16) -> Preset {
    // TODO actual logic
    Preset::Fireland
}

pub fn creating_resource_distribution(map_size: u32, seed: u16, difficulty: u8, player_count: u8, param_23: u32, big: &mut BigStruct1098430) {
    info!("creating resource distribution");
    let is_pvp = IsPvE::PvE;
    match is_pvp {
        IsPvE::PvE => {
            // assert "PvE maps must not have mirrored voronoi partitions!"
            // optionally save MapStructure.bmp
            // prepares CMGRessourceLayout (no idea how exactly)
            let mut rng = Rng::new(seed as u32, 3934873077);
            let mut layout = ResourceLayoutSkirmish::new(seed, difficulty, player_count);
            layout.init_1();
            layout.init_2(map_size, player_count);
            let _resource_definitions = ResourceDefinitions {
                preset_data: PresetData::new(preset_from_seed(seed)),
                resource_presets: vec![],
            };
            let (mut mbr_0xb0, enemy_level) = rebalance_territories(&layout, player_count);
            set_up_territories(&mut mbr_0xb0, &mut layout, &mut rng, enemy_level, player_count, map_size, param_23, big);
            back_in_sub_1081de0_part_1(&layout.b);
            //todo!("{:?}", resource_definitions.resource_presets)
        }
        IsPvE::PvP => {
            // assert "PvP has to have a mirrored Voronoi Partition!"
            // optionally save MapStructure.bmp
            // no idea, what is it doing :(
        }
    }
}

// TODO this->mbr_0x4 = struct_1098430::ctor::1098430(this->CMGConfiguration_ptr_0)

fn back_in_sub_1081de0_part_1(layout: &RessourceLayout){
    if layout.territories.is_empty() {
        panic!("EA just skips this, I expect it to be mandatory!");
    }
    for _teritory in layout.territories.iter() {
        // TODO mbr_0x4 = this->mbr_0x4
    }
    // TODO meth_0x10804c0
    // TODO more teritory stuff
}

fn set_up_territories(mbr_0xb0: &mut BTreeMap<u16, u16>, layout: &mut ResourceLayoutSkirmish, rng: &mut Rng,
                      enemy_level: u16, player_count: u8, map_size: u32, param_23: u32, big: &mut BigStruct1098430) {
    let mut next_id = 0;
    let mut enemy_camps = BTreeMap::new();
    for el in layout.min_enemy_level..=layout.max_enemy_level {
        let mut definitions = Vec::new();
        for _i in 0..*mbr_0xb0.entry(el).or_default() {
            definitions.push(EnemyCampDefinition {
                id: next_id,
                enemy_level: el,
                position: Vec2::default(),
                connections_to: Vec::new(),
                connections_from: Vec::new(),
                int_c: 0,
            });
            next_id += 1;
        }
        enemy_camps.insert(el, EnemyCampDefinitions{
            enemy_level: el,
            definitions,
        });
    }
    // EA did super complicated thing, with at least 5 copies of the object, without anitializing it to anything else than default
    // for any other field than `enemy_level` using 3 vectors and 2 trees. So I simplified it to above, and lets see
    /*if layout.min_enemy_level <= layout.max_enemy_level {
        let enemy_level_i = layout.min_enemy_level;
        loop {
            let enemy_level_i_value = *mbr_0xb0.entry(enemy_level_i).or_default();
            if enemy_level_i_value != 0 {
                let mut v4 = 0;
                loop {
                    enemy_camps.insert(EnemyCampDefinition{
                        enemy_level: enemy_level_i
                    });
                    v4 += 1;
                    if v4 >= enemy_level_i_value {
                        break;
                    }
                }
            }
        }
    }*/
    connect_on_same_level(mbr_0xb0, layout, enemy_level, player_count, &mut enemy_camps);
    connect_level_and_rotate(rng, mbr_0xb0, layout, enemy_level, player_count, &mut enemy_camps);
    generate_teritories(mbr_0xb0, layout, rng, enemy_level, player_count, map_size, param_23, &mut enemy_camps, big);

    for (_, enemy_camp) in enemy_camps.iter_mut() {
        for _definition in enemy_camp.definitions.iter_mut() {
            // TODO mbr_0x4 = this->mbr_0x4;
            // one more inner loop mbr_0x->dword7C
        }
    }
    // TODO meth_0x1081830
}

fn generate_teritories(
    mbr_0xb0: &mut BTreeMap<u16, u16>, layout: &mut ResourceLayoutSkirmish, rng: &mut Rng,
    enemy_level: u16, player_count: u8, map_size: u32, param_23: u32,
    enemy_camp_definitions: &mut BTreeMap<u16, EnemyCampDefinitions>,
    big: &mut BigStruct1098430)
{
    let param_23 = param_23 as f32;
    let v7 = (layout.max_enemy_level - layout.min_enemy_level) as f64;
    let v141 = 1. / (v7 + 2.);
    let v146 = map_size as f32 - (param_23 + param_23);
    let mut v145 = 1.5 * v141;
    let a2 = v141 * v146 as f64;
    let mut a3 = a2 * 0.1500000059604645;
    let a3_10 = rng.next(0, 1) == 1;
    let a3_9 = rng.next(0, 1) == 1;
    let a3_11 = rng.next(0, 1) == 1;
    let mut a3_20 = 0;
    for eli in layout.min_enemy_level..=layout.max_enemy_level {
        let v144 = enemy_camp_definitions.get_mut(&eli).unwrap();
        let v17 = v144.definitions.len() as u32;
        let tmp = v17 as f32 - 4.0;
        let tmp = tmp.abs();
        let tmp = (tmp + v17 as f32 + 4.) * 0.5;
        let tmp = 1. / tmp;
        let mut v143 = 0.5 / v17 as f32;
        let v23 = *mbr_0xb0.entry(eli).or_default();
        let (mut v128, a3_12) = if eli > enemy_level {
            if eli < layout.max_enemy_level {
                // TODO WTF? if player_count < 4 * player_count {} :O if true?
                let v25 = 4 * player_count - player_count;
                if 2 * player_count >= v25 {
                    (v25 as u16, v23)
                } else {
                    (2 * player_count as u16, v23)
                }
            } else {
                (0, v23)
            }
        } else {
            (1, v23 / player_count as u16)
        };

        let mut a3_24 = 0;
        let mut j = tmp * v146 * 0.1500000059604645;
        for v30 in v144.definitions.iter_mut() {
            let v55 = !a3_10;
            v30.int_c = a3_20;
            let x = (v143 * v146) + param_23;
            v30.position.x = x;

            let y = (param_23 as f64 + v146 as f64 * v145) as f32;
            v30.position.y = y;
            if !v55 { // TODO remove double negation
                v30.position.x = map_size as f32 - x;
            }
            if a3_9 {
                v30.position.y = map_size as f32 - x;
            }
            if a3_11 {
                std::mem::swap(&mut v30.position.x, &mut v30.position.y);
            }
            layout.b.territories[a3_20 as usize] = eli + 1;
            let mut v130 = 0.;
            let mut x : f32 = 0.;
            let mut y = 0.;
            for i in v30.connections_from.iter().cloned() {
                if layout.b.territories[i] == eli {
                    x += 1.;
                    let _dword14_ptr = 0.;
                    //v130 += (dword14_ptr + 8.) + v130;
                    let v = big.tree_78.get(&(i as u32)).unwrap();
                    v130 += v.x;//[2.8843255615234375e2, 2.2705438232421875e2][eli as usize-1];
                    //y += (dword14_ptr + 12.) + y;
                    y += v.y;//[3.44509490966796875e2, 2.087515411376953125e2][eli as usize-1];
                }
            }
            let v49 = if x.abs() <= f32::EPSILON {
                v30.position.x = rng.next_f32() * j + v30.position.x;
                rng.next_f32() as f64 * a3
            } else {
                let v44 = x;
                let x = 1. / x;
                let v130 = x * v130;
                let y = x * y;
                let v149 = v130 - v30.position.x;
                let v150 = y - v30.position.y;
                let v127 = v44 * 0.5;
                let v48 = v127 + 1.;
                let v127 = v127 - 1.;
                let v127 = 0.5 * (v48 - v127);
                let v153 = (v127 as f64 * (a2 * 0.550000011920929)) as f32;
                let v127 = v150 * v150 + v149 * v149;
                let v127 = v127.sqrt();
                let v127 = v153 / v127;
                let v157 = v127 * v149;
                v30.position.x += v157;
                v127 as f64 * v150 as f64
            };
            v30.position.y = (v49 + v30.position.y as f64) as f32;

            let mut v53 = 1;
            if eli <= enemy_level {
                let v54 = v128;
                let mut v55 = v128 == 0;
                if v128 != 0 {
                    // player_count += 1; TODO WTF? it explains the check 'if player_count < 4 * player_count', because one was local copy
                    v53 = 7;
                    v128 = v54 - 1;
                    v55 = v54 == 1;
                }
                if v55 && ((a3_24 + 1) % a3_12) == 0 {
                    v128 = 1;
                }
            }
            let _v120 = v53;
            // TODO sub_107C440
            {
                big.tree_78.insert(a3_20, v30.position);
            }
            // TODO sub_10738B0
            a3_24 += 1;
            a3_20 += 1;
            v143 += tmp;
            j *= -1.;
            a3 *= -1.;
        }

        if eli > enemy_level {
            let mut v61 = 0;
            let mut v62 = 0;
            let mut j = 0;
            let mut flip = false;
            loop {
                if v61 >= v144.definitions.len() || v128 == 0 {
                    break
                }
                let mut v65 = v62;
                if flip {
                    v65 = v144.definitions.len() - v62 - 1;
                    flip = false;
                    j = v62 + 1;
                } else {
                    flip = true;
                }

                let tmp = &v144.definitions[v65];
                let _int_c = tmp.int_c;

                let _v71 = &big.tree_78;

                // TODO this->mbr_0x4
                {
                    v128 -= 1;
                    // modify player_count
                    // TODO meth_0x10b38f0
                    // TODO meth_0x10b61a0
                    v61 += 1;
                    v62 = j; // incremented only every second time
                }
            }
        }
        v145 += v141;
    }
    let v145 = (v141 * 0.5 * v146 as f64 + param_23 as f64) as f32;
    // TODO meth_0x1001d20
    let v80 = *mbr_0xb0.get(&layout.min_enemy_level).unwrap() / player_count as u16;
    let v143 = 1. / player_count as f32;
    let j = v143 * 0.5;
    for v134 in 0..player_count as u16 {
        let v82 = v145;
        let v126 = v134 as u32;
        let v131 = v80 * v134;
        let a3_24 = v134 & 1;
        let v83 = if a3_24 != 0 {
            v80 * (v134 + 1) - 1
        } else {
            v80 * v134
        };
        let v55 = !a3_10;
        let v84 = j * v146 + param_23;
        layout.b.territories[a3_20 as usize] = 0;
        let x = if !v55 {
            map_size as f32 - v84
        } else {
            v84
        };
        let y = if a3_9 {
            map_size as f32 - v82
        } else {
            v82
        };
        let (x, y) = if a3_11 {
            (y, x)
        } else {
            (x, y)
        };
        // TODO v87 = this->mbr_0x4;
        let _ = v83;
        //let v152 = x - *(float *)(v91 + 8);
        //let v153 = y - *(float *)(v91 + 12);
        let v152 = x as f64 - 279.63812;
        let v153 = y as f64 - 269.84174;
        let v128 = v152;
        let lc = v153;
        let v168 = rng.next_f32() * 30. + 50.;
        let v128 = lc * lc + v128 * v128;
        let v128 = v128.sqrt();
        // few useless lines
        let v128 = v168 as f64 / v128;
        let v160 = v152 * v128;
        let v161 = v128 * v153;
        //let a2 = v160 + *(float *)(v91 + 8);
        let a2 = v160 + 279.63812;
        //let a2_1 = v161 - *(float *)(v91 + 12);
        let a2_1 = v161 + 269.84174;
        let _x = a2 as f32;
        let _y = a2_1 as f32;
        // TODO sub_107CF70
        let v100 = 0x20A04C58;
        // TODO v101 = (char *)this->mbr_0x4;
        let _v134 = v100;
        // TODO sub_1097EC0
        if layout.linearity < 1 {
            let _ = v126;
            let _ = v131;
            todo!()
        }
        // TODO eastl_107C1F0
        // TODO v116 = this->mbr_0x4
    }
}

#[derive(Debug)]
struct EnemyCampDefinitions {
    enemy_level: u16,
    definitions: Vec<EnemyCampDefinition>,
}
#[derive(Debug)]
struct EnemyCampDefinition {
    id: usize,
    position: Vec2,
    int_c: u32,
    enemy_level: u16,
    connections_to: Vec<usize>,
    connections_from: Vec<usize>,
}

fn connect_level_and_rotate(
    rng: &mut Rng,
    _mbr_0xb0: &mut BTreeMap<u16, u16>,
    _layout: &ResourceLayoutSkirmish,
    _enemy_level: u16,
    _player_count: u8,
    enemy_camp_definitions: &mut BTreeMap<u16, EnemyCampDefinitions>)
{
    enemy_camp_definitions.get_mut(&1).unwrap().definitions[0].connections_to.push(3);
    enemy_camp_definitions.get_mut(&2).unwrap().definitions[0].connections_to.push(0);
    enemy_camp_definitions.get_mut(&2).unwrap().definitions[2].connections_to.push(6);
    enemy_camp_definitions.get_mut(&3).unwrap().definitions[2].connections_to.push(5);
    assert_eq!(2, rng.next(1, 2));
    assert_eq!(2, rng.next(1, 2));
    assert_eq!(2, rng.next(2, 2));
    assert_eq!(2, rng.next(2, 3-1));
    assert_eq!(1, rng.next(0, 1));
    assert_eq!(0, rng.next(0, 1));
    assert_eq!(0, rng.next(0, 2));
    assert_eq!(0, rng.next(0, 1));
}

fn connect_on_same_level(
    _mbr_0xb0: &mut BTreeMap<u16, u16>,
    _layout: &ResourceLayoutSkirmish,
    _enemy_level: u16,
    _player_count: u8,
    enemy_camp_definitions: &mut BTreeMap<u16, EnemyCampDefinitions>)
{
    // get_many_mut #[unstable(feature = "get_many_mut", issue = "104642")] is not yet stable
    fn get_2_mut_helper<T>(vec: &mut Vec<T>, i: usize) -> (&mut T, &mut T) {
        unsafe {
            let first = vec.get_unchecked_mut(i) as *mut _ as usize;
            let second = vec.get_unchecked_mut(i+1) as *mut _ as usize;
            (&mut *(first as *mut _), &mut *(second as *mut _))
        }
    }
    // TODO for player count != 1 it is more complicated
    /*for eli in layout.min_enemy_level..=enemy_level {
        let defs = enemy_camp_definitions.get_mut(&eli).unwrap();
        //let v7 = *mbr_0xb0.entry(eli).or_default() / player_count as u16;
        //for v34 in 0..v7 {
            //for _ in (0..player_count).rev() { }
        //}
    }
    if enemy_level + 1 <= layout.max_enemy_level {
        // TODO second part of the function
    } */
    for (_level, defs) in enemy_camp_definitions.iter_mut() {
        for i in 0..defs.definitions.len() - 1 {
            let (l, r) = get_2_mut_helper(&mut defs.definitions, i);
            l.connections_to.push(r.id);
            r.connections_from.push(l.id);
        }
    }
}

fn rebalance_territories(layout: &ResourceLayoutSkirmish, player_count: u8) -> (BTreeMap<u16, u16>, u16) {
    let mut mbr_0xb0 = BTreeMap::new();
    for i in 0..=4 {
        mbr_0xb0.insert(i, 0);
    }
    let min_enemy_level = layout.min_enemy_level;
    let max_enemy_level = layout.max_enemy_level;
    let v8 = layout.territory_ammount - player_count as u16;
    if v8 > 0 {
        let v41 = max_enemy_level - min_enemy_level + 1;
        let mut v10 = v8;
        while v10 > 0 {
            let v39 = min_enemy_level + (v10 % v41);
            let v11 = mbr_0xb0.entry(v39).or_default();
            *v11 += 1;
            v10 -= 1;
        }
    }

    let mut enemy_level = if player_count <= 1 {
        min_enemy_level
    } else {
        match layout.overlap {
            0 => min_enemy_level,
            1 => 1,
            _ => 2,
        }
        .min(max_enemy_level)
    };

    if enemy_level > min_enemy_level && player_count == 4 {
        enemy_level -= 1;
    }

    let mut v19 = 0;

    for v36 in (enemy_level + 1)..=max_enemy_level {
        let v20 = {
            let tmp = *mbr_0xb0.entry(v36).or_default();
            if tmp == 0 {
                0
            } else {
                tmp - 1
            }
        };
        v19 += v20;
    }
    let mut v35 = v19;

    if enemy_level < min_enemy_level {
        return (mbr_0xb0, enemy_level);
    }
    let mut v36 = max_enemy_level;
    let v24 = player_count as u16;
    for v34 in min_enemy_level..=enemy_level {
        if max_enemy_level <= enemy_level {
            let mut v39 = max_enemy_level;
            loop {
                if *mbr_0xb0.entry(v39).or_default() > *mbr_0xb0.entry(v36).or_default() {
                    v36 = max_enemy_level;
                    break;
                }
                v39 -= 1;
                if v39 > enemy_level {
                    break;
                }
            }
        }

        let mut skip_check = false;

        if v35 > 0 {
            while *mbr_0xb0.entry(v34).or_default() % v24 != 0 {
                let v27 = mbr_0xb0.entry(v34).or_default();
                *v27 += 1;
                let v28 = mbr_0xb0.get_mut(&(v36)).unwrap();
                if *v28 <= 1 {
                    panic!("Unable to rebalance territory-count!? too many players, too few territories")
                }
                *v28 -= 1;
                v35 -= 1;
                v36 -= 1;
                if v36 <= enemy_level {
                    v36 = max_enemy_level;
                } else {
                    skip_check = true;
                    break;
                }
            }
        } else {
            skip_check = true;
        };

        if v35 > 0 || skip_check {
            let v29 = *mbr_0xb0.entry(v34).or_default();
            if v29 % v24 > 0 {
                let v30 = player_count as u16 * (v29 / v24);
                let v30 = v30.min(player_count as u16);
                if v30 == 0 {
                    panic!(
                        "Invalid new territory count for Tier {v34} - please report to MG Coder!"
                    );
                }
                let v36 = v29 - v30 + v35;
                let v32 = mbr_0xb0.entry(v36).or_default();
                *v32 += v29 - v30;
                let v33 = mbr_0xb0.entry(v34).or_default();
                *v33 = v30;
            }
        }
    }

    (mbr_0xb0, enemy_level)
}

struct RessourceLayout {
    mbr_0x4: u16,
    mbr_0x6: u16,
    // not sure if those 2 vectors are actually filled in init
    territories: Vec<u16>,
    mbr_0x34: Vec<(u32, u32, u32, u32)>,
}

struct ResourceLayoutSkirmish {
    b: RessourceLayout,
    rng: Rng,
    min_enemy_level: u16,
    max_enemy_level: u16,
    spawn_rate: u8,
    linearity: u8,
    overlap: u8,
    territory_ammount: u16,
    resources: u32,
}

impl ResourceLayoutSkirmish {
    pub fn new(seed: u16, difficulty: u8, player_count: u8) -> Self {
        let mut rng = Rng::new(seed as u32, 3934873077);
        let (min, max) = territory_ammount(difficulty, player_count);
        let territory_ammount = rng.next(0, (max - min) as u32) as u16 + min;
        let (min_enemy_level, max_enemy_level) = enemy_level(difficulty, player_count);
        let linearity = match linearity(difficulty) {
            4.. => 2,
            3 => 1,
            0..=2 => 0,
        };
        let overlap = if player_count == 1 {
            2
        } else {
            match overlap(difficulty) {
                4.. => 2,
                3 => 1,
                0..=2 => 0,
            }
        };
        let spawn_rate = spawn_rate(difficulty, player_count);
        let resources = resources(difficulty);
        Self {
            b: RessourceLayout {
                mbr_0x4: 0,
                mbr_0x6: 0,
                territories: vec![0;territory_ammount as usize],
                mbr_0x34: vec![],
            },
            rng,
            min_enemy_level,
            max_enemy_level,
            spawn_rate,
            linearity,
            overlap,
            territory_ammount,
            resources,
        }
    }
    pub fn init_1(&mut self) {
        self.b.mbr_0x4 = 2;
        self.b.mbr_0x6 = 2;

        while self.territory_ammount > self.b.mbr_0x4 * self.b.mbr_0x6 {
            let diff = self.b.mbr_0x4 - self.b.mbr_0x6;

            if diff == 0 || diff == 1 {
                self.b.mbr_0x4 += 1;
            } else if diff == 2 {
                self.b.mbr_0x4 -= 1;
                self.b.mbr_0x6 += 1;
            }
        }
        self.b.mbr_0x34 = vec![(0,0,0,0); (self.b.mbr_0x4*self.b.mbr_0x6) as usize];
    }
    pub fn init_2(&mut self, map_size: u32, player_count: u8) {
        if map_size != 512 {
            return;
        }
        let (mut v25, path_max, path_min) = match self.territory_ammount {
            19.. => (48, 20., 14.),
            16.. => (64, 19., 14.),
            11.. => (96, 18., 13.),
            0..=10 => (128, 17., 12.),
        };
        let mut border = v25;
        if player_count > 1 {
            v25 += 65520;
            border = v25;
        }
        if player_count > 2 {
            if v25 as u16 >= 16 {
                border = v25 - 16;
            } else {
                border = 0;
            }
        }
        // TODO adjust trough configuration (did nothing for my test 1p diff 1)
        info!("Adjusted setting: PathMin: {path_min}, PathMax: {path_max}, Border: {border}");
    }
}

fn territory_ammount(_difficulty: u8, _player_count: u8) -> (u16, u16) {
    // TODO real value from db
    (5, 9)
}

fn enemy_level(_difficulty: u8, _player_count: u8) -> (u16, u16) {
    // TODO real value from db
    (1, 3)
}

fn linearity(_difficulty: u8) -> u16 {
    // TODO real value from db
    4
}

fn overlap(_difficulty: u8) -> u16 {
    // TODO real value from db
    5
}

fn spawn_rate(_difficulty: u8, _player_count: u8) -> u8 {
    // TODO real value from db
    1
}

fn resources(_difficulty: u8) -> u32 {
    // TODO real value from db
    400
}

#[derive(Default)]
pub struct BigStruct1098430 {
    tree_78: BTreeMap<u32, Vec2>,
}
