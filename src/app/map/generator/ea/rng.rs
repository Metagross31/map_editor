
pub struct Rng(u32, u32);

impl Rng {
    pub fn new(main_seed: u32, sub_seed: u32) -> Self {
        Self(main_seed, sub_seed)
    }

    pub fn next(&mut self, min: u32, max: u32) -> u32 {
        self.0 = self.0.wrapping_mul(self.1);
        min + ((((max - min) as u64 + 1) * self.0 as u64) >> 32) as u32
    }

    pub fn next_f32(&mut self) -> f32 {
        self.0 = self.0.wrapping_mul(self.1);
        self.0 as f32 * 2.3283064e-10
    }
}
