
use egui::Ui;
use sr_libs::utils::commands::{encode_str, read_string, read_u16, read_u32};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Cliffs(pub Vec<Cliff>);
#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Cliff {
    pub name: String,
    pub xml: String,
}

impl Cliffs {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (two, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 2 failed, because: {e:?}")))?;
        if two != 2 {
            return Err(MapError::MapFileParsing (format!("Expected 2 found {two}.")));
        }
        let (zero, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 0 failed, because: {e:?}")))?;
        if zero != 0 {
            return Err(MapError::MapFileParsing (format!("Expected 0 found {zero}.")));
        }
        let (count, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing count failed, because: {e:?}")))?;
        let mut cliffs = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _i in 0..count {
            let (name, buf) = read_string(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing name {_i} failed, because: {e:?}")))?;
            let (xml, buf) = read_string(buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing xml {_i} failed, because: {e:?}")))?;
            cliffs.push(Cliff{ name: name.to_string(), xml: xml.to_string() });
            loop_buf = buf;
        }
        if !loop_buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones?): {buf:?}")))
        }

        Ok(Self(cliffs))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u16_le(2);
        buf.put_u16_le(0);
        buf.put_u32_le(self.0.len() as u32);
        for cliff in self.0.iter() {
            encode_str(&mut buf, &cliff.name);
            encode_str(&mut buf, &cliff.xml);
        }
        buf.to_vec()
    }
}

impl Fix for Cliffs {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for Cliffs {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Cliffs {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        for cliff in self.0.iter_mut() {
            cliff.show_mut(ui);
        }
    }
}

impl Cliff {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
