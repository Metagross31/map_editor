use std::fmt::{Debug, Formatter};
use std::ops::IndexMut;
use egui::{Color32, ColorImage, DragValue, Id, Ui, Widget};
use egui_dock::DockState;
use sr_libs::utils::commands::{encode_str, ParsingError, read_f32, read_string, read_u16, read_u32, read_u64, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};
use crate::retained_image::RetainedImage;

pub struct Terrain{
    pub terrain: TerrainData,
    pub gui_data: GuiData,
}

#[derive(serde::Serialize)]
pub struct TerrainData{
    pub category : u8,
    pub map_layer: u8,
    pub aspects: Vec<()>,
    pub terrain: MapVersion,
}
#[derive(serde::Serialize)]
pub enum MapVersion {
    Type23 {
        y: u16,
        x: u16,
        blocking: Blocking,
        heights_map: Heights,
        aviatic_heights_map: Heights,
        texture_masks: TextureMasks,
        water: CGdWaterMap,
        cliffs: CGdCliffs,
    },
    /*Type22 {

    },
    Type21 {

    },
    Type20 {

    },*/
}


bitflags::bitflags! {
    #[derive(serde::Serialize, Copy, Clone)]
    pub struct BlockingFlags: u16 {
        const BlockBit0 = 1;
        const BlockBit1 = 1 << 1;
        const BlockBit2 = 1 << 2;
        const BlockBit3 = 1 << 3;
        const BlockBit = Self::BlockBit0.bits() | Self::BlockBit1.bits() | Self::BlockBit2.bits() | Self::BlockBit3.bits();
        const Flying = 1 << 4;
        const GroundAndFlying = Self::BlockBit.bits() | Self::Flying.bits();
        const MapBorderBit = 1 << 5;
        const MapBorder = Self::GroundAndFlying.bits() | Self::MapBorderBit.bits();
        const NoBuild = 1 << 6;
        const Cliff = 1 << 7;
        const Unknown8 = 1 << 8;
        const Unknown9 = 1 << 9;
        const VisBlocking = 1 << 10;
        const VisAndBlockBit = Self::BlockBit.bits() | Self::VisBlocking.bits();
        const VisAndGroundAndFlying = Self::GroundAndFlying.bits() | Self::VisBlocking.bits();
        const IgnoreBlocking = 1 << 11;
    }
}

#[derive(serde::Serialize)]
pub struct Blocking{
    pub y: u32,
    pub x: u32,
    pub blocking: Vec<BlockingFlags>,
}

#[derive(Clone, serde::Serialize)]
pub struct Heights{
    pub unk: Option<u64>,
    pub iy: u32,
    pub ix: u32,
    pub fy: f32,
    pub fx: f32,
    pub heights: Vec<f32>,
}

#[allow(non_camel_case_types)]
#[derive(serde::Serialize)]
pub struct TextureMaskData {
    pub terrain_textures_default_col: String,
    pub terrain_textures_default_ble: String,
    pub terrain_textures_default_nor: String,
    pub terrain_textures_default_spec: String,
    pub y: u32,
    pub x: u32,
    pub data: Vec<u8>,
}

#[allow(non_camel_case_types)]
#[derive(serde::Serialize)]
pub struct TextureMaskSomething {
    pub s1: String,
    pub unk0: u32,
    pub unk1: u32,
    pub unk2: u32,
    pub unk3: u32,
}

#[derive(serde::Serialize)]
pub struct TextureMasks {
    pub atlas_col: String,
    pub atlas_nor: String,
    pub f0_1: f32,
    pub f1_0: f32,
    pub f2_1: f32,
    pub f3_0_75: f32,
    pub masks: Vec<TextureMaskData>,
    pub unks1: Vec<TextureMaskSomething>,
}

#[allow(non_camel_case_types)]
#[serde_with::serde_as]
#[derive(serde::Serialize)]
pub struct CGdWaterMap_Unk1 {
    pub unk: u8,
    pub f0: f32,
    pub s1: String,
    pub s2: String,
    pub s3: String,
    pub s4: String,
    pub s5: String,
    pub f1: [f32;6],
    pub s6: String,
    pub f2: [f32;6],
    pub s7: String,
    #[serde_as(as = "[_; 49]")]
    pub f3: [f32;49],
    pub i0: u32,
    pub i1: u32,
    pub f4: [f32;2],
    pub s8: String,
    pub f5: [f32;5],
    pub s9: String,
    pub data: Vec<u8>,
}
#[derive(serde::Serialize)]
pub struct CGdWaterMap {
    pub y: u32,
    pub x: u32,
    pub data: Vec<u8>,
    pub unk1s: Vec<CGdWaterMap_Unk1>,
}
#[derive(serde::Serialize)]
pub struct CliffMask {
    pub y: u32,
    pub x: u32,
    pub data: Vec<u32>,
}

#[derive(serde::Serialize)]
pub struct CGdCliffs {
    pub bits_0: CliffMask,
    pub bits_1: CliffMask,
}


fn read_32_expect_0<'a>(buf: &'a[u8], field_name: &'static str) -> anyhow::Result<&'a[u8], MapError> {
    let (zero, buf) = read_u32(buf)
        .map_err(|e| MapError::MapFileParsing(format!("Parsing {field_name} failed, because: {e:?}")))?;
    if zero != 0 {
        return Err(MapError::MapFileParsing (format!("Expected 0 found {zero}!")));
    }
    Ok(buf)
}
impl Terrain {
    pub fn from_bytes(buf: &[u8]) -> anyhow::Result<Self, MapError> {
        let (category, map_layer,aspects, map_version, buf) = Self::header(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing header failed, because: {e:?}")))?;
        let (terrain, buf) = match map_version {
            23 => {
                let (map_sub_version, buf) = read_u8(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing map_sub_version failed, because: {e:?}")))?;
                match map_sub_version {
                    2 | 3 | 1 => {
                        todo!("map sub version {map_sub_version}")
                    }
                    0 => {
                        // continue below
                    }
                    _ => unreachable!(),
                }
                let (y, buf) = read_u16(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing x failed, because: {e:?}")))?;
                let (x, buf) = read_u16(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing y failed, because: {e:?}")))?;
                let buf = read_32_expect_0(buf, "str0")?;
                let buf = read_32_expect_0(buf, "str1")?;
                let buf = read_32_expect_0(buf, "num")?;
                let buf = read_32_expect_0(buf, "str2")?;
                let buf = read_32_expect_0(buf, "str3")?;
                let buf = read_32_expect_0(buf, "str4")?;
                let buf = read_32_expect_0(buf, "str5")?;
                let (blocking, buf) = Blocking::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing blocking failed, because: {e:?}")))?;
                let (heights_map, buf) = Heights::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing height data 1 failed, because: {e:?}")))?;
                let (aviatic_heights_map, buf) = Heights::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing height data 2 failed, because: {e:?}")))?;
                let (texture_masks, buf) = TextureMasks::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing TextureMasks failed, because: {e:?}")))?;
                let (unk_0x1529860, buf) = CGdWaterMap::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing Unk0x1529860 failed, because: {e:?}")))?;
                let (unk_0x1569330, buf) = CGdCliffs::from_bytes(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing Unk0x1569330 failed, because: {e:?}")))?;
                (MapVersion::Type23 {
                    x,
                    y,
                    blocking,
                    heights_map,
                    aviatic_heights_map,
                    texture_masks,
                    water: unk_0x1529860,
                    cliffs: unk_0x1569330,
                }, buf)
            }
            22 => {
                let (map_sub_version, _buf) = read_u8(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing map_sub_version failed, because: {e:?}")))?;
                if map_sub_version == 2 {
                    todo!("map sub version 2")
                }
                todo!("Map version 22")
            }
            21 => {
                let (map_sub_version, _buf) = read_u8(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing map_sub_version failed, because: {e:?}")))?;
                if map_sub_version == 2 {
                    todo!("map sub version 2")
                }
                todo!("Map version 21")
            }
            20 => {
                /*let (map_sub_version, buf) = read_u8(buf)
                    .map_err(|e| MapError::MapFileParsing(format!("Parsing map_sub_version failed, because: {e:?}")))?;
                match map_sub_version {
                    2 | 3 => {
                        todo!("map sub version {map_sub_version}")
                    }
                    _ => unreachable!(),
                }*/
                todo!("Map version 20")
            }
            _ => return Err(MapError::MapFileParsing(format!("Parsing failed, because: map_version {map_version} is unknown"))),
        };
        if !buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left: {buf:?}")))
        }
        let terrain = TerrainData{category, map_layer, aspects, terrain };
        let gui_data = GuiData::new(&terrain);
        Ok(Self{terrain, gui_data})
    }
    fn header(buf: &[u8]) -> anyhow::Result<(u8, u8, Vec<()>, u8, &[u8]), ParsingError> {
        let (eight, buf) = read_u32(buf)?;
        if eight != 8 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 8 found {eight}!")});
        }
        let (zero, buf) = read_u32(buf)?; // empty string
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero}!")});
        }
        let (category, buf) = read_u8(buf)?;
        let (map_layer, buf) = read_u8(buf)?;
        let (aspect_count, buf) = read_u16(buf)?;
        if aspect_count != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 aspects found {aspect_count}!")});
        }
        let (map_version, buf) = read_u8(buf)?;
        Ok((category, map_layer, vec![], map_version, buf))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u32_le(8);
        encode_str(&mut buf, "");
        buf.put_u8(self.terrain.category);
        buf.put_u8(self.terrain.map_layer);
        buf.put_u16_le(self.terrain.aspects.len() as u16);
        match &self.terrain.terrain {
            MapVersion::Type23 { x, y, blocking, heights_map, aviatic_heights_map, texture_masks, water: unk_0x1529860, cliffs: unk_0x1569330 } => {
                buf.put_u8(23);
                buf.put_u8(0); // map_sub_version
                buf.put_u16_le(*y);
                buf.put_u16_le(*x);
                encode_str(&mut buf, "");
                encode_str(&mut buf, "");
                buf.put_u32_le(0);
                encode_str(&mut buf, "");
                encode_str(&mut buf, "");
                encode_str(&mut buf, "");
                encode_str(&mut buf, "");
                blocking.write(&mut buf);
                heights_map.write(&mut buf);
                aviatic_heights_map.write(&mut buf);
                texture_masks.write(&mut buf);
                unk_0x1529860.write(&mut buf);
                unk_0x1569330.write(&mut buf);
            }
            /*MapVersion::Type22 { .. } => {
                todo!()
            }
            MapVersion::Type21 { .. } => {
                todo!()
            }*/
        }
        buf.to_vec()
    }
}

impl Blocking {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (y, buf) = read_u32(buf)?;
        let (x, buf) = read_u32(buf)?;
        let mut blocking = Vec::with_capacity((x*y) as usize);
        let mut loop_buf = buf;
        for _ in 0..x*y {
            let (b, buf) = read_u16(loop_buf)?;
            loop_buf = buf;
            let b = BlockingFlags::from_bits(b)
                .ok_or_else(||ParsingError::UnexpectedData {internal_error:
                    format!("b{b:16b} is not an valid value of BlockingFlags")})?;
            blocking.push(b);
        }
        Ok((Self{x, y, blocking}, loop_buf))
    }

    fn write(&self, buf: &mut BytesMut) {
        buf.put_u32_le(self.y);
        buf.put_u32_le(self.x);
        for b in self.blocking.iter() {
            buf.put_u16_le(b.bits());
        }
    }
}

impl Heights {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (big_const, buf) = read_u32(buf)?;
        if big_const != 0x46451D {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0x46451D found {big_const}!")});
        }
        let (one_or_two, buf) = read_u32(buf)?;
        let (unk, buf) = match one_or_two {
            1 => (None, buf),
            2 => {
                let (unk, buf) = read_u64(buf)?;
                (Some(unk), buf)
            },
            _ => {
                return Err(ParsingError::UnexpectedData {internal_error:format!("Expected version 2 found {one_or_two}!")});
            }
        };
        let (iy, buf) = read_u32(buf)?;
        let (ix, buf) = read_u32(buf)?;
        let (fy, buf) = read_f32(buf)?;
        let (fx, buf) = read_f32(buf)?;
        //assert!((fy - (iy as f32 * 1.4)).abs() < 1., "{fy}  {iy} * 1.4 < 1");
        //assert!((fx - (ix as f32 * 1.4)).abs() < 1., "{fx}  {ix} * 1.4 < 1");
        let (count, buf) = read_u32(buf)?;
        let mut heights = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (h, buf) = read_f32(loop_buf)?;
            loop_buf = buf;
            heights.push(h);
        }
        Ok((Self{unk, ix, iy, fx, fy, heights}, loop_buf))
    }

    fn write(&self, buf: &mut BytesMut) {
        buf.put_u32_le(0x46451D);
        match self.unk {
            None => {
                buf.put_u32_le(1);
            }
            Some(unk) => {
                buf.put_u32_le(2);
                buf.put_u64_le(unk);
            }
        }
        buf.put_u32_le(self.iy);
        buf.put_u32_le(self.ix);
        buf.put_f32_le(self.fy);
        buf.put_f32_le(self.fx);
        buf.put_u32_le(self.heights.len() as u32);
        for height in self.heights.iter().copied() {
            buf.put_f32_le(height);
        }
    }
}

impl TextureMasks {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (eight, buf) = read_u8(buf)?;
        // not supporting other versions for now
        if eight != 8 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 8 found {eight}!")});
        }
        let (atlas_col, buf) = read_string(buf)?;
        let (atlas_nor, buf) = read_string(buf)?;
        let (f0_1, buf) = read_f32(buf)?;
        let (f1_0, buf) = read_f32(buf)?;
        let (f2_1, buf) = read_f32(buf)?;
        let (f3_0_75, buf) = read_f32(buf)?;
        let (count, buf) = read_u8(buf)?;
        let mut unks0 = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (terrain_textures_default_col, buf) = read_string(loop_buf)?;
            let (terrain_textures_default_ble, buf) = read_string(buf)?;
            let (terrain_textures_default_nor, buf) = read_string(buf)?;
            let (terrain_textures_default_spec, buf) = read_string(buf)?;
            let (y, buf) = read_u32(buf)?;
            let (x, buf) = read_u32(buf)?;
            let (data, buf) = buf.split_at((x*y) as usize);
            unks0.push(TextureMaskData {
                terrain_textures_default_col: terrain_textures_default_col.to_string(),
                terrain_textures_default_ble: terrain_textures_default_ble.to_string(),
                terrain_textures_default_nor: terrain_textures_default_nor.to_string(),
                terrain_textures_default_spec: terrain_textures_default_spec.to_string(),
                x,
                y,
                data: data.to_vec(),
            });
            loop_buf = buf;
        }
        let (count, buf) = read_u8(loop_buf)?;
        let mut unks1 = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (s1, buf) = read_string(loop_buf)?;
            let (unk0, buf) = read_u32(buf)?;
            let (unk1, buf) = read_u32(buf)?;
            let (unk2, buf) = read_u32(buf)?;
            let (unk3, buf) = read_u32(buf)?;
            loop_buf = buf;
            unks1.push(TextureMaskSomething {
                s1: s1.to_string(),
                unk0,
                unk1,
                unk2,
                unk3,
            });
        }
        Ok((Self{atlas_col: atlas_col.to_owned(), atlas_nor: atlas_nor.to_owned(), f0_1, f1_0, f2_1, f3_0_75, masks: unks0, unks1}, loop_buf))
    }

    fn write(&self, buf: &mut BytesMut) {
        buf.put_u8(8);
        encode_str(buf, &self.atlas_col);
        encode_str(buf, &self.atlas_nor);
        buf.put_f32_le(self.f0_1);
        buf.put_f32_le(self.f1_0);
        buf.put_f32_le(self.f2_1);
        buf.put_f32_le(self.f3_0_75);
        buf.put_u8(self.masks.len() as u8);
        for unk in self.masks.iter() {
            encode_str(buf, &unk.terrain_textures_default_col);
            encode_str(buf, &unk.terrain_textures_default_ble);
            encode_str(buf, &unk.terrain_textures_default_nor);
            encode_str(buf, &unk.terrain_textures_default_spec);
            buf.put_u32_le(unk.y);
            buf.put_u32_le(unk.x);
            buf.extend_from_slice(&unk.data);
        }
        buf.put_u8(self.unks1.len() as u8);
        for unk in self.unks1.iter() {
            encode_str(buf, &unk.s1);
            buf.put_u32_le(unk.unk0);
            buf.put_u32_le(unk.unk1);
            buf.put_u32_le(unk.unk2);
            buf.put_u32_le(unk.unk3);
        }
    }
}

impl CGdWaterMap {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (seven, buf) = read_u8(buf)?;
        // not supporting other versions for now
        if seven != 7 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 7 found {seven}!")});
        }
        let (y, buf) = read_u32(buf)?;
        let (x, buf) = read_u32(buf)?;
        let (data, buf) = buf.split_at((x*y) as usize);
        let (count, buf) = read_u8(buf)?;
        let mut unk1s = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (u, buf) = CGdWaterMap_Unk1::from_bytes(loop_buf)?;
            loop_buf = buf;
            unk1s.push(u);
        }
        Ok((Self{x, y, data: data.to_vec(), unk1s}, loop_buf))
    }

    fn write(&self, buf: &mut BytesMut) {
        buf.put_u8(7);
        buf.put_u32_le(self.y);
        buf.put_u32_le(self.x);
        buf.extend_from_slice(&self.data);
        buf.put_u8(self.unk1s.len() as u8);
        for unk in self.unk1s.iter() {
            buf.put_u8(unk.unk);
            buf.put_u8(12);
            buf.put_f32_le(unk.f0);
            encode_str(buf, &unk.s1);
            encode_str(buf, &unk.s2);
            encode_str(buf, &unk.s3);
            encode_str(buf, &unk.s4);
            encode_str(buf, &unk.s5);
            for f in unk.f1.iter().copied() {
                buf.put_f32_le(f);
            }
            encode_str(buf, &unk.s6);
            for f in unk.f2.iter().copied() {
                buf.put_f32_le(f);
            }
            encode_str(buf, &unk.s7);
            for f in unk.f3.iter().copied() {
                buf.put_f32_le(f);
            }
            buf.put_u32_le(unk.i0);
            buf.put_u32_le(unk.i1);
            for f in unk.f4.iter().copied() {
                buf.put_f32_le(f);
            }
            encode_str(buf, &unk.s8);
            for f in unk.f5.iter().copied() {
                buf.put_f32_le(f);
            }
            encode_str(buf, &unk.s9);
            buf.put_u32_le(unk.data.len() as u32);
            buf.extend_from_slice(&unk.data);
        }
    }
}

impl CGdWaterMap_Unk1 {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (unk, buf) = read_u8(buf)?;
        let (twelve, buf) = read_u8(buf)?;
        if twelve != 12 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 12 found {twelve}!")});
        }
        let (f0, buf) = read_f32(buf)?;
        let (s1, buf) = read_string(buf)?;
        let (s2, buf) = read_string(buf)?;
        let (s3, buf) = read_string(buf)?;
        let (s4, buf) = read_string(buf)?;
        let (s5, buf) = read_string(buf)?;
        let mut f1 = [0.;6];
        let mut loop_buf = buf;
        for i in 0..6 {
            let (f, buf) = read_f32(loop_buf)?;
            f1[i] = f;
            loop_buf = buf;
        }
        let (s6, buf) = read_string(loop_buf)?;
        let mut f2 = [0.;6];
        let mut loop_buf = buf;
        for i in 0..6 {
            let (f, buf) = read_f32(loop_buf)?;
            f2[i] = f;
            loop_buf = buf;
        }
        let (s7, buf) = read_string(loop_buf)?;
        let mut f3 = [0.;49];
        let mut loop_buf = buf;
        for i in 0..49 {
            let (f, buf) = read_f32(loop_buf)?;
            f3[i] = f;
            loop_buf = buf;
        }
        let (i0, buf) = read_u32(loop_buf)?;
        let (i1, buf) = read_u32(buf)?;
        let (f, buf) = read_f32(buf)?;
        let mut f4 = [f,0.];
        let (f, buf) = read_f32(buf)?;
        f4[1] = f;
        let (s8, buf) = read_string(buf)?;
        let mut f5 = [0.;5];
        let mut loop_buf = buf;
        for i in 0..5 {
            let (f, buf) = read_f32(loop_buf)?;
            f5[i] = f;
            loop_buf = buf;
        }
        let (s9, buf) = read_string(loop_buf)?;
        let (count, buf) = read_u32(buf)?;
        let (data, buf) = buf.split_at(count as usize);
        let s1 = s1.to_string();
        let s2 = s2.to_string();
        let s3 = s3.to_string();
        let s4 = s4.to_string();
        let s5 = s5.to_string();
        let s6 = s6.to_string();
        let s7 = s7.to_string();
        let s8 = s8.to_string();
        let s9 = s9.to_string();
        Ok((Self{unk, f0, s1, s2, s3, s4, s5, f1, s6, f2, s7, f3, i0, i1, f4, s8, f5, s9, data: data.to_vec()}, buf))
    }
}

impl CGdCliffs {
    fn from_bytes(buf: &[u8]) -> anyhow::Result<(Self, &[u8]), ParsingError> {
        let (three, buf) = read_u8(buf)?;
        // not supporting other versions for now
        if three != 3 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 3 found {three}!")});
        }
        let (y, buf) = read_u32(buf)?;
        let (x, buf) = read_u32(buf)?;
        let size = (y * ((x + 31) >> 5)) as usize;
        let (data, buf) = buf.split_at(size * 4);
        let unks0 = CliffMask {x, y, data: unsafe{std::slice::from_raw_parts(data.as_ptr() as *const _, size)}.to_vec()};
        let (y, buf) = read_u32(buf)?;
        let (x, buf) = read_u32(buf)?;
        let size = (y * ((x + 31) >> 5)) as usize;
        let (data, buf) = buf.split_at(size * 4);
        let unks1 = CliffMask {x, y, data: unsafe{std::slice::from_raw_parts(data.as_ptr() as *const _, size)}.to_vec()};
        Ok((Self{ bits_0: unks0, bits_1: unks1 }, buf))
    }

    fn write(&self, buf: &mut BytesMut) {
        buf.put_u8(3);
        buf.put_u32_le(self.bits_0.y);
        buf.put_u32_le(self.bits_0.x);
        for i in self.bits_0.data.iter().copied() {
            buf.put_u32_le(i);
        }
        buf.put_u32_le(self.bits_1.y);
        buf.put_u32_le(self.bits_1.x);
        for i in self.bits_1.data.iter().copied() {
            buf.put_u32_le(i);
        }
    }
}

impl Debug for Terrain {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Terrain").finish()
    }
}

impl Fix for Terrain {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for Terrain {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Terrain {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        let GuiData{ tree, images } = &mut self.gui_data;
        egui_dock::DockArea::new(tree)
            .style(egui_dock::Style::from_egui(ui.style().as_ref()))
            .show_close_buttons(false)
            .show_add_buttons(false)
            .show_inside(ui, &mut TabViewer(images, &mut self.terrain));
    }
}

pub struct GuiData{
    tree: DockState<UiTabs>,
    images: Images,
}

struct Images{
    scale: f32,
    block_bit: RetainedImage,
    flying: RetainedImage,
    map_border: RetainedImage,
    cliff: RetainedImage,
    vision: RetainedImage,
    heights_map: RetainedImage,
    heights_map_small: Vec<RetainedImage>,
    aviatic_heights_map: RetainedImage,
    aviatic_heights_map_small: Vec<RetainedImage>,
    cliff_bits_0: RetainedImage,
    cliff_bits_1: RetainedImage,

    texture_masks: Vec<RetainedImage>,
}

impl GuiData {
    pub fn new(terain:&TerrainData) -> Self {
        let (blocking, heights_map, aviatic_heights_map, texture_masks, cliffs) = match &terain.terrain {
            MapVersion::Type23 { blocking, heights_map, aviatic_heights_map, texture_masks, cliffs, .. } => {
                (blocking, heights_map, aviatic_heights_map, texture_masks, cliffs)
            }
        };
        let mut block_bit = ColorImage::new([blocking.x as usize, blocking.y as usize], Color32::BLACK);
        for y in 0..blocking.y as usize {
            for x in 0..blocking.x as usize {
                let bit = blocking.blocking[(y * blocking.x as usize) + x];
                let shift = if bit.contains(BlockingFlags::BlockBit3) { 64 } else { 0 };
                let r = shift + if bit.contains(BlockingFlags::BlockBit0) { 128 } else { 0 };
                let g = shift + if bit.contains(BlockingFlags::BlockBit1) { 128 } else { 0 };
                let b = shift + if bit.contains(BlockingFlags::BlockBit2) { 128 } else { 0 };
                *block_bit.index_mut((x, blocking.y as usize - (1 + y))) = Color32::from_rgb(r, g, b);
            }
        }
        let block_bit = RetainedImage::from_color_image("block_bit", block_bit);

        let mut flying = ColorImage::new([blocking.x as usize, blocking.y as usize], Color32::BLACK);
        for y in 0..blocking.y as usize {
            for x in 0..blocking.x as usize {
                let bit = blocking.blocking[(y * blocking.x as usize) + x];
                let r = if bit.contains(BlockingFlags::Flying) { 255 } else { 0 };
                *flying.index_mut((x, blocking.y as usize - (1 + y))) = Color32::from_rgb(r, 0, 0);
            }
        }
        let flying = RetainedImage::from_color_image("flying", flying);

        let mut map_border = ColorImage::new([blocking.x as usize, blocking.y as usize], Color32::BLACK);
        for y in 0..blocking.y as usize {
            for x in 0..blocking.x as usize {
                let bit = blocking.blocking[(y * blocking.x as usize) + x];
                let r = if bit.contains(BlockingFlags::MapBorderBit) { 255 } else { 0 };
                *map_border.index_mut((x, blocking.y as usize - (1 + y))) = Color32::from_rgb(r, 0, 0);
            }
        }
        let map_border = RetainedImage::from_color_image("map_border", map_border);

        let mut cliff = ColorImage::new([blocking.x as usize, blocking.y as usize], Color32::BLACK);
        for y in 0..blocking.y as usize {
            for x in 0..blocking.x as usize {
                let bit = blocking.blocking[(y * blocking.x as usize) + x];
                let r = if bit.contains(BlockingFlags::Cliff) { 255 } else { 0 };
                *cliff.index_mut((x, blocking.y as usize - (1 + y))) = Color32::from_rgb(r, 0, 0);
            }
        }
        let cliff = RetainedImage::from_color_image("cliff", cliff);

        let mut vision = ColorImage::new([blocking.x as usize, blocking.y as usize], Color32::BLACK);
        for y in 0..blocking.y as usize {
            for x in 0..blocking.x as usize {
                let bit = blocking.blocking[(y * blocking.x as usize) + x];
                let r = if bit.contains(BlockingFlags::VisAndBlockBit) { 255 } else { 0 };
                *vision.index_mut((x, blocking.y as usize - (1 + y))) = Color32::from_rgb(r, 0, 0);
            }
        }
        let vision = RetainedImage::from_color_image("vision", vision);

        let mut heights_map_img = ColorImage::new([heights_map.ix as usize, heights_map.iy as usize], Color32::BLACK);
        let scale_it = 255. / heights_map.heights.iter().copied().reduce(f32::max).unwrap();
        for y in 0..heights_map.iy as usize {
            for x in 0..heights_map.ix as usize {
                let h = heights_map.heights[(y * heights_map.ix as usize) + x];
                let l = (h * scale_it) as u8;
                *heights_map_img.index_mut((x, (heights_map.iy as usize-1)-y)) = Color32::from_gray(l);
            }
        }
        let heights_map_img = RetainedImage::from_color_image("heights map", heights_map_img);

        let mut heights_map_small_imgs = vec![];
        let mut offset = heights_map.iy as usize * heights_map.ix as usize;
        for scale in [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024].into_iter() {
            let size_y = heights_map.iy as usize / scale;
            let size_x = heights_map.ix as usize / scale;
            if size_y == 0 && size_x == 0 {
                break;
            }
            let size_y = size_y.max(1);
            let size_x = size_x.max(1);
            let mut heights_map_img = ColorImage::new([size_x, size_y], Color32::BLACK);
            for y in 0..size_y {
                for x in 0..size_x {
                    let h = heights_map.heights[(y * size_x) + x + offset];
                    let l = (h * scale_it) as u8;
                    *heights_map_img.index_mut((x, (size_y-1)-y)) = Color32::from_gray(l);
                }
            }
            offset += size_y * size_x;
            let heights_map_img = RetainedImage::from_color_image(format!("heights map / {scale}"), heights_map_img);
            heights_map_small_imgs.push(heights_map_img);
        }

        let mut aviatic_heights_map_img = ColorImage::new([aviatic_heights_map.ix as usize, aviatic_heights_map.iy as usize], Color32::BLACK);
        let scale_it = 255. / aviatic_heights_map.heights.iter().copied().reduce(f32::max).unwrap();
        for y in 0..aviatic_heights_map.iy as usize {
            for x in 0..aviatic_heights_map.ix as usize {
                let h = aviatic_heights_map.heights[(y * aviatic_heights_map.ix as usize) + x];
                let l = (h * scale_it) as u8;
                *aviatic_heights_map_img.index_mut((x, (aviatic_heights_map.iy as usize-1)-y)) = Color32::from_gray(l);
            }
        }
        let aviatic_heights_map_img = RetainedImage::from_color_image("aviatic heights map", aviatic_heights_map_img);

        let mut aviatic_heights_map_small_imgs = vec![];
        let mut offset = aviatic_heights_map.iy as usize * aviatic_heights_map.ix as usize;
        for scale in [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024].into_iter() {
            let size_y = aviatic_heights_map.iy as usize / scale;
            let size_x = aviatic_heights_map.ix as usize / scale;
            if size_y == 0 && size_x == 0 {
                break;
            }
            let size_y = size_y.max(1);
            let size_x = size_x.max(1);
            let mut heights_map_img = ColorImage::new([size_x, size_y], Color32::BLACK);
            for y in 0..size_y {
                for x in 0..size_x {
                    let h = aviatic_heights_map.heights[(y * size_x) + x + offset];
                    let l = (h * scale_it) as u8;
                    *heights_map_img.index_mut((x, (size_y-1)-y)) = Color32::from_gray(l);
                }
            }
            offset += size_y * size_x;
            let heights_map_img = RetainedImage::from_color_image(format!("aviatic heights map / {scale}"), heights_map_img);
            aviatic_heights_map_small_imgs.push(heights_map_img);
        }

        let mut texture_masks_tabs = Vec::new();
        let mut texture_masks_images = Vec::with_capacity(texture_masks.masks.len());
        for (i, unk) in texture_masks.masks.iter().enumerate() {
            texture_masks_tabs.push(UiTabs::TextureMask(i));

            let mut img = ColorImage::new([unk.x as usize, unk.y as usize], Color32::BLACK);
            for y in 0..unk.y as usize {
                for x in 0..unk.x as usize {
                    let l = unk.data[(y * unk.x as usize) + x];
                    *img.index_mut((x, unk.y as usize - (1 + y))) = Color32::from_gray(l);
                }
            }
            let img = RetainedImage::from_color_image(format!("TextureMask_{i}"), img);
            texture_masks_images.push(img);
        }

        let cliff_to_image = |debug_name: &str, cliff: &CliffMask|{
            let mut img = ColorImage::new([cliff.x as usize, cliff.y as usize], Color32::BLACK);
            for y in 0..cliff.y as usize {
                for x in 0..cliff.x as usize {
                    let l = cliff.data[(y * (cliff.x / 32) as usize) + x / 32];
                    let bit_index = x % 32;
                    let bit = l & 1 << bit_index;
                    let v = if bit == 0 { 0 } else { 255 };
                    *img.index_mut((x, cliff.y as usize - (1 + y))) = Color32::from_gray(v);
                }
            }
            RetainedImage::from_color_image(debug_name, img)
        };

        let cliff_bits_0 = cliff_to_image("CliffBits_0", &cliffs.bits_0);
        let cliff_bits_1 = cliff_to_image("CliffBits_1", &cliffs.bits_1);

        let tree = DockState::new(vec![
            UiTabs::Group {name: "Blocking".to_owned(),
                tabs: DockState::new(vec![UiTabs::Blocking, UiTabs::Flying, UiTabs::MapBorder, UiTabs::Cliff, UiTabs::Vision])},
            UiTabs::Group {name: "Height maps".to_owned(), tabs: DockState::new(vec![UiTabs::HeightMap, UiTabs::AviaticHeightMap])},
            UiTabs::Group {name: "Texture masks".to_owned(), tabs: DockState::new(texture_masks_tabs)},
            UiTabs::Group {name: "Cliffs".to_owned(), tabs: DockState::new(vec![UiTabs::Cliff0, UiTabs::Cliff1])},
            UiTabs::Config,
            UiTabs::Test,
        ]);

        let images = Images{
            scale: 1.0,
            block_bit,
            flying,
            map_border,
            cliff,
            vision,
            heights_map: heights_map_img,
            heights_map_small: heights_map_small_imgs,
            aviatic_heights_map: aviatic_heights_map_img,
            aviatic_heights_map_small: aviatic_heights_map_small_imgs,
            cliff_bits_0,
            cliff_bits_1,

            texture_masks: texture_masks_images,
        };
        Self{tree, images}
    }
}

impl<'a> egui_dock::TabViewer for TabViewer<'a> {
    type Tab = UiTabs;

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        match tab {
            UiTabs::Config => "Config".into(),
            UiTabs::Group { name, .. } => (&*name).into(),

            UiTabs::Blocking => "Blocking".into(),
            UiTabs::Flying => "Flying".into(),
            UiTabs::MapBorder => "MapBorder".into(),
            UiTabs::Cliff => "Cliff".into(),
            UiTabs::Cliff0 => "Cliff0".into(),
            UiTabs::Cliff1 => "Cliff1".into(),
            UiTabs::Vision => "Vision".into(),
            UiTabs::HeightMap => "Height map".into(),
            UiTabs::AviaticHeightMap => "Aviatic height map".into(),

            UiTabs::TextureMask(i) => format!("TextureMask_{i}").into(),

            UiTabs::Test => "Test".into(),
        }
    }

    fn ui(&mut self, ui: &mut Ui, tab: &mut Self::Tab) {
        match tab {
            UiTabs::Config => {
                ui.horizontal(|ui|{
                    ui.label("image scale");
                    DragValue::new(&mut self.0.scale)
                        .clamp_range(0.2..=5.)
                        .speed(0.05)
                        .ui(ui);
                });
            }
            UiTabs::Group { name, tabs } => {
                egui_dock::DockArea::new(tabs)
                    .id(Id::new(name))
                    .style(egui_dock::Style::from_egui(ui.style().as_ref()))
                    .show_close_buttons(false)
                    .show_add_buttons(false)
                    .show_inside(ui, self);
            }

            UiTabs::Blocking => { self.0.block_bit.show_scaled(ui, self.0.scale); }
            UiTabs::Flying => { self.0.flying.show_scaled(ui, self.0.scale); }
            UiTabs::MapBorder => { self.0.map_border.show_scaled(ui, self.0.scale); }
            UiTabs::Cliff => { self.0.cliff.show_scaled(ui, self.0.scale); }
            UiTabs::Cliff0 => { self.0.cliff_bits_0.show_scaled(ui, self.0.scale * 2.); }
            UiTabs::Cliff1 => { self.0.cliff_bits_1.show_scaled(ui, self.0.scale * 2.); }
            UiTabs::Vision => { self.0.vision.show_scaled(ui, self.0.scale); }
            UiTabs::HeightMap => {
                self.0.heights_map.show_scaled(ui, self.0.scale * 2.);
                for hm in self.0.heights_map_small.iter_mut() {
                    hm.show_scaled(ui, self.0.scale * 2.);
                }
            }
            UiTabs::AviaticHeightMap => {
                self.0.aviatic_heights_map.show_scaled(ui, self.0.scale * 2.);
                for hm in self.0.aviatic_heights_map_small.iter_mut() {
                    hm.show_scaled(ui, self.0.scale * 2.);
                }
            }

            UiTabs::Test => {
                match &self.1.terrain {
                    MapVersion::Type23 { heights_map, .. } => {
                        ui.label(format!("heights_map.heights.len: {}", heights_map.heights.len()));
                    }
                }
            }
            UiTabs::TextureMask(i) => {
                let texture_masks = match &self.1.terrain {
                    MapVersion::Type23 { texture_masks, .. } => {
                        texture_masks
                    }
                };
                let mask = &texture_masks.masks[*i];
                ui.horizontal(|ui|{
                    ui.label("terrain_textures_default_col:");
                    ui.label(&mask.terrain_textures_default_col);
                });
                ui.horizontal(|ui|{
                    ui.label("terrain_textures_default_ble:");
                    ui.label(&mask.terrain_textures_default_ble);
                });
                ui.horizontal(|ui|{
                    ui.label("terrain_textures_default_nor:");
                    ui.label(&mask.terrain_textures_default_nor);
                });
                ui.horizontal(|ui|{
                    ui.label("terrain_textures_default_spec:");
                    ui.label(&mask.terrain_textures_default_spec);
                });
                self.0.texture_masks[*i].show_scaled(ui, self.0.scale * 2.);
            }
        }
    }
}

struct TabViewer<'a>(&'a mut Images, &'a mut TerrainData);

enum UiTabs {
    Config,
    Group{name: String, tabs: DockState<UiTabs>},

    Blocking,
    Flying,
    MapBorder,
    Cliff,
    Cliff0,
    Cliff1,
    Vision,
    HeightMap,
    AviaticHeightMap,

    TextureMask(usize),

    Test,
}
