use egui::Ui;
use sr_libs::utils::commands::{ParsingError, read_u16, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::AcquirableEntity;
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct PowerSlots(pub Vec<PowerSlot>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PowerSlot {
    pub b: AcquirableEntity,
    pub unknown: u8,
}

impl PowerSlots {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(PowerSlot::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for e in &self.0 {
            e.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for PowerSlots {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for PowerSlots {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl PowerSlot {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut power_slots = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (power_slot, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} PowerSlots, failed to parse the next PowerSlot, because: {e:?}", power_slots.len())))?;
            power_slots.push(power_slot);
            loop_buf = buf;
        }
        Ok(power_slots)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (nineteen, buf) = read_u16(buf)?;
        if nineteen != 19 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 19 found {nineteen}, there should be only PowerSlots!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (one, buf) = read_u8(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PowerSlot 1)")});
        }
        let (unknown, buf) = read_u8(buf)?;
        let (zero, buf) = read_u16(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (PowerSlot 2)")});
        }
        let (zero, buf) = read_u8(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (PowerSlot 3)")});
        }
        let (one, buf) = read_u8(buf)?;
        if one != 1 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 1 found {one} (PowerSlot 4)")});
        }
        Ok((Self{ b, unknown }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(19);
        self.b.write(buf);
        buf.put_u8(1);
        buf.put_u8(self.unknown);
        buf.put_u16_le(0);
        buf.put_u8(0);
        buf.put_u8(1);
    }
}

impl Validate for PowerSlot {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for PowerSlot {
    fn fix(&mut self, _map: &Map) {
    }
}

impl PowerSlot {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
