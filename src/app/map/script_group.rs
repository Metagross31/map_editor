use egui::Ui;
use sr_libs::utils::commands::{encode_str, ParsingError, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::IdEntity;
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct ScriptGroups(pub Vec<ScriptGroup>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct ScriptGroup {
    pub b: IdEntity,
    pub unknown_u8: u8,
    pub scripts: Vec<String>,
    pub unknown_u32: u32,
}

impl ScriptGroups {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(ScriptGroup::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for script_group in &self.0 {
            script_group.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for ScriptGroups {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for ScriptGroups {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl ScriptGroup {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut script_groups = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (script_group, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} script_groups, failed to parse the next ScriptGroup, because: {e:?}", script_groups.len())))?;
            script_groups.push(script_group);
            loop_buf = buf;
        }
        if !loop_buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left: {buf:?}")))
        }
        Ok(script_groups)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (four, buf) = read_u16(buf)?;
        if four != 4 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 4 found {four}, there should be only script_groups!")});
        }
        let (b, buf) = IdEntity::read(buf)?;
        let (unknown_u8, buf) = read_u8(buf)?;
        let (count, buf) = read_u16(buf)?;
        let mut loop_buf = buf;
        let mut scripts = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let (script, buf) = read_string(loop_buf)?;
            loop_buf = buf;
            scripts.push(script.to_string());
        }
        let (unknown_u32, buf) = read_u32(loop_buf)?;
        Ok((Self{ b, unknown_u8, scripts, unknown_u32 }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(4);
        self.b.write(buf);
        buf.put_u8(self.unknown_u8);
        buf.put_u16_le(self.scripts.len() as u16);
        for script in self.scripts.iter() {
            encode_str(buf, script);
        }
        buf.put_u32_le(self.unknown_u32);
    }
}

impl Validate for ScriptGroup {
    fn validate(&self, _map: &Map) -> IssueCounter {
        let mut i = IssueCounter::default();
        if self.b.often_one != 1 {
            i.add_warning(format!("Unknown field that is often 1 is {} on script group", self.b.often_one))
        }
        i
    }
}

impl Fix for ScriptGroup {
    fn fix(&mut self, _map: &Map) {
        //self.b.b.b.often_one = 1;
    }
}

impl ScriptGroup {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because parsing some ID would be too much work :(
            ui.push_id(self as *mut _ as usize, |ui|{
                if self.b.often_one != 1 {
                    let r = ui.button("FIX warning");
                    if r.clicked() {
                        self.b.often_one = 1;
                    }
                    r.on_hover_text("The field is usually one, otherwise game does not load it, but editor is OK with it");
                    ui.colored_label(ui.style().visuals.warn_fg_color, format!("{:?}", self.b));
                } else {
                    ui.label(format!("{:?}", self.b));
                }
                ui.label(format!("unknown_u8: {:?} unknown_u32: {:?}", self.unknown_u8, self.unknown_u32));
                ui.collapsing("scripts", |ui|{
                    for script in self.scripts.iter() {
                        ui.label(script);
                    }
                });
            });
        });
    }
}
