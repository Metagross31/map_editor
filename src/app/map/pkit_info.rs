
use egui::Ui;
use sr_libs::utils::commands::{encode_str, read_f32, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PkitInfo {
    pub unknown: u8,
    pub maybe_strings: [String;5],
    pub y: u16,
    pub x: u16,
    pub another_0: u16,
    pub something: Vec<u8>,
    pub pkit_starting_points: Vec<PkitStart>,
    pub more_zeros: [u32;2],
}
#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PkitStart {
    pub name: String,
    pub y: f32,
    pub x: f32,
}

impl PkitInfo {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (six, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 6 failed, because: {e:?}")))?;
        if six != 6 {
            return Err(MapError::MapFileParsing (format!("Expected 6 found {six}.")));
        }
        let (unknown, buf) = read_u8(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing unknown failed, because: {e:?}")))?;
        let (str0, buf) = read_string(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing str 0 failed, because: {e:?}")))?;
        let (str1, buf) = read_string(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing str 1 failed, because: {e:?}")))?;
        let (str2, buf) = read_string(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing str 2 failed, because: {e:?}")))?;
        let (str3, buf) = read_string(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing str 3 failed, because: {e:?}")))?;
        let (str4, buf) = read_string(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing str 4 failed, because: {e:?}")))?;
        let (count_of_something, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing count of something failed, because: {e:?}")))?;
        let (y, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing y failed, because: {e:?}")))?;
        let (x, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing x failed, because: {e:?}")))?;
        let (another_0, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing another_0 failed, because: {e:?}")))?;

        if count_of_something as usize > buf.len() {
            return Err(MapError::MapFileParsing(format!("Parsing failed, because: not enough data to read {count_of_something}")))
        }
        let (something, buf) = buf.split_at(count_of_something as usize);
        let something = something.to_vec();

        let (count, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing count failed, because: {e:?}")))?;

        let mut pkit_starting_points = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _i in 0..count {
            let (name, buf) = read_string(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing string {_i} failed, because: {e:?}")))?;
            let (y, buf) = read_f32(buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing y {_i} failed, because: {e:?}")))?;
            let (x, buf) = read_f32(buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing x {_i} failed, because: {e:?}")))?;

            pkit_starting_points.push(PkitStart{ name: name.to_string(), y, x, });
            loop_buf = buf;
        }
        let (zero6, buf) = read_u32(loop_buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 0 6 failed, because: {e:?}")))?;
        let (zero7, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 0 7 failed, because: {e:?}")))?;
        if !buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones?): {buf:?}")))
        }

        Ok(Self{
            unknown,
            maybe_strings: [str0.to_string(), str1.to_string(), str2.to_string(), str3.to_string(), str4.to_string()],
            y,
            x,
            another_0,
            something,
            pkit_starting_points,
            more_zeros: [zero6, zero7],
        })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u8(6);
        buf.put_u8(self.unknown);
        for str in self.maybe_strings.iter() {
            encode_str(&mut buf, str);
        }
        buf.put_u32_le(self.something.len() as u32);
        buf.put_u16_le(self.y);
        buf.put_u16_le(self.x);
        buf.put_u16_le(self.another_0);
        buf.extend_from_slice(&self.something);
        buf.put_u32_le(self.pkit_starting_points.len() as u32);
        for start in self.pkit_starting_points.iter() {
            encode_str(&mut buf, &start.name);
            buf.put_f32_le(start.y);
            buf.put_f32_le(start.x);
        }
        for zero in self.more_zeros.iter().copied() {
            buf.put_u32_le(zero);
        }
        buf.to_vec()
    }
}

impl Fix for PkitInfo {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for PkitInfo {
    fn validate(&self, _map: &Map) -> IssueCounter {
        let mut issues = IssueCounter::default();
        if self.another_0 != 0 {
            issues.add_warning(format!("Expected zero 5 is {} instead on pkit info", self.another_0));
        }
        for (i, zero) in self.more_zeros.iter().copied().enumerate() {
            if zero != 0 {
                issues.add_warning(format!("Expected zero({}) is {zero} instead on pkit info", 6 + i));
            }
        }
        issues
    }
}

impl PkitInfo {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.label(format!("version or something? {}", self.unknown));
        ui.label(format!("only 0s? {} {:?}", self.another_0, self.more_zeros));
        ui.label(format!("x: {} y: {}", self.x, self.y));
        ui.group(|ui|{
            for str in self.maybe_strings.iter() {
                ui.label(str);
            }
        });
        ui.label(format!("something: {:?}", self.something));
        ui.group(|ui|{
            for s in self.pkit_starting_points.iter() {
                ui.label(format!("{}: {}/{}", s.name, s.x, s.y));
            }
        });
    }
}
