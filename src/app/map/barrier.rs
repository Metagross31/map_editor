use std::ops::Deref;
use egui::Ui;
use sr_libs::utils::commands::{encode_str, ParsingError, read_f32, read_string, read_u16, read_u32, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::AcquirableEntity;
use crate::app::map::{Map, MapError, MapTable};

#[derive(serde::Serialize)]
pub struct Barriers(pub Vec<Barrier>);
#[derive(serde::Serialize)]
pub struct BarrierSets(pub Vec<BarrierSet>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Barrier {
    pub b: AcquirableEntity,
    pub entity_id: u32,
    pub zero_or_two: u8,
    pub set_name: String,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct BarrierSet {
    pub b: AcquirableEntity,
    pub unknown: f32,
    pub production_cost: u32,
    pub often_one_1: u8,
    pub often_one_2: u8,
    pub another_often_one: u8,
    pub name: String,
}

impl Barriers {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(Barrier::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for b in &self.0 {
            b.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for Barriers {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for Barriers {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl Barrier {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut barriers = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (b, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} Barrier, failed to parse the next Barrier, because: {e:?}", barriers.len())))?;
            barriers.push(b);
            loop_buf = buf;
        }
        Ok(barriers)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (e, buf) = read_u16(buf)?;
        if e != 14 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 14 found {e}, there should be only Barriers!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (seven, buf) = read_u8(buf)?;
        if seven != 7 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 7 found {seven} (Barrier 7)")});
        }
        let (entity_id, buf) = read_u32(buf)?;
        let (set_name, buf) = read_string(buf)?;
        let (zero_or_two, buf) = read_u8(buf)?;
        Ok((Self{ b, entity_id, set_name: set_name.to_string(), zero_or_two }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(14);
        self.b.write(buf);
        buf.put_u8(7);
        buf.put_u32_le(self.entity_id);
        encode_str(buf, &self.set_name);
        buf.put_u8(self.zero_or_two);
    }
}

impl Validate for Barrier {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut i = IssueCounter::default();
        if let Some((t, _, _, _)) = map.map.get(&1026) {
            match t.borrow().deref() {
                MapTable::BarrierSets(b, _) => {
                    let count = b.0.iter().filter(|set| set.name == self.set_name).count();
                    match count {
                        0 => i.add_bug(format!("No barrier set named '{}' exist for barrier {}", self.set_name, self.entity_id)),
                        1 => {}
                        _ => i.add_bug(format!("More than 1 barrier set named '{}' exist", self.set_name)),
                    }
                }
                t => {
                    i.add_bug(format!("{:?} is not an barrier set", t.name()))
                }
            }
        } else {
            i.add_bug("There is an barrier, but no barrier set".to_string())
        }
        i
    }
}

impl Fix for Barrier {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Barrier {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}

impl BarrierSets {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(BarrierSet::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for b in &self.0 {
            b.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for BarrierSets {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for BarrierSets {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl BarrierSet {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut barriers = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (b, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} BarrierSet, failed to parse the next BarrierSet, because: {e:?}", barriers.len())))?;
            barriers.push(b);
            loop_buf = buf;
        }
        Ok(barriers)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (sixteen, buf) = read_u16(buf)?;
        if sixteen != 16 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 16 found {sixteen}, there should be only BarrierSets!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (six, buf) = read_u32(buf)?;
        if six != 6 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 6 found {six} (BarrierSet 6)")});
        }
        let (zero, buf) = read_u8(buf)?;
        if zero != 0 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 0 found {zero} (BarrierSet 0 1)")});
        }
        let (unknown, buf) = read_f32(buf)?;
        let (often_one_1, buf) = read_u8(buf)?;
        let (often_one_2, buf) = read_u8(buf)?;
        let (another_often_one, buf) = read_u8(buf)?;
        let (production_cost, buf) = read_u32(buf)?;
        let (name, buf) = read_string(buf)?;
        Ok((Self{ b, unknown, often_one_1, often_one_2, another_often_one, production_cost, name: name.to_string() }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(16);
        self.b.write(buf);
        buf.put_u32_le(6);
        buf.put_u8(0);
        buf.put_f32_le(self.unknown);
        buf.put_u8(self.often_one_1);
        buf.put_u8(self.often_one_2);
        buf.put_u8(self.another_often_one);
        buf.put_u32_le(self.production_cost);
        encode_str(buf, &self.name);
    }
}

impl Validate for BarrierSet {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut i = IssueCounter::default();
        if let Some((t, _, _, _)) = map.map.get(&1025) {
            match t.borrow().deref() {
                MapTable::Barriers(b, _) => {
                    let count = b.0.iter().filter(|b| b.set_name == self.name).count();
                    if count == 0 {
                        i.add_bug(format!("No barrier in set '{}'", self.name));
                    }
                }
                t => {
                    i.add_bug(format!("{:?} is not an barrier", t.name()))
                }
            }
        } else {
            i.add_bug("There is an barrier set, but no barrier".to_string())
        }
        i
    }
}

impl Fix for BarrierSet {
    fn fix(&mut self, _map: &Map) {
    }
}

impl BarrierSet {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
