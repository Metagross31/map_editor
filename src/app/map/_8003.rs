
use egui::Ui;
use sr_libs::utils::commands::{encode_str, read_string, read_u16, read_u32};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct Data8003 {
    pub something: Vec<String>,
}

impl Data8003 {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (two, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 2 failed, because: {e:?}")))?;
        if two != 2 {
            return Err(MapError::MapFileParsing (format!("Expected 2 found {two}.")));
        }
        let (zero, buf) = read_u16(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing 0 failed, because: {e:?}")))?;
        if zero != 0 {
            return Err(MapError::MapFileParsing (format!("Expected 0 found {zero}.")));
        }
        let (count, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Parsing count failed, because: {e:?}")))?;
        let mut something = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _i in 0..count {
            let (str, buf) = read_string(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("Parsing string {_i} failed, because: {e:?}")))?;
            something.push(str.to_string());
            loop_buf = buf;
        }
        if !loop_buf.is_empty() {
            return Err(MapError::MapFileParsing(format!("There are unparsed bytes left (Could there be multiple zones?): {buf:?}")))
        }

        Ok(Self{ something })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u16_le(2);
        buf.put_u16_le(0);
        buf.put_u32_le(self.something.len() as u32);
        for str in self.something.iter() {
            encode_str(&mut buf, str)
        }
        buf.to_vec()
    }
}

impl Fix for Data8003 {
    fn fix(&mut self, _map: &Map) {

    }
}

impl Validate for Data8003 {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Data8003 {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            ui.label(format!("{self:?}"));
        });
    }
}
