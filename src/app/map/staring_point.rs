use egui::Ui;
use sr_libs::utils::commands::{ParsingError, read_u16, read_u8};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::entity::{AcquirableEntity};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
pub struct StartingPoints(pub Vec<StartingPoint>);

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct StartingPoint {
    pub b: AcquirableEntity,
}

impl StartingPoints {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Ok(Self(StartingPoint::from_bytes(buf)?))
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for starting_point in &self.0 {
            starting_point.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl Fix for StartingPoints {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.0 {
            b.fix(map)
        }
    }
}

impl Validate for StartingPoints {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.0.iter().map(|b| b.validate(map)).sum()
    }
}

impl StartingPoint {
    pub fn from_bytes(buf: &[u8]) -> Result<Vec<Self>, MapError> {
        let mut starting_points = vec![];
        let mut loop_buf = buf;
        while !loop_buf.is_empty() {
            let (starting_point, buf) = Self::read(loop_buf)
                .map_err(|e| MapError::MapFileParsing(format!("While having already parsed {} starting_points, failed to parse the next StartingPoint, because: {e:?}", starting_points.len())))?;
            starting_points.push(starting_point);
            loop_buf = buf;
        }
        Ok(starting_points)
    }
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (tweny_five, buf) = read_u16(buf)?;
        if tweny_five != 25 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 25 found {tweny_five}, there should be only starting points!")});
        }
        let (b, buf) = AcquirableEntity::read(buf)?;
        let (two, buf) = read_u8(buf)?;
        if two != 2 {
            return Err(ParsingError::UnexpectedData {internal_error:format!("Expected 2 found {two}, this is probably unsupported version!")});
        }
        Ok((Self{ b }, buf))
    }
    pub fn write(&self, buf: &mut BytesMut) {
        buf.put_u16_le(25);
        self.b.write(buf);
        buf.put_u8(2);
    }
}

impl Validate for StartingPoint {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl Fix for StartingPoint {
    fn fix(&mut self, _map: &Map) {
    }
}

impl StartingPoint {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.group(|ui|{
            // using address, because parsing some ID would be too much work :(
            ui.push_id(self as *mut _ as usize, |ui|{
                if self.b.b.b.often_one != 1 {
                    let r = ui.button("FIX warning");
                    if r.clicked() {
                        self.b.b.b.often_one = 1;
                    }
                    r.on_hover_text("The field is usually one, otherwise game does not load it, but editor is OK with it");
                    ui.colored_label(ui.style().visuals.warn_fg_color, format!("{self:?}"));
                } else {
                    ui.label(format!("{self:?}"));
                }
            });
        });
    }
}
