use crate::app::gui::{IssueCounter, Validate};
use crate::app::map::{Map, OtherFile};

pub struct Atlas {
    pub errors: Vec<String>,
}

impl Atlas {
    pub fn new(files: &[OtherFile]) -> Self {
        let col =
            files.iter().find(|f| f.relative_path == "terrain\\atlas_col.dds")
                .or_else(|| files.iter().find(|f| f.relative_path == "terrain/atlas_col.dds"));
        let nor =
            files.iter().find(|f| f.relative_path == "terrain\\atlas_nor.dds")
                .or_else(|| files.iter().find(|f| f.relative_path == "terrain/atlas_nor.dds"));

        let mut errors = vec![];

        if let Some(col) = col {
            if &col.content[84..88] != b"DXT5" {
                errors.push(format!("'terrain\\atlas_col.dds' is in format {}, instead of DXT5",
                                     std::str::from_utf8(&col.content[84..88]).unwrap_or("unknown")));
            }
        } else {
            errors.push("'terrain\\atlas_col.dds' is missing".to_string());
        }
        if let Some(nor) = nor {
            if &nor.content[84..88] != b"DXT5" {
                errors.push(format!("'terrain\\atlas_nor.dds' is in format {}, instead of DXT5",
                                     std::str::from_utf8(&nor.content[84..88]).unwrap_or("unknown")));
            }
        } else {
            errors.push("'terrain\\atlas_nor.dds' is missing".to_string());
        }
        Self{ errors }
    }
}

impl Validate for Atlas {
    fn validate(&self, _map: &Map) -> IssueCounter {
        let mut i = IssueCounter::default();
        for e in &self.errors {
            i.add_bug(e.clone())
        }
        i
    }
}
