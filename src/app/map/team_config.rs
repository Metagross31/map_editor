use std::collections::BTreeMap;
use eframe::egui::Ui;
use egui::{Color32, ComboBox, RichText};
use egui_extras::Column;
use sr_libs::utils::commands::{encode_str, ParsingError, read_u16};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use sr_libs::utils::commands::{read_string, read_u32, read_u8};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::{Map, MapError};

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamConfigurations {
    pub configurations: Vec<TeamConfiguration>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamConfiguration {
    pub setup: TeamSetup,
    pub relation: TeamRelations,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub enum TeamSetup {
    V4(TeamSetupV4),
    V5(TeamSetupV5),
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamRelations {
    pub team_relations: BTreeMap<Relation, RelationType>,
    pub team_identifiers: TeamIdentifiers,
}
#[derive(serde_with::SerializeDisplay)]
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Relation {
    pub team_a: u8,
    pub team_b: u8,
}
impl Relation {
    pub fn new(a: u8, b: u8) -> Self {
        Self{
            team_a: a,
            team_b: b,
        }
    }
}

impl std::fmt::Display for Relation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(self, f)
    }
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub enum TeamIdentifiers {
    V5(Vec<TeamIdentifierV5>),
    V6(Vec<TeamIdentifierV6>),
}

#[derive(serde::Serialize)]
#[derive(Debug, Clone)]
pub struct TeamIdentifierV5 {
    pub team_identifier_name: String,
    pub team_id: u8,
    pub unknown_reference: u8,
    pub unknown_int: u32,
    pub fow: u8,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamIdentifierV6 {
    pub team_identifier_name: String,
    pub team_id: u8,
    pub unknown_int: u32,
    pub fow: u8,
}

#[derive(serde::Serialize)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(u8)]
pub enum PlayerType {
    Human = 1,
    Monster = 2,
    Any = 3,
    EmptySlot = 4,
}

#[derive(serde::Serialize)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(u8)]
pub enum RelationType
{
    Neutral = 0x00,
    Friends = 0x01,
    Enemies = 0xff
}

impl std::fmt::Display for RelationType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(self, f)
    }
}

#[derive(serde::Serialize)]
#[derive(Debug)]
struct TeamRelation {
    teams: Relation,
    relation: RelationType,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PlayerV4 {
    player_type: PlayerType,
    player_tag: String,
    unknown: u8,
    kit_tag: String,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamV4
{
    pub team_name: String,
    pub team_id: u8,
    pub unknown_reference: u8,
    pub players: Vec<PlayerV4>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamSetupV4 {
    pub setup: String,
    pub teams: Vec<TeamV4>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct PlayerV5 {
    pub player_type: PlayerType,
    pub player_tag: String,
    pub kit_tag: String,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamV5
{
    pub team_name: String,
    pub team_id: u8,
    pub players: Vec<PlayerV5>,
}

#[derive(serde::Serialize)]
#[derive(Debug)]
pub struct TeamSetupV5 {
    pub setup: String,
    pub teams: Vec<TeamV5>,
}

impl TeamConfigurations {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        Self::from_bytes_inner(buf)
            .map_err(|e| MapError::MapFileParsing(format!("Failed to parse team configurations, because: {e:?}")))
    }
    fn from_bytes_inner(buf: &[u8]) -> Result<Self, ParsingError> {
        let (count, buf) = read_u32(buf)?;
        let mut configurations = Vec::with_capacity(count as usize);
        let mut loop_buf = buf;
        for _ in 0..count {
            let (setup, buf) = TeamSetup::read(loop_buf)?;
            let (relation, buf) = TeamRelations::read(buf)?;

            loop_buf = buf;
            configurations.push(TeamConfiguration {
                setup,
                relation,
            })
        }
        if !loop_buf.is_empty() {
            return Err(ParsingError::UnexpectedData { internal_error: format!("remaining bytes: {loop_buf:?}") })
        }
        Ok(Self {
            configurations,
        })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_u32_le(self.configurations.len() as u32);
        for configuration in &self.configurations {
            configuration.setup.write(&mut buf);
            configuration.relation.write(&mut buf);
        }
        buf.to_vec()
    }
}

impl TeamSetup {
    fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (setup_version, buf) = read_u8(buf)?;
        let (setup, buf) = read_string(buf)?;
        let (count, buf) = read_u8(buf)?;
        let mut loop_buf = buf;
        match setup_version {
            4 => {
                let mut teams = Vec::with_capacity(count as usize);
                for _ in 0..count {
                    let (team_name, buf) = read_string(loop_buf)?;
                    let (team_id, buf) = read_u8(buf)?;
                    let (unknown_reference, buf) = read_u8(buf)?;
                    let (player_count, buf) = read_u8(buf)?;
                    let mut players = Vec::with_capacity(player_count as usize);
                    loop_buf = buf;
                    for _ in 0..player_count {
                        let (player, buf) = PlayerV4::read(buf)?;
                        players.push(player);
                        loop_buf = buf;
                    }

                    teams.push(TeamV4 {
                        team_name: team_name.to_string(),
                        team_id,
                        unknown_reference,
                        players,
                    })
                }
                Ok((TeamSetup::V4(TeamSetupV4 {
                    setup: setup.to_string(),
                    teams,
                }), loop_buf))
            }
            5 => {
                let mut teams = Vec::with_capacity(count as usize);
                for _ in 0..count {
                    let (team_name, buf) = read_string(loop_buf)?;
                    let (team_id, buf) = read_u8(buf)?;
                    let (player_count, buf) = read_u8(buf)?;
                    let mut players = Vec::with_capacity(player_count as usize);
                    loop_buf = buf;
                    for _ in 0..player_count {
                        let (player, buf) = PlayerV5::read(loop_buf)?;
                        players.push(player);
                        loop_buf = buf;
                    }

                    teams.push(TeamV5 {
                        team_name: team_name.to_string(),
                        team_id,
                        players,
                    });
                }
                Ok((TeamSetup::V5(TeamSetupV5 {
                    setup: setup.to_string(),
                    teams,
                }), loop_buf))
            }
            _ => Err(ParsingError::UnexpectedData { internal_error: format!("Setup version: {setup_version} not supported") }),
        }
    }

    fn write(&self, buf: &mut BytesMut) {
        match self {
            TeamSetup::V4(v4) => {
                buf.put_u8(4);
                encode_str(buf, &v4.setup);
                buf.put_u8(v4.teams.len() as u8);
                for team in &v4.teams {
                    encode_str(buf, &team.team_name);
                    buf.put_u8(team.team_id);
                    buf.put_u8(team.unknown_reference);
                    buf.put_u8(team.players.len() as u8);
                    for player in &team.players {
                        player.write(buf);
                    }
                }
            }
            TeamSetup::V5(v5) => {
                buf.put_u8(5);
                encode_str(buf, &v5.setup);
                buf.put_u8(v5.teams.len() as u8);
                for team in &v5.teams {
                    encode_str(buf, &team.team_name);
                    buf.put_u8(team.team_id);
                    buf.put_u8(team.players.len() as u8);
                    for player in &team.players {
                        player.write(buf);
                    }
                }
            }
        }
    }
}

impl PlayerV4 {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (player_type, buf) = PlayerType::read(buf)?;
        let (player_tag, buf) = read_string(buf)?;
        let (unknown, buf) = read_u8(buf)?;
        let (kit_tag, buf) = read_string(buf)?;
        Ok((PlayerV4 {
            player_type,
            player_tag: player_tag.to_string(),
            unknown,
            kit_tag: kit_tag.to_string(),
        }, buf))
    }
    fn write(&self, buf: &mut BytesMut) {
        self.player_type.write(buf);
        encode_str(buf, &self.player_tag);
        buf.put_u8(self.unknown);
        encode_str(buf, &self.kit_tag);
    }
}

impl PlayerV5 {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (player_type, buf) = PlayerType::read(buf)?;
        let (player_tag, buf) = read_string(buf)?;
        let (kit_tag, buf) = read_string(buf)?;
        Ok((PlayerV5 {
            player_type,
            player_tag: player_tag.to_string(),
            kit_tag: kit_tag.to_string(),
        }, buf))
    }
    fn write(&self, buf: &mut BytesMut) {
        self.player_type.write(buf);
        encode_str(buf, &self.player_tag);
        encode_str(buf, &self.kit_tag);
    }
}

impl PlayerType {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (player_type, buf) = read_u8(buf)?;
        match player_type {
            1 => Ok((PlayerType::Human, buf)),
            2 => Ok((PlayerType::Monster, buf)),
            3 => Ok((PlayerType::Any, buf)),
            4 => Ok((PlayerType::EmptySlot, buf)),
            _ => Err(ParsingError::UnexpectedData { internal_error: format!("{player_type} is not a valid player type") })
        }
    }
    fn write(&self, buf: &mut BytesMut) {
        match self {
            PlayerType::Human => buf.put_u8(1),
            PlayerType::Monster => buf.put_u8(2),
            PlayerType::Any => buf.put_u8(3),
            PlayerType::EmptySlot => buf.put_u8(4),
        }
    }
}

impl TeamRelations {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (relations_version, buf) = read_u8(buf)?;
        let (count, buf) = read_u16(buf)?;
        let mut team_relations = BTreeMap::new();
        let mut loop_buf = buf;
        for _ in 0..count {
            let (relation, buf) = TeamRelation::read(loop_buf)?;
            team_relations.insert(relation.teams, relation.relation);
            loop_buf = buf;
        }

        let (team_identifiers, buf) = TeamIdentifiers::read(relations_version, loop_buf)?;
        Ok((TeamRelations {
            team_relations,
            team_identifiers,
        }, buf))
    }
    fn write(&self, buf: &mut BytesMut) {
        match self.team_identifiers {
            TeamIdentifiers::V5(_) => buf.put_u8(5),
            TeamIdentifiers::V6(_) => buf.put_u8(6),
        }
        buf.put_u16_le(self.team_relations.len() as u16);
        for team_relation in &self.team_relations {
            let team_relation = TeamRelation {
                teams: *team_relation.0,
                relation: *team_relation.1,
            };
            team_relation.write(buf);
        }
        self.team_identifiers.write(buf);
    }
}

impl TeamRelation {
    pub fn read(buf: &[u8]) -> Result<(Self, &[u8]), ParsingError> {
        let (team_a, buf) = read_u8(buf)?;
        let (team_b, buf) = read_u8(buf)?;
        let (relation, buf) = read_u8(buf)?;

        let relation = match relation {
            0x00 => RelationType::Neutral,
            0x01 => RelationType::Friends,
            0xff => RelationType::Enemies,
            _ => return Err(ParsingError::UnexpectedData { internal_error: format!("Relation : {relation} is not a valid value") }),
        };
        Ok((TeamRelation {
            teams: Relation {
                team_a,
                team_b,
            },
            relation,
        }, buf))
    }
    fn write(&self, buf: &mut BytesMut) {
        buf.put_u8(self.teams.team_a);
        buf.put_u8(self.teams.team_b);
        let relation = match self.relation {
            RelationType::Neutral => 0x00,
            RelationType::Friends => 0x01,
            RelationType::Enemies => 0xff,
        };
        buf.put_u8(relation);
    }
}

impl TeamIdentifiers {
    pub fn len(&self) -> usize {
        match self {
            TeamIdentifiers::V5(vec) => vec.len(),
            TeamIdentifiers::V6(vec) => vec.len(),
        }
    }
    pub fn iter_names(&self) -> Box<dyn Iterator<Item = &str> + '_> {
        match self {
            TeamIdentifiers::V5(vec) =>
                Box::new(vec.iter().map(|i| i.team_identifier_name.as_str())),
            TeamIdentifiers::V6(vec) =>
                Box::new(vec.iter().map(|i| i.team_identifier_name.as_str())),
        }
    }
}

impl TeamIdentifiers {
    pub fn read(version: u8, buf: &[u8]) -> Result<(TeamIdentifiers, &[u8]), ParsingError> {
        let (count, buf) = read_u16(buf)?;
        match version {
            5 => {
                let mut identifiers = Vec::with_capacity(count as usize);
                let mut loop_buf = buf;
                for _ in 0..count {
                    let (team_identifier_name, buf) = read_string(loop_buf)?;
                    let (team_id, buf) = read_u8(buf)?;
                    let (unknown_reference, buf) = read_u8(buf)?;
                    let (unknown_int, buf) = read_u32(buf)?;
                    let (fow, buf) = read_u8(buf)?;
                    loop_buf = buf;
                    identifiers.push(TeamIdentifierV5 {
                        team_identifier_name: team_identifier_name.to_string(),
                        team_id,
                        unknown_reference,
                        unknown_int,
                        fow,
                    });
                }
                identifiers.sort_by_key(|i| i.team_id);
                Ok((TeamIdentifiers::V5(identifiers), loop_buf))
            }
            6 => {
                let mut identifiers = Vec::with_capacity(count as usize);
                let mut loop_buf = buf;
                for _ in 0..count {
                    let (team_identifier_name, buf) = read_string(loop_buf)?;
                    let (team_id, buf) = read_u8(buf)?;
                    let (unknown_int, buf) = read_u32(buf)?;
                    let (fow, buf) = read_u8(buf)?;
                    loop_buf = buf;
                    identifiers.push(TeamIdentifierV6 {
                        team_identifier_name: team_identifier_name.to_string(),
                        team_id,
                        unknown_int,
                        fow,
                    });
                }
                //identifiers.sort_by_key(|i| i.team_id); // Would be nice I guess... BUT EA...
                identifiers.sort_by(|l, r| l.team_identifier_name.cmp(&r.team_identifier_name));
                Ok((TeamIdentifiers::V6(identifiers), loop_buf))
            }
            _ => Err(ParsingError::UnexpectedData { internal_error: format!("Relations version: {version} not supported, remaining {} bytes: {:x?}", buf.len(), buf) }),
        }
    }
    fn write(&self, buf: &mut BytesMut) {
        match self {
            TeamIdentifiers::V5(v5) => {
                buf.put_u16_le(v5.len() as u16);
                for i in v5 {
                    encode_str(buf, &i.team_identifier_name);
                    buf.put_u8(i.team_id);
                    buf.put_u8(i.unknown_reference);
                    buf.put_u32_le(i.unknown_int);
                    buf.put_u8(i.fow);
                }
            }
            TeamIdentifiers::V6(v6) => {
                buf.put_u16_le(v6.len() as u16);
                for i in v6 {
                    encode_str(buf, &i.team_identifier_name);
                    buf.put_u8(i.team_id);
                    buf.put_u32_le(i.unknown_int);
                    buf.put_u8(i.fow);
                }
            }
        }
    }
}

impl TeamConfigurations {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        for configuration in &mut self.configurations {
            ui.group(|ui| {
                configuration.show_mut(ui);
            });
        }
    }
}

impl Validate for TeamConfigurations {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.configurations.iter().map(|c| c.validate(map)).sum()
    }
}

impl Fix for TeamConfigurations {
    fn fix(&mut self, _map: &Map) {
        for c in &mut self.configurations {
            match &mut c.setup {
                TeamSetup::V4(v4) => {
                    c.setup = TeamSetup::V5(
                        TeamSetupV5 {
                            setup: v4.setup.clone(),
                            teams: v4.teams.iter().map(|v4| {
                                TeamV5 {
                                    team_name: v4.team_name.clone(),
                                    team_id: v4.team_id,
                                    players: v4.players.iter().map(|v4| {
                                        PlayerV5 {
                                            player_type: v4.player_type,
                                            player_tag: v4.player_tag.clone(),
                                            kit_tag: v4.kit_tag.clone(),
                                        }
                                    }).collect(),
                                }
                            }).collect(),
                        }
                    );
                }
                _ => {},
            }
            for (teams, relation) in c.relation.team_relations.iter_mut() {
                if teams.team_a == teams.team_b && *relation != RelationType::Friends {
                    *relation = RelationType::Friends
                }
            }
            match &mut c.relation.team_identifiers {
                TeamIdentifiers::V6(v6) => {
                    for t in v6 {
                        t.fow = 0;
                        t.unknown_int = 0;
                    }
                }
                TeamIdentifiers::V5(v5) => {
                    c.relation.team_identifiers = TeamIdentifiers::V6(v5.iter().map(|v5| {
                        TeamIdentifierV6 {
                            team_identifier_name: v5.team_identifier_name.clone(),
                            team_id: v5.team_id,
                            unknown_int: v5.unknown_int,
                            fow: v5.fow,
                        }
                    }).collect())
                },
            }
        }
        // TODO
    }
}

impl TeamConfiguration {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.collapsing("setup", |ui| {
            self.setup.show_mut(ui);
        });
        ui.collapsing("relation", |ui| {
            self.relation.show_mut(ui);
        });
    }
}

impl Validate for TeamConfiguration {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.setup.validate(map) + self.relation.validate(map)
    }
}

impl TeamSetup {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        match self {
            Self::V4(v4) => v4.show_mut(ui),
            Self::V5(v5) => v5.show_mut(ui),
        }
    }
}

impl Validate for TeamSetup {
    fn validate(&self, map: &Map) -> IssueCounter {
        match self {
            Self::V4(_v4) => IssueCounter::with_warning("Team setup is V4".to_string()),
            Self::V5(v5) => v5.validate(map),
        }
    }
}

impl TeamSetupV4 {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.colored_label(ui.style().visuals.warn_fg_color, "V4");
        ui.horizontal(|ui| {
            ui.label("setup");
            ui.text_edit_singleline(&mut self.setup);
        });
        ui.collapsing("teams", |ui| {
            for team in &mut self.teams {
                ui.group(|ui| {
                    ui.push_id(team.team_id, |ui| {
                        team.show_mut(ui);
                    });
                });
            }
        });
    }
}

impl TeamV4 {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.label(format!("team_id: {}", self.team_id));
        ui.label(format!("team_name: {}", self.team_name));
        ui.label(format!("unknown_reference: {}", self.unknown_reference));
        ui.collapsing("players", |ui| {
            for player in &mut self.players {
                ui.push_id(&player.player_tag, |ui|{
                    ui.group(|ui| {
                        ui.label(format!("player_type: {:?}", player.player_type));
                        ui.label(format!("player_tag: {}", player.player_tag));
                        ui.label(format!("unknown: {}", player.unknown));
                        ui.label(format!("kit_tag: {}", player.kit_tag));
                    });
                });
            }
        });
    }
}

impl TeamSetupV5 {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        if !self.teams.iter().any(|t|t.players.iter().any(|p| p.player_type == PlayerType::Human)) {
            ui.colored_label(ui.style().visuals.error_fg_color, "No human player");
        }
        if self.teams.iter().any(|t|t.players.iter().any(|p| p.player_type == PlayerType::Any)) {
            ui.colored_label(ui.style().visuals.error_fg_color, "`Any` player type not allowed");
        }
        ui.horizontal(|ui| {
            ui.label("setup");
            ui.text_edit_singleline(&mut self.setup);
        });
        ui.collapsing("teams", |ui| {
            for team in &mut self.teams {
                ui.group(|ui| {
                    ui.push_id(team.team_id, |ui| {
                        team.show_mut(ui);
                    });
                });
            }
        });
    }
}

impl Validate for TeamSetupV5 {
    fn validate(&self, map: &Map) -> IssueCounter {
        // any validation on the name?
        let mut issues : IssueCounter = self.teams.iter().map(|t| t.validate(map)).sum();
        if !self.teams.iter().any(|t| t.players.iter().any(|p| p.player_type == PlayerType::Human)) {
            issues.add_bug("No human players in team setup".to_string());
        }
        issues
    }
}

impl TeamV5 {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.label(format!("team_id: {}", self.team_id));
        ui.label(format!("team_name: {}", self.team_name));
        ui.collapsing("players", |ui| {
            for player in &mut self.players {
                ui.group(|ui| {
                    ui.push_id(&player.player_tag, |ui|{
                        ComboBox::new("player_type", "player_type")
                            .selected_text(format!("{:?}", player.player_type))
                            .show_ui(ui, |ui| {
                                ui.selectable_value(&mut player.player_type, PlayerType::Human, "Human");
                                ui.selectable_value(&mut player.player_type, PlayerType::Monster, "Monster");
                            });
                        ui.label(format!("player_tag: {}", player.player_tag));
                        ui.label(format!("kit_tag: {}", player.kit_tag));
                    });
                });
            }
        });
    }
}

impl Validate for TeamV5 {
    fn validate(&self, _map: &Map) -> IssueCounter {
        if self.players.iter().any(|p| p.player_type == PlayerType::Any) {
            IssueCounter::with_bug("Player `Any` not allowed in team".to_string())
        } else {
            IssueCounter::default() // TODO
        }
    }
}

impl TeamRelations {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        // TODO get actual line height
        let line_height = 28.;
        ui.collapsing("team_relations", |ui| {
            egui_extras::TableBuilder::new(ui)
                .columns(Column::auto().at_least(80.).resizable(true), self.team_identifiers.len() + 1)
                .header(line_height, |mut header| {
                    header.col(|ui| {
                        ui.label("");
                    });
                    for identifier in self.team_identifiers.iter_names() {
                        header.col(|ui| {
                            ui.centered_and_justified(|ui| {
                                ui.label(identifier);
                            });
                        });
                    }
                })
                .body(|body| {
                    body.rows(line_height, self.team_identifiers.len(), |mut row| {
                        let row_index = row.index();
                        row.col(|ui| {
                            ui.label(self.team_identifiers.iter_names().nth(row_index).unwrap());
                        });
                        let team_a = row_index + 1;
                        for team_b in 1..=self.team_identifiers.len() as u8 {
                            row.col(|ui| {
                                let teams = Relation{
                                    team_a: team_a as u8,
                                    team_b,
                                };
                                let relation = *self.team_relations.get(&teams).unwrap();
                                ui.centered_and_justified(|ui| {
                                    ui.label(relation.colored_text())
                                      .context_menu(|ui| {
                                          for r in [RelationType::Neutral, RelationType::Friends, RelationType::Enemies] {
                                              if ui.button(r.colored_text()).clicked() {
                                                  *self.team_relations.get_mut(&teams).unwrap() = r;
                                                  let teams = Relation{
                                                      team_a: teams.team_b,
                                                      team_b: teams.team_a,
                                                  };
                                                  *self.team_relations.get_mut(&teams).unwrap() = r;
                                              }
                                          }
                                      });
                                });
                            });
                        }
                    });
                });
        });
        ui.collapsing("team_identifiers", |ui| {
            self.team_identifiers.show_mut(ui);
        });
    }
}

impl RelationType {
    pub fn colored_text(&self) -> RichText {
        match self {
            RelationType::Neutral => RichText::new("Neutral").color(Color32::GRAY),
            RelationType::Friends => RichText::new("Friends").color(Color32::LIGHT_GREEN),
            RelationType::Enemies => RichText::new("Enemies").color(Color32::RED),
        }
    }
}

impl Validate for TeamRelations {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut issues = self.team_identifiers.validate(map);
        for (teams, relation) in self.team_relations.iter() {
            if teams.team_a == teams.team_b && *relation != RelationType::Friends {
                issues.add_bug(format!("Team {} is {:?} with itself", self.team_identifiers.iter_names().nth(teams.team_a as usize).unwrap(), relation))
            }
            let inverse = self.team_relations.get(teams).unwrap();
            if relation != inverse {
                let team_a = self.team_identifiers.iter_names().nth(teams.team_a as usize).unwrap();
                let team_b = self.team_identifiers.iter_names().nth(teams.team_b as usize).unwrap();
                issues.add_bug(format!("Team {team_a} is {relation:?} with team {team_b}, but team {team_b} is {inverse:?} with team {team_a}"))
            }
        }
        issues
    }
}

impl TeamIdentifiers {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        match self {
            TeamIdentifiers::V5(v5) => {
                for i in v5 {
                    ui.group(|ui| {
                        ui.label(format!("team_id: {}", i.team_id));
                        ui.label(format!("team_identifier_name: {}", i.team_identifier_name));
                        ui.label(format!("unknown_reference: {}", i.unknown_reference));
                        ui.label(format!("unknown_byte: {}", i.fow));
                        ui.label(format!("unknown_int: {}", i.unknown_int));
                    });
                }
            }
            TeamIdentifiers::V6(v6) => {
                for i in v6 {
                    ui.group(|ui| {
                        ui.label(format!("team_id: {}", i.team_id));
                        ui.label(format!("team_identifier_name: {}", i.team_identifier_name));
                        ui.label(format!("fog of war: {}", i.fow));
                        ui.label(format!("unknown_int: {}", i.unknown_int));
                    });
                }
            }
        }
    }
}

impl Validate for TeamIdentifiers {
    fn validate(&self, _map: &Map) -> IssueCounter {
        match self {
            TeamIdentifiers::V5(_) => IssueCounter::with_bug("TeamIdentifiers is v4".to_string()),
            TeamIdentifiers::V6(_) => IssueCounter::default(),
        }
    }
}
