pub mod team_config;
mod buildings;
mod entity;
mod atlas;
mod object;
mod squad;
mod barrier;
mod power_slot;
mod monuments;
mod player_kit;
#[allow(dead_code)]
pub mod generator;
mod terrain;
mod atmo_zones;
mod effect_zones;
mod staring_point;
mod effect;
mod script_group;
mod _8003;
mod cliff;
mod texture_layer;
mod pkit_info;

#[cfg(test)]
mod tests;
mod crc;

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::io::Write;
use std::ops::Deref;
use std::path::Path;
use egui::Color32;
use local_encoding::Encoder;
use log::{info, trace, warn};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use script_validator::{EntityType, SpawnGroupsValidationResult, ValidationResult};
use crate::app::gui::{Fix, IssueCounter, Validate};
use crate::app::map::_8003::Data8003;
use crate::app::map::atlas::Atlas;
use crate::app::map::atmo_zones::AtmoZones;
use crate::app::map::barrier::{Barriers, BarrierSets};
use crate::app::map::buildings::Buildings;
use crate::app::map::cliff::Cliffs;
use crate::app::map::effect::Effects;
use crate::app::map::effect_zones::EffectZones;
use crate::app::map::entity::{IdEntity, Param};
use crate::app::map::monuments::Monuments;
use crate::app::map::object::Objects;
use crate::app::map::pkit_info::PkitInfo;
use crate::app::map::player_kit::PlayerKits;
use crate::app::map::power_slot::PowerSlots;
use crate::app::map::script_group::ScriptGroups;
use crate::app::map::squad::Squads;
use crate::app::map::staring_point::StartingPoints;
use crate::app::map::terrain::Terrain;
use crate::app::map::texture_layer::TextureLayers;

pub struct Map {
    // no support for 12p (3x4p) maps
    pub map: BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>,
    pub entity_tags: BTreeMap<String, (u32, EntityType)>,
    pub map_name: String,
    pub global_variables: Option<Script>,
    pub script_list: Option<Script>,
    pub script_groups: Option<Script>,
    pub spawn_groups: Option<Script>,
    pub scripts: Vec<Script>,
    pub other_files: Vec<OtherFile>,
    pub atlas: Atlas,
}

pub enum MapTable {
    Unknown(Vec<u8>),
    Terrain(Terrain),
    ScriptGroups(ScriptGroups, usize),
    ParsingFailed(Vec<u8>, u32, String),
    Objects(Objects, usize),
    AtmoZones(AtmoZones),
    EffectZones(EffectZones),
    Buildings(Buildings, usize),
    Squads(Squads, usize),
    Barriers(Barriers, usize),
    BarrierSets(BarrierSets, usize),
    StartingPoints(StartingPoints, usize),
    Effects(Effects, usize),
    PowerSlots(PowerSlots, usize),
    Monuments(Monuments, usize),
    PlayerKits(PlayerKits, usize),
    TeamConfigurations(team_config::TeamConfigurations),
    TextureLayers(TextureLayers),
    Cliffs(Cliffs),
    F8003(Data8003),
    PkitInfo(PkitInfo),
}

pub struct Script {
    pub name: String,
    pub folder: u8,
    pub content: String,
    pub validation_result: ScriptValidationResult,
}
pub enum ScriptValidationResult{
    GlobalVariables,
    ScriptList(Result<Vec<String>, String>),
    ScriptGroups(Result<BTreeMap<String, Vec<String>>, String>),
    NotPartOfHierarchy,
    Validated(ValidationResult, EntityType),
    SpawnGroupsValidated(SpawnGroupsValidationResult),
    IncludedMultipleTimes(Vec<ValidationResult>),
}

impl ScriptValidationResult {
    pub fn add(&mut self, other: Self) {
        match self {
            ScriptValidationResult::GlobalVariables => unreachable!("Global variables included in a script"),
            ScriptValidationResult::ScriptList(_) => unreachable!("Script list included in a script"),
            ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups included in a script"),
            ScriptValidationResult::SpawnGroupsValidated(_) => unreachable!("Spawn groups included in a script"),
            ScriptValidationResult::NotPartOfHierarchy => { *self = other; }
            ScriptValidationResult::Validated(_, _) => {
                let mut old = Self::NotPartOfHierarchy;
                std::mem::swap(&mut old, self);
                let r =
                    match old {
                        ScriptValidationResult::Validated(r, _) => r,
                        _ => unreachable!() };
                match other {
                    ScriptValidationResult::GlobalVariables => unreachable!("Script can not be both global variables, and normal script at same time!"),
                    ScriptValidationResult::ScriptList(_) => unreachable!("Script can not be both script list, and normal script at same time!"),
                    ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups can not include a script"),
                    ScriptValidationResult::SpawnGroupsValidated(_) => todo!("Spawn groups can not include a script"),
                    ScriptValidationResult::NotPartOfHierarchy => unreachable!("Removing not supported"),
                    ScriptValidationResult::Validated(nr, _) => {
                        *self = Self::IncludedMultipleTimes(vec![r, nr]);
                    }
                    ScriptValidationResult::IncludedMultipleTimes(_) => unreachable!("multiple can be only on the left")
                }
            }
            ScriptValidationResult::IncludedMultipleTimes(multiple) => {
                match other {
                    ScriptValidationResult::GlobalVariables => unreachable!("Script can not be both global variables, and normal script at same time!"),
                    ScriptValidationResult::ScriptList(_) => unreachable!("Script can not be both script list, and normal script at same time!"),
                    ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups can not include a script"),
                    ScriptValidationResult::SpawnGroupsValidated(_) => todo!("Spawn groups can not include a script"),
                    ScriptValidationResult::NotPartOfHierarchy => unreachable!("Removing not supported"),
                    ScriptValidationResult::Validated(nr, _) => { multiple.push(nr); }
                    ScriptValidationResult::IncludedMultipleTimes(_) => unreachable!("multiple can be only on the left")
                }
            }
        }
    }
}

pub struct OtherFile {
    pub relative_path: String,
    pub content: Vec<u8>,
}

const COMMUNITY_MAP_PREFIX: &'static str = "bf1\\map\\ug_not_shipped\\";
const SCRIPT : &str = "script";
impl Map {
    pub fn fix(&mut self) {
        for (_, (table, _, _, _)) in &self.map {
            table.borrow_mut().fix(&self)
        }
        // no fix for scripts yet
    }
    pub fn from_pak_files(files: Vec<(String, Vec<u8>)>) -> Result<Self, MapError> {
        for (file, _) in &files {
            if !file.starts_with(COMMUNITY_MAP_PREFIX) {
                // Should allow reading official maps? (they are much more complex, because of multiple files, ...)
                return Err(MapError::NotACommunityMapPrefix(format!("Path '{file}' does not start with '{COMMUNITY_MAP_PREFIX}' prefix")))
            };
        }
        Self::from_files(files.into_iter().map(|(file, data)|{
                if file.starts_with(COMMUNITY_MAP_PREFIX) {
                    (file[COMMUNITY_MAP_PREFIX.len()..].to_string(), data)
                } else {
                    unreachable!()
                }
        }))
    }
    pub fn from_files(files: impl Iterator<Item=(String, Vec<u8>)>) -> Result<Self, MapError> {
        let mut map = None;
        let mut map_name = None;
        let mut global_variables = None;
        let mut script_list : Option<Script> = None;
        let mut script_groups : Option<Script> = None;
        let mut spawn_groups : Option<Script> = None;
        let mut scripts = vec![];
        let mut other_files = vec![];

        for (file, data) in files {
            if file.ends_with(".map") {
                if map.is_some() {
                    return Err(MapError::MultipleMapFiles(map_name.unwrap(), file));
                }
                map = Some(data);
                let name = file[0..file.len()-4].to_string();
                if map_name.is_none() {
                    map_name = Some(name);
                } else if Some(name) != map_name {
                    return Err(MapError::MultipleNames(map_name.unwrap(), file));
                }
            } else if file.ends_with(".lua") {
                let prefix_pos = if let Some(p) = file.find('\\') {
                    p + 1
                } else {
                    if let Some(p) = file.find('/') {
                        p + 1
                    } else {
                        return Err(MapError::WrongScriptPath(file));
                    }
                };
                let folder = file[prefix_pos + SCRIPT.len() .. prefix_pos + SCRIPT.len()+1]
                    .parse()
                    .map_err(|_| MapError::WrongScriptPath(file.to_string()))?;
                let script = match std::str::from_utf8(&data) {
                    Ok(content) => {
                        let name = file[prefix_pos + SCRIPT.len()+2..].to_string();
                        Script{ name, folder, content: content.to_string(), validation_result: ScriptValidationResult::NotPartOfHierarchy }
                    }
                    Err(e) => {
                        if let Ok(content) = local_encoding::Encoding::ANSI.to_string(&data) {
                            let name = file[prefix_pos + SCRIPT.len()+2..].to_string();
                            Script{ name, folder, content, validation_result: ScriptValidationResult::NotPartOfHierarchy }
                        } else {
                            return Err(MapError::NonUtf8Content(file.clone(), e));
                        }
                    }
                };
                scripts.push(script)
            } else {
                let prefix_pos = if let Some(p) = file.find('\\') {
                    p + 1
                } else {
                    if let Some(p) = file.find('/') {
                        p + 1
                    } else {
                        return Err(MapError::WrongScriptPath(file.clone()));
                    }
                };
                other_files.push(OtherFile{
                    relative_path: file[prefix_pos..].to_string(),
                    content: data,
                })
            }
        }

        let map = match map {
            None => { return Err(MapError::NoMap); }
            Some(map) => map,
        };

        info!("opening map file");
        let mut bytes = sr_libs::cff::check_signature(&map).unwrap();
        let mut map = BTreeMap::new();
        while bytes.len() >= 16 {
            let (input, table) = sr_libs::cff::rw_utils::parse_abstract_cff_table(bytes)
                .map_err(|e| MapError::MapFileParsing(format!("{e:?}")))?;
            let mut decompressor = flate2::read::ZlibDecoder::new(table.data);
            let mut data = Vec::new();
            use std::io::Read;
            let e = decompressor.read_to_end(&mut data);
            if e.is_err() {
                return Err(MapError::MapFileParsing(format!("{:?}", e.unwrap_err())));
            }
            if table.decompressed_size != data.len() as u32 {
                return Err(MapError::MapFileParsing(format!("Expected size: {}, real size: {}, for table: {}", table.decompressed_size, data.len(), table.id)));
            }
            let original = data.clone();
            map.insert(table.id, (RefCell::new(MapTable::from_data(table.id, data)), original, table.unknown1, table.unknown2));
            bytes = input;
        }
        trace!("Found {} tables", map.len());

        trace!("extracting entity tags");
        let mut entity_tags = BTreeMap::new();
        for (_, (t, _, _, _)) in map.iter() {
            fn foreach<'a, I: Iterator<Item = &'a IdEntity>>(entities: I, et: EntityType, entity_tags: &mut BTreeMap<String, (u32, EntityType)>) {
                for entity in entities {
                    let mut tag = None;
                    let mut id = None;
                    for param in entity.params.iter() {
                        match param {
                            Param::Tag(t) => { tag = Some(t.to_ascii_lowercase()) }
                            Param::Id(i) => { id = Some(*i) }
                            _ => {}
                        }
                    }
                    if let Some(tag) = tag {
                        match id {
                            None => {warn!("Entity {tag} does not hane an ID");}
                            Some(id) => {assert!(entity_tags.insert(tag, (id, et)).is_none());}
                        }
                    }
                }
            }
            match t.borrow().deref() {
                MapTable::ScriptGroups(e, _) => foreach(e.0.iter().map(|e| &e.b), EntityType::ScriptGroup, &mut entity_tags),
                MapTable::Objects(e, _) => foreach(e.0.iter().map(|e| &e.b.b), EntityType::Object, &mut entity_tags),
                MapTable::Buildings(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::Building, &mut entity_tags),
                MapTable::Squads(e, _) => foreach(e.0.iter().map(|e| &e.b.b), EntityType::Squad, &mut entity_tags),
                MapTable::Barriers(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::BarrierModule, &mut entity_tags),
                MapTable::BarrierSets(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::BarrierSet, &mut entity_tags),
                MapTable::StartingPoints(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::StartingPoint, &mut entity_tags),
                MapTable::Effects(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::StartingPoint, &mut entity_tags),
                MapTable::PowerSlots(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::PowerSlot, &mut entity_tags),
                MapTable::Monuments(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::TokenSlot, &mut entity_tags),
                MapTable::PlayerKits(_, _) => {},
                MapTable::AtmoZones(_) |
                MapTable::EffectZones(_) |
                MapTable::Terrain(_) |
                MapTable::TextureLayers(_) |
                MapTable::Cliffs(_) |
                MapTable::F8003(_) |
                MapTable::PkitInfo(_) |
                MapTable::Unknown(_) |
                MapTable::ParsingFailed(_, _, _) |
                MapTable::TeamConfigurations(_) => {},
            }
        }
        trace!("extracted {} entity tags", entity_tags.len());

        /*trace!("precompute entity ids");
        fn update_base_id(base_id: &mut usize, table: &u32, map: &BTreeMap<u32, (RefCell<MapTable>, u16, u16)>) {
            macro_rules! update_base_id {
                ($e:ident, $start:ident, $base_id:ident) => {
                    {
                        *$start = *$base_id;
                        *$base_id += $e.0.len();
                    }
                };
            }
            if let Some((t, _, _)) = map.get(table) {
                match t.borrow_mut().deref_mut() {
                    MapTable::Objects(e, start) => update_base_id!(e, start, base_id),
                    MapTable::Buildings(e, start) => update_base_id!(e, start, base_id),
                    MapTable::Squads(e, start) => update_base_id!(e, start, base_id),
                    MapTable::Barriers(e, start) => update_base_id!(e, start, base_id),
                    MapTable::BarrierSets(e, start) => update_base_id!(e, start, base_id),
                    MapTable::PowerSlots(e, start) => update_base_id!(e, start, base_id),
                    MapTable::Monuments(e, start) => update_base_id!(e, start, base_id),
                    MapTable::PlayerKits(e, start) => update_base_id!(e, start, base_id),
                    MapTable::AtmoZones(_) => {
                        *base_id += 1;
                    }
                    MapTable::Terrain(_) |
                    MapTable::Unknown(_) |
                    MapTable::ParsingFailed(_, _, _) |
                    MapTable::TeamConfigurations(_) => {
                        error!("IDs are most likely incorrect! table: {table} base_id: {base_id}");
                        if 1014 == *table || 1011 == *table {
                            *base_id += 1;
                        }
                    },
                }
            }
        }

        let mut base_id = 1;
        const TABLE_LOAD_ORDER : &[u32] = &[
            1014,
            ATMO_ZONES_TABLE_ID,
            1011,
            1013,
            SQUADS_TABLE_ID,
            OBJECTS_TABLE_ID,
            BARRIER_SETS_TABLE_ID,
            BARRIERS_TABLE_ID,
            BUILDINGS_TABLE_ID,
            EFFECTS_TABLE_ID,
            1007,
            PLAYER_KITS_TABLE_ID,
            1009,
            1018,
            1024,
            1002,
            STARTING_POINTS_TABLE_ID,
            1028,
            POWER_SLOTS_TABLE_ID,
            MONUMENTS_TABLE_ID,
            1033,
            1034,
            1032,
            1017,
        ];
        for table in TABLE_LOAD_ORDER {
            update_base_id(&mut base_id, table, &map);
        }*/

        trace!("validating scripts");
        let map_name = map_name.unwrap();
        let all_scripts = scripts;
        let mut scripts = Vec::with_capacity(all_scripts.len());
        for script in all_scripts {
            if script.name.eq_ignore_ascii_case("_GlobalVars.lua")
                || script.name.eq_ignore_ascii_case(&format!("_{map_name}_GlobalVars.lua")) {
                if global_variables.is_some() {
                    unreachable!()
                }
                global_variables = Some(Script{ validation_result: ScriptValidationResult::GlobalVariables, .. script });
            } else if script.name.eq_ignore_ascii_case("_scriptList.lua")
                || script.name.eq_ignore_ascii_case(&format!("_{map_name}_scriptList.lua")) {
                if script_list.is_some() {
                    unreachable!()
                }
                let r = script_validator::validate_script_list(&script.name, &script.content)
                    .map_err(|e| format!("{e:?}"));
                script_list = Some(Script{ validation_result: ScriptValidationResult::ScriptList(r), .. script });
            } else if script.name.eq_ignore_ascii_case("_scriptgroups.lua")
                || script.name.eq_ignore_ascii_case(&format!("_{map_name}_scriptgroups.lua")) {
                if script_groups.is_some() {
                    unreachable!()
                }
                let r = script_validator::validate_script_groups(&script.name, &script.content)
                    .map_err(|e| format!("{e:?}"));
                script_groups = Some(Script{ validation_result: ScriptValidationResult::ScriptGroups(r), .. script });
            } else if script.name.eq_ignore_ascii_case("_spawngroups.lua")
                || script.name.eq_ignore_ascii_case(&format!("_{map_name}_spawngroups.lua")) {
                if spawn_groups.is_some() {
                    unreachable!()
                }
                spawn_groups = Some(script);
            } else {
                scripts.push(script)
            }
        }
        let atlas = Atlas::new(&other_files);
        let gv = global_variables.as_ref().map(|s| (s.name.as_str(), s.content.as_str()));
        for script in &mut scripts {
            if let Some(et) = entity_tags.get(&script.name[0 .. script.name.len() - 4].to_ascii_lowercase()).map(|(_, et)| *et) {
                trace!("validate: {}", script.name);
                let validation_result = script_validator::validate(&script.name, &script.content, gv, et);
                trace!("script {} for entity type {:?} with result: {}", script.name, et, script_validator::summary(&validation_result));
                script.validation_result.add(ScriptValidationResult::Validated(validation_result, et));
            }
            if script.name.eq_ignore_ascii_case("_main.lua")
                || script.name.eq_ignore_ascii_case(&format!("_{map_name}.lua")) {
                trace!("validate: {}", script.name);
                let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
                script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
            } else if (&script.name[0 .. script.name.len() - 4]).eq_ignore_ascii_case(&map_name) {
                trace!("validate: {}", script.name);
                let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
                script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
            } else if let Some(script_groups) = &script_groups {
                match &script_groups.validation_result {
                    ScriptValidationResult::ScriptGroups(Ok(script_groups)) => {
                        if let Some((_, _group)) = script_groups.iter().find(|(key, _)| (&script.name[0 .. script.name.len() - 4]).eq_ignore_ascii_case(key)) {
                            trace!("validate: {}", script.name);
                            let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                            trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
                            script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                        }
                    }
                    _ => {}
                }
            }
        }
        if let Some(sl) = &mut script_list {
            if let ScriptValidationResult::ScriptList(Ok(list)) = &sl.validation_result {
                for script in &mut scripts {
                    if list.iter().any(|le| script.name.eq_ignore_ascii_case(le)) {
                        trace!("validate: {}", script.name);
                        let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                        trace!("(script list) {} with result: {}", script.name, script_validator::summary(&validation_result));
                        script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                    } else {
                        // TODO add error (to UI) if script does not exist
                        warn!("(script list) {} does not exist", script.name);
                    }
                }
            }
        }
        let spawn_groups = if let Some(spawn_groups) = spawn_groups {
            trace!("validate: {}", spawn_groups.name);
            let r = script_validator::validate_spawn_groups(&spawn_groups.name, &spawn_groups.content, gv);
            if let Ok((_, Ok(list))) = &r {
                for script in &mut scripts {
                    if list.iter().any(|le| script.name[0 .. script.name.len()-4].eq_ignore_ascii_case(le)) {
                        trace!("validate: {}", script.name);
                        let validation_result = script_validator::validate_spawn_group(&spawn_groups.content, &script.name, &script.content, gv, EntityType::World);
                        trace!("(spawn group) {} with result: {}", script.name, script_validator::summary(&validation_result));
                        script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                    } else {
                        // TODO add error (to UI) if script does not exist
                        warn!("(spawn group) {} does not exist", script.name);
                    }
                }
            }
            Some(Script{ validation_result: ScriptValidationResult::SpawnGroupsValidated(r), .. spawn_groups })
        } else {
            None
        };
        // TODO script_groups `_Member` sufixed scripts
        info!("Map parsed");
        Ok(Self{ map, entity_tags, map_name, global_variables, script_list, script_groups, spawn_groups, scripts, other_files, atlas })
    }

    pub fn to_pak_files(&self) -> std::io::Result<Vec<(String, Vec<u8>)>> {
        let mut files = vec![];
        let name = &self.map_name;

        let map = self.as_blob()?;
        files.push((format!("{COMMUNITY_MAP_PREFIX}{name}.map"), map));

        for script in self.scripts.iter().chain(self.script_groups.iter()).chain(self.script_list.iter()) {
            files.push(
                (format!("{COMMUNITY_MAP_PREFIX}{name}\\{SCRIPT}{}\\{}", script.folder, script.name),
                 local_encoding::Encoding::ANSI.to_bytes(&script.content).unwrap()));
        }
        for other in &self.other_files {
            files.push((format!("{COMMUNITY_MAP_PREFIX}{name}\\{}", other.relative_path), other.content.clone()));
        }

        Ok(files)
    }

    pub fn save_pak(&self, path: &Path) -> std::io::Result<()> {
        crate::app::pak::save(path, self.to_pak_files()?)
    }

    pub fn save_map_files(&mut self, path: &Path) -> std::io::Result<()> {
        let name = path.file_name().unwrap().to_str().unwrap();
        let name = name[..name.len() - 4].to_string();
        let folder_path = path.parent().unwrap().join(&name);
        self.map_name = name;
        std::fs::create_dir_all(&folder_path)?;
        for other_file in &self.other_files {
            let slash_index =
                other_file.relative_path.rfind('\\')
                .or_else(||other_file.relative_path.rfind('/'));
            if let Some(i) = slash_index {
                std::fs::create_dir_all(&folder_path.join(&other_file.relative_path[..i]))?;
            }
            std::fs::write(&folder_path.join(&other_file.relative_path), &other_file.content)?;
        }
        for script in self.scripts.iter().chain(self.script_groups.iter()).chain(self.script_list.iter()) {
            let folder = folder_path.join(format!("{SCRIPT}{}", script.folder));
            std::fs::create_dir_all(&folder)?;
            let path = folder.join(&script.name);
            std::fs::write(&path, &script.content)?;
        }

        std::fs::write(path, self.as_blob()?)
    }
    pub fn extract(&self, folder: &Path) -> std::io::Result<()> {
        std::fs::create_dir_all(&folder)?;
        for (id, (table, original, _, _)) in &self.map {
            let path = folder.join(format!("{id}"));
            let modified = table.borrow().to_bytes();
            if modified.iter().zip(original).any(|(m, o)| *m != *o) {
                info!("{id}:{:?} changed", table.borrow().name());
            }
            std::fs::write(&path, modified)?;
            let path = folder.join(format!("{id}.orig"));
            std::fs::write(&path, &original)?;
            if let Some(text) = table.borrow().as_text() {
                let path = folder.join(format!("{id}.json"));
                std::fs::write(&path, text)?;
            }
        }
        Ok(())
    }
    fn as_blob(&self) -> std::io::Result<Vec<u8>> {
        let mut buf = BytesMut::new();
        buf.put_slice(&sr_libs::cff::SIGNATURE);
        buf.put_slice(&[0,0,0,1,0,0,0,1,0,0,0,0,0,0,0]);
        for (id, (table, _, unk1, unk2)) in self.map.iter() {
            let table = table.borrow().to_bytes();
            let decompressed_size = table.len() as u32;
            let mut compressor = flate2::write::ZlibEncoder::new(Vec::new(), flate2::Compression::none());
            compressor.write_all(&table)?;
            let compressed_table = compressor.finish()?;
            buf.put_u32_le(*id);
            buf.put_u16_le(*unk1);
            buf.put_u32_le(compressed_table.len() as u32);
            buf.put_u16_le(*unk2);
            buf.put_u32_le(decompressed_size);
            buf.put_slice(&compressed_table);
        }
        buf.put_u32_le(crc::compute(&buf));
        Ok(buf.to_vec())
    }
}

const TERRAIN_TABLE_ID: u32 = 1000;
const OBJECTS_TABLE_ID: u32 = 1001;
const PLAYER_KITS_TABLE_ID: u32 = 1004;
const SQUADS_TABLE_ID: u32 = 1005;
const BUILDINGS_TABLE_ID: u32 = 1006;
const ATMO_ZONES_TABLE_ID: u32 = 1010;
const EFFECT_ZONES_TABLE_ID: u32 = 1011;
const _1014_TABLE_ID: u32 = 1014;
const SCRIPT_GROUPS_TABLE_ID: u32 = 1018;
const LIGHTINGS_TABLE_ID: u32 = 1019;
const BARRIERS_TABLE_ID: u32 = 1025;
const BARRIER_SETS_TABLE_ID: u32 = 1026;
const STARTING_POINTS_TABLE_ID: u32 = 1027;
const POWER_SLOTS_TABLE_ID: u32 = 1029;
const MONUMENTS_TABLE_ID: u32 = 1030;
const EFFECTS_TABLE_ID: u32 = 1031;
const TEXTURE_LAYERS_TABLE_ID: u32 = 8001;
const CLIFF_TABLE_ID: u32 = 8002;
const F8003_TABLE_ID: u32 = 8003;
const TEAM_CONFIGURATIONS_TABLE_ID: u32 = 9000;
const PKIT_START_INFO_TABLE_ID: u32 = 9010;

impl Validate for Map {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut issues = IssueCounter::default();
        for (_, (t, _, _, _)) in self.map.iter() {
            issues += t.borrow().validate(map);
        }
        for script in self.scripts.iter() {
            issues += script.validate(map);
        }
        issues += self.atlas.validate(map);
        issues
    }
}

impl MapTable {
    pub fn from_data(id: u32, data: Vec<u8>) -> Self {
        Self::from_data_inner(id, &data)
            .unwrap_or_else(|e|{
                match e {
                    MapError::NoMap => Self::Unknown(data),
                    _ => {
                        Self::ParsingFailed(data, id, format!("parsing {id} file failed because: {e:?}"))
                    }
                }
            })
    }
    fn from_data_inner(id: u32, data: &[u8]) -> Result<Self, MapError> {
        match id {
            TERRAIN_TABLE_ID => Ok(Self::Terrain(Terrain::from_bytes(&data)?)),
            SCRIPT_GROUPS_TABLE_ID => Ok(Self::ScriptGroups(ScriptGroups::from_bytes(&data)?, 0)),
            OBJECTS_TABLE_ID => Ok(Self::Objects(Objects::from_bytes(&data)?, 0)),
            PLAYER_KITS_TABLE_ID => Ok(Self::PlayerKits(PlayerKits::from_bytes(&data)?, 0)),
            SQUADS_TABLE_ID => Ok(Self::Squads(Squads::from_bytes(&data)?, 0)),
            BUILDINGS_TABLE_ID => Ok(Self::Buildings(Buildings::from_bytes(&data)?, 0)),
            ATMO_ZONES_TABLE_ID => Ok(Self::AtmoZones(AtmoZones::from_bytes(&data)?)),
            EFFECT_ZONES_TABLE_ID => Ok(Self::EffectZones(EffectZones::from_bytes(&data)?)),
            // LIGHTINGS_TABLE_ID =>  lighting
            BARRIERS_TABLE_ID => Ok(Self::Barriers(Barriers::from_bytes(&data)?, 0)),
            BARRIER_SETS_TABLE_ID => Ok(Self::BarrierSets(BarrierSets::from_bytes(&data)?, 0)),
            STARTING_POINTS_TABLE_ID => Ok(Self::StartingPoints(StartingPoints::from_bytes(&data)?, 0)),
            // 1027 startingPoint
            POWER_SLOTS_TABLE_ID => Ok(Self::PowerSlots(PowerSlots::from_bytes(&data)?, 0)),
            MONUMENTS_TABLE_ID => Ok(Self::Monuments(Monuments::from_bytes(&data)?, 0)),
            EFFECTS_TABLE_ID => Ok(Self::Effects(Effects::from_bytes(&data)?, 0)),
            TEAM_CONFIGURATIONS_TABLE_ID => Ok(Self::TeamConfigurations(team_config::TeamConfigurations::from_bytes(&data)?)),
            TEXTURE_LAYERS_TABLE_ID => Ok(Self::TextureLayers(TextureLayers::from_bytes(&data)?)),
            CLIFF_TABLE_ID => Ok(Self::Cliffs(Cliffs::from_bytes(&data)?)),
            F8003_TABLE_ID => Ok(Self::F8003(Data8003::from_bytes(&data)?)),
            PKIT_START_INFO_TABLE_ID => Ok(Self::PkitInfo(PkitInfo::from_bytes(&data)?)),
            _ => Err(MapError::NoMap)
        }
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            MapTable::Unknown(data) => data.clone(),
            MapTable::ParsingFailed(data, _, _) => data.clone(),

            MapTable::Terrain(t) => t.to_bytes(),
            MapTable::TeamConfigurations(tc) => tc.to_bytes(),
            MapTable::TextureLayers(e) => e.to_bytes(),
            MapTable::Cliffs(e) => e.to_bytes(),
            MapTable::F8003(e) => e.to_bytes(),
            MapTable::PkitInfo(e) => e.to_bytes(),

            MapTable::AtmoZones(e) => e.to_bytes(),
            MapTable::EffectZones(e) => e.to_bytes(),
            MapTable::ScriptGroups(e, _) => e.to_bytes(),
            MapTable::Buildings(e, _) => e.to_bytes(),
            MapTable::Objects(e, _) => e.to_bytes(),
            MapTable::Squads(e, _) => e.to_bytes(),
            MapTable::Barriers(e, _) => e.to_bytes(),
            MapTable::BarrierSets(e, _) => e.to_bytes(),
            MapTable::StartingPoints(e, _) => e.to_bytes(),
            MapTable::Effects(e, _) => e.to_bytes(),
            MapTable::PowerSlots(e, _) => e.to_bytes(),
            MapTable::Monuments(e, _) => e.to_bytes(),
            MapTable::PlayerKits(e, _) => e.to_bytes(),
        }
    }
    pub fn as_text(&self) -> Option<String> {
        match self {
            MapTable::Unknown(_) => None,
            MapTable::ParsingFailed(_, _, _) => None,
            MapTable::Terrain(_) => None,

            MapTable::TeamConfigurations(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::TextureLayers(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::Cliffs(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::F8003(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::PkitInfo(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),

            MapTable::AtmoZones(e) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::EffectZones(e) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::ScriptGroups(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Buildings(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Objects(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Squads(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Barriers(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::BarrierSets(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::StartingPoints(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Effects(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::PowerSlots(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Monuments(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::PlayerKits(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
        }
    }
    pub fn name(&self) -> Option<&'static str> {
        match self {
            MapTable::Unknown(_) => None,
            MapTable::ParsingFailed(_, _, _) => None,

            MapTable::TeamConfigurations(_) => Some("Team Configurations"),
            MapTable::Terrain(_) => Some("Terrain"),
            MapTable::TextureLayers(_) => Some("Texture Layers"),
            MapTable::Cliffs(_) => Some("Cliffs"),
            MapTable::F8003(_) => Some("8003 something"),
            MapTable::PkitInfo(_) => Some("PKit info"),

            MapTable::AtmoZones(_) => Some("AtmoZones"),
            MapTable::EffectZones(_) => Some("EffectZones"),
            MapTable::ScriptGroups(_, _) => Some("Script Groups"),
            MapTable::Buildings(_, _) => Some("Buildings"),
            MapTable::Objects(_, _) => Some("Objects"),
            MapTable::Squads(_, _) => Some("Squads"),
            MapTable::Barriers(_, _) => Some("Barriers"),
            MapTable::BarrierSets(_, _) => Some("Barrier Sets"),
            MapTable::StartingPoints(_, _) => Some("Starting Points"),
            MapTable::Effects(_, _) => Some("Effects"),
            MapTable::PowerSlots(_, _) => Some("Power Slots"),
            MapTable::Monuments(_, _) => Some("Monuments"),
            MapTable::PlayerKits(_, _) => Some("Player kits"),
        }
    }
}
impl Validate for MapTable {
    fn validate(&self, map: &Map) -> IssueCounter {
        match self {
            MapTable::Unknown(_) => IssueCounter::default(),
            MapTable::ParsingFailed(_, _, e) => IssueCounter::with_bug(e.clone()),

            MapTable::Terrain(t) => t.validate(map),
            MapTable::TeamConfigurations(tc) => tc.validate(map),
            MapTable::TextureLayers(e) => e.validate(map),
            MapTable::Cliffs(e) => e.validate(map),
            MapTable::F8003(e) => e.validate(map),
            MapTable::PkitInfo(e) => e.validate(map),

            MapTable::AtmoZones(e) => e.validate(map),
            MapTable::EffectZones(e) => e.validate(map),
            MapTable::ScriptGroups(e, _) => e.validate(map),
            MapTable::Buildings(e, _) => e.validate(map),
            MapTable::Objects(e, _) => e.validate(map),
            MapTable::Squads(e, _) => e.validate(map),
            MapTable::Barriers(e, _) => e.validate(map),
            MapTable::BarrierSets(e, _) => e.validate(map),
            MapTable::StartingPoints(e, _) => e.validate(map),
            MapTable::Effects(e, _) => e.validate(map),
            MapTable::PowerSlots(e, _) => e.validate(map),
            MapTable::Monuments(e, _) => e.validate(map),
            MapTable::PlayerKits(e, _) => e.validate(map),
        }
    }
}

#[derive(Debug)]
pub enum MapError {
    NoMap,
    NotACommunityMapPrefix(String),
    MultipleMapFiles(String, String),
    WrongScriptPath(String),
    MultipleNames(String, String),
    NonUtf8Content(String, std::str::Utf8Error),
    MapFileParsing(String),
}

macro_rules! show_it_mut {
    ($name:literal, $ctx:ident, $open: ident, $showable:ident) => {
        {
            egui::Window::new($name).open($open).show($ctx, |ui|{
                egui::ScrollArea::vertical().show(ui, |ui|{
                    $showable.show_mut(ui);
                });
            });
        }
    };
    ($name:literal, $ctx:ident, $open: ident, $showable:expr, $base_id:ident) => {
        {
            egui::Window::new($name).open($open).show($ctx, |ui|{
                // just to avoid unused warning
                let _ = $base_id;
                //use egui::Widget;
                //egui::DragValue::new($base_id).ui(ui);
                egui::ScrollArea::vertical().show(ui, |ui|{
                    for (_i, s) in $showable.iter_mut().enumerate() {
                        //ui.label(format!("{}", i + *$base_id));
                        s.show_mut(ui);
                    }
                })
            });
        }
    };
}

impl MapTable {
    pub fn show_mut(&mut self, table_id: u32, open: &mut bool, _map: &Map, ctx: &egui::Context) {
        match self {
            MapTable::Unknown(unk) => {
                egui::Window::new(format!("{table_id}")).open(open).show(ctx, |ui|{
                    ui.colored_label(Color32::RED, "I have no idea what this table is, if you have an idea, please help");
                    if !unk.is_empty() {
                        egui::ScrollArea::vertical().show(ui, |ui|{
                            ui.collapsing(format!("show all {} bytes", unk.len()), |ui|{
                                ui.label(format!("{unk:02X?}"));
                            });
                            ui.collapsing(format!("show all {} bytes as text", unk.len()), |ui|{
                                ui.label(String::from_utf8_lossy(unk));
                            });
                        });
                    }
                });
            }
            MapTable::ParsingFailed(_, id, error) => {
                egui::Window::new(id.to_string()).open(open).show(ctx, |ui|{
                    ui.colored_label(ui.style().visuals.error_fg_color, error);
                });
            }
            MapTable::TeamConfigurations(cfg) =>
                show_it_mut!("Team Configurations", ctx, open, cfg),
            MapTable::Terrain(t) =>
                show_it_mut!("Terrain", ctx, open, t),
            MapTable::TextureLayers(e) =>
                show_it_mut!("Texture Layers", ctx, open, e),
            MapTable::Cliffs(e) =>
                show_it_mut!("Cliffs", ctx, open, e),
            MapTable::F8003(e) =>
                show_it_mut!("8003 something", ctx, open, e),
            MapTable::PkitInfo(e) =>
                show_it_mut!("Pkit info", ctx, open, e),
            MapTable::AtmoZones(e) =>
                show_it_mut!("AtmoZones", ctx, open, e),
            MapTable::EffectZones(e) =>
                show_it_mut!("EffectZones", ctx, open, e),
            MapTable::ScriptGroups(e, base_id) =>
                show_it_mut!("Script Groups", ctx, open, &mut e.0, base_id),
            MapTable::Buildings(e, base_id) =>
                show_it_mut!("Buildings", ctx, open, &mut e.0, base_id),
            MapTable::Objects(e, base_id) =>
                show_it_mut!("Objects", ctx, open, &mut e.0, base_id),
            MapTable::Squads(e, base_id) =>
                show_it_mut!("Squads", ctx, open, &mut e.0, base_id),
            MapTable::Barriers(e, base_id) =>
                show_it_mut!("Barriers", ctx, open, &mut e.0, base_id),
            MapTable::BarrierSets(e, base_id) =>
                show_it_mut!("Barrier sets", ctx, open, &mut e.0, base_id),
            MapTable::StartingPoints(e, base_id) =>
                show_it_mut!("Starting points", ctx, open, &mut e.0, base_id),
            MapTable::Effects(e, base_id) =>
                show_it_mut!("Effects", ctx, open, &mut e.0, base_id),
            MapTable::PowerSlots(e, base_id) =>
                show_it_mut!("Power slots", ctx, open, &mut e.0, base_id),
            MapTable::Monuments(e, base_id) =>
                show_it_mut!("Monuments", ctx, open, &mut e.0, base_id),
            MapTable::PlayerKits(e, base_id) =>
                show_it_mut!("Player kits", ctx, open, &mut e.0, base_id),
        }
    }
}

impl Fix for MapTable {
    fn fix(&mut self, map: &Map) {
        match self {
            MapTable::Unknown(_) => {}
            MapTable::ParsingFailed(_, _, _) => {}

            MapTable::TeamConfigurations(tc) => tc.fix(map),
            MapTable::Terrain(t) => t.fix(map),
            MapTable::TextureLayers(e) => e.fix(map),
            MapTable::Cliffs(e) => e.fix(map),
            MapTable::F8003(e) => e.fix(map),
            MapTable::PkitInfo(e) => e.fix(map),

            MapTable::AtmoZones(e) => e.fix(map),
            MapTable::EffectZones(e) => e.fix(map),
            MapTable::ScriptGroups(e, _) => e.fix(map),
            MapTable::Buildings(e, _) => e.fix(map),
            MapTable::Objects(e, _) => e.fix(map),
            MapTable::Squads(s, _) => s.fix(map),
            MapTable::Barriers(e, _) => e.fix(map),
            MapTable::BarrierSets(e, _) => e.fix(map),
            MapTable::StartingPoints(e, _) => e.fix(map),
            MapTable::Effects(e, _) => e.fix(map),
            MapTable::PowerSlots(e, _) => e.fix(map),
            MapTable::Monuments(e, _) => e.fix(map),
            MapTable::PlayerKits(e, _) => e.fix(map),
        }
    }
}

impl Validate for Script {
    fn validate(&self, _map: &Map) -> IssueCounter {
        let mut issues = IssueCounter::default();
        if self.folder != 1 {
            issues.add_warning(format!("Script '{}' in folder {}", self.name, self.folder));
        }
        if self.folder > 3 {
            issues.add_bug(format!("Script '{}' in folder {}", self.name, self.folder));
        }
        match &self.validation_result {
            ScriptValidationResult::GlobalVariables => {}
            ScriptValidationResult::ScriptList(Ok(_)) => {}
            ScriptValidationResult::ScriptGroups(Ok(_)) => {}
            ScriptValidationResult::ScriptList(Err(e)) |
            ScriptValidationResult::ScriptGroups(Err(e)) => {
                issues.add_bug(e.clone());
            }
            ScriptValidationResult::NotPartOfHierarchy => {
                issues.add_bug(format!("Script {} is not part of hierarchy", self.name));
            }
            ScriptValidationResult::Validated(Err(e), _) |
            ScriptValidationResult::Validated(Ok((_, Err(e))), _) => {
                issues.add_bug(format!("Script {} error: {e:?}", self.name));
            }
            ScriptValidationResult::SpawnGroupsValidated(Ok((_, Ok(_)))) => {}
            ScriptValidationResult::SpawnGroupsValidated(Err(e)) |
            ScriptValidationResult::SpawnGroupsValidated(Ok((_, Err(e)))) => {
                issues.add_bug(format!("Script {} error: {e:?}", self.name))
            }
            ScriptValidationResult::Validated(Ok((_, Ok(_))), _) => { }
            ScriptValidationResult::IncludedMultipleTimes(results) => {
                issues.add_warning("Script is included multiple times".to_string());
                for r in results {
                    match r {
                        Ok((_, Ok(_))) => {}
                        Ok((_, Err(e))) => {
                            issues.add_bug(format!("Script {} error: {e:?}", self.name))
                        }
                        Err(e) => {
                            issues.add_bug(format!("Script {} error: {e:?}", self.name))
                        }
                    }
                }
            }
        }
        issues
    }
}

impl Script {
    pub fn revalidate(&mut self, gv: Option<(&str, &str)>) {
        match self.validation_result {
            ScriptValidationResult::ScriptList(_) => {
                let r = script_validator::validate_script_list(&self.name, &self.content)
                    .map_err(|e| format!("{e:?}"));
                self.validation_result = ScriptValidationResult::ScriptList(r);
            }
            ScriptValidationResult::ScriptGroups(_) => {
                let r = script_validator::validate_script_groups(&self.name, &self.content)
                    .map_err(|e| format!("{e:?}"));
                self.validation_result = ScriptValidationResult::ScriptGroups(r);
            }
            ScriptValidationResult::Validated(_, et) => {
                let r = script_validator::validate(&self.name, &self.content, gv, et);
                self.validation_result = ScriptValidationResult::Validated(r, et);
            }
            ScriptValidationResult::SpawnGroupsValidated(_) => {
                let r = script_validator::validate_spawn_groups(&self.name, &self.content, gv);
                self.validation_result = ScriptValidationResult::SpawnGroupsValidated(r);
            }
            // caller's responsibility to revalidate
            ScriptValidationResult::GlobalVariables => { }
            ScriptValidationResult::NotPartOfHierarchy => { }
            ScriptValidationResult::IncludedMultipleTimes(_) => { }
        }
    }
}
