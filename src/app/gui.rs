mod state_machine;

use std::collections::BTreeSet;
use std::iter::Sum;
use std::ops::{Add, AddAssign};
use egui::{ScrollArea, Ui};
use log::info;
use crate::app::map::{Script, ScriptValidationResult};

#[derive(Default)]
pub struct GUI {
    map_tables: BTreeSet<u32>,
    script_globals: bool,
    script_list: bool,
    script_groups: bool,
    spawn_groups: bool,
    scripts: BTreeSet<usize>,
    script_theme_editor: bool,
    other: BTreeSet<usize>,
    explain_issues: bool,
    show_debug_table_list: bool,
    issues: IssueCounter,
}

#[derive(Default)]
pub struct IssueCounter {
    bugs: usize,
    warnings: usize,
    error_text: Vec<String>,
    warning_text: Vec<String>,
}

impl IssueCounter {
    pub fn add_bug(&mut self, explain: String) {
        self.bugs += 1;
        self.error_text.push(explain);
    }
    pub fn add_warning(&mut self, explain: String) {
        self.warnings += 1;
        self.warning_text.push(explain);
    }
    pub fn with_bug(explain: String) -> Self {
        let mut new = Self::default();
        new.add_bug(explain);
        new
    }
    pub fn with_warning(explain: String) -> Self {
        let mut new = Self::default();
        new.add_warning(explain);
        new
    }
}

impl Add for IssueCounter {
    type Output = Self;

    fn add(mut self, mut rhs: Self) -> Self::Output {
        self.error_text.append(&mut rhs.error_text);
        self.warning_text.append(&mut rhs.warning_text);
        Self {
            bugs: self.bugs + rhs.bugs,
            warnings: self.warnings + rhs.warnings,
            error_text: self.error_text,
            warning_text: self.warning_text,
        }
    }
}
impl AddAssign for IssueCounter {
    fn add_assign(&mut self, mut rhs: Self) {
        self.bugs += rhs.bugs;
        self.warnings += rhs.warnings;
        self.error_text.append(&mut rhs.error_text);
        self.warning_text.append(&mut rhs.warning_text);
    }
}
impl Sum for IssueCounter {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(IssueCounter::default(), |state, next| state + next)
    }
}

pub trait Validate {
    fn validate(&self, map: &super::map::Map) -> IssueCounter;
}
pub trait Fix {
    fn fix(&mut self, map: &super::map::Map);
}

impl GUI {
    pub fn new_map(&mut self, map: &super::map::Map) {
        self.map_tables.clear();
        self.script_globals = false;
        self.script_list = false;
        self.script_groups = false;
        self.spawn_groups = false;
        self.scripts.clear();
        self.script_theme_editor = false;
        info!("validating map");
        self.issues = map.validate(map);
    }
    pub fn show_mut(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        egui::SidePanel::left(egui::Id::new(42)).show(ctx, |ui| { ui.label(&map.map_name);
        //egui::Window::new(&map.map_name).show(ctx, |ui| {
            ScrollArea::vertical().show(ui, |ui| {
                if self.issues.bugs != 0 {
                    ui.colored_label(ui.style().visuals.error_fg_color, format!("bugs: {}", self.issues.bugs));
                }
                if self.issues.warnings != 0 {
                    ui.colored_label(ui.style().visuals.warn_fg_color, format!("warnings: {}", self.issues.warnings));
                }
                if self.issues.bugs != 0 || self.issues.warnings != 0 {
                    if ui.button("FIX").clicked() {
                        map.fix();
                        self.new_map(map);
                    }
                    ui.checkbox(&mut self.explain_issues, "explain issues");
                }
                ui.collapsing("map tables:", |ui| {
                    for (id, (table, _, _, _)) in map.map.iter() {
                        let mut b = self.map_tables.contains(id);
                        if (if let Some(name) = table.borrow().name() {
                            ui.checkbox(&mut b, name)
                        } else {
                            ui.checkbox(&mut b, &format!("{id}"))
                        }).changed() {
                            if b {
                                self.map_tables.insert(*id);
                            } else {
                                self.map_tables.remove(id);
                            }
                        }
                    }
                });
                ui.collapsing("scripts:", |ui| {
                    if let Some(script) = &map.global_variables {
                        ui.checkbox(&mut self.script_globals, &script.name);
                    }
                    if let Some(script) = &map.script_list {
                        ui.checkbox(&mut self.script_list, &script.name);
                    }
                    if let Some(script) = &map.script_groups {
                        ui.checkbox(&mut self.script_groups, &script.name);
                    }
                    if let Some(script) = &map.spawn_groups {
                        ui.checkbox(&mut self.spawn_groups, &script.name);
                    }
                    for (i, script) in map.scripts.iter().enumerate() {
                        let mut b = self.scripts.contains(&i);
                        if ui.checkbox(&mut b, &script.name).changed() {
                            if b {
                                self.scripts.insert(i);
                            } else {
                                self.scripts.remove(&i);
                            }
                        }
                    }
                    ui.checkbox(&mut self.script_theme_editor, "script theme editor");
                });
                ui.collapsing("other:", |ui| {
                    for (i, o) in map.other_files.iter().enumerate() {
                        if extension_type(path_extension(&o.relative_path)) != FileType::Unknown {
                            let mut b = self.other.contains(&i);
                            if ui.checkbox(&mut b, &o.relative_path).changed() {
                                if b {
                                    self.other.insert(i);
                                } else {
                                    self.other.remove(&i);
                                }
                            }
                        } else {
                            ui.label(&o.relative_path);
                        }
                    }
                    for error in &map.atlas.errors {
                        ui.colored_label(ui.style().visuals.error_fg_color, error);
                    }
                });
                ui.checkbox(&mut self.show_debug_table_list, "dbg table list");
            });
        });

        if self.explain_issues && (self.issues.bugs != 0 || self.issues.warnings != 0) {
            egui::Window::new("Issues").show(ctx, |ui| {
                ScrollArea::vertical().show(ui, |ui|{
                    for explain in &self.issues.error_text {
                        ui.colored_label(ui.style().visuals.error_fg_color, explain);
                    }
                    for explain in &self.issues.warning_text {
                        ui.colored_label(ui.style().visuals.warn_fg_color, explain);
                    }
                });
            });
        }

        self.maps(map, ctx);
        self.scripts(map, ctx);
        self.script_theme_editor(ctx);
        self.other_files(map, ctx);
        if self.show_debug_table_list {
            egui::Window::new("table IDs debug").show(ctx, |ui|{
                ScrollArea::vertical().show(ui, |ui|{
                    for (table_id, (table,_ , s1,s2)) in map.map.iter() {
                        ui.label(format!("{table_id} {s1} {s2} {:?}", table.borrow().name()));
                    }
                });
            });
        }
    }
}


fn show_script_mut(script: Option<&mut Script>, open: &mut bool, ctx: &egui::Context) -> bool {
    if *open == false || script.is_none() {
        return false;
    }
    let script = script.unwrap();
    egui::Window::new(&script.name).open(open).show(ctx, |ui| {
        let theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx());
        const LANGUAGE: &str = "lua";
        let mut layouter = |ui: &Ui, string: &str, wrap_width: f32| {
            let mut layout_job =
                egui_extras::syntax_highlighting::highlight(ui.ctx(), &theme, string, LANGUAGE);
            layout_job.wrap.max_width = wrap_width;
            ui.fonts(|f| f.layout_job(layout_job))
        };

        ScrollArea::vertical().show(ui, |ui| {
            fn show_validation (validation_result: &script_validator::ValidationResult, ui: &mut Ui) {
                match validation_result {
                    Ok((_, Ok(states))) => {
                        ui.collapsing("dbg state machine", |ui|{
                            state_machine::show_state_machine(states, ui);
                        });
                    }
                    Ok((_, Err(e))) |
                    Err(e) => {
                        ui.colored_label(ui.style().visuals.error_fg_color, format!("{e:?}"));
                    }
                }
            }
            match &script.validation_result {
                ScriptValidationResult::GlobalVariables => {}
                ScriptValidationResult::ScriptList(r) => {
                    match r {
                        Ok(_) => {}
                        Err(e) => {
                            ui.colored_label(ui.style().visuals.error_fg_color, format!("{e}"));
                        }
                    }
                }
                ScriptValidationResult::ScriptGroups(r) => {
                    match r {
                        Ok(groups) => {
                            ui.collapsing("goups", |ui|{
                                ui.label(format!("{:#?}", groups));
                            });
                        }
                        Err(e) => {
                            ui.colored_label(ui.style().visuals.error_fg_color, format!("{e}"));
                        }
                    }
                }
                ScriptValidationResult::NotPartOfHierarchy => {
                    ui.colored_label(ui.style().visuals.error_fg_color, "Script is not part of an hierarchy");
                }
                ScriptValidationResult::SpawnGroupsValidated(_r) => {
                    /*match _r {
                        Ok((_, Ok(emitters))) => {
                            for emitter in emitters {
                                ui.label(emitter);
                            }
                        }
                        _ => {
                            ui.label(&format!("{r:#?}"));
                        }
                    }*/
                    // TODO
                }
                ScriptValidationResult::Validated(r, _) => show_validation(r, ui),
                ScriptValidationResult::IncludedMultipleTimes(results) => {
                    ui.colored_label(ui.style().visuals.error_fg_color, "Script included multiple times!");
                    for result in results {
                        show_validation(result, ui);
                    }
                }
            }
            let r = ui.add(
                egui::TextEdit::multiline(&mut script.content)
                    .font(egui::TextStyle::Monospace) // for cursor height
                    .code_editor()
                    .desired_rows(10)
                    .lock_focus(true)
                    .desired_width(f32::INFINITY)
                    .layouter(&mut layouter),
            );
            r.changed()
        }).inner
    }).and_then(|r| r.inner).unwrap_or_default()
}

impl GUI {
    fn maps(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        self.map_tables.retain(|map_table|{
            let mut retain = true;
            map.map.get(map_table).unwrap().0.borrow_mut().show_mut(*map_table, &mut retain, map, ctx);
            retain
        });
    }
    
    fn scripts(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        let all = show_script_mut(map.global_variables.as_mut(), &mut self.script_globals, ctx);
        let all = show_script_mut(map.script_list.as_mut(), &mut self.script_list, ctx) || all;
        let all = show_script_mut(map.script_groups.as_mut(), &mut self.script_groups, ctx) || all;
        let all = show_script_mut(map.spawn_groups.as_mut(), &mut self.spawn_groups, ctx) || all;
        self.scripts.retain(|index|{
            let script = &mut map.scripts[*index];
            let mut retain = true;
            if show_script_mut(Some(script), &mut retain, ctx) {
                let gv = map.global_variables.as_ref().map(|s| (s.name.as_str(), s.content.as_str()));
                script.revalidate(gv)
            }
            retain
        });
        if all {
            for script in &mut map.scripts {
                let gv = map.global_variables.as_ref().map(|s| (s.name.as_str(), s.content.as_str()));
                script.revalidate(gv)
            }
        }
    }
    fn script_theme_editor(&mut self, ctx: &egui::Context) {
        if self.script_theme_editor {
            egui::Window::new("script theme editor").show(ctx, |ui| {
                let mut theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx());
                ui.collapsing("Theme", |ui| {
                    ui.group(|ui| {
                        theme.ui(ui);
                        theme.clone().store_in_memory(ui.ctx());
                    });
                });
            });
        }
    }
    fn other_files(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        self.other.retain(|index|{
            let o = &mut map.other_files[*index];
            let mut retain = true;
            let ext = path_extension(&o.relative_path);
            egui::Window::new(&o.relative_path).open(&mut retain).show(ctx, |ui|{
                match extension_type(ext) {
                    FileType::Text => {
                        match std::str::from_utf8(&o.content) {
                            Ok(text) => {
                                let theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx());
                                let mut layouter = |ui: &Ui, string: &str, wrap_width: f32| {
                                    let mut layout_job =
                                        egui_extras::syntax_highlighting::highlight(ui.ctx(), &theme, string, &o.relative_path);
                                    layout_job.wrap.max_width = wrap_width;
                                    ui.fonts(|f| f.layout_job(layout_job))
                                };
                                let mut text = text.to_string();
                                ScrollArea::vertical().show(ui, |ui|{
                                    let r = ui.add(
                                        egui::TextEdit::multiline(&mut text)
                                            .font(egui::TextStyle::Monospace) // for cursor height
                                            .code_editor()
                                            .desired_rows(10)
                                            .lock_focus(true)
                                            .desired_width(f32::INFINITY)
                                            .layouter(&mut layouter),
                                    );
                                    if r.changed() {
                                        o.content = text.into();
                                    }
                                });
                            }
                            Err(err) => {
                                ui.colored_label(ui.style().visuals.error_fg_color, format!("{}", err));
                            }
                        }
                    }
                    FileType::Image => {
                        ui.image(egui::ImageSource::Bytes {uri: o.relative_path.clone().into(), bytes: o.content.clone().into()});
                    }
                    FileType::Unknown => {}
                }
            });
            retain
        });
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum FileType { Text, Image, Unknown }

fn path_extension(path: &str) -> Option<&str> {
    /*std::path::Path::try_from(path).ok()?
        .extension()?
        .to_str()*/
    let dot = path.rfind(".")? + 1;
    Some(&path[dot..])
}
fn extension_type(ext: Option<&str>) -> FileType {
    fn extension_type(ext: Option<&str>) -> Option<FileType> {
        let ext = ext?;
        if ext.eq_ignore_ascii_case("xml") ||
            ext.eq_ignore_ascii_case("md") ||
            ext.eq_ignore_ascii_case("json") {
            return Some(FileType::Text)
        }
        if ext.eq_ignore_ascii_case("dds") ||
            ext.eq_ignore_ascii_case("png") ||
            ext.eq_ignore_ascii_case("jpg") ||
            ext.eq_ignore_ascii_case("tga") ||
            ext.eq_ignore_ascii_case("bmp") {
            return Some(FileType::Image)
        }
        Some(FileType::Unknown)
    }
    extension_type(ext).unwrap_or(FileType::Unknown)
}
