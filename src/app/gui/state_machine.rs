
use egui::{CollapsingHeader, Ui};
use script_validator::{Action, Condition, Event, ParamValue, States};

pub fn show_state_machine(states: &States, ui: &mut Ui) {
    if let Some(ea_representation) = &states.ea_representation {
        ui.collapsing("EA's representation", |ui|{
            ui.horizontal(|ui|{
                if ui.button("copy to clippboard").clicked() {
                    ui.ctx().output_mut(|out|{
                        out.copied_text = ea_representation.clone()
                    });
                }
                ui.label(format!(" length: {:.2} kB", ea_representation.len() as f32 / 1024.));
            });
            egui::ScrollArea::vertical().max_height(500.).show(ui, |ui| {
                ui.code(ea_representation);
            });
        });
    }
    if states.states.len() > 1 {
        ui.horizontal(|ui|{
            ui.label("First state: ");
            ui.label(&states.first_state);
        });
    }
    if ui.button("copy states to clippboard").clicked() {
        ui.ctx().output_mut(|out|{
            out.copied_text = format!("{:#?}", states);
        });
    }
    for (name, state) in states.states.iter() {
        ui.collapsing(name, |ui|{
            if !state.events.is_empty() {
                ui.collapsing("events", |ui|{show_events(&state.events, ui);});
            }
            if !state.dead_events.is_empty() {
                ui.collapsing("dead events", |ui|{show_events(&state.dead_events, ui);});
            }
            if !state.death_events.is_empty() {
                ui.collapsing("death events", |ui|{show_events(&state.death_events, ui);});
            }
            if !state.respawn_events.is_empty() {
                ui.collapsing("respawn events", |ui|{show_events(&state.respawn_events, ui);});
            }
        });
    }
}

fn show_events(events: &[Event], ui: &mut Ui) {
    for event in events {
        ui.collapsing(&event.name, |ui|{
            if let Some(goto_state) = &event.goto_state {
                if goto_state != "self" {
                    ui.horizontal(|ui|{
                        ui.label("GOTO: ");
                        ui.label(goto_state);
                    });
                }
            }
            if !event.conditions.is_empty() {
                ui.collapsing("conditions", |ui|{
                    for condition in &event.conditions {
                        show_condition(condition, ui);
                    }
                });
            }
            if !event.actions.is_empty() {
                ui.collapsing("actions", |ui|{
                    for action in &event.actions {
                        show_action(action, ui);
                    }
                });
            }
        });
    }
}

fn show_condition(condition: &Condition, ui: &mut Ui) {
    ui.group(|ui|{
        match condition {
            Condition::Single { name, function, params, execute } => {
                ui.horizontal(|ui|{
                    ui.label("Name: ");
                    ui.label(name);
                });
                ui.horizontal(|ui|{
                    ui.label("Function: ");
                    ui.label(function);
                });
                ui.horizontal(|ui|{
                    ui.label("Execute: ");
                    ui.label(execute);
                });
                show_parameters(params, ui);
            }
            Condition::OR(or) => {
                CollapsingHeader::new("OR")
                    .default_open(true)
                    .show(ui, |ui|{
                        for c in or {
                            show_condition(c, ui);
                        }
                    });
            }
        }
    });
}

fn show_parameters(params: &[ParamValue], ui: &mut Ui) {
    if params.is_empty() {
        return;
    }
    ui.horizontal(|ui|{
        ui.label("Params: ");
        for param in params {
            match param {
                ParamValue::String(s) => ui.label(&format!("'{s}'")),
                ParamValue::Number(n) => ui.label(&format!("{n}")),
                ParamValue::Bool(b) => ui.label(&format!("{b}")),
            };
            ui.label(", ");
        }
    });
}

fn show_action(action: &Action, ui: &mut Ui) {
    ui.group(|ui|{
        ui.horizontal(|ui|{
            ui.label("Name: ");
            ui.label(&action.name);
        });
        ui.horizontal(|ui|{
            ui.label("Function: ");
            ui.label(&action.function);
        });
        ui.horizontal(|ui|{
            ui.label("Execute: ");
            ui.label(&action.execute);
        });
        show_parameters(&action.params, ui);
    });
}
