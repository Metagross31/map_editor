mod pak;
mod map;
mod gui;

use std::path::{Path, PathBuf};
use eframe::egui;
use log::info;

/// We derive Deserialize/Serialize, so we can persist app state on shutdown.
#[derive(Default, serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    #[serde(skip)]
    state: AppState,
    #[serde(skip)]
    map: Option<map::Map>,
    #[serde(skip)]
    gui: gui::GUI,
    #[serde(skip)]
    edit_style: bool,
}

enum NextState{OpenMap, EditMap, Close}
enum AppState {
    OpenMap(egui_file::FileDialog),
    EditMap,
    SaveMap(egui_file::FileDialog, NextState),
    Extract(egui_file::FileDialog, NextState),
    GenerateMap(map::generator::Generator),
}

impl Default for AppState {
    fn default() -> Self {
        //let mut dialog = egui_file::FileDialog::open_file(None);
        let mut dialog = egui_file::FileDialog::open_file(Some(r#"C:\Users\xxxku\Documents\BattleForge\map"#.into()));
        dialog.open();
        Self::OpenMap(dialog)
    }
}

const LAST_OPENED_MAP_FILE : &str = "LAST_OPENED_MAP_FILE";

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        let app : Self = if let Some(storage) = cc.storage {
            let path : String = eframe::get_value(storage, LAST_OPENED_MAP_FILE)
                .unwrap_or(r#"C:\Users\xxxku\Documents\BattleForge\map"#.into());
            let mut dialog = egui_file::FileDialog::open_file(Some(path.into()));
            dialog.open();
            TemplateApp{
                state: AppState::OpenMap(dialog),
                ..eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default()
            }
        } else {
            Default::default()
        };

        egui_extras::install_image_loaders(&cc.egui_ctx);

        app
    }

    fn top_panel(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        ui.ctx().send_viewport_cmd(egui::ViewportCommand::Close);
                    }
                });
                if ui.button("❗ WIP ❗ Generate random map").clicked() {
                    let g = map::generator::generate_map();
                    self.state = AppState::GenerateMap(g);
                }
                let mut new_state = None;
                match &mut self.state {
                    AppState::OpenMap(dialog) => {
                        dialog.show(ctx);
                        if dialog.selected() {
                            let path = dialog.path().unwrap();
                            if let Some(s) = frame.storage_mut() {
                                eframe::set_value(s, LAST_OPENED_MAP_FILE, &path.to_str().unwrap().to_string());
                            }
                            let extension = path.extension().unwrap().to_str().unwrap().to_lowercase();
                            if extension == "pak" {
                                info!("loading {path:?} (as packed map)");
                                let files = pak::open(&path).unwrap();
                                let map = map::Map::from_pak_files(files).unwrap();
                                self.gui.new_map(&map);
                                self.map = Some(map);
                                new_state = Some(AppState::EditMap);
                            } else if extension == "map"{
                                fn dir_files(prefix_len: usize, folder: &Path, files: &mut Vec<(String, Vec<u8>)>) -> std::io::Result<()> {
                                    if folder.exists() {
                                        for e in std::fs::read_dir(folder)? {
                                            let e = e?;
                                            let metadata = e.metadata()?;
                                            if metadata.is_dir() {
                                                dir_files(prefix_len, &e.path(), files)?
                                            } else if metadata.is_file() {
                                                let path = e.path().to_str().unwrap()[prefix_len..].to_string();
                                                let data = std::fs::read(e.path())?;
                                                files.push((path, data));
                                            }
                                        }
                                    }
                                    Ok(())
                                }
                                fn all_files(path: &Path, files: &mut Vec<(String, Vec<u8>)>) -> std::io::Result<()> {
                                    let map_data = std::fs::read(path)?;
                                    let map_file_name = path.file_name().unwrap().to_str().unwrap().to_string();
                                    let folder_name : &str = &map_file_name[..map_file_name.len() - 4];
                                    let parent = path.parent().unwrap(); // maps in the root of file system, are not supported
                                    let folder = parent.join(folder_name);
                                    let prefix_len = parent.to_str().unwrap().len() + 1; // +1 for a folder separator
                                    files.push((map_file_name, map_data));
                                    dir_files(prefix_len, &folder, files)
                                }

                                info!("loading {path:?} (as map with folder)");
                                let mut files = Vec::new();
                                all_files(&path, &mut files).unwrap();
                                let map = map::Map::from_files(files.into_iter()).unwrap();
                                self.gui.new_map(&map);
                                self.map = Some(map);
                                new_state = Some(AppState::EditMap);
                            } else {
                                panic!("Not supported map format")
                            }
                        }
                    }
                    AppState::EditMap => {
                        ui.menu_button("save", |ui|{
                            let dialog_and =
                                | new_state: fn(egui_file::FileDialog, NextState) -> AppState,
                                  dialog: fn(Option<PathBuf>) -> egui_file::FileDialog,
                                  next: NextState,
                                  frame: &mut eframe::Frame|
                                    {
                                        let path : Option<PathBuf> =
                                            frame.storage_mut()
                                                .and_then(|s| eframe::get_value(s, LAST_OPENED_MAP_FILE))
                                                .map(|p: String| PathBuf::from(p));
                                        let mut dialog = dialog(path);
                                        dialog.open();
                                        Some(new_state(dialog, next))
                                    };
                            let save_and =
                                | new_state: fn(egui_file::FileDialog, NextState) -> AppState,
                                  next: NextState,
                                  frame: &mut eframe::Frame| dialog_and(new_state, egui_file::FileDialog::save_file, next, frame);
                            let extract_and =
                                | new_state: fn(egui_file::FileDialog, NextState) -> AppState,
                                  next: NextState,
                                  frame: &mut eframe::Frame| dialog_and(new_state, |path| egui_file::FileDialog::select_folder(
                                        path.map(|p|{
                                            let file_name = p.file_name().unwrap().to_str().unwrap();
                                            p.parent().unwrap().join(&file_name[0..file_name.len() - 4]).into()
                                        })), next, frame);
                            if ui.button("Save and continue").clicked() {
                                new_state = save_and(AppState::SaveMap, NextState::EditMap, frame);
                            } else if ui.button("Save and open another").clicked() {
                                new_state = save_and(AppState::SaveMap, NextState::OpenMap, frame);
                            } else if ui.button("Save and close").clicked() {
                                new_state = save_and(AppState::SaveMap, NextState::Close, frame);
                            } else if ui.button("Extract and continue").clicked() {
                                new_state = extract_and(AppState::Extract, NextState::EditMap, frame);
                            } else if ui.button("Extract and open another").clicked() {
                                new_state = extract_and(AppState::Extract, NextState::OpenMap, frame);
                            } else if ui.button("Extract and close").clicked() {
                                new_state = extract_and(AppState::Extract, NextState::Close, frame);
                            }
                        });
                    }
                    AppState::SaveMap(dialog, next) => {
                        dialog.show(ctx);
                        if dialog.selected() {
                            let path = dialog.path().unwrap().to_path_buf();
                            let extension = path.extension().unwrap().to_str().unwrap().to_lowercase();
                            if extension == "pak" {
                                info!("saving to {path:?} (as packed map)");
                                self.map.as_ref().unwrap().save_pak(&path).unwrap();
                            } else if extension == "map"{
                                info!("saving to {path:?} (as map with folder)");
                                self.map.as_mut().unwrap().save_map_files(&path).unwrap();
                            } else {
                                panic!("Not supported map format")
                            }
                            new_state = match next {
                                NextState::OpenMap => Some(AppState::OpenMap({
                                    let mut dialog = egui_file::FileDialog::open_file(Some(path));
                                    dialog.open();
                                    dialog
                                })),
                                NextState::EditMap => Some(AppState::EditMap),
                                NextState::Close => {ui.ctx().send_viewport_cmd(egui::ViewportCommand::Close); None},
                            };
                        }
                    }
                    AppState::Extract(dialog, next) => {
                        dialog.show(ctx);
                        if dialog.selected() {
                            let path = dialog.path().unwrap().to_path_buf();
                            info!("extracting to {path:?}");
                            self.map.as_ref().unwrap().extract(&path).unwrap();
                            new_state = match next {
                                NextState::OpenMap => Some(AppState::OpenMap(egui_file::FileDialog::open_file(Some(path)))),
                                NextState::EditMap => Some(AppState::EditMap),
                                NextState::Close => {ui.ctx().send_viewport_cmd(egui::ViewportCommand::Close); None},
                            };
                        }
                    }
                    AppState::GenerateMap(g) => {
                        if let Some(map) = g.finish() {
                            self.gui.new_map(&map);
                            self.map = Some(map);
                            new_state = Some(AppState::EditMap)
                        }
                    }
                }
                if let Some(new_state) = new_state {
                    self.state = new_state;
                }
                ui.menu_button("⚙", |ui|{
                    ui.checkbox(&mut self.edit_style, "ui settings");
                });
                ui.label(concat!("version: ",env!("CARGO_PKG_VERSION")));
            });
        });
    }
}

impl eframe::App for TemplateApp {
    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {

        self.top_panel(ctx, frame);
        egui::Window::new("ui settings").open(&mut self.edit_style).show(ctx, |ui|{
            ctx.settings_ui(ui);
        });
        match &mut self.state {
            AppState::OpenMap(_) => {}
            AppState::EditMap => {
                self.gui.show_mut(self.map.as_mut().unwrap(), ctx);
            }
            AppState::SaveMap(_, _) => {}
            AppState::Extract(_, _) => {}
            AppState::GenerateMap(g) => {g.show(ctx);}
        }
    }

    /// Called by the framework to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }
}
