use eframe::emath::Vec2;
use eframe::epaint::{ColorImage, TextureHandle};
use eframe::epaint::textures::TextureOptions;
use egui::load::SizedTexture;
use egui::Ui;

pub enum RetainedImage {
    ColorImage(String, ColorImage),
    Texture(TextureHandle),
}

impl RetainedImage {
    pub fn from_color_image(name: impl Into<String>, img: ColorImage) -> Self {
        Self::ColorImage(name.into(), img)
    }
    pub fn show_scaled(&mut self, ui: &mut Ui, scale: f32) {
        match self {
            Self::ColorImage(name, img) => {
                let texture = ui.ctx().load_texture(std::mem::take(name), std::mem::take(img), TextureOptions::default());
                *self = Self::Texture(texture);
            }
            Self::Texture(texture) => {
                let size = texture.size();
                let size = Vec2::new(size[0] as f32 * scale, size[1] as f32 * scale);
                ui.image(egui::ImageSource::Texture(SizedTexture::new(texture, size)));
            }
        }
    }
}