#![warn(clippy::all, rust_2018_idioms)]

mod app;
pub(crate) mod retained_image;

pub use app::TemplateApp;
