
use std::collections::BTreeMap;
use anyhow::{anyhow, Context};
use log::trace;
use rlua::{Lua, Table, Value, Error, Number};

pub type ValidationResult = anyhow::Result<(Lua, anyhow::Result<States>)>;
pub type SpawnGroupsValidationResult = anyhow::Result<(Lua, anyhow::Result<Vec<String>>)>;

pub fn summary(result: &ValidationResult) -> String {
    match result {
        Ok((_, Ok(s))) => format!("parsed ok (with {} states)", s.states.len()),
        Ok((_, Err(e))) => format!("validation error: {e}"),
        Err(e) => format!("parsing error: {e}"),
    }
}

#[derive(Debug)]
pub struct States{
    pub states: BTreeMap<String, State>,
    pub first_state: String,
    pub ea_representation: Option<String>,
}

#[derive(Debug)]
pub struct State {
    //name: String,
    pub events: Vec<Event>,
    pub dead_events: Vec<Event>,
    pub death_events: Vec<Event>,
    pub respawn_events: Vec<Event>,
}

#[derive(Debug)]
pub struct Event {
    pub name: String,
    pub actions: Vec<Action>,
    pub conditions: Vec<Condition>,
    pub goto_state: Option<String>,
}

#[derive(Debug)]
pub struct Action {
    pub name: String,
    pub function: String,
    pub execute: String,
    pub params: Vec<ParamValue>,
}

#[derive(Debug)]
pub enum Condition {
    Single{
        name: String,
        function: String,
        execute: String,
        params: Vec<ParamValue>,
    },
    OR(Vec<Condition>),
}

pub enum ParamValue { String(String), Number(f64), Bool(bool) }

impl std::fmt::Debug for ParamValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::String(s) => std::fmt::Debug::fmt(s, f),
            Self::Bool(b) => std::fmt::Debug::fmt(b, f),
            Self::Number(n) => std::fmt::Debug::fmt(n, f),
        }
    }
}

#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum EntityType {
    None = 1,
    Figure = 2,
    Building = 3,
    Object = 4,
    ScriptGroup = 5,
    Squad = 6,
    Player = 7,
    World = 8,
    BarrierModule = 9,
    BarrierSet = 10,
    PowerSlot = 11,
    TokenSlot = 12,
    AbilityWorldObject = 13,
    StartingPoint = 25,
    Effect = 28,
}

pub fn validate_script_list(name: &str, file: &str) -> Result<Vec<String>, Error> {
    let lua = Lua::new();
    lua.context(|state|{
        state.load(file.as_bytes())
             .set_name(name.as_bytes())?
            .eval::<Vec<String>>()
    })
}

pub fn validate_script_groups(name: &str, file: &str) -> Result<BTreeMap<String, Vec<String>>, Error> {
    let lua = Lua::new();
    let mut groups = BTreeMap::new();
    lua.context(|state|{
        let table = state.load(file.as_bytes())
             .set_name(name.as_bytes())?
            .eval::<Table>()?;
        for pair in table.pairs::<String, Table>() {
            let (name, t) = pair?;
            let mut tags = Vec::with_capacity(t.len()? as usize);
            for pair in t.pairs::<Number, String>() {
                tags.push(pair?.1)
            }
            groups.insert(name, tags);
        }
        Ok(groups)
    })
}

pub fn validate_spawn_groups(name: &str, file: &str, global_variables: Option<(&str, &str)>) -> SpawnGroupsValidationResult {
    let lua = unsafe{Lua::new_with_debug()};
    global_vars(&lua, name, 3, EntityType::World)?; // TODO where to get difficulty?
    load_before_user_files(&lua)?;
    lua.context(|state|{
        if let Some((name, file)) = global_variables {
            state.load(file.as_bytes())
                 .set_name(name.as_bytes())?
                .exec()?;
        }
        state.load(file.as_bytes())
             .set_name(name.as_bytes())?
            .exec()
    })?;
    load_after_user_files(&lua)?;
    let spawn_templates = lua.context(|c|{
        for pair in c.globals().pairs::<Value, Value>() {
            let (key, value) = pair.unwrap();
            match key {
                Value::String(s) => {
                    if s.to_str().unwrap() == "AllSpawnWaveEmitters" {
                        match value {
                            Value::Table(t) => {
                                let mut templates = Vec::with_capacity(t.len()? as usize);
                                let pairs = t.pairs::<String, Value>();
                                for pair in pairs {
                                    let (name, _) = pair?;
                                    templates.push(name);
                                }
                                return Ok(templates)
                            }
                            Value::Nil => {
                                todo!("Handle no spawn template case")
                            }
                            _ => unreachable!()
                        }
                    }
                }
                _ => {}
            }
        }
        unreachable!()
    });
    Ok((lua, spawn_templates))
}
pub fn validate(name: &str, file: &str, global_variables: Option<(&str, &str)>, entity_type: EntityType) -> ValidationResult {
    let lua = unsafe{Lua::new_with_debug()};
    global_vars(&lua, name, 3, entity_type)?; // TODO where to get difficulty?
    load_before_user_files(&lua)?;
    if let Some((name, file)) = global_variables {
        lua.context(|state|{
                state.load(file.as_bytes())
                     .set_name(name.as_bytes())?
                    .exec()
        }).with_context(|| name.to_owned())?;
    }
    let states = validate_with_lua(&lua, name, file);
    Ok((lua, states))
}
pub fn validate_spawn_group(spawn_groups: &str, name: &str, file: &str, global_variables: Option<(&str, &str)>, entity_type: EntityType) -> ValidationResult {
    let lua = unsafe{Lua::new_with_debug()};
    global_vars(&lua, name, 3, entity_type)?; // TODO where to get difficulty?
    load_before_user_files(&lua)?;
    if let Some((name, file)) = global_variables {
        lua.context(|state|{
            state.load(file.as_bytes())
                 .set_name(name.as_bytes())?
                .exec()
        }).with_context(|| name.to_owned())?;
    }
    lua.context(|state|{
        state.load(spawn_groups.as_bytes())
             .set_name(b"_scriptgroups.lua")?
            .exec()
    }).context("_scriptgroups.lua")?;
    let states = validate_with_lua(&lua, name, file);
    Ok((lua, states))
}

pub fn validate_with_lua(lua: &Lua, name: &str, file: &str) -> anyhow::Result<States> {
    // I do not think I want it as an config option, because it is so huge
    const INCLUDE_EAS_REPRESENTATION : bool = false;
    trace!("LUA run {name}");
    if !file.is_empty() {
        lua.context(|state|{
            state.globals().set("INCLUDE_EAS_REPRESENTATION", Value::Boolean(INCLUDE_EAS_REPRESENTATION))?;
            state.load(file.as_bytes())
                .set_name(name.as_bytes())?
                .exec()
        })?;
    }
    trace!("LUA map scrip finished");
    load_after_user_files(&lua)?;
    trace!("LUA states generated");
    lua.context(|c|{
        let mut ea_representation : Option<String> = None;
        let mut state_machine: Option<anyhow::Result<States>> = None;
        for pair in c.globals().pairs::<Value, Value>() {
            let (key, value) = pair.unwrap();
            match key {
                Value::String(s) => {
                    if INCLUDE_EAS_REPRESENTATION && s.to_str().unwrap() == "EAsRepresentationAsString" {
                        let ea_str = match value {
                            Value::String(s) => {
                                String::from_utf8(s.as_bytes().to_vec()).unwrap()
                            }
                            _ => unreachable!()
                        };
                        if let Some(state_machine) = state_machine {
                            return state_machine.map(|s| States{ ea_representation: Some(ea_str), .. s })
                        }
                        ea_representation = Some(ea_str)
                    } else if s.to_str().unwrap() == "tStateMachine" {
                        trace!("LUA parse states");
                        let start = std::time::Instant::now();
                        let states = States::from_value(value).context("Parsing state machine failed");
                        trace!("LUA parsing states took: {:?}", start.elapsed());
                        if ea_representation.is_some() || !INCLUDE_EAS_REPRESENTATION {
                            return states.map(|s| States{ ea_representation, .. s })
                        }
                        state_machine = Some(states)
                    }
                }
                _ => {}
            }
        }
        unreachable!()
    })
}

fn global_vars(lua: &Lua, name: &str, difficulty: u8, entity_type: EntityType) -> anyhow::Result<()> {
    lua.context(|c|{
        let print = c.create_function(|_, string: String| {
            log::debug!("LUA: {string}");
            Ok(())
        })?;
        c.globals().set("print", print)?;

        let dofile = c.create_function(|context, string: String| {
            let lowercase = string.to_lowercase();
            for (name, bytes) in [
                ("wp_attackandpatrolarea.lua", include_str!("lua/warfarePatterns/WP_AttackAndPatrolArea.lua")),
                ("wp_respawn.lua", include_str!("lua/warfarePatterns/WP_Respawn.lua")),
                ("wp_spawnwave.lua", include_str!("lua/warfarePatterns/WP_SpawnWave.lua")),
            ] {
                if lowercase.contains(name) {
                    return context.load(bytes.as_bytes())
                        .set_name(string.as_bytes())?
                        .exec()
                }
            }
            log::error!("Unhandle script load: {}", string);
            Ok(())
        })?;
        c.globals().set("dofileWP", dofile)?;

        c.globals().set("MissionDifficulty", Value::Number(difficulty as f64))?;
        c.globals().set("CScriptMain_Entity_Type", Value::Number(entity_type as u8 as f64))?;
        c.globals().set("CScriptMain_Script_File", c.create_string(name.as_bytes())?)?;
        Ok::<(), rlua::prelude::LuaError>(())
    }).context("global variables")?;
    Ok(())
}

impl States {
    pub const NO_STATES_ERROR : &'static str = "No state, this is not an error, but probably not intended";

    pub fn from_value(v: Value) -> anyhow::Result<Self> {
        let states = match v {
            Value::Table(t) => {
                let raw_states : Table = t.get_with_context("tStates")?;
                let mut states = BTreeMap::new();
                let mut first_state = None;
                for pair in raw_states.pairs::<Value, Table>() {
                    let (_, s) = pair?;
                    let name : String = s.get_with_context("StateName")?;
                    if first_state.is_none() {
                        first_state = Some(name.clone());
                    }
                    let raw_events : Table = s.get_with_context("tEvents")?;
                    let mut events = Vec::with_capacity(raw_events.len()?.unsigned_abs());
                    for pair in raw_events.pairs::<Value, Value>() {
                        let (_, e) = pair?;
                        events.push(Event::from_value(e).with_context(|| format!("State {name}"))?)
                    }
                    let raw_events : Table = s.get_with_context("tDeadEvents")?;
                    let mut dead_events = Vec::with_capacity(raw_events.len()?.unsigned_abs());
                    for pair in raw_events.pairs::<Value, Value>() {
                        let (_, e) = pair?;
                        dead_events.push(Event::from_value(e).with_context(|| format!("State {name}"))?)
                    }
                    let raw_events : Table = s.get_with_context("tDeathEvents")?;
                    let mut death_events = Vec::with_capacity(raw_events.len()?.unsigned_abs());
                    for pair in raw_events.pairs::<Value, Value>() {
                        let (_, e) = pair?;
                        death_events.push(Event::from_value(e).with_context(|| format!("State {name}"))?)
                    }
                    let raw_events : Table = s.get_with_context("tRespawnEvents")?;
                    let mut respawn_events = Vec::with_capacity(raw_events.len()?.unsigned_abs());
                    for pair in raw_events.pairs::<Value, Value>() {
                        let (_, e) = pair?;
                        respawn_events.push(Event::from_value(e).with_context(|| format!("State {name}"))?)
                    }
                    states.insert(name, State{ events, dead_events, death_events, respawn_events });
                }

                let first_state = first_state.ok_or(anyhow!(Self::NO_STATES_ERROR.to_string()))?;
                Self{states, first_state, ea_representation: None}
            }
            t => anyhow::bail!("State is not a table, but: {:?}", t),
        };

        states.validate_transitions().context("validate_transitions")?;
        Ok(states)
    }
    fn validate_transitions(&self) -> Result<(), Error> {
        for (_, s) in self.states.iter() {
            for e in s.events.iter().chain(s.dead_events.iter()).chain(s.death_events.iter()).chain(s.respawn_events.iter()) {
                if let Some(next) = &e.goto_state {
                    if next == "self" {
                        continue
                    }
                    if !self.states.contains_key(next) && !next.ends_with(":DEAD") { // dead is final state, that should be generated
                        return Err(Error::RuntimeError(format!("State '{next}' not found")))
                    }
                }
            }
        }
        Ok(())
    }
}

impl Event {
    pub fn from_value(v: Value) -> anyhow::Result<Self> {
        match v {
            Value::Table(t) => {
                let name : String = t.get_with_context("sName")?;
                let goto_state : anyhow::Result<String> = t.get_with_context("GotoState");
                let goto_state = goto_state.ok();
                let raw_actions : Table = t.get_with_context("Actions")?;
                let mut actions = Vec::with_capacity(raw_actions.len()?.unsigned_abs());
                for pair in raw_actions.pairs::<Value, Value>() {
                    let (_, a) = pair?;
                    actions.push(Action::from_value(a).with_context(|| format!("Event {name}"))?);
                }
                let raw_conditions : Table = t.get_with_context("Conditions")?;
                let mut conditions = Vec::with_capacity(raw_conditions.len()?.unsigned_abs());
                for pair in raw_conditions.pairs::<Value, Value>() {
                    let (_, a) = pair?;
                    conditions.push(Condition::from_value(a).with_context(|| format!("Event {name}"))?);
                }
                Ok(Self{ name, actions, conditions, goto_state })
            }
            t => anyhow::bail!("Event is not a table, but: {:?}", t)
        }
    }
}

impl Action {
    pub fn from_value(v: Value) -> anyhow::Result<Self> {
        match v {
            Value::Table(t) => {
                let name : String = t.get_with_context("sName")?;
                let function : String = t.get_with_context("sFunction")?;
                let execute : String = t.get_with_context("sExecute")?;
                let raw_params : Table = t.get_with_context("ParamsArray")?;
                let mut params = Vec::with_capacity(raw_params.len()?.unsigned_abs());
                for pair in raw_params.pairs::<Value, Value>() {
                    let (_, p) = pair?;
                    match p {
                        Value::String(s) => params.push(ParamValue::String(s.to_str()?.to_string())),
                        Value::Boolean(b) => params.push(ParamValue::Bool(b)),
                        Value::Number(n) => params.push(ParamValue::Number(n)),
                        _ => anyhow::bail!("Unexpected type: {p:?}"),
                    }
                }
                Ok(Self{ name, function, execute, params })
            }
            t => anyhow::bail!("Action is not a table, but: {:?}", t)
        }
    }
}

impl Condition {
    pub fn from_value(v: Value) -> anyhow::Result<Self> {
        match v {
            Value::Table(t) => {
                let negated : bool = t.get_with_context("bNegated")?;
                let name : String = if negated { t.get_with_context("sNegated")? } else { t.get_with_context("sName")? };
                match name.as_str() {
                    "OR" => {
                        let logical_conditions : Table = t.get_with_context("tLogicalConditions")?;
                        let mut inner = Vec::with_capacity(logical_conditions.len()?.unsigned_abs());
                        for pair in logical_conditions.pairs::<Value, Value>() {
                            let (k, c) = pair?;
                            match k {
                                Value::Number(_) => {},
                                _ => continue,// skip any additional entries, for example `UpdateInterval` is between conditions
                            }
                            inner.push(Condition::from_value(c).context("OR inner condition")?);
                        }
                        Ok(Condition::OR(inner))
                    }
                    _ => {
                        let function : String = t.get_with_context("sFunction")?;
                        let execute : String = t.get_with_context("sExecute")?;
                        let raw_params : Table = t.get_with_context("ParamsArray")?;
                        let mut params = Vec::with_capacity(raw_params.len()?.unsigned_abs());
                        for pair in raw_params.pairs::<Value, Value>() {
                            let (_, p) = pair?;
                            match p {
                                Value::String(s) => params.push(ParamValue::String(s.to_str()?.to_string())),
                                Value::Boolean(b) => params.push(ParamValue::Bool(b)),
                                Value::Number(n) => params.push(ParamValue::Number(n)),
                                _ => anyhow::bail!("Unexpected type: {p:?}"),
                            }
                        }
                        Ok(Self::Single{ name, function, execute, params })
                    }
                }
            }
            t => anyhow::bail!("Condition is not a table, but: {:?}", t)
        }
    }
}

#[allow(dead_code)]
fn debug_print_table(table: Table) {
    fn debug_print_table(table: Table, indent: &str) -> String {
        let mut s = String::new();
        for pair in table.pairs::<Value, Value>() {
            let (k, v) = pair.unwrap();
            let k = match k {
                Value::Nil => "`nill`".to_string(),
                Value::Boolean(b) => b.to_string(),
                Value::LightUserData(_) => "`LightUserData`".to_string(),
                Value::Number(n) => format!("Number({n})"),
                Value::String(s) => s.to_str().unwrap().to_string(),
                Value::Table(_) => unreachable!("Table a key of table does not make much sense"),
                Value::Function(_) => "`function`".to_string(),
                Value::Thread(_) => "`thread`".to_string(),
                Value::UserData(_) => "`UserData`".to_string(),
                Value::Error(e) => format!("Error({e})"),
            };
            let v = match v {
                Value::Nil => "`nill`".to_string(),
                Value::Boolean(b) => b.to_string(),
                Value::LightUserData(_) => "`LightUserData`".to_string(),
                Value::Number(n) => format!("Number({n})"),
                Value::String(s) => s.to_str().unwrap().to_string(),
                Value::Table(t) => format!("TABLE\n{}", debug_print_table(t, &format!("{indent}\t"))),
                Value::Function(_) => "`function`".to_string(),
                Value::Thread(_) => "`thread`".to_string(),
                Value::UserData(_) => "`UserData`".to_string(),
                Value::Error(e) => format!("Error({e})"),
            };
            s.push_str(&format!("{indent}{k}: {v}\n"));
        }
        s
    }
    println!("{}", debug_print_table(table, ""))
}

trait GetWithContext<'lua> {
    fn get_with_context<T: rlua::FromLua<'lua> + Clone>(&self, key: &'static str) -> anyhow::Result<T>;
}

impl<'lua> GetWithContext<'lua> for Table<'lua> {
    fn get_with_context<T: rlua::FromLua<'lua> + Clone>(&self, key: &'static str) -> anyhow::Result<T> {
        self.get(key).context(key)
    }
}


macro_rules! include_file {
    ($file:literal) => {
        (std::borrow::Cow::Borrowed(&$file[4..]), std::borrow::Cow::Borrowed(include_str!($file)))
    };
    ($name:literal, $file:literal) => {
        (std::borrow::Cow::Borrowed($name), std::borrow::Cow::Borrowed(include_str!($file)))
    };
}

fn load_before_user_files(lua: &Lua) -> anyhow::Result<()> {
    for (name, text) in [
        include_file!("lua/cpp.lua"),
        include_file!("lua/GdsBase_0.lua"),
        include_file!("lua/tool_lua5.lua"),
        include_file!("lua/GdsDebug.lua"),
        include_file!("lua/GdsData.lua"),
        include_file!("lua/GdsApiWrappers.lua"),
        include_file!("lua/GdsStateMachineSystem.lua"),
        include_file!("lua/GdsStateSystem.lua"),
        include_file!("lua/GdsEventSystem.lua"),
        include_file!("lua/GdsActionConditionSystem.lua"),
        include_file!("lua/GdsWarfarePatternSystem.lua"),
        include_file!("lua/GdsSpawnGroupSystem.lua"),
        include_file!("lua/GdsParamSystem.lua"),
        include_file!("lua/GdsUnitTestSystem.lua"),
        include_file!("lua/definitions/GdsParams.lua"),
        include_file!("lua/definitions/GdsDbExport.lua"),
        include_file!("lua/definitions/GdsStates.lua"), // SR official
        include_file!("lua/definitions/GdsEvents.lua"),
        include_file!("lua/cpp_missing.lua"),
        include_file!("lua/definitions/GdsAutoGenerated.lua"),
        include_file!("lua/definitions/GdsNetworkVariables.lua"),
        include_file!("lua/warfarePatterns/WP.lua"),
        include_file!("lua/GdsBase_1.lua"),
    ] {
        lua.context(|state|{
            state.load(text.as_bytes())
                 .set_name(name.as_bytes())?
                .exec()
        }).with_context(|| format!("While parsing: {name}"))?;
    }
    Ok(())
}

fn load_after_user_files(lua: &Lua) -> anyhow::Result<()> {
    for (name, text) in [
        include_file!("lua/GdsBase_2.lua"),
    ] {
        lua.context(|state|{
            state.load(text.as_bytes())
                 .set_name(name.as_bytes())?
                .exec()
        }).with_context(|| format!("While parsing: {name}"))?;
    }
    Ok(())
}
