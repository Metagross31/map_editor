function UnitTest(_ARG_0_)
  for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_) do
    if _FORV_7_.TestSetup then
      for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
        table.insert({}, _FORV_12_)
      end
    elseif _FORV_7_.AssertTrue or _FORV_7_.AssertFalse then
      table.insert({}, _FORV_7_)
    end
  end
  State({
    StateName = GetScriptTag() .. "_WaitForStart",
    OnOneTimeEvent({
      EventName = "UnitTest" .. _UPVALUE0_ .. "_StartDelay",
      Conditions = {
        MapTimerIsNotElapsed({
          Name = "mt_UnitTestWaitTimer",
          Seconds = UnitTestStartDelay or 5
        })
      },
      Actions = {
        MapTimerStart({
          Name = "mt_UnitTestWaitTimer"
        })
      }
    }),
    OnEvent({
      EventName = "UnitTest" .. _UPVALUE0_ .. "_TestSetupAndTestStart",
      Conditions = {
        MapTimerIsElapsed({
          Name = "mt_UnitTestWaitTimer",
          Seconds = UnitTestStartDelay or 5
        }),
        unpack(_UPVALUE1_)
      },
      Actions = {},
      GotoState = "UnitTest" .. _UPVALUE0_
    })
  })
  for _FORV_7_, _FORV_8_ in ipairs({}) do
    if _FORV_8_.AssertTrue or not _FORV_8_.AssertFalse then
    end
    for _FORV_15_, _FORV_16_ in ipairs(_FORV_8_) do
      table.insert({""}, CreateParamString(_FORV_16_) or "RELEASE BUILDS DO NOT FULLY SUPPORT UNIT TESTS")
    end
    if (_FORV_8_.AssertTrue or not _FORV_8_.AssertFalse) == true then
      table.insert({}, MapFlagSetTrue({
        Name = "mf_" .. GetScriptTag() .. "_" .. _UPVALUE0_ .. "_Success"
      }))
      table.insert({}, MapValueIncrease({
        Name = "mv_" .. GetScriptTag() .. "_SuccessCounter"
      }))
      table.insert({}, MapFlagSetTrue({
        Name = "mf_" .. GetScriptTag() .. "_" .. _UPVALUE0_ .. "_FAILED"
      }))
      table.insert({}, MapValueIncrease({
        Name = "mv_" .. GetScriptTag() .. "_FailureCounter"
      }))
      table.insert({}, DebugErrorMessage({
        Message = GetScriptTag() .. " Test #" .. _UPVALUE0_ .. " failed!" .. [[


Assert]] .. "True" .. [[

{]] .. table.concat({""}, [[

    ]]) .. [[

}]]
      }))
      sAssertThisTest = "True" .. "_Success"
      sAssertThisFallThrough = "True" .. "_FAILED"
    else
      table.insert({}, MapFlagSetTrue({
        Name = "mf_" .. GetScriptTag() .. "_" .. _UPVALUE0_ .. "_FAILED"
      }))
      table.insert({}, MapValueIncrease({
        Name = "mv_" .. GetScriptTag() .. "_FailureCounter"
      }))
      table.insert({}, MapFlagSetTrue({
        Name = "mf_" .. GetScriptTag() .. "_" .. _UPVALUE0_ .. "_Success"
      }))
      table.insert({}, MapValueIncrease({
        Name = "mv_" .. GetScriptTag() .. "_SuccessCounter"
      }))
      table.insert({}, DebugErrorMessage({
        Message = GetScriptTag() .. " Test #" .. _UPVALUE0_ .. " failed!" .. [[


Assert]] .. "True" .. [[

{]] .. table.concat({""}, [[

    ]]) .. [[

}]]
      }))
      sAssertThisTest = "True" .. "_FAILED"
      sAssertThisFallThrough = "True" .. "_Success"
    end
    OnEvent({
      EventName = "UnitTest" .. _UPVALUE0_ .. "_Assert" .. sAssertThisTest .. table.concat({""}, [[

    ]]),
      Conditions = {
        unpack(_FORV_8_)
      },
      Actions = {},
      GotoState = "UnitTest" .. _UPVALUE0_ + 1
    })
    OnEvent({
      EventName = "UnitTest" .. _UPVALUE0_ .. "_Assert" .. sAssertThisFallThrough .. table.concat({""}, [[

    ]]),
      Conditions = {},
      Actions = {},
      GotoState = "UnitTest" .. _UPVALUE0_ + 1
    })
    State({
      StateName = "UnitTest" .. _UPVALUE0_
    })
    _UPVALUE0_ = _UPVALUE0_ + 1
  end
end
function TestEnd(_ARG_0_)
  table.insert(_UPVALUE0_, MapFlagIsTrue({
    Name = "mf_UnitTests_" .. GetScriptTag() .. "_Finished"
  }))
  table.insert(_UPVALUE1_, GetScriptTag())
  State({
    StateName = "UnitTest" .. _UPVALUE2_,
    OnOneTimeEvent({
      EventName = "UnitTests_" .. GetScriptTag() .. "_Finished",
      Conditions = {},
      Actions = {
        MapFlagSetTrue({
          Name = "mf_UnitTests_" .. GetScriptTag() .. "_Finished"
        })
      }
    })
  })
  _UPVALUE2_ = 1
end
function TestSummary(_ARG_0_)
  if not _UPVALUE0_ == 1 then
    TestEnd({})
  end
  State({
    StateName = GetScriptTag() .. "_WaitForTestsFinished",
    OnOneTimeEvent({
      EventName = "AllTestsFinished",
      Conditions = {
        unpack(_UPVALUE1_)
      },
      Actions = {
        DebugInfoMessage({
          Message = [[
All Unit Tests have finished. If there were no error messages then the tests were successful.

These test scripts have run:
	]] .. table.concat(_UPVALUE2_, [[
.lua
	]])
        })
      }
    })
  })
end
function TestSetup(_ARG_0_)
  _ARG_0_.TestSetup = true
  return _ARG_0_
end
function AssertTrue(_ARG_0_)
  _ARG_0_.AssertTrue = true
  return _ARG_0_
end
function AssertFalse(_ARG_0_)
  _ARG_0_.AssertFalse = true
  return _ARG_0_
end
