function string.identical(_ARG_0_, _ARG_1_)
  assert(type(_ARG_0_) == "string", "ERROR: string.identical(): first parameter is not a string!")
  assert(type(_ARG_1_) == "string", "ERROR: string.identical(): second parameter is not a string!")
  if string.len(_ARG_0_) ~= string.len(_ARG_1_) then
    return false
  end
  if string.find(_ARG_0_, _ARG_1_, 1, 1) ~= 1 then
    return false
  end
  return true
end
function string.capitalize(_ARG_0_)
  return string.upper(string.sub(_ARG_0_, 1, 1)) .. strlower(string.sub(_ARG_0_, 2))
end
function string.join(_ARG_0_, _ARG_1_)
  if table.getn(_ARG_1_) == 0 then
    return ""
  end
  for _FORV_7_ = 2, table.getn(_ARG_1_) do
  end
  return _ARG_1_[1] .. _ARG_0_ .. _ARG_1_[_FORV_7_]
end
function string.split(_ARG_0_, _ARG_1_)
  if string.find("", _ARG_0_, 1) then
    error("delimiter matches empty string!")
  end
  while true do
    if string.find(_ARG_1_, _ARG_0_, 1) then
      table.insert({}, string.sub(_ARG_1_, 1, string.find(_ARG_1_, _ARG_0_, 1) - 1))
    else
      table.insert({}, string.sub(_ARG_1_, string.find(_ARG_1_, _ARG_0_, 1) + 1))
      break
    end
  end
  return {}
end
function findfile(_ARG_0_)
  if io.open(_ARG_0_, "r") then
    io.close(io.open(_ARG_0_, "r"))
    return true
  end
  return nil, io.open(_ARG_0_, "r")
end
function isreadonly(_ARG_0_)
  if findfile(_ARG_0_) and io and io.open then
    if io.open(_ARG_0_, "w") then
      io.close((io.open(_ARG_0_, "w")))
      return false
    end
    return true
  end
end
if not old_dofile then
  old_dofile = dofile
  function dofile(_ARG_0_)
    if SCRIPTSYSTEM then
      if pcall(old_dofile, _ARG_0_) then
        return pcall(old_dofile, _ARG_0_)
      end
      if DEBUG then
        CScriptMain.ShowMessage("dofile ERROR: " .. tostring(pcall(old_dofile, _ARG_0_)) .. [[


]] .. debug.traceback())
      end
    else
      if not findfile(_ARG_0_) then
        assert(nil, "ERROR: dofile(" .. tostring(_ARG_0_) .. [[
): file not found!

]] .. debug.traceback())
      end
      return old_dofile(_ARG_0_)
    end
  end
end
function readfile(_ARG_0_)
  if not findfile(_ARG_0_) then
    assert(nil, "readfile(): file not found: " .. tostring(_ARG_0_))
  end
  io.close((io.open(_ARG_0_, "rt")))
  return (io.open(_ARG_0_, "rt"):read("*a"))
end
function writefile(_ARG_0_, _ARG_1_)
  io.open(_ARG_0_, "wt"):write(_ARG_1_)
  io.close((io.open(_ARG_0_, "wt")))
end
function appendfile(_ARG_0_, _ARG_1_)
  io.open(_ARG_0_, "a+"):write(_ARG_1_)
  io.close((io.open(_ARG_0_, "a+")))
end
function table.find(_ARG_0_, _ARG_1_)
  return table.foreachi(_ARG_0_, function(_ARG_0_, _ARG_1_)
    if _ARG_1_ == _UPVALUE0_ then
      return _ARG_0_
    end
  end)
end
function table.add(_ARG_0_, _ARG_1_)
  if not table.find(_ARG_0_, _ARG_1_) then
    table.insert(_ARG_0_, _ARG_1_)
  end
end
--[[
function table.copy(_ARG_0_, _ARG_1_)
  assert(type(_ARG_0_) == "table")
  for _FORV_6_, _FORV_7_ in pairs(_ARG_0_) do
    if type(_FORV_7_) ~= "table" then
    else
      _ARG_1_[_ARG_0_], _ARG_1_ = {
        [_FORV_6_] = _FORV_7_,
        [_FORV_6_] = _ARG_1_[_FORV_7_],
        [_FORV_6_] = table.copy(_FORV_7_, _ARG_1_)
      }, _ARG_1_ or {}
      if _ARG_1_[_FORV_7_] then
      else
      end
    end
  end
  return {
    [_FORV_6_] = _FORV_7_,
    [_FORV_6_] = _ARG_1_[_FORV_7_],
    [_FORV_6_] = table.copy(_FORV_7_, _ARG_1_)
  }
end ]]
function table.copy(t)
  local t2 = {};
  for k,v in pairs(t) do
    if type(v) == "table" then
        t2[k] = table.copy(v);
    else
        t2[k] = v;
    end
  end
  return t2;
end



function table.keys(_ARG_0_)
  if not _ARG_0_ or type(_ARG_0_) ~= "table" then
    return {}
  end
  for _FORV_6_, _FORV_7_ in pairs(_ARG_0_) do
  end
  return {
    [1] = _FORV_6_
  }
end
if not SCRIPTSYSTEM then --[[
  function os.execute(...)
    if nil.n == 1 then
    else
      for _FORV_5_ = 1, nil.n do
        nil[_FORV_5_] = tostring(nil[_FORV_5_])
        if string.find(nil[_FORV_5_], "s") and not string.find(nil[_FORV_5_], "\"") then
        else
        end
      end
    end
    if execute_echo then
      print("> " .. _FOR_.sub((nil[1] .. " \"" .. nil[_FORV_5_] .. "\"") .. " " .. nil[_FORV_5_], 2))
    end
    return _UPVALUE0_(" " .. _FOR_.sub((nil[1] .. " \"" .. nil[_FORV_5_] .. "\"") .. " " .. nil[_FORV_5_], 2))
  end ]]
end

function dostring(_ARG_0_)
  assert(type(_ARG_0_) == "string", "dostring(): chunk is not a string")
  local loaded = loadstring(_ARG_0_)
  if not loaded then
    assert(nil, "dostring(_sChunk): loadstring(\"" .. _ARG_0_ .. "\") returned nil")
  end
  return loaded()
end
function WriteTableRecursive(tableToWrite, indent, formatBoolOption)
  table.sort(table.keys(tableToWrite), function(_ARG_0_, _ARG_1_)
    if type(_ARG_0_) == "number" and type(_ARG_1_) ~= "number" then
    elseif type(_ARG_0_) ~= "number" and type(_ARG_1_) == "number" then
    else
    end
    return _ARG_0_ < _ARG_1_
  end)
  if indent == nil then
    indent = 2
  end
  local out = ""
  local stringIndent = string.rep(" ", indent)
  for key, value in pairs(tableToWrite) do
    if type(value) == "table" then
      if type(key) == "string" then
        if formatBoolOption then
          out = out .. stringIndent .. key .. " =\n" .. stringIndent .. "{\n"
        else
          out = out .. stringIndent .. "[\"" .. key .. "\"] =\n" .. stringIndent .. "{\n"
        end
      else
        out = out .. stringIndent .. "[" .. key .. "] =\n" .. stringIndent .. "{\n"
      end
      out = out .. WriteTableRecursive(value, indent + 2, formatBoolOption)
      out = out .. stringIndent .. "},\n"
    elseif type(value) == "string" then
      if type(key) == "string" then
        out = out .. stringIndent .. "" .. key .. "" .. " = " .. "\"" .. tostring(value) .. "\"" .. ",\n"
      else
        out = out .. stringIndent .. "[" .. key .. "]" .. " = " .. "\"" .. tostring(value) .. "\"" .. ",\n"
      end
    elseif type(key) == "string" then
      out = out .. stringIndent .. "" .. key .. "" .. " = " .. tostring(value) .. ",\n"
    else
      out = out .. stringIndent .. "[" .. key .. "]" .. " = " .. tostring(value) .. ",\n"
    end
  end
  return out
end
function WriteTableToString(tableToWrite, name)
    assert(tableToWrite, "WriteTableToFile: first parameter is nil, must be a table!")
    assert(type(tableToWrite) == "table", "WriteTableToFile: first parameter must be a table!")
    local out = ""
    if type(name) == "string" then
      out = out .. name .. "\n{\n"
    else
      out = out .. "return\n{\n"
    end
    out = out .. WriteTableRecursive(tableToWrite)
    out = out .. "}\n"
    return out
end
function WriteTableToFile(tableToWrite, filename, _ARG_2_, name)
  assert(filename, "WriteTableToFile: second parameter is nil, must be a string (filename)!")
  assert(type(filename) == "string", "WriteTableToFile: second parameter must be a string!")
  os.remove(filename)
  assert(io.open(filename, "w"))
  io.open(filename, "w"):write(WriteTableToString(tableToWrite, name))
  io.close(io.open(filename, "a+"))
end
