function DefineState(_ARG_0_)
  table.insert(tStateSpecs, _ARG_0_)
end
function RegisterStates()
  table.foreachi(tStateSpecs, function(_ARG_0_, _ARG_1_)
    RegisterStateFunction(_ARG_0_, _ARG_1_.sName)
  end)
end
function RegisterStateFunction(_ARG_0_, _ARG_1_)
  dostring((string.gsub(string.gsub([[
	function _sFuncName (_tParams)
		local _tCheckedParams = CheckParams('_sFuncName', tStateSpecs[_iIndex], _tParams)
		Create_sFuncName(_tCheckedParams)
	end
	]], "_sFuncName", _ARG_1_), "_iIndex", tostring(_ARG_0_))))
end
function VerifyUniqueStateName(_ARG_0_)
  --if DEBUG then
    assert(type(_ARG_0_.StateName) == "string", "StateName must be a string!")
    if StateExists(_ARG_0_.StateName) then
      assert(nil, "StateName '" .. _ARG_0_.StateName .. "' not unique!")
    end
  --end
end
function StateExists(_ARG_0_)
  table.foreachi(tStateMachine.tStates, function(_ARG_0_, _ARG_1_)
    if _ARG_1_.StateName == _UPVALUE0_ then
      return true
    end
  end)
  return false
end
function SaveState(_ARG_0_)
  _ARG_0_.tEvents = tEvents
  _ARG_0_.tDeadEvents = tDeadEvents
  _ARG_0_.tDeathEvents = tDeathEvents
  _ARG_0_.tRespawnEvents = tRespawnEvents
  table.insert(tStateMachine.tStates, _ARG_0_)
  ClearStateEventTables()
end
function CreateState(_ARG_0_)
  VerifyUniqueStateName(_ARG_0_)
  CreateRustState(_ARG_0_.StateName, tEvents, tDeadEvents, tDeathEvents, tRespawnEvents)
  SaveState(_ARG_0_)
end
function CreateOutcries(_ARG_0_)
  if DEBUG then
    assert(type(_ARG_0_.OutcryList) == "table", "CreateOutcries - param 'OutcryList' is missing or not a table!")
  end
  for _FORV_4_, _FORV_5_ in pairs(_ARG_0_.OutcryList) do
    _FORV_5_.MinDelay = nil
    if _FORV_5_.MinDelay and _FORV_5_.MinDelay > 0 then
      table.insert({
        MapFlagIsTrue({Name = _FORV_4_})
      }, OR({
        MapTimerIsElapsed({
          Name = "mt_" .. string.sub(_FORV_4_, 4) .. "_Delay" .. tostring(_FORV_5_.MinDelay),
          Seconds = _FORV_5_.MinDelay
        }),
        MapTimerIsNotRunning({
          Name = "mt_" .. string.sub(_FORV_4_, 4) .. "_Delay" .. tostring(_FORV_5_.MinDelay)
        })
      }))
      table.insert({
        MapFlagSetFalse({Name = _FORV_4_})
      }, MapTimerStart({
        Name = "mt_" .. string.sub(_FORV_4_, 4) .. "_Delay" .. tostring(_FORV_5_.MinDelay)
      }))
    end
    if 0 < table.getn(_FORV_5_) then
      for _FORV_12_, _FORV_13_ in ipairs(_FORV_5_) do
        _FORV_13_.MinDelay = nil
        table.insert({
          MapFlagSetFalse({Name = _FORV_4_})
        }, MissionOutcry(_FORV_13_))
      end
    else
      table.insert({
        MapFlagSetFalse({Name = _FORV_4_})
      }, MissionOutcry(_FORV_5_))
    end
    OnEvent({
      EventName = "OUTCRY: " .. _FORV_4_,
      Conditions = {
        MapFlagIsTrue({Name = _FORV_4_})
      },
      Actions = {
        MapFlagSetFalse({Name = _FORV_4_})
      }
    })
  end
  State({StateName = "Outcries"})
end
