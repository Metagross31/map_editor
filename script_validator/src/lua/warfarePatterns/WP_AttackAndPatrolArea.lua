DefineWarfarePattern({
  sName = sPatternName,
  sDesc = "Creates a Attack and Defend Area WP.",
  iCreateDate = GetDays(2008, 1, 15),
  ktScriptTagTargetParam,
  "PatrolPoints",
  "NumPatrolSquads",
  Pattern = function(_ARG_0_)
    WP_AddControlState(_ARG_0_)
    State({
      StateName = WP_GetStartStateName(),
      OnEvent({
        EventName = "Squads go to TargetTag: " .. _ARG_0_.TargetTag,
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Seconds = kDecideMedium
          }),
          SquadIdleIsNotInRange({
            For = "ALL",
            Tag = GetScriptTag(),
            TargetTag = _ARG_0_.TargetTag,
            Range = 16
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_UpdateInterval"
          }),
          SquadGridGoto({
            Tag = GetScriptTag(),
            TargetTag = _ARG_0_.TargetTag
          })
        },
        GotoState = "self"
      }),
      OnEvent({
        EventName = "Squads arrived at TargetTag: " .. _ARG_0_.TargetTag,
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Seconds = kDecideMedium
          }),
          SquadIdleIsInRange({
            For = "ALL",
            Tag = GetScriptTag(),
            TargetTag = _ARG_0_.TargetTag,
            Range = 16
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_UpdateInterval"
          })
        },
        GotoState = WP_GetStartStateName() .. "_DoPatrol"
      })
    })
    State({
      StateName = WP_GetStartStateName() .. "_DoPatrol",
      OnEvent({
        EventName = "SquadPatrol IdleReset",
        Conditions = {
          EntityTimerIsRunning({
            Name = "et_WP_IdleTimeOut_" .. GetScriptTag()
          }),
          SquadIsBusy({
            For = "ANY",
            Tag = GetScriptTag()
          })
        },
        Actions = {
          EntityTimerStop({
            Name = "et_WP_IdleTimeOut_" .. GetScriptTag()
          })
        },
        GotoState = "self"
      }),
      OnEvent({
        EventName = "SquadPatrol IdleCheck",
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Seconds = kDecideMedium
          }),
          EntityTimerIsNotRunning({
            Name = "et_WP_IdleTimeOut_" .. GetScriptTag()
          }),
          SquadIsIdle({
            For = "ALL",
            Tag = GetScriptTag()
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_IdleTimeOut_" .. GetScriptTag()
          })
        },
        GotoState = "self"
      }),
      OnEvent({
        EventName = "SquadPatrol: " .. _ARG_0_.PatrolPoints,
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_IdleTimeOut_" .. GetScriptTag(),
            Seconds = kDecideMedium
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_UpdateInterval"
          }),
          SquadPatrol({
            Tag = GetScriptTag(),
            TargetTag = _ARG_0_.PatrolPoints
          })
        },
        GotoState = "self"
      }),
      WP_AddStandardEvents(_ARG_0_)
    })
  end,
  WP_AddStandardParams()
})
