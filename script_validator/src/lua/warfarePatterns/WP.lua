function WP_AddStandardParams()
  return unpack({
    ktStateNameParam,
    ktStartConditionsParam,
    ktStartActionsParam,
    ktFinishActionsParam,
    ktExitConditionsParam,
    ktExitActionsParam,
    ktFinishStateParam,
    ktExitStateParam,
    ktDeathStateParam,
    ktAllowExitBeforeStartParam
  })
end
function RegisterPatterns(_ARG_0_)
  for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_) do
    sPatternName = _FORV_5_
    dofileWP(ksGdsWarfarePatternPath .. _FORV_5_ .. ".lua")
  end
end
kDecideFast = 1
kDecideMedium = 3
kDecideSlow = 6
kDecideVerySlow = 12
kDecideSeldom = 30
RegisterPatterns({
  "WP_AttackAndPatrolArea",
  "WP_Respawn",
  "WP_SpawnWave"
})
