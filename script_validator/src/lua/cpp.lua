

ScriptMain = {}
function ScriptMain:GetMapName()
    return "No_map"
end

CScriptMain = {}
function CScriptMain:CreateScriptGroup(name)
end
function CScriptMain:GdRandom(min, max)
    return min
end

CppStateMachine = {
    _FakeUserData = "CppStateMachine"
}

function CppStateMachine:AddState(state)
    --print("CppStateMachine:AddState: " .. tostring(state))
    local CppState = {
        _FakeUserData = "CppState"
    }

    function CppState:AddTransition(name, transition)
        --print("CppState:AddTransition: " .. tostring(name) .. " " .. type(transition))
        local CppAction = {
            _FakeUserData = "CppAction"
        }

        function CppAction:AddTransition(action)
            --print("CppAction:AddAction: " .. tostring(action))
          return 42
        end
      return CppAction
    end
    CppState.StateName = state.StateName
  return CppState
end




function CScriptMain.CreateStateMachine()
    return CppStateMachine
end

function CScriptMain.FileExists(name)
    -- todo actually check if file exists
    return 0
end

function CScriptMain.AddEntityToScriptGroup(_ARG_0_, _ARG_1_)

end

function CreateRustState(name, tEvents, tDeadEvents, tDeathEvents, tRespawnEvents)
    local events = {}
    if tEvents ~= nil then
        table.foreachi(tEvents, function(i, e)
            table.insert(events, prioritiseOriginal(e))
        end)
    end
    if tDeadEvents ~= nil then
        table.foreachi(tDeadEvents, function(i, e)
            table.insert(events, prioritiseOriginal(e))
        end)
    end
    if tDeathEvents ~= nil then
        table.foreachi(tDeathEvents, function(i, e)
            table.insert(events, prioritiseOriginal(e))
        end)
    end
    if tRespawnEvents ~= nil then
        table.foreachi(tRespawnEvents, function(i, e)
            table.insert(events, prioritiseOriginal(e))
        end)
    end

  --_State(name, events)
end

function prioritiseOriginal(o)
    local r = {}
    if type(o) == "table" then
        local userParams = o["tUserParams"]
        local c = 0
        if userParams ~= nil then
            table.foreach(o, function(i, inner)
                c = c + 1
            end)
        end
        if userParams ~= nil and c > 0 then
            r.__all__ = o
            r.__all__["tUserParams"] = nil
            table.foreach(userParams, function(i, inner)
                r[i] = prioritiseOriginal(inner)
            end)
        else
            table.foreach(o, function(i, inner)
                r[i] = prioritiseOriginal(inner)
            end)
        end
    else
        r = o
    end
    return r
end

local realPrint = print
function print(text)
    local info = debug.getinfo(2);
    return realPrint(info.short_src .. ":" .. info.linedefined .. ": " .. text)
end