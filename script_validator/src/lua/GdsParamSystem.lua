-- Decompiled using luadec 2.0.2 by sztupy (http://winmo.sztupy.hu)
-- Command line was: D:\lua\Decompiler+DecompiledScript\gdsparamsystem.lub 

GetValidParamsString = function(l_1_0, l_1_1)
  local l_1_2 = "\n\nValid params for " .. l_1_1 .. "{} are:"
  table.foreachi(l_1_0, function(l_1_0, l_1_1)
    if not l_1_1.bConstant then
      if l_1_1.sType == "table" then
        l_1_2 = l_1_2 .. "\n" .. l_1_1.sName .. " = { " .. l_1_1.sContentType .. " }"
      else
        l_1_2 = l_1_2 .. "\n" .. l_1_1.sName .. " = " .. l_1_1.sType
      end
      if l_1_1.bRequired == false then
        if type(l_1_1.DefaultValue) == "string" then
          l_1_2 = l_1_2 .. " (optional, default: \"" .. l_1_1.DefaultValue .. "\")"
        else
          l_1_2 = l_1_2 .. " (optional, default: " .. tostring(l_1_1.DefaultValue) .. ")"
        end
      end
    end
   end)
  return l_1_2
end

AssertNoIndexedParameters = function(params, l_2_1)
    local DEBUG = true
  if DEBUG then
    table.foreachi(params, function(_i, value)
    assert(nil, tostring(l_2_1) .. " - unnamed parameter detected!\n\nValue: " .. tostring(value) .. "\nType: " .. type(value) .. "\n")
   end)
  end
end

AssertFunctionSpec = function(l_3_0, l_3_1)
    local DEBUG = true
  if DEBUG and type(l_3_0) ~= "table" then
    assert(nil, "ERROR: " .. tostring(l_3_1) .. " not found in tActionConditionSpecs!\nMake sure the function is defined in GdsActions.lua, GdsConditions.lua or accompanying file!\n")
  end
end

AssertTable = function(l_4_0, l_4_1)
    local DEBUG = true
  if DEBUG and type(l_4_0) ~= "table" then
    assert(nil, "ERROR: " .. tostring(l_4_1) .. " parameter '" .. tostring(l_4_0) .. "' is not a table! Please check definition of this function!")
  end
end

AssertParamType = function(l_5_0, l_5_1, l_5_2, l_5_3)
    local DEBUG = true
  if DEBUG and type(l_5_0) ~= l_5_1 and (l_5_1 ~= "boolean" or l_5_0 == 0 or l_5_0 ~= 1) then
    assert(nil, "ERROR: " .. tostring(l_5_3) .. " parameter '" .. tostring(l_5_2) .. "' requires a '" .. l_5_1 .. "' but is a '" .. type(l_5_0) .. "'!")
  end
end

AssertEmptyStrings = function(l_6_0, l_6_1, l_6_2)
    local DEBUG = true
  if DEBUG and type(l_6_0) == "string" and l_6_0 == "" then
    assert(nil, "ERROR: " .. tostring(l_6_2) .. " parameter '" .. tostring(l_6_1) .. "' is an empty string!")
  end
end

AssertParamRequired = function(l_7_0, l_7_1, l_7_2)
    local DEBUG = true
  if DEBUG and l_7_0.bRequired then
    assert(nil, "ERROR: " .. tostring(l_7_2) .. " parameter '" .. tostring(l_7_1) .. "' is required but missing!")
  end
end

AssertMissingConstantValue = function(l_8_0, l_8_1, l_8_2)
    local DEBUG = true
  if DEBUG and l_8_0 == nil then
    assert(nil, "ERROR: " .. tostring(l_8_2) .. " parameter '" .. tostring(l_8_1) .. "' is a constant but the value is nil!")
  end
end

AssertMissingValue = function(l_9_0, l_9_1, l_9_2)
    local DEBUG = true
  if DEBUG and l_9_0 == nil then
    assert(nil, "ERROR: " .. tostring(l_9_2) .. " parameter '" .. tostring(l_9_1) .. "' is optional but has no default value set in its parameter definition!")
  end
end

AssertParamIsString = function(l_10_0, l_10_1, l_10_2)
    local DEBUG = true
  if DEBUG and type(l_10_0) ~= "string" then
    assert(nil, "ERROR: " .. tostring(l_10_2) .. " parameter '" .. tostring(l_10_1) .. "' is not a string but requires a Prefix!")
  end
end

AssertCorrectPrefix = function(l_11_0, l_11_1, l_11_2, l_11_3, l_11_4)
    local DEBUG = true
  if DEBUG and l_11_0 ~= l_11_1 then
    assert(nil, "ERROR: " .. tostring(l_11_4) .. " parameter '" .. tostring(l_11_3) .. "' requires prefix of '" .. l_11_1 .. "' but has a prefix of '" .. l_11_0 .. "'.\nThe complete parameter string is: '" .. l_11_2 .. "'.")
  end
end

AssertCorrectAvatarVarPrefix = function(l_12_0, l_12_1, l_12_2, l_12_3, l_12_4)
    local DEBUG = true
  if DEBUG then
    local l_12_5 = string.find(l_12_0, l_12_1)
    if l_12_5 ~= 1 then
      assert(nil, "ERROR: " .. tostring(l_12_4) .. " parameter '" .. tostring(l_12_3) .. "' requires a secondary prefix in this style '" .. l_12_1 .. "'.\nThe complete parameter string is: '" .. l_12_2 .. "'.")
    end
  end
end

AssertCorrectCharAfterPrefix = function(l_13_0, l_13_1, l_13_2, l_13_3, l_13_4)
    local DEBUG = true
  if DEBUG then
    local l_13_5 = string.gsub(l_13_0, l_13_1, "")
    if l_13_5 ~= "" then
      assert(nil, "ERROR: " .. tostring(l_13_4) .. " parameter '" .. tostring(l_13_3) .. "' requires any letter of '" .. l_13_1 .. "' as first character after the prefix - but it is a '" .. l_13_0 .. "'.\nThe complete parameter string is: '" .. l_13_2 .. "'.")
    end
  end
end

AssertCorrectCharactersInString = function(l_14_0, l_14_1, l_14_2, l_14_3, l_14_4)
    local DEBUG = true
  if DEBUG then
    local l_14_5 = string.gsub(l_14_0, l_14_1, "")
    if l_14_5 ~= "" then
      assert(nil, "ERROR: " .. tostring(l_14_4) .. " parameter '" .. tostring(l_14_3) .. "' allows only letters of '" .. l_14_1 .. "' but these invalid characters '" .. l_14_5 .. "' were found.\nCommon mistake: to use the 'xy_P123_' prefix in Entity or Map vars - the 'P123_' prefix must only be used for Avatar variables.\nThe complete parameter string is: '" .. l_14_2 .. "'.")
    end
  end
end

AssertIncorrectParameter = function(l_15_0, l_15_1, l_15_2, l_15_3)
    local DEBUG = true
  if DEBUG and not l_15_0 then
    assert(nil, tostring(l_15_2) .. " parameter '" .. tostring(l_15_1) .. "' not defined for this function! Could be a typo or definition error." .. GetValidParamsString(l_15_3, l_15_2))
  end
end

AssertTableContentTypesMatch = function(l_16_0, l_16_1, l_16_2)
    local DEBUG = true
  if DEBUG then
    table.foreachi(l_16_0, function(l_1_0, l_1_1)
    if type(l_1_1) == "table" and l_1_1.sType ~= l_16_1 then
      assert(nil, tostring(l_16_2) .. " table content types differ: '" .. tostring(l_16_1) .. "' expected, got '" .. tostring(l_1_1.sType) .. "' (" .. tostring(l_1_1.sName) .. ")")
    end
   end)
  end
end

AssertDefaultGetInPlatformScript = function(l_17_0, l_17_1)
    local DEBUG = true
  if DEBUG and IsPlatformScript() then
    if l_17_0.sDefaultValueGetFunction == "GetScriptTag()" then
      assert(nil, tostring(l_17_1) .. " missing 'Tag' parameter! In Platform scripts the 'Tag' parameter must be set explicitly.\nCalled from platform script: '" .. GetScriptTag() .. "'")
    elseif l_17_0.sDefaultValueGetFunction == "GetEntityX()" then
      assert(nil, tostring(l_17_1) .. " missing 'X' parameter! In Platform scripts the 'X' parameter must be set explicitly.\nCalled from platform script: '" .. GetScriptTag() .. "'")
    elseif l_17_0.sDefaultValueGetFunction == "GetEntityY()" then
      assert(nil, tostring(l_17_1) .. " missing 'Y' parameter! In Platform scripts the 'Y' parameter must be set explicitly.\nCalled from platform script: '" .. GetScriptTag() .. "'")
    elseif l_17_0.sDefaultValueGetFunction == "GetEntityDirection()" then
      assert(nil, tostring(l_17_1) .. " missing 'Direction' parameter! In Platform scripts the 'Direction' parameter must be set explicitly.\nCalled from platform script: '" .. GetScriptTag() .. "'")
    end
  end
end

AssertFilePathAndExtension = function(l_18_0, l_18_1, l_18_2)
  if l_18_1.sModifyExtension and string.find(l_18_0, l_18_1.sModifyExtension, 1, 1) then
    assert(nil, tostring(l_18_2) .. " found file extension '" .. l_18_1.sModifyExtension .. "'. Please remove extension from this function in script " .. GetScriptTag() .. ".")
  end
  if l_18_1.sModifyPath and l_18_1.sModifyPath ~= "" and string.find(string.upper(l_18_0), string.upper(l_18_1.sModifyPath), 1, 1) then
    assert(nil, tostring(l_18_2) .. " found file path '" .. l_18_1.sModifyPath .. "'. Please remove path from this function in script " .. GetScriptTag() .. ".")
  end
  if l_18_1.sModifyPathFunction then
    local l_18_3 = dostring("return " .. l_18_1.sModifyPathFunction)
    if string.find(string.upper(l_18_0), string.upper(l_18_3), 1, 1) then
      assert(nil, tostring(l_18_2) .. " found file path '" .. l_18_3 .. "'. Please remove path from this function in script " .. GetScriptTag() .. ".")
    end
  end
end

AssertQuantorValue = function(l_19_0, l_19_1, l_19_2, l_19_3)
    local DEBUG = true
  if DEBUG then
    local l_19_4 = assert
    l_19_4(type(l_19_1) == "number", tostring(l_19_3) .. " - unknown 'For' parameter named '" .. tostring(l_19_2) .. "'. ")
    l_19_4 = l_19_0.sType
    if l_19_4 == "action" then
      l_19_4 = assert
      l_19_4(l_19_1 == ALL, tostring(l_19_3) .. " - Actions only allow Quantor 'All' in 'For' parameter!")
    end
  end
end

AssertDoubleNegation = function(l_20_0, l_20_1, l_20_2, l_20_3)
    local DEBUG = true
  if DEBUG and l_20_2 and (l_20_1 == NotAll or l_20_1 == None) then
    local l_20_4 = "NotAll"
    if l_20_1 == None then
      l_20_4 = "None"
    end
    assert(nil, tostring(l_20_3) .. " {For = " .. l_20_4 .. "} - this is a double-negation!\nPlease replace  NotCondition{For = NotAll/None}  with it's non-negated counterpart (eg. NoSquadNotInRange >> AllSquadsInRange).\n\nNote: this script will still run as intended but you will get this message each and every time ...")
    VT(l_20_0)
  end
end

CheckParams = function(FuncName, Params, ParamValues, Negated)
    local DEBUG = true
    --print("CheckParams: FuncName: " .. tostring(FuncName))
    --print("CheckParams: Params: " .. tprint(Params))
    --print("CheckParams: ParamValues: " .. tprint(ParamValues))
    --print("CheckParams: Negated: " .. tostring(Negated))
  if DEBUG then
    assert(type(FuncName) == "string", "FuncName is not a string")
    assert(type(Params) == "table", "Missing Param Specs")
    if ParamValues == nil then
        ParamValues = {}
    end
    if type(ParamValues) ~= "table" then
        print(debug.traceback())
        print("CheckParams: ParamValues type: " .. type(ParamValues))
      assert(nil, FuncName .. ": parameter(s) not a table, please use { } brackets instead of ( )")
    end
  end
  local l_21_4 = {}
  AssertFunctionSpec(Params, FuncName)
  AssertNoIndexedParameters(ParamValues, FuncName)
  local Return = {}
  local Params0 = -1
  if tConfig.bUsageStatistics then
    if not Params.iCount then
      Params.iCount = 0
    end
    Params.iCount = Params.iCount + 1
  end
  table.foreachi(Params, function(l_1_0, l_1_1)
    AssertTable(l_1_1, FuncName)
    local sName = l_1_1.sName
    local resolvedParam = nil
    local sValue = ParamValues[sName]
    if type(sValue) == "string" and sValue == "default" then
      sValue = nil
    end
    if sValue ~= nil then
      AssertParamType(sValue, l_1_1.sType, sName, FuncName)
      if not l_1_1.bAllowEmptyString then
        AssertEmptyStrings(sValue, sName, FuncName)
      end
      if type(sValue) == "table" then
        resolvedParam = {}
        resolvedParam[sName] = sValue
        AssertTableContentTypesMatch(resolvedParam[sName], l_1_1.sContentType, FuncName)
      else
        resolvedParam = sValue
      end
    elseif l_1_1.bConstant then
      resolvedParam = l_1_1.Value
      if not resolvedParam and l_1_1.sDefaultValueGetFunction then
        resolvedParam = dostring("return " .. l_1_1.sDefaultValueGetFunction)
      end
      AssertMissingConstantValue(resolvedParam, sName, FuncName)
    else
      AssertParamRequired(l_1_1, sName, FuncName)
      resolvedParam = l_1_1.DefaultValue
      if l_1_1.sDefaultValueGetFunction then
        AssertDefaultGetInPlatformScript(l_1_1, FuncName)
        resolvedParam = dostring("return " .. l_1_1.sDefaultValueGetFunction)
      end
    end
    if not l_1_1.bEventList then
      AssertMissingValue(resolvedParam, sName, FuncName)
    end
    if l_1_1.bModifyValue then
      if l_1_1.bModifyToUpper then
        resolvedParam = string.upper(resolvedParam)
      end
      if l_1_1.bQuantor then
        local l_22_5 = _G[string.upper(resolvedParam)]
        AssertQuantorValue(Params, l_22_5, resolvedParam, FuncName)
        resolvedParam = l_22_5
        AssertDoubleNegation(Params, l_22_5, Negated, FuncName)
      elseif l_1_1.bPlayerQuantor then
        local l_22_6 = _G[string.upper(resolvedParam)]
        if type(l_22_6) == "number" then
          resolvedParam = "sg_AllPlayers"
        else
          l_22_6 = ALL
        end
        if Params.bCondition and l_22_6 then
          l_21_4[l_1_0 - 1] = l_22_6
        end
        AssertDoubleNegation(Params, l_22_6, Negated, FuncName)
      elseif l_1_1.iModifyFactor then
        resolvedParam = resolvedParam * l_1_1.iModifyFactor
      elseif l_1_1.iModifyAddValue and (resolvedParam ~= 0 or not l_1_1.bModifyOnlyIfNotZero) then
        resolvedParam = resolvedParam + l_1_1.iModifyAddValue
        do return end
        if l_1_1.sModifyEventFlag then
          resolvedParam = string.format(l_1_1.sModifyEventFlag, resolvedParam)
        elseif l_1_1.sModifyPath then
          AssertFilePathAndExtension(resolvedParam, l_1_1, FuncName)
          resolvedParam = string.format("%s%s%s", l_1_1.sModifyPath, resolvedParam, l_1_1.sModifyExtension or "")
        elseif l_1_1.sModifyPathFunction then
          AssertFilePathAndExtension(resolvedParam, l_1_1, FuncName)
          local l_22_7 = dostring("return " .. l_1_1.sModifyPathFunction)
          resolvedParam = string.format("%s%s%s", l_22_7, resolvedParam, l_1_1.sModifyExtension)
        elseif l_1_1.sModifyPrefix then
          resolvedParam = l_1_1.sModifyPrefix .. resolvedParam
        end
      end
    end
    if DEBUG and l_1_1.sRequiredPrefix then
      AssertParamIsString(resolvedParam, sName, FuncName)
      local l_22_8 = string.len(l_1_1.sRequiredPrefix)
      local l_22_9 = string.sub(resolvedParam, 1, l_22_8)
      local l_22_10 = string.sub(resolvedParam, l_22_8 + 1)
      if l_22_9 ~= ksInternalVariablesPrefix then
        AssertCorrectPrefix(l_22_9, l_1_1.sRequiredPrefix, resolvedParam, sName, FuncName)
        if l_1_1.sAvatarVarPrefix then
          local l_22_11 = l_22_8
          local l_22_12 = l_22_8 + string.len(l_1_1.sAvatarVarPrefix)
          AssertCorrectAvatarVarPrefix(l_22_10, l_1_1.sAvatarVarPrefix, resolvedParam, sName, FuncName)
          local l_22_15, l_22_16 = string.find(l_22_10, l_1_1.sAvatarVarPrefix)
          l_22_10 = string.sub(l_22_10, l_22_16 + 1)
        end
        local l_22_13 = string.sub
        local l_22_14 = resolvedParam
        l_22_13 = l_22_13(l_22_14, l_22_8 + 1, l_22_8 + 1)
        l_22_14 = AssertCorrectCharAfterPrefix
        l_22_14(l_22_13, l_1_1.sAllowedCharsAfterPrefix, resolvedParam, sName, FuncName)
        l_22_14 = AssertCorrectCharactersInString
        l_22_14(l_22_10, l_1_1.sAllowedChars, resolvedParam, sName, FuncName)
      elseif DEBUG and l_1_1.sAllowedChars then
        AssertCorrectCharactersInString(resolvedParam, l_1_1.sAllowedChars, resolvedParam, sName, FuncName)
      end
    end
    if l_1_1.bAddEntityVarSuffix then
      resolvedParam = string.format("%s(%s)", resolvedParam, string.upper(GetScriptTag()))
    end
    if type(resolvedParam) == "boolean" then
      if resolvedParam == true then
        resolvedParam = 1
      else
        resolvedParam = 0
      end
    end
    if sName == "Range" then
      upvalue_2560 = resolvedParam
    end
    if DEBUG then
      if not l_21_4.tSpecParam then
        l_21_4.tSpecParam = {}
      end
      table.insert(l_21_4.tSpecParam, l_1_1)
    end
    if type(resolvedParam) == "table" and not l_1_1.bPassTableToCode then
      table.foreach(resolvedParam, function(l_1_0, l_1_1)
      l_21_4[l_1_0] = l_1_1
      end)
    elseif l_1_1.bEventParam then
      l_21_4[sName] = resolvedParam
    else
      table.insert(l_21_4, resolvedParam)
    end
   end)
  --if DEBUG then
    table.foreach(ParamValues, function(l_2_0, l_2_1)
    local l_23_2 = table.foreachi(Params, function(l_1_0, l_1_1)
      if l_1_1.sName == l_2_0 then
        return true
      end
      end)
    AssertIncorrectParameter(l_23_2, l_2_0, FuncName, Params)
   end)
  --end
  if l_21_4["Conditions"] ~= nil then
    Return.Conditions = l_21_4["Conditions"]
  end
  if l_21_4["TimerStartConditions"] ~= nil then
    Return.TimerStartConditions = l_21_4["TimerStartConditions"]
  end
  if l_21_4["TimerElapsedConditions"] ~= nil then
    Return.TimerElapsedConditions = l_21_4["TimerElapsedConditions"]
  end
  if l_21_4["OnConditions"] ~= nil then
    Return.OnConditions = l_21_4["OnConditions"]
  end
  if l_21_4["OffConditions"] ~= nil then
    Return.OffConditions = l_21_4["OffConditions"]
  end
  if l_21_4["Actions"] ~= nil then
    Return.Actions = l_21_4["Actions"]
  end
  if l_21_4["TimerStartActions"] ~= nil then
    Return.TimerStartActions = l_21_4["TimerStartActions"]
  end
  if l_21_4["TimerElapsedActions"] ~= nil then
    Return.TimerElapsedActions = l_21_4["TimerElapsedActions"]
  end
  if l_21_4["OnActions"] ~= nil then
    Return.OnActions = l_21_4["OnActions"]
  end
  if l_21_4["OffActions"] ~= nil then
    Return.OffActions = l_21_4["OffActions"]
  end
  if l_21_4["StateName"] ~= nil then
    Return.StateName = l_21_4["StateName"]
  end
  if l_21_4["EventName"] ~= nil then
    Return.EventName = l_21_4["EventName"]
  end
  if l_21_4["sFunction"] ~= nil then
    Return.sFunction = l_21_4["sFunction"]
  end
  if l_21_4["sNegated"] ~= nil then
    Return.sNegated = l_21_4["sNegated"]
  end
  if l_21_4["GotoState"] ~= nil then
    Return.GotoState = l_21_4["GotoState"]
  end
  if l_21_4["fCommand"] ~= nil then
    Return.fCommand = l_21_4["fCommand"]
  end
  if l_21_4["Seconds"] ~= nil then
    Return.Seconds = l_21_4["Seconds"]
  end
  if #l_21_4 > 0 then
    Return.ParamsArray = {}
      table.foreach(l_21_4, function(i, v)
        if i ~= "tSpecParam" then
            local def = l_21_4.tSpecParam[i]
            if type(v) ~= def.sType then
                if def.sType == "boolean" and type(v) == "number" and (v == 0 or v == 1) then
                    if v == 0
                    then v = false
                    else v = true
                    end
                else
                    print("CheckParams: " .. def.sName .. " expected type: " .. def.sType .. " real type: " .. type(v) .. " value: " .. tostring(v))
                end
            end
            -- TODO more validation?
            Return.ParamsArray[i] = v
        end
      end)
  end
  table.foreach(Params, function(l_1_0, l_1_1)
    Return[l_1_0] = l_1_1
      if l_1_0 == "Conditions" then
      elseif l_1_0 == "Actions" then
      elseif l_1_0 == "StateName" then
      elseif l_1_0 == "StateName" then
      elseif l_1_0 == "EventName" then
      elseif l_1_0 == "iCreateDate" then
      elseif l_1_0 == "bAction" then
      elseif l_1_0 == "sType" then
      elseif l_1_0 == "sName" then
      elseif l_1_0 == "sFunction" then
      elseif l_1_0 == "sDesc" then
      elseif l_1_0 == "bDocumentationOnly" then
      elseif l_1_0 == "bDocumentationDontShow" then
      elseif l_1_0 == "bCondition" then
      elseif l_1_0 == "sNegated" then
      elseif l_1_0 == "fCommand" then
      elseif l_1_0 == "bFilter" then
      elseif l_1_0 == "sCategory" then
      elseif type(l_1_0) == "number" then
      else
        print("Missing handling for: " .. l_1_0)
        print(debug.traceback())
      end
      end)
  Return.sName = FuncName
  if Params.sName == "MissionOutcry" then
    --print("MissionOutcry params:" .. tprint(l_21_4))
  end
  if Negated then
    Return.sNegated = Params.sName
  else
    Return.sNegated = Params.sNegated
  end
  Return.bNegated = Negated
  Return.bLuaFunction = Params.bLuaFunction
  Return.sFunction = Params.sFunction
  Return.sType = Params.sType
  Return.bDebugCommand = Params.bDebugCommand
  if Params.iUpdateInterval then
    Return.iUpdateInterval = Params.iUpdateInterval
    if Params0 >= 0 then
      if Params0 <= 2 then
        Return.iUpdateInterval = Params0 + 1
      elseif Params0 <= 20 then
        local Params1 = Params0 / 3 - 1
        Return.iUpdateInterval = math.ceil(Params1 * Params1 + Params0 + 1)
      elseif Params0 > 30 then
        assert(nil, "Range > 30 ... are you sure you want to do this???\nPlease consider using 2 or more Range conditions with a smaller Range!\nFor example, 2 conditions with Range 20 will check 800 tiles, whereas one condition with Range 40 will check 1600!")
      else
        Return.iUpdateInterval = 60
      end
    end
    if IsSinglePlayerCampaignMap() and Return.iUpdateInterval > 1 then
      local Params2 = math.random(0, kiUpdateIntervalMaxRandom)
      Return.iUpdateInterval = math.ceil(Return.iUpdateInterval + Params2)
    end
  end
  if DEBUG then
    SaveUserParams(Return, ParamValues)
  end
  return Return
end

SaveUserParams = function(l_22_0, l_22_1)
  l_22_0.tUserParams = {}
  for l_22_5,l_22_6 in pairs(l_22_1) do
    l_22_0.tUserParams[l_22_5] = l_22_6
  end
end


SetParams = function(l_22_0, l_22_1)
  for l_22_5,l_22_6 in pairs(l_22_1) do
    l_22_0[l_22_5] = l_22_6
  end
end

