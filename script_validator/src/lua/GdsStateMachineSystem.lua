function ExecuteCommandString(_ARG_0_)
    local DEBUG = true
  uCommand = nil
  if false and not tConfig.bNoCode then -- TODO check conditions
    if DUMP2DISK then
      if _ARG_0_.sType == "filter" then
      else
      end
      if _ARG_0_.bNegated then
        DumpCommand(("\tuLogical" .. _UPVALUE0_ + 1) .. " = " .. _ARG_0_.sExecute)
      end
      DumpCommand(("\tuLogical" .. _UPVALUE0_ + 1) .. " = " .. _ARG_0_.sExecute)
    end
    if DEBUG then
      bSuccess, uCommand = pcall(dostring, "return " .. _ARG_0_.sExecute)
        print("ExecuteCommandString: " .. tostring(bSuccess) .. " " .. tprint(_ARG_0_))
      if not bSuccess then
        MB("Error calling code function: " .. tostring(uCommand) .. " via execute string:\n" .. _ARG_0_.sExecute .. [[


Additional debug info follows next ...]])
        _ARG_0_.sCommandString = CreateParamString(_ARG_0_)
        VT(_ARG_0_)
        assert(false, "stopping this script after an error occured")
      end
    else
      uCommand = dostring("return " .. _ARG_0_.sExecute)
    end
    if _ARG_0_.iUpdateInterval then
      if DEBUG then
        LogSM(_UPVALUE2_("\t\t>> UpdateInterval = %s", _ARG_0_.iUpdateInterval))
      end
      uCommand:SetUpdateInterval(_ARG_0_.iUpdateInterval)
    end
    if _ARG_0_.bNegated then
      if DEBUG then
        LogSM("\t\t>> Negated = true")
      end
      GdsNegateCondition(uCommand)
    end
    if not FINAL and uCommand.SetExecString then
      uCommand:SetExecString((CreateParamString(_ARG_0_)))
    end
  end
  return uCommand
end
if not SCRIPTCHECK then
  function AddFilter(_ARG_0_, _ARG_1_)
    if _ARG_1_.tFilters then
      table.foreachi(_ARG_1_.tFilters, function(_ARG_0_, _ARG_1_)
        GdsAddFilter(_UPVALUE0_, _ARG_1_)
      end)
    end
    if _ARG_1_.tTargetFilters then
      table.foreachi(_ARG_1_.tTargetFilters, function(_ARG_0_, _ARG_1_)
        GdsAddFilter(_UPVALUE0_, _ARG_1_, true)
      end)
    end
  end
  ConditionCounter = 0
  function ProcessLogicalConditions(_ARG_0_)
    if _ARG_0_.tLogicalConditions then
      ConditionCounter = ConditionCounter + 1
      for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_.tLogicalConditions) do
        CreateCondition(_FORV_5_)
        --_ARG_0_.uCondition:AddCondition(_FORV_5_.uCondition)
        if DUMP2DISK then
          DumpCommand("\tuLogical" .. ConditionCounter .. ":AddCondition(uCommand)")
        end
      end
      if DUMP2DISK then
        DumpCommand("\tuCommand = uLogical" .. ConditionCounter)
      end
      ConditionCounter = ConditionCounter - 1
    end
  end
  function CreateCondition(_ARG_0_)
    _ARG_0_.uCondition = ExecuteCommandString(_ARG_0_)
    AddFilter(_ARG_0_.uCondition, _ARG_0_)
    ProcessLogicalConditions(_ARG_0_)
  end
  function AddTransitionCondition(_ARG_0_, _ARG_1_, _ARG_2_)
    --if _UPVALUE0_ then
      assert(type(_ARG_1_) == "table", "Conditions is not a Table!")
      assert(type(_ARG_1_.sExecute) == "string", "sExecute condition is not a string!")
      --LogSM(_UPVALUE1_("\tC: %s", _ARG_1_.sExecute))
    --end
    CreateCondition(_ARG_1_)
    GdsAddCondition(_ARG_0_, _ARG_1_)
  end
  function AddTransitionConditions(_ARG_0_, _ARG_1_, _ARG_2_)
    --if _UPVALUE0_ then
      assert(type(_ARG_1_) == "table", "Conditions is not a table!")
    --end
    table.foreachi(_ARG_1_, function(_ARG_0_, _ARG_1_)
      AddTransitionCondition(_UPVALUE0_, _ARG_1_, _UPVALUE1_)
    end)
  end
  function AddTransitionAction(_ARG_0_, _ARG_1_, _ARG_2_)
    -- TODO handle transitions, including dead states
    --[[
    --if _UPVALUE0_ then
      assert(type(_ARG_1_) == "table", "Action is not a Table!")
      assert(type(_ARG_1_.sExecute) == "string", "sExecute action is not a string!")
      LogSM(tostring("\tA: %s", _ARG_1_.sExecute))
    --end
    if _ARG_1_.bLuaFunction then
      return
    end
    if not tConfig.bNoCode then
      _ARG_1_.uAction = ExecuteCommandString(_ARG_1_)
      AddFilter(_ARG_1_.uAction, _ARG_1_)
    end
    GdsAddAction(_ARG_0_, _ARG_1_)]]
  end
  function AddTransitionActions(_ARG_0_, _ARG_1_, _ARG_2_)
    --if _UPVALUE0_ then
      assert(type(_ARG_1_) == "table", "Actions is not a table!")
    --end
    table.foreachi(_ARG_1_, function(_ARG_0_, _ARG_1_)
      if FINAL and _ARG_1_.bDebugCommand == true then
      else
        AddTransitionAction(_UPVALUE0_, _ARG_1_, _UPVALUE1_)
      end
    end)
  end
end
function CreateDeadStates(_ARG_0_, _ARG_1_)
  if table.getn(_ARG_0_.tRespawnEvents) == 0 then
    if not tStateMachine.uDeadEndState then
      tStateMachine.uDeadEndState = GdsCreateState(tStateMachine.uStateMachine, "DEADEND")
      _ARG_1_.DEADEND = _uDeadEndState
    end
    AddTransitionCondition(GdsCreateTransition(_ARG_0_.uState, tStateMachine.uDeadEndState, _ARG_0_.StateName, "DEADEND", "OnDeathEvent"), SquadIsDead({
      Tag = GetScriptTag()
    }), "_uDeadEndT")
    table.foreach(_ARG_0_.tDeathEvents, function(_ARG_0_, _ARG_1_)
      if type(_ARG_1_.GotoState) == "string" then
        if _UPVALUE0_ then
          assert(_UPVALUE1_[_ARG_1_.GotoState], "OnSpawnEvent - GotoState: could not find a State named '" .. tostring(_tEvent.GotoState) .. "'.")
        end
        _ARG_1_.uTransition = GdsCreateTransition(tStateMachine.uDeadEndState, _UPVALUE1_[_ARG_1_.GotoState], _UPVALUE2_, _ARG_1_.GotoState, _ARG_1_.sName)
      else
        _ARG_1_.uTransition = GdsCreateTransition(tStateMachine.uDeadEndState, tStateMachine.uDeadEndState, _UPVALUE2_, _UPVALUE2_, _ARG_1_.sName)
      end
      AddTransitionConditions(_ARG_1_.uTransition, _ARG_1_.Conditions, "_uDeath")
      AddTransitionActions(_ARG_1_.uTransition, _ARG_1_.Actions, "_uDeath")
    end)
  else
    _ARG_1_[_ARG_0_.StateName .. ":DEAD"] = GdsCreateState(tStateMachine.uStateMachine, _ARG_0_.StateName .. ":DEAD")
    table.foreach(_ARG_0_.tDeadEvents, function(_ARG_0_inner, _ARG_1_inner)
      AddTransitionConditions(GdsCreateTransition(_ARG_0_, _ARG_0_, _ARG_1_, _ARG_1_, _ARG_1_inner.sName), _ARG_1_inner.Conditions, "nix")
      AddTransitionActions(GdsCreateTransition(_ARG_0_, _ARG_0_, _ARG_1_, _ARG_1_, _ARG_1_inner.sName), _ARG_1_inner.Actions, "nix")
    end)
    table.foreach(_ARG_0_.tRespawnEvents, function(_ARG_0_inner, _ARG_1_inner)
      if not _ARG_0_.tRespawnEvents.uToDeadTransition then
        _ARG_0_.tRespawnEvents.uToDeadTransition = GdsCreateTransition(_ARG_0_.uState, _ARG_1_, _ARG_0_.StateName, _UPVALUE2_, _ARG_1_.sName .. "_EntityDied")
        AddTransitionCondition(_ARG_0_.tRespawnEvents.uToDeadTransition, SquadIsDead({
          Tag = GetScriptTag()
        }), "_uToD")
      end
      if _ARG_1_inner.WaitTime > 0 then
        AddTransitionAction(_ARG_0_.tRespawnEvents.uToDeadTransition, EntityTimerStart({
          Name = _UPVALUE3_("GDS_RespawnEvent#%i_State#%i_Timer", _ARG_0_inner, _ARG_0_.iStateIndex)
        }), "_uToD")
      end
      AddTransitionActions(_ARG_0_.tRespawnEvents.uToDeadTransition, _ARG_1_inner.OnDeathActions, "_uToD")
      if _ARG_1_inner.GotoState == nil or _ARG_1_inner.GotoState == "self" then
        _ARG_1_inner.uTransition = GdsCreateTransition(_ARG_1_, _ARG_0_.uState, _UPVALUE2_, _ARG_0_.StateName, _ARG_1_inner.sName .. "_EntityRespawned")
      else
        _ARG_1_inner.uTransition = GdsCreateTransition(_ARG_1_, _UPVALUE4_[_ARG_1_inner.GotoState], _UPVALUE2_, _ARG_1_inner.GotoState, _ARG_1_inner.sName .. "_EntityRespawnedToState")
      end
      if _ARG_1_inner.WaitTime > 0 then
        if #_ARG_1_inner.Conditions == 0 then
          AddTransitionCondition(_ARG_1_inner.uTransition, EntityTimerIsElapsed({
            Name = _UPVALUE3_("GDS_RespawnEvent#%i_State#%i_Timer", _ARG_0_inner, _ARG_0_.iStateIndex),
            Seconds = _ARG_1_inner.WaitTime
          }), "_uR")
        else
          AddTransitionCondition(_ARG_1_inner.uTransition, OR({
            EntityTimerIsElapsed({
              Name = _UPVALUE3_("GDS_RespawnEvent#%i_State#%i_Timer", _ARG_0_inner, _ARG_0_.iStateIndex),
              Seconds = _ARG_1_inner.WaitTime
            }),
            EntityFlagIsTrue({
              Name = "GDSEntityStartsDespawned"
            })
          }), "_uR")
        end
      end
      AddTransitionConditions(_ARG_1_inner.uTransition, _ARG_1_inner.Conditions, "_uR")
      if _ARG_1_inner.WaitTime > 0 then
        AddTransitionAction(_ARG_1_inner.uTransition, EntityTimerStop({
          Name = _UPVALUE3_("GDS_RespawnEvent#%i_State#%i_Timer", _ARG_0_inner, _ARG_0_.iStateIndex)
        }), "_uR")
        AddTransitionAction(_ARG_1_inner.uTransition, EntityFlagSetFalse({
          Name = "GDSEntityStartsDespawned"
        }), "_uR")
      end
      AddTransitionAction(_ARG_1_inner.uTransition, SquadRespawn({
        Tag = GetScriptTag(),
        TargetTag = _ARG_1_inner.TargetTag,
        HealthPercent = _ARG_1_inner.HealthPercent
      }), "_uR")
      AddTransitionActions(_ARG_1_inner.uTransition, _ARG_1_inner.Actions, "_uR")
    end)
  end
end


function CreateStateMachine()
  if 0 < table.getn(tStateMachine.tStates) then
    local states = {}
    tStateMachine.uStateMachine = GdsCreateStateMachine(GetScriptTag())
    --print(tprint(tStateMachine))
    table.foreachi(tStateMachine.tStates, function(_ARG_0_, _ARG_1_)
      _ARG_1_.iStateIndex = _ARG_0_
      --print("CreateStateMachine _ARG_1_: " .. tostring(_ARG_1_))
      _ARG_1_.uState = GdsCreateState(tStateMachine.uStateMachine, _ARG_1_.StateName)
      states[_ARG_1_.StateName] = _ARG_1_.uState
    end)
    table.foreachi(tStateMachine.tStates, function(_ARG_0_, state)
      if (IsSquadScript() or IsScriptGroupScript() or IsObjectScript() or IsPlayerScript() or IsBuildingScript()) and not IsWarfarePatternScript() then
        CreateDeadStates(state, state)
      end
      table.foreachi(state.tEvents, function(_ARG_0_, _ARG_1_)
        _ARG_1_.uState = state.uState
      --print("CreateStateMachine _ARG_1_: " .. tostring(state))
        _ARG_1_.iEvent = _ARG_0_
        if _ARG_1_.GotoState == nil or _ARG_1_.GotoState == "self" then
            --print("CreateStateMachine state: " .. tprint(state))
          _ARG_1_.uTransition = GdsCreateTransition(state.uState, state.uState, state.StateName, state.StateName, _ARG_1_.sName, _ARG_1_.EventName)
        elseif _ARG_1_.GotoState == ":DEAD" then
          _ARG_1_.uTransition = GdsCreateTransition(state.uState, states[state.StateName .. ":DEAD"], state.StateName, state.StateName .. ":DEAD", _ARG_1_.sName, _ARG_1_.EventName)
        else
          --if DEBUG then
            assert(states[_ARG_1_.GotoState], "Error in GotoState: could not find a State named '" .. tostring(_ARG_1_.GotoState) .. "'.")
            assert(tostring(state.uState) ~= tostring(states[_ARG_1_.GotoState]), "Error in GotoState: linking to self state (" .. state.StateName ..")'" .. tostring(_ARG_1_.GotoState) .. "' not allowed.")
          --end
          _ARG_1_.uTransition = GdsCreateTransition(state.uState, states[_ARG_1_.GotoState], state.StateName, _ARG_1_.GotoState, _ARG_1_.sName, _ARG_1_.EventName)
          if IsSquadScript() then
            table.insert(_ARG_1_.Conditions, 1, SquadIsAlive({
              Tag = GetScriptTag()
            }))
          elseif IsBuildingScript() then
            table.insert(_ARG_1_.Conditions, 1, BuildingIsAlive({
              Tag = GetScriptTag()
            }))
          end
        end
        AddTransitionConditions(_ARG_1_.uTransition, _ARG_1_.Conditions, "uT" .. _ARG_0_)
        AddTransitionActions(_ARG_1_.uTransition, _ARG_1_.Actions, "uT" .. _ARG_0_)
      end)
    end)
    if _UPVALUE2_ then
      LogSM(_UPVALUE3_("CreateStateMachine(\"%s\") done", GetScriptTag()))
    end
  end
end
