function DefineEvent(_ARG_0_)
  table.insert(tEventSpecs, _ARG_0_)
end
function RegisterEvents()
  table.foreachi(tEventSpecs, function(_ARG_0_, _ARG_1_)
    RegisterEventFunction(_ARG_0_, _ARG_1_.sName)
  end)
end
function RegisterEventFunction(_ARG_0_, _ARG_1_)
  dostring((string.gsub(string.gsub([[
	function sFuncName (tParams)
		local tCheckedParams = CheckParams('sFuncName', tEventSpecs[iIndex], tParams)
        --print("RegisterEventFunction: tCheckedParams: " .. "once?")
        --print("RegisterEventFunction: tCheckedParams: " .. tprint(tCheckedParams))
		CreatesFuncName(tCheckedParams)
	end
	]], "sFuncName", _ARG_1_), "iIndex", tostring(_ARG_0_))))
end
function AddAliveCondition(_ARG_0_)
  if IsSquadScript() then
    table.insert(_ARG_0_, SquadIsAlive({}))
  elseif IsBuildingScript() then
    table.insert(_ARG_0_, BuildingIsAlive({}))
  end
end
function CreateOnEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
  end
  IncreaseNumEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumEvents())
  table.insert(tEvents, _ARG_0_)
end
function CreateOnOneTimeEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
  end
  IncreaseNumOneTimeEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumOneTimeEvents())
  table.insert(_ARG_0_.Conditions, 1, EntityFlagIsFalse({
    Name = string.format("GDSOneTimeEvent%iHasTriggered", GetNumOneTimeEvents())
  }))
  table.insert(_ARG_0_.Actions, 1, EntityFlagSetTrue({
    Name = string.format("GDSOneTimeEvent%iHasTriggered", GetNumOneTimeEvents())
  }))
  table.insert(tEvents, _ARG_0_)
end
F1 = "F1"
F2 = "F2"
F3 = "F3"
F4 = "F4"
F5 = "F5"
F6 = "F6"
F7 = "F7"
F8 = "F8"
function CreateOnKeyPressEvent(_ARG_0_, _ARG_1_)
  if not FINAL then
    if DEBUG then
      assert(type(_ARG_0_.Key) == "string", _ARG_0_.sName .. ": Key must be a string!")
      assert(_ARG_0_.Key == "F1" or _ARG_0_.Key == "F2" or _ARG_0_.Key == "F3" or _ARG_0_.Key == "F4" or _ARG_0_.Key == "F5" or _ARG_0_.Key == "F6" or _ARG_0_.Key == "F7" or _ARG_0_.Key == "F8", _ARG_0_.sName .. ": Key must be between 'F1' and 'F8'!")
    end
    if _ARG_0_.Shift == 1 then
      _ARG_0_.Shift = 0
      _ARG_0_.Alt = 1
    end
    if _ARG_0_.Shift == 0 and _ARG_0_.Ctrl == 0 and _ARG_0_.Alt == 0 then
      _ARG_0_.Ctrl = 1
    end
    if _ARG_0_.Shift == 1 then
    else
    end
    if _ARG_0_.Ctrl == 1 then
    else
    end
    if _ARG_0_.Alt == 1 then
    else
    end
    table.insert(_ARG_0_.Conditions, PlayerFlagIsTrue({
      Name = "pf_KeyPressEvent_" .. "" .. "" .. "" .. _ARG_0_.Key,
      Player = _ARG_0_.Player
    }))
    table.insert(_ARG_0_.Actions, PlayerFlagSetFalse({
      Name = "pf_KeyPressEvent_" .. "" .. "" .. "" .. _ARG_0_.Key,
      Player = _ARG_0_.Player
    }))
    if _ARG_1_ then
      OnOneTimeEvent({
        EventName = "pf_KeyPressEvent_" .. "" .. "" .. "" .. _ARG_0_.Key,
        Conditions = _ARG_0_.Conditions,
        Actions = _ARG_0_.Actions,
        GotoState = _ARG_0_.GotoState
      })
    else
      OnEvent({
        EventName = "pf_KeyPressEvent_" .. "" .. "" .. "" .. _ARG_0_.Key,
        Conditions = _ARG_0_.Conditions,
        Actions = _ARG_0_.Actions,
        GotoState = _ARG_0_.GotoState
      })
    end
  end
end
function CreateOnOneTimeKeyPressEvent(_ARG_0_)
  CreateOnKeyPressEvent(_ARG_0_, true)
end
function CreateOnToggleEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.OnConditions) == "table", _ARG_0_.sName .. ": OnConditions is not a table!")
    assert(type(_ARG_0_.OnActions) == "table", _ARG_0_.sName .. ": OnActions is not a table!")
    assert(type(_ARG_0_.OffConditions) == "table", _ARG_0_.sName .. ": OffConditions is not a table!")
    assert(type(_ARG_0_.OffActions) == "table", _ARG_0_.sName .. ": OffActions is not a table!")
  end
  IncreaseNumToggleEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumToggleEvents())
  table.insert(_ARG_0_.OnConditions, 1, EntityFlagIsFalse({
    Name = string.format("GDSToggleEvent%iIsOn", GetNumToggleEvents())
  }))
  table.insert(_ARG_0_.OnActions, 1, EntityFlagSetTrue({
    Name = string.format("GDSToggleEvent%iIsOn", GetNumToggleEvents())
  }))
  table.insert(_ARG_0_.OffConditions, 1, EntityFlagIsTrue({
    Name = string.format("GDSToggleEvent%iIsOn", GetNumToggleEvents())
  }))
  table.insert(_ARG_0_.OffActions, 1, EntityFlagSetFalse({
    Name = string.format("GDSToggleEvent%iIsOn", GetNumToggleEvents())
  }))
  OnEvent({
    Conditions = _ARG_0_.OnConditions,
    Actions = _ARG_0_.OnActions,
    EventName = "(Toggle) [ON ] " .. _ARG_0_.EventName
  })
  OnEvent({
    Conditions = _ARG_0_.OffConditions,
    Actions = _ARG_0_.OffActions,
    EventName = "(Toggle) [OFF] " .. _ARG_0_.EventName
  })
end
function CreateOnTimerEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.TimerStartConditions) == "table", _ARG_0_.sName .. ": TimerStartConditions is not a table!")
    assert(type(_ARG_0_.TimerStartActions) == "table", _ARG_0_.sName .. ": TimerStartActions is not a table!")
    assert(type(_ARG_0_.TimerElapsedConditions) == "table", _ARG_0_.sName .. ": TimerElapsedConditions is not a table!")
    assert(type(_ARG_0_.TimerElapsedActions) == "table", _ARG_0_.sName .. ": TimerElapsedActions is not a table!")
    assert(_ARG_0_.Seconds or _ARG_0_.Minutes, _ARG_0_.sName .. ": both Seconds and Minutes parameters are missing!")
  end
  IncreaseNumTimerEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumTimerEvents())
  table.insert(_ARG_0_.TimerStartConditions, 1, EntityFlagIsFalse({
    Name = string.format("GDSTimerEvent%iIsOn", GetNumTimerEvents())
  }))
  table.insert(_ARG_0_.TimerStartActions, EntityFlagSetTrue({
    Name = string.format("GDSTimerEvent%iIsOn", GetNumTimerEvents())
  }))
  table.insert(_ARG_0_.TimerStartActions, EntityTimerStart({
    Name = string.format("GDSTimerEvent%iTimer", GetNumTimerEvents())
  }))
  table.insert(_ARG_0_.TimerElapsedConditions, 1, EntityTimerIsElapsed({
    Name = string.format("GDSTimerEvent%iTimer", GetNumTimerEvents()),
    Seconds = _ARG_0_.Seconds,
    Minutes = _ARG_0_.Minutes
  }))
  if _ARG_0_.OneTime == 1 then
    table.insert(_ARG_0_.TimerElapsedActions, EntityTimerStop({
      Name = string.format("GDSTimerEvent%iTimer", GetNumTimerEvents())
    }))
  else
    table.insert(_ARG_0_.TimerElapsedActions, EntityTimerStart({
      Name = string.format("GDSTimerEvent%iTimer", GetNumTimerEvents())
    }))
  end
  OnEvent({
    Conditions = _ARG_0_.TimerElapsedConditions,
    Actions = _ARG_0_.TimerElapsedActions,
    EventName = "(OneTimeTimer) " .. "[ELAPSED] " .. _ARG_0_.EventName
  })
  OnEvent({
    Conditions = _ARG_0_.TimerStartConditions,
    Actions = _ARG_0_.TimerStartActions,
    EventName = "(OneTimeTimer) " .. "[START] " .. _ARG_0_.EventName
  })
end
function CreateOnOneTimeTimerEvent(_ARG_0_)
  CreateOnTimerEvent(_ARG_0_)
end
function CreateOnIntervalEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
    assert(type(_ARG_0_.Seconds) == "number", _ARG_0_.sName .. ": Seconds is not a number! " .. tprint(_ARG_0_))
  end
  IncreaseNumTimerEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumTimerEvents())
  table.insert(_ARG_0_.Conditions, 1, EntityTimerIsElapsed({
    Name = string.format("GDSIntervalEvent%iTimer", GetNumTimerEvents()),
    Seconds = _ARG_0_.Seconds
  }))
  table.insert(_ARG_0_.Actions, 1, EntityTimerStart({
    Name = string.format("GDSIntervalEvent%iTimer", GetNumTimerEvents())
  }))
  OnEvent({
    Conditions = _ARG_0_.Conditions,
    Actions = _ARG_0_.Actions,
    EventName = "[INTERVAL-EXECUTE] " .. _ARG_0_.EventName
  })
  OnEvent({
    EventName = "[INTERVAL-START] " .. _ARG_0_.EventName,
    Conditions = {
      OR({
        EntityTimerIsElapsed({
          Name = string.format("GDSIntervalEvent%iTimer", GetNumTimerEvents()),
          Seconds = _ARG_0_.Seconds
        }),
        EntityTimerIsNotRunning({
          Name = string.format("GDSIntervalEvent%iTimer", GetNumTimerEvents())
        })
      })
    },
    Actions = {
      EntityTimerStart({
        Name = string.format("GDSIntervalEvent%iTimer", GetNumTimerEvents())
      })
    }
  })
end
function CreateOnRespawnEvent(_ARG_0_)
  assert(table.getn(tDeathEvents) == 0, "ERROR: can't use OnRespawnEvent together with OnDeathEvent in the same state!")
  assert(IsSquadScript() or IsScriptGroupScript(), [[
OnRespawn - the entity executing this script is not a Squad!
Note: make sure you don't run entity scripts by adding them to the ScriptList, this is not supported.]])
  if type(_ARG_0_.Conditions) ~= "table" then
    _ARG_0_.Conditions = {}
  end
  if type(_ARG_0_.Actions) ~= "table" then
    _ARG_0_.Actions = {}
  end
  if type(_ARG_0_.OnDeathActions) ~= "table" then
    _ARG_0_.OnDeathActions = {}
  end
  if type(_ARG_0_.RespawnDelaySeconds) ~= "number" then
    _ARG_0_.RespawnDelaySeconds = 0
  end
  if type(_ARG_0_.RespawnDelayMinutes) ~= "number" then
    _ARG_0_.RespawnDelayMinutes = 0
  end
  _ARG_0_.WaitTime = 60 * _ARG_0_.RespawnDelayMinutes + _ARG_0_.RespawnDelaySeconds
  if _ARG_0_.StartDespawned == 1 then
    OnOneTimeEvent({
      EventName = "OnRespawnEvent_EntityStartsDespawned",
      Conditions = {},
      Actions = {
        EntityTimerStart({
          Name = string.format("GDS_RespawnEvent#%i_State#%i_Timer", #tRespawnEvents + 1, #tStateMachine.tStates + 1)
        }),
        EntityFlagSetTrue({
          Name = "GDSEntityStartsDespawned"
        }),
        SquadVanish({})
      },
      GotoState = ":DEAD"
    })
  end
  table.insert(tRespawnEvents, _ARG_0_)
end
function CreateOnIdleGoHomeEvent(_ARG_0_, _ARG_1_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.TargetTag) == "string", _ARG_0_.sName .. ": TargetTag is not a string!")
  end
  if type(_ARG_0_.GoActions) ~= "table" then
    _ARG_0_.GoActions = {}
  end
  if type(_ARG_0_.HomeActions) ~= "table" then
    _ARG_0_.HomeActions = {}
  end
  if type(_ARG_0_.OneTimeHomeActions) ~= "table" then
    _ARG_0_.OneTimeHomeActions = {}
  end
  if not _ARG_0_.Range then
    _ARG_0_.Range = ktRangeEventParam.DefaultValue
  end
  if DEBUG and _ARG_0_.Range < ktRangeEventParam.DefaultValue then
    assert(false, _ARG_0_.sName .. ": Range is smaller than the recommended default of " .. ktRangeEventParam.DefaultValue(" which could lead to Squads never being able to reach their home position!"))
  end
  if string.upper(_ARG_0_.TargetTag) == "STARTPOS" then
    _ARG_0_.TargetTag = _ARG_0_.Tag .. "_STARTPOS"
    OnOneTimeEvent({
      EventName = _ARG_0_.EventName .. "_" .. _ARG_0_.Tag .. "_STARTPOSMARKERSPAWN",
      Conditions = {
        SquadIsAlive({
          Tag = _ARG_0_.Tag
        })
      },
      Actions = {
        ObjectSpawnWithTag({
          Tag = _ARG_0_.TargetTag,
          TargetTag = _ARG_0_.Tag,
          ObjectId = 449
        })
      }
    })
  end
  IncreaseNumIdleGoHomeEvents()
  table.insert(_ARG_0_.Conditions, (EntityIsNotInRange({
    Tag = _ARG_0_.Tag,
    TargetTag = _ARG_0_.TargetTag,
    Range = _ARG_0_.HomeRange
  })))
  table.insert(_ARG_0_.Conditions, 1, SquadIsIdle({
    Tag = _ARG_0_.Tag
  }))
  table.insert(_ARG_0_.GoActions, SquadGoto({
    Tag = _ARG_0_.Tag,
    TargetTag = _ARG_0_.TargetTag,
    Run = _ARG_0_.Run,
    Direction = _ARG_0_.Direction
  }))
  _ARG_0_.Actions = _ARG_0_.GoActions
  _ARG_0_.EventName = string.format("(#%i_GoingHome) %s", GetNumIdleGoHomeEvents(), _ARG_0_.EventName)
  table.insert(tEvents, _ARG_0_)
  if table.getn(_ARG_0_.HomeActions) > 0 then
    OnToggleEvent({
      EventName = _ARG_0_.EventName .. "_" .. _ARG_0_.Tag .. "_HomeActions",
      OnConditions = {
        SquadIsIdle({
          Tag = _ARG_0_.Tag
        }),
        EntityIsInRange({
          Tag = _ARG_0_.Tag,
          TargetTag = _ARG_0_.TargetTag,
          Range = _ARG_0_.HomeRange
        })
      },
      OnActions = _ARG_0_.HomeActions,
      OffConditions = {
        EntityIsNotInRange({
          Tag = _ARG_0_.Tag,
          TargetTag = _ARG_0_.TargetTag,
          Range = _ARG_0_.HomeRange
        })
      },
      OffActions = {}
    })
  end
  if table.getn(_ARG_0_.OneTimeHomeActions) > 0 then
    OnOneTimeEvent({
      EventName = _ARG_0_.EventName .. "_" .. _ARG_0_.Tag .. "_OneTimeHomeActions",
      Conditions = {
        SquadIsIdle({
          Tag = _ARG_0_.Tag
        }),
        EntityIsInRange({
          Tag = _ARG_0_.Tag,
          TargetTag = _ARG_0_.TargetTag,
          Range = _ARG_0_.HomeRange
        })
      },
      Actions = _ARG_0_.OneTimeHomeActions
    })
  end
end
function CreateOnIdleContinueAttackEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
    assert(type(_ARG_0_.Tag) == "string", _ARG_0_.sName .. ": Tag is not a string!")
    assert(type(_ARG_0_.TargetTag) == "string", _ARG_0_.sName .. ": TargetTag is not a string!")
  end
  table.insert(_ARG_0_.Conditions, 1, EntityIsAlive({
    Tag = _ARG_0_.TargetTag,
    For = "Any"
  }))
  table.insert(_ARG_0_.Conditions, 2, SquadIsIdle({
    Tag = _ARG_0_.Tag,
    For = "All"
  }))
  table.insert(_ARG_0_.Actions, SquadAttack({
    Tag = _ARG_0_.Tag,
    TargetTag = _ARG_0_.TargetTag,
    AttackGroupSize = _ARG_0_.AttackGroupSize
  }))
  if not _ARG_0_.EventName or _ARG_0_.EventName == "" then
    _ARG_0_.EventName = "OnIdleContinueAttackEvent"
  else
    _ARG_0_.EventName = "OnIdleContinueAttackEvent_" .. _ARG_0_.EventName
  end
  OnEvent({
    EventName = _ARG_0_.EventName,
    Conditions = _ARG_0_.Conditions,
    Actions = _ARG_0_.Actions
  })
end
function CreateOnDeadEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
  end
  IncreaseNumDeadEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumDeadEvents())
  _ARG_0_.EventName = string.format("%s OnDeadEvent#%i", _ARG_0_.EventName, GetNumDeadEvents())
  table.insert(tDeadEvents, _ARG_0_)
end
function CreateOnOneTimeDeadEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
  end
  IncreaseNumOneTimeDeadEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumOneTimeDeadEvents())
  _ARG_0_.EventName = string.format("%s OneTimeDead#%i", _ARG_0_.EventName, GetNumOneTimeDeadEvents())
  table.insert(_ARG_0_.Conditions, 1, EntityFlagIsFalse({
    Name = string.format("GDSOneTimeDeadEvent%iHasTriggered", GetNumOneTimeDeadEvents())
  }))
  table.insert(_ARG_0_.Actions, 1, EntityFlagSetTrue({
    Name = string.format("GDSOneTimeDeadEvent%iHasTriggered", GetNumOneTimeDeadEvents())
  }))
  table.insert(tDeadEvents, _ARG_0_)
end
function CreateOnSpawnEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
    assert(type(_ARG_0_.GotoState) == "string", _ARG_0_.sName .. ": GotoState is not a string!")
  end
  IncreaseNumDeadEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumDeadEvents())
  _ARG_0_.EventName = string.format("%s OnDeadEvent#%i", _ARG_0_.EventName, GetNumDeadEvents())
  table.insert(_ARG_0_.Conditions, 1, EntityIsAlive({
    Tag = GetScriptTag()
  }))
  table.insert(tDeadEvents, _ARG_0_)
end
function CreateOnOneTimeSpawnEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
    assert(type(_ARG_0_.GotoState) == "string", _ARG_0_.sName .. ": GotoState is not a string!")
  end
  IncreaseNumOneTimeDeadEvents()
  _ARG_0_.sName = string.format("%s#%i", _ARG_0_.sName, GetNumOneTimeDeadEvents())
  _ARG_0_.EventName = string.format("%s OneTimeDead#%i", _ARG_0_.EventName, GetNumOneTimeDeadEvents())
  table.insert(_ARG_0_.Conditions, 1, EntityIsAlive({
    Tag = GetScriptTag()
  }))
  table.insert(_ARG_0_.Conditions, 1, EntityFlagIsFalse({
    Name = string.format("GDSOneTimeSpawnEvent%iHasTriggered", GetNumOneTimeDeadEvents())
  }))
  table.insert(_ARG_0_.Actions, 1, EntityFlagSetTrue({
    Name = string.format("GDSOneTimeSpawnEvent%iHasTriggered", GetNumOneTimeDeadEvents())
  }))
  table.insert(tDeadEvents, _ARG_0_)
end
function CreateOnBuildingNotUnderAttackEvent(_ARG_0_)
    local DEBUG = true
  if DEBUG then
    assert(type(_ARG_0_.Conditions) == "table", _ARG_0_.sName .. ": Conditions is not a table!")
    assert(type(_ARG_0_.Actions) == "table", _ARG_0_.sName .. ": Actions is not a table!")
    assert(type(_ARG_0_.Tag) == "string", _ARG_0_.sName .. ": Tag is not a string!")
  end
  table.insert(_ARG_0_.Conditions, 1, EntityTimerIsNotRunning({
    Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag
  }))
  OnEvent({
    EventName = _ARG_0_.EventName .. "_UnderAttackInit",
    Conditions = {
      EntityTimerIsNotRunning({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag
      }),
      BuildingWasJustHit({
        For = "ANY",
        Tag = _ARG_0_.Tag
      })
    },
    Actions = {
      EntityTimerStart({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag
      })
    }
  })
  OnEvent({
    EventName = _ARG_0_.EventName .. "_UnderAttackRepeat",
    Conditions = {
      EntityTimerIsElapsed({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag,
        Seconds = math.random(8, 22) / 10
      }),
      EntityTimerIsNotElapsed({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag,
        Seconds = 5
      }),
      BuildingWasJustHit({
        For = "ANY",
        Tag = _ARG_0_.Tag
      })
    },
    Actions = {
      EntityTimerStart({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag
      })
    }
  })
  OnEvent({
    EventName = _ARG_0_.EventName .. "_AttackOver",
    Conditions = {
      EntityTimerIsElapsed({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag,
        Seconds = 5
      })
    },
    Actions = {
      EntityTimerStop({
        Name = "et_OnBuildingNotUnderAttack_" .. _ARG_0_.Tag
      })
    }
  })
  OnEvent({
    EventName = _ARG_0_.EventName,
    Conditions = _ARG_0_.Conditions,
    Actions = _ARG_0_.Actions,
    GotoState = _ARG_0_.GotoState
  })
end
