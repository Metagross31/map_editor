DefineAction({
  sName = "AudioAmbientPlay",
  sFunction = "CScriptActionAmbientStreamPlay",
  ktAmbientStreamFileParam,
  ktScriptTagTargetParam,
  sDesc = "Plays the specified AmbientStream at the TargetTag position.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 9, 22)
})
DefineAction({
  sName = "AudioSoundFXStop",
  sFunction = "CScriptActionAudioStop",
  ktSoundFileParam,
  AudioTypeSoundFX,
  sDesc = "Stops the Audio if it is playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineAction({
  sName = "CameraCenterOnTarget",
  sFunction = "CScriptActionCameraCenterOnTarget",
  ktScriptTagTargetParam,
  sDesc = "Centers the Camera on the Tag's position.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 9, 19)
})
DefineAction({
  sName = "CutsceneCameraPlay",
  sFunction = "CScriptActionCameraTrackPlay",
  ktCameraPathParam,
  ktCameraParam,
  ktCameraTargetTagParam,
  0,
  sDesc = "Plays the specified Camera Track. TargetTag is optional, it overwrites TargetTag in Cutscene if used.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2008, 5, 29)
})
DefineAction({
  sName = "CutsceneCameraPlayFullScreen",
  sFunction = "CScriptActionCameraTrackPlay",
  ktCameraPathParam,
  ktCameraParam,
  ktCameraTargetTagParam,
  1,
  sDesc = "Plays the specified Camera Track in fullscreen mode.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2008, 10, 23)
})
DefineAction({
  sName = "PlayerCardBuildingFakePlay",
  sFunction = "CScriptActionCardBuildingFakePlay",
  "",
  ktScriptTagTargetParam,
  ktScriptTagPlayerNoQuantorParam,
  ktCardBuildingIdParam,
  ktDirectionParam,
  sDesc = "Emulates the playing of a CardBuilding but without Power consumption and for any Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2011, 12, 5)
})
DefineAction({
  sName = "PlayerCardBuildingFakePlayWithTag",
  sFunction = "CScriptActionCardBuildingFakePlay",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktScriptTagPlayerNoQuantorParam,
  ktCardBuildingIdParam,
  ktDirectionParam,
  sDesc = "Emulates the playing of a CardBuilding but without Power consumption and for any Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2011, 12, 5)
})
DefineAction({
  sName = "CardActionClone",
  sFunction = "CScriptActionCardClone",
  ktCardIdParam,
  sDesc = "Creates a clone of the given card ID. Only squad and builing card type are supported. Returns the card ID of the newly created card.",
  sCategory = "Card",
  iCreateDate = GetDays(2011, 12, 9)
})
DefineAction({
  sName = "PlayerCardSpellFakePlay",
  sFunction = "CScriptActionCardSpellFakePlay",
  ktScriptTagTargetParam,
  ktAimTagParam,
  ktPlayerNoQuantorParam,
  ktCardSpellIdParam,
  sDesc = "Emulates the playing of a CardSpell but without Power consumption and for any Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 10, 23)
})
DefineAction({
  sName = "PlayerCardSquadFakePlay",
  sFunction = "CScriptActionCardSquadFakePlay",
  "",
  ktScriptTagTargetParam,
  ktScriptTagPlayerNoQuantorParam,
  ktCardSquadIdParam,
  ktDirectionParam,
  ktExecuteSpellParam,
  ktPlayCheerAnimParam,
  sDesc = "Emulates the playing of a CardSquad but without Power consumption and for any Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineAction({
  sName = "PlayerCardSquadFakePlayWithTag",
  sFunction = "CScriptActionCardSquadFakePlay",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktScriptTagPlayerNoQuantorParam,
  ktCardSquadIdParam,
  ktDirectionParam,
  ktExecuteSpellParam,
  ktPlayCheerAnimParam,
  sDesc = "Emulates the playing of a CardSquad but without Power consumption and for any Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineAction({
  sName = "CutsceneBegin",
  sFunction = "CScriptActionCutsceneBeginEnd",
  1,
  ktMusicFileParam,
  ktFigureRenderOnlyParam,
  sDesc = "Starts a Cutscene.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 9, 25)
})
DefineAction({
  sName = "CutsceneEnd",
  sFunction = "CScriptActionCutsceneBeginEnd",
  0,
  "",
  "",
  sDesc = "Ends a running Cutscene.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 9, 3)
})
DefineAction({
  sName = "CutsceneRenderOnly",
  sFunction = "CScriptActionCutsceneRenderOnly",
  ktScriptTagParam,
  1,
  sDesc = "Renders only the entities from the ScriptTag group. Affects only Figures.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2008, 8, 18)
})
DefineAction({
  sName = "CutsceneRenderAll",
  sFunction = "CScriptActionCutsceneRenderOnly",
  "",
  0,
  sDesc = "Resets render flags after using CutsceneRenderOnly so all entities are rendered again.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2008, 8, 18)
})
DefineAction({
  sName = "CutsceneRewardCardShowOff",
  sFunction = "CScriptActionCutsceneRewardCardShowOff",
  ktScriptTagTargetParam,
  ktCardSquadIdParam,
  sDesc = "Plays the Reward Card ShowOff Animation & Effects on the specified TargetTag.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineAction({
  sName = "CutsceneSay",
  sFunction = "CScriptActionCutsceneSay",
  ktScriptTagParam,
  ktTextTagParam,
  ktTextParam,
  -1,
  sDesc = "Displays text during a Cutscene.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 9, 3)
})
for _FORV_7_, _FORV_8_ in ipairs({
  "Info",
  "Warning",
  "Error"
}) do
  local params1 = {
      "Info Message from Scripter",
      "~Warning~ Message from Scripter",
      "~!~ ERROR ~!~ Message from Scripter"
    }
  DefineAction({
    sName = "Debug" .. _FORV_8_ .. "Message",
    sFunction = "CScriptActionDebugMessageBox",
    ktMessageBoxMessageParam,
    params1[_FORV_7_],
    _FORV_7_,
    ktShowMessageBoxParam,
    sDesc = "Shows a MessageBox to the user (ignored in release builds).",
    sCategory = "Debug",
    bDebugCommand = true,
    iCreateDate = GetDays(2007, 8, 14)
  })
end
for i, _FORV_11_ in ipairs({
  "Squad",
  "PlayerSquad",
  "Building",
  "PlayerBuilding"
}) do
  local params1 = {
      ktTeamParam, -- should not be there as far as I can tell, but EA's magic requires it
      ktScriptTagPlayerNoQuantorParam,
      ktTeamParam, -- should not be there as far as I can tell, but EA's magic requires it
      ktScriptTagPlayerNoQuantorParam,
    }
  local params2 = {
      ktSquadIdParam,
      ktSquadIdParam,
      ktBuildingIdParam,
      ktBuildingIdParam
    }
  local params3 = {
      TypeSquad,
      TypeSquad,
      TypeBuilding,
      TypeBuilding
    }
  local params4 = {
      "Squad",
      "Squad",
      "Building",
      "Building"
    }
  DefineAction({
    sName = _FORV_11_ .. "Spawn",
    sFunction = "CScriptActionEntitySpawn",
    "",                     -- script_tag
    "",                     -- script_group
    ktScriptTagTargetParam, -- target_tag
    params1[i],             -- player (or nothing)
    params2[i],             -- res id
    ktDirectionParam,       -- direction
    params3[i],             -- type (squad or building)
    ktHealthPercentParam,   -- health_percent
    ktSpawnAmountParam,     -- amount (this parameter should be constant but it somehow works)
    sDesc = "Spawns (creates) a squad or building entity on the map.",
    sCategory = params4[i],
    iCreateDate = GetDays(2007, 9, 10)
  })
  DefineAction({
    sName = _FORV_11_ .. "SpawnWithTag",
    sFunction = "CScriptActionEntitySpawn",
    ktScriptTagParam,       -- script_tag
    "",                     -- script_group
    ktScriptTagTargetParam, -- target_tag
    params1[i],             -- player (or nothing)
    params2[i],             -- res id
    ktDirectionParam,       -- direction
    params3[i],             -- type (squad or building)
    ktHealthPercentParam,   -- health_percent
    ktSpawnAmountParam,     -- amount (this parameter should be constant but it somehow works)
    sDesc = "Spawns (creates) a squad or building entity on the map.",
    sCategory = params4[i],
    iCreateDate = GetDays(2007, 9, 10)
  })
  if _FORV_11_ == "Squad" or _FORV_11_ == "PlayerSquad" then
    DefineAction({
      sName = _FORV_11_ .. "SpawnIntoGroup",
      sFunction = "CScriptActionEntitySpawn",
      ktScriptTagParam,       -- script_tag
      ktScriptGroupParam,     -- script_group
      ktScriptTagTargetParam, -- target_tag
      params1[i],             -- player (or nothing)
      ktSquadIdParam,         -- res id
      ktDirectionParam,       -- direction
      TypeSquad,              -- type
      ktHealthPercentParam,   -- health_percent
      ktSpawnAmountParam,     -- amount (this parameter should be constant but it somehow works)
      sDesc = "Spawns (creates) a squad or building squad entity on the map.",
      sCategory = params4[i],
      iCreateDate = GetDays(2008, 1, 23)
    })
  end
end
DefineAction({
  sName = "ObjectSpawn",
  sFunction = "CScriptActionEntitySpawn",
  "",                     -- script_tag
  ktScriptTagTargetParam, -- target_tag
  ktObjectIdParam,        -- res id
  ktDirectionParam,       -- direction
  ktHeightParam,          -- height
  ktScalingParam,         -- scaling
  sDesc = "Spawns (creates) an object entity on the map.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 9, 10)
})
DefineAction({
  sName = "ObjectSpawnWithTag",
  sFunction = "CScriptActionEntitySpawn",
  ktScriptTagParam,       -- script_tag
  ktScriptTagTargetParam, -- target_tag
  ktObjectIdParam,        -- res id
  ktDirectionParam,       -- direction
  ktHeightParam,          -- height
  ktScalingParam,         -- scaling
  sDesc = "Spawns (creates) an object entity on the map.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 9, 10)
})
DefineAction({
  sName = "AbilitySpawn",
  sFunction = "CScriptActionEntitySpawn",
  "",                     -- script_tag
  "",                     -- script_group
  ktScriptTagTargetParam, -- target_tag
  ktTeamParam,            -- player or team
  ktAbilityIdParam,       -- res id
  0,                      -- direction
  TypeAbilityWorldObject, -- type
  100,                    -- health_percent
  1,                      -- amount
  sDesc = "Spawns (creates) an ability entity on the map.",
  sCategory = "Ability",
  iCreateDate = GetDays(2008, 11, 4)
})
DefineAction({
  sName = "AbilitySpawnWithTag",
  sFunction = "CScriptActionEntitySpawn",
  ktScriptTagParam,       -- script_tag
  "",                     -- script_group
  ktScriptTagTargetParam, -- target_tag
  ktTeamParam,            -- player or team
  ktAbilityIdParam,       -- res id
  0,                      -- direction
  TypeAbilityWorldObject, -- type
  100,                    -- health_percent
  1,                      -- amount
  sDesc = "Spawns (creates) an ability entity on the map.",
  sCategory = "Ability",
  iCreateDate = GetDays(2008, 11, 4)
})
DefineAction({
  sName = "AbilitySpawnIntoGroup",
  sFunction = "CScriptActionEntitySpawn",
  ktScriptTagParam,       -- script_tag
  ktScriptGroupParam,     -- script_group
  ktScriptTagTargetParam, -- target_tag
  ktTeamParam,            -- player or team
  ktAbilityIdParam,       -- res id
  0,                      -- direction
  TypeAbilityWorldObject, -- type
  100,                    -- health_percent
  1,                      -- amount
  sDesc = "Spawns (creates) an ability entity on the map.",
  sCategory = "Ability",
  iCreateDate = GetDays(2008, 11, 4)
})
RegisterEnums("CScriptActionGUIEventQueueModify", {
  "EventNotify",
  "EventWarning",
  "EventSuccess",
  "EventFailed"
})
DefineAction({
  sName = "MissionEventInfo",
  sFunction = "CScriptActionGUIEventQueueModify",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  EventNotify,
  sDesc = "Puts a message into the EventQueue.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionEventWarning",
  sFunction = "CScriptActionGUIEventQueueModify",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  EventWarning,
  sDesc = "Puts a message into the EventQueue.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionEventSuccess",
  sFunction = "CScriptActionGUIEventQueueModify",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  EventSuccess,
  sDesc = "Puts a message into the EventQueue.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionEventFailure",
  sFunction = "CScriptActionGUIEventQueueModify",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  EventFailed,
  sDesc = "Puts a message into the EventQueue.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MiniMapAlert",
  sFunction = "CScriptActionGUIMiniMapAlert",
  ktScriptTagTargetParam,
  ktMiniMapAlertTypeParam,
  sDesc = "Show an Alert on MiniMap at the specified TargetTag location.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 19)
})
DefineAction({
  sName = "MissionOutcry",
  sFunction = "CScriptActionGUIMissionOutcry",
  ktScriptTagPlayerParam,
  ktScriptTagParam,
  ktTextTagParam,
  ktTextParam,
  -1,
  ktDurationSecondsParam,
  ktPortraitFileNameParam,
  sDesc = "Displays a voiced text outside a Cutscene. Several Outcries can be queued as a conversation.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 10, 23)
})
RegisterEnums("CScriptActionGUIMissionTaskModify", {
  "TaskInProgress",
  "TaskSuccess",
  "TaskFailed",
  "TaskDispose",
  "TaskOnlyTransferXP"
})
DefineAction({
  sName = "MissionTaskSetActive",
  sFunction = "CScriptActionGUIMissionTaskModify",
  ktScriptTagPlayerParam,
  ktMissionTaskTagParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  TaskInProgress,
  sDesc = "Adds, removes or modifies a Mission Task.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionTaskSetSolved",
  sFunction = "CScriptActionGUIMissionTaskModify",
  ktScriptTagPlayerParam,
  ktMissionTaskTagParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  TaskSuccess,
  sDesc = "Adds, removes or modifies a Mission Task.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionTaskSetFailed",
  sFunction = "CScriptActionGUIMissionTaskModify",
  ktScriptTagPlayerParam,
  ktMissionTaskTagParam,
  ktScriptTagTargetParam,
  ktShortTextParam,
  ktDetailedTextParam,
  TaskFailed,
  sDesc = "Adds, removes or modifies a Mission Task.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionTaskRemove",
  sFunction = "CScriptActionGUIMissionTaskModify",
  ktScriptTagPlayerParam,
  ktMissionTaskTagParam,
  "",
  "",
  "",
  TaskDispose,
  sDesc = "Adds, removes or modifies a Mission Task.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 9, 26)
})
DefineAction({
  sName = "MissionTaskOnlyTransferXP",
  sFunction = "CScriptActionGUIMissionTaskModify",
  "sg_AllPlayers",
  ktMissionTaskTagParam,
  "",
  "",
  "",
  TaskOnlyTransferXP,
  sDesc = "Adds, removes or modifies a Mission Task.",
  sCategory = "Mission",
  iCreateDate = GetDays(2009, 2, 12)
})
DefineAction({
  sName = "TutorialDeckSlotDisable",
  sFunction = "CScriptActionGUITutorialCardSlotEnable",
  ktSlotParam,
  0,
  sDesc = "Disables the given Deck Slot.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 11, 12)
})
DefineAction({
  sName = "TutorialDeckSlotEnable",
  sFunction = "CScriptActionGUITutorialCardSlotEnable",
  ktSlotParam,
  1,
  sDesc = "Enables the given Deck Slot.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 11, 12)
})
DefineAction({
  sName = "TutorialMarkerAdd",
  sFunction = "CScriptActionGUITutorialMarker",
  ktTutorialMarkerTagParam,
  ktWidgetNameParam,
  ktMarkerFileParam,
  "",
  0,
  ktTutorialMarkerPositionParam,
  sDesc = "Adds a new tutorial marker or moves an existing one with the same tag.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineAction({
  sName = "TutorialDetailMarkerAdd",
  sFunction = "CScriptActionGUITutorialMarker",
  ktTutorialMarkerTagParam,
  ktWidgetNameParam,
  ktMarkerFileParam,
  ktDetailFileParam,
  0,
  ktTutorialMarkerPositionParam,
  sDesc = "Adds a new tutorial marker or moves an existing one with the same tag, with detail info.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineAction({
  sName = "TutorialMarkerRemove",
  sFunction = "CScriptActionGUITutorialMarker",
  ktTutorialMarkerTagParam,
  "",
  "",
  "",
  1,
  0,
  sDesc = "Removes a tutorial marker.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineAction({
  sName = "MapReset",
  sFunction = "CScriptActionMapReset",
  sDesc = "Resets the map, all Scripts and Variables (Sandbox restart).",
  sCategory = "Special",
  iCreateDate = GetDays(2008, 6, 25),
  bDocumentationDontShow = true
})
DefineAction({
  sName = "AudioMusicPlay",
  sFunction = "CScriptActionMusicPlay",
  ktMusicFileParam,
  sDesc = "Plays the specified Music Track.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 9, 22)
})
for _FORV_15_, _FORV_16_ in ipairs({"Network"}) do
  for _FORV_20_, _FORV_21_ in ipairs({
    "SetTrue",
    "SetFalse",
    "Toggle"
  }) do
    local params1 = {
        1,
        0,
        0
      }
    local params2 = {
        Assign,
        Assign,
        Toggle
      }
    local params3 = {
        "Changes the flag accordingly.",
        "Changes the flag accordingly.",
        "Toggles the flag, eg if it is true it will be set to false and vice versa."
      }
    DefineAction({
      sName = _FORV_16_ .. "Flag" .. _FORV_21_,
      sFunction = "CScriptActionNetworkVariableModify",
      _G["kt" .. _FORV_16_ .. "FlagNameParam"],
      params1[_FORV_20_],
      params2[_FORV_20_],
      sDesc = params3[_FORV_20_],
      sCategory = "Variable",
      iCreateDate = GetDays(2008, 9, 1)
    })
  end
end
for _FORV_19_, _FORV_20_ in ipairs({"Network"}) do
  for _FORV_24_, _FORV_25_ in ipairs({
    "Set",
    "Add",
    "Subtract",
    "Increase",
    "Decrease"
  }) do
    local params1 = {
        ktValueParam,
        ktValueParam,
        ktValueParam,
        1,
        1
      }
    local params2 = {
        Assign,
        Add,
        Subtract,
        Add,
        Subtract
      }
    local params3 = {
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Increases the value by one, shorthand for Add with Value 1.",
        "Decreases the value by one, shorthand for Subtract with Value 1."
      }
    DefineAction({
      sName = _FORV_20_ .. "Value" .. _FORV_25_,
      sFunction = "CScriptActionNetworkVariableModify",
      _G["kt" .. _FORV_20_ .. "ValueNameParam"],
      params1[_FORV_24_],
      params2[_FORV_24_],
      sDesc = params3[_FORV_24_],
      sCategory = "Variable",
      iCreateDate = GetDays(2008, 9, 1)
    })
  end
end
RegisterEnums("CScriptActionOctoberVersionOnlyEntityDoDamageInRange", {
  "DamageInRangeRadius",
  "DamageInRangeRect",
  "DamageInRangeRay"
})
DefineAction({
  sName = "OctoberVersionOnly_DoDamageToTeamSquadInRange",
  sFunction = "CScriptActionOctoberVersionOnlyEntityDoDamageInRange",
  ktTeamParam,
  "",
  ktScriptTagTargetParam,
  ktRangeParam,
  ktAmountParam,
  DamageInRangeRadius,
  sDesc = "Does damage to Squads in Range.",
  sCategory = "OctoberVersionOnly",
  iCreateDate = GetDays(2007, 10, 1)
})
DefineAction({
  sName = "OctoberVersionOnly_DoDamageToTeamSquadInRect",
  sFunction = "CScriptActionOctoberVersionOnlyEntityDoDamageInRange",
  ktTeamParam,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  0,
  ktAmountParam,
  DamageInRangeRect,
  sDesc = "Does damage to Squads in Range.",
  sCategory = "OctoberVersionOnly",
  iCreateDate = GetDays(2007, 10, 1)
})
DefineAction({
  sName = "OctoberVersionOnly_DoDamageToTeamSquadInRay",
  sFunction = "CScriptActionOctoberVersionOnlyEntityDoDamageInRange",
  ktTeamParam,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  0,
  ktAmountParam,
  DamageInRangeRay,
  sDesc = "Does damage to Squads in Range.",
  sCategory = "OctoberVersionOnly",
  iCreateDate = GetDays(2007, 10, 9)
})
DefineAction({
  sName = "BarrierBuildOrRepair",
  sFunction = "CScriptActionQuantorBarrierBuildOrRepair",
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  sDesc = "Builds the specified BarrierSet and gives it to the Player, or repairs it if the Player owns that BarrierSet.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineAction({
  sName = "EntityAbilityAdd",
  sFunction = "CScriptActionQuantorEntityAbilityAddRemove",
  ktScriptTagParam,
  ktAbilityIdParam,
  0,
  ktAbilityAddToFiguresParam,
  sDesc = "Adds or removes Abilities of an entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 11)
})
DefineAction({
  sName = "EntityAbilityRemove",
  sFunction = "CScriptActionQuantorEntityAbilityAddRemove",
  ktScriptTagParam,
  ktAbilityIdParam,
  1,
  ktAbilityAddToFiguresParam,
  sDesc = "Adds or removes Abilities of an entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 11)
})
DefineAction({
  sName = "EntitySpellCast",
  sFunction = "CScriptActionQuantorEntityCastSpell",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktSpellIdParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the TargetEntity. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 29)
})
DefineAction({
  sName = "EntitySpellCastOnClosestSquad",
  sFunction = "CScriptActionQuantorEntityCastSpell",
  ktScriptTagParam,
  ktSpellIdParam,
  0,
  CollectorSquadsInRange,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Squad. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 17)
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestPlayerSquad",
  ktScriptTagParam,
  ktSpellIdParam,
  ktPlayerNoQuantorParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Squad. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 17),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestSquad({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestTeamSquad",
  ktScriptTagParam,
  ktSpellIdParam,
  ktTeamParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Squad. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 17),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestSquad({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "EntitySpellCastOnClosestBuilding",
  sFunction = "CScriptActionQuantorEntityCastSpell",
  ktScriptTagParam,
  ktSpellIdParam,
  0,
  CollectorBuildingsInRange,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Building. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 20)
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestPlayerBuilding",
  ktScriptTagParam,
  ktSpellIdParam,
  ktPlayerNoQuantorParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Building. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 20),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestBuilding({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestTeamBuilding",
  ktScriptTagParam,
  ktSpellIdParam,
  ktTeamParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Building. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 20),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestBuilding({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "EntitySpellCastOnClosestGenerator",
  sFunction = "CScriptActionQuantorEntityCastSpell",
  ktScriptTagParam,
  ktSpellIdParam,
  ktIgnoreEmptyGeneratorsParam,
  CollectorGeneratorsInRange,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Power Generator. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 6, 25)
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestPlayerGenerator",
  ktScriptTagParam,
  ktSpellIdParam,
  ktIgnoreEmptyGeneratorsParam,
  ktPlayerNoQuantorParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Generator. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 6, 25),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestGenerator({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId,
        IgnoreEmpty = _ARG_0_.IgnoreEmpty
      }),
      Filters = tFilters,
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestTeamGenerator",
  ktScriptTagParam,
  ktSpellIdParam,
  ktIgnoreEmptyGeneratorsParam,
  ktTeamParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest Generator. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 6, 25),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntitySpellCastOnClosestGenerator({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId,
        IgnoreEmpty = _ARG_0_.IgnoreEmpty
      }),
      Filters = tFilters,
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "EntitySpellCastOnClosestTarget",
  sFunction = "CScriptActionQuantorEntityCastSpell",
  ktScriptTagParam,
  ktSpellIdParam,
  0,
  CollectorAttackableInRange,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest attackable Target. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 4, 23)
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestPlayerTarget",
  ktScriptTagParam,
  ktSpellIdParam,
  ktPlayerNoQuantorParam,
  ktIgnoreUnattackableParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest attackable Target. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 4, 23),
  fCommand = function(_ARG_0_, _ARG_1_)
    if _ARG_0_.IgnoreUnattackable == 1 or _ARG_0_.IgnoreUnattackable == true then
      table.insert({
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }, FilterEntityHasNotAbilityLine({AbilityLineId = 9}))
    end
    return CreateFilterCommand({
      EntitySpellCastOnClosestTarget({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "EntitySpellCastOnClosestTeamTarget",
  ktScriptTagParam,
  ktSpellIdParam,
  ktTeamParam,
  ktIgnoreUnattackableParam,
  sDesc = "Lets the Entity(s) cast the given Spell on the closest attackable Target. Target must be valid for the given Spell.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 4, 23),
  fCommand = function(_ARG_0_, _ARG_1_)
    if _ARG_0_.IgnoreUnattackable == 1 or _ARG_0_.IgnoreUnattackable == true then
      table.insert({
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }, FilterEntityHasNotAbilityLine({AbilityLineId = 9}))
    end
    return CreateFilterCommand({
      EntitySpellCastOnClosestTarget({
        Tag = _ARG_0_.Tag,
        SpellId = _ARG_0_.SpellId
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
for _FORV_5_, _FORV_6_ in ipairs({
  "Squad",
  "Object",
  "Building"
}) do
  DefineAction({
    sName = _FORV_6_ .. "TypeChange",
    sFunction = "CScriptActionQuantorEntityChange",
    ktScriptTagParam,
    _G["kt" .. _FORV_6_ .. "IdParam"],
    sDesc = "Changes the type of an entity.",
    sCategory = _FORV_6_,
    iCreateDate = GetDays(2007, 9, 10)
  })
end
DefineAction({
  sName = "EntityPlayerSet",
  sFunction = "CScriptActionQuantorEntityChangeOwner",
  ktScriptTagParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Sets the Player (Owner) the entity belongs to, changing who can select & control the entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 4)
})
DefineAction({
  sName = "EntityTeamSet",
  sFunction = "CScriptActionQuantorEntityChangeTeam",
  ktScriptTagParam,
  ktTeamParam,
  sDesc = "Sets the Team the entity belongs to, possibly changing it's friend-foe relationships.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 3)
})
DefineAction({
  sName = "EntityDirectionSet",
  sFunction = "CScriptActionQuantorEntityDirectionModify",
  ktScriptTagParam,
  "",
  ktDirectionParam,
  ktVariationParam,
  0,
  sDesc = "Changes Direction of an Entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineAction({
  sName = "EntityDirectionSetRelative",
  sFunction = "CScriptActionQuantorEntityDirectionModify",
  ktScriptTagParam,
  "",
  ktDirectionParam,
  ktVariationParam,
  1,
  sDesc = "Changes Direction of an Entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineAction({
  sName = "EntityDirectionLookAtTarget",
  sFunction = "CScriptActionQuantorEntityDirectionModify",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  0,
  ktVariationParam,
  0,
  sDesc = "Changes Direction of an Entity.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineAction({
  sName = "EffectEnable",
  sFunction = "CScriptActionQuantorEntityEffectEnableDisable",
  ktScriptTagParam,
  0,
  sDesc = "Enables (starts) the editor-placed effect.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 28)
})
DefineAction({
  sName = "EffectDisable",
  sFunction = "CScriptActionQuantorEntityEffectEnableDisable",
  ktScriptTagParam,
  1,
  sDesc = "Disables (stops) the editor-placed effect.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 28)
})
DefineAction({
  sName = "EffectToggle",
  sFunction = "CScriptActionQuantorEntityEffectEnableDisable",
  ktScriptTagParam,
  2,
  sDesc = "Toggles the editor-placed effect.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 28)
})
DefineAction({
  sName = "EffectPowerSet",
  sFunction = "CScriptActionQuantorEntityEffectPowerModify",
  ktScriptTagParam,
  ktEffectPercentParam,
  Assign,
  sDesc = "Sets the Effect's 'power' to the specified percentage.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 27)
})
DefineAction({
  sName = "EffectPowerAdd",
  sFunction = "CScriptActionQuantorEntityEffectPowerModify",
  ktScriptTagParam,
  ktEffectPercentParam,
  Add,
  sDesc = "Adds this percentage to the Effect's 'power'. Will never go over 100%.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 27)
})
DefineAction({
  sName = "EffectPowerSubtract",
  sFunction = "CScriptActionQuantorEntityEffectPowerModify",
  ktScriptTagParam,
  ktEffectPercentParam,
  Subtract,
  sDesc = "Subtracts this percentage from the Effect's 'power'. Will never go below 5%.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 5, 27)
})
DefineAction({
  sName = "EffectStart",
  sFunction = "CScriptActionQuantorEntityEffectStart",
  ktScriptTagParam,
  "",
  ktEffectFileParam,
  ktEffectPercentParam,
  sDesc = "Starts the effect on the TargetTag entities.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "EffectStartTargeted",
  sFunction = "CScriptActionQuantorEntityEffectStart",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktEffectFileParam,
  ktEffectPercentParam,
  sDesc = "Starts the effect from the Tag entities to the TargetTag entities.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "TutorialEffectStart",
  sFunction = "CScriptActionQuantorEntityEffectStart",
  ktScriptTagParam,
  ktPvETypeParam,
  ktEffectFileParam,
  255,
  sDesc = "Starts the effect on all Squads with that PvE Type.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineAction({
  sName = "EffectStopAll",
  sFunction = "CScriptActionQuantorEntityEffectStop",
  ktScriptTagParam,
  sDesc = "Stops all effects on the entities.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "TutorialEffectStopAll",
  sFunction = "CScriptActionQuantorEntityEffectStop",
  "sg_TutorialEffectsGroup",
  sDesc = "Stops all effects started with the TutorialEffect command.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 26)
})
RegisterEnums("CScriptActionQuantorEntityGateOpenClose", {
  "GateStateToggle",
  "GateStateOpen",
  "GateStateClose"
})
DefineAction({
  sName = "BarrierGateToggle",
  sFunction = "CScriptActionQuantorEntityGateOpenClose",
  ktScriptTagParam,
  GateStateToggle,
  sDesc = "Opens or closes all Gates of the BarrierSet, depending on their current state.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 1, 22)
})
DefineAction({
  sName = "BarrierGateOpen",
  sFunction = "CScriptActionQuantorEntityGateOpenClose",
  ktScriptTagParam,
  GateStateOpen,
  sDesc = "Opens all Gates of the BarrierSet.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 1, 22)
})
DefineAction({
  sName = "BarrierGateClose",
  sFunction = "CScriptActionQuantorEntityGateOpenClose",
  ktScriptTagParam,
  GateStateClose,
  sDesc = "Closes all Gates of the BarrierSet.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 1, 22)
})
if not CScriptActionQuantorEntityHealthModify then
  CScriptActionQuantorEntityHealthModify = {
    Set = 0,
    Add = 1,
    Subtract = 2
  }
end
for _FORV_7_, _FORV_8_ in ipairs({
  "Set",
  "Add",
  "Subtract"
}) do
  local params1 = {
      CScriptActionQuantorEntityHealthModify.Set,
      CScriptActionQuantorEntityHealthModify.Add,
      CScriptActionQuantorEntityHealthModify.Subtract
    }
  local params2 = {
      0,
      0,
      ktDoDamageParam
    }
  DefineAction({
    sName = "EntityHealth" .. _FORV_8_,
    sFunction = "CScriptActionQuantorEntityHealthModify",
    ktScriptTagParam,
    ktPercentParam,
    params1[_FORV_7_],
    params2[_FORV_7_],
    sDesc = "Modifies the health (lifepoints) of the entity in percentage of its max. health.",
    sCategory = "Entity",
    iCreateDate = GetDays(2007, 9, 6)
  })
end
DefineAction({
  sName = "BuildingModeChange",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  ktModeIdParam,
  0,
  0,
  sDesc = "Changes the Building's Mode. Does NOT consume Power from the owning Player.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 3, 32)
})
DefineAction({
  sName = "SquadModeChange",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  ktModeIdParam,
  0,
  0,
  sDesc = "Changes the Squad's Mode. Does NOT consume Power from the owning Player.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 32)
})
DefineAction({
  sName = "BuildingModeSetEnabled",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  0,
  0,
  sDesc = "Changes the Building's Mode to the 'enabled' mode. Does NOT consume Power from the owning Player.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "BuildingModeSetDisabled",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  1,
  0,
  sDesc = "Changes the Building's Mode to the 'disabled' mode. Does NOT consume Power from the owning Player.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "BuildingModeToggle",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  0,
  1,
  sDesc = "Toggles the Building's Mode to 'enabled' or 'disabled'. Does NOT consume Power from the owning Player.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "SquadModeSetEnabled",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  0,
  0,
  sDesc = "Changes the Squad's Mode to the 'enabled' mode. Does NOT consume Power from the owning Player.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "SquadModeSetDisabled",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  1,
  0,
  sDesc = "Changes the Squad's Mode to the 'disabled' mode. Does NOT consume Power from the owning Player.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "SquadModeToggle",
  sFunction = "CScriptActionQuantorEntityModeChange",
  ktScriptTagParam,
  0,
  0,
  1,
  sDesc = "Toggles the Squad's Mode to 'enabled' or 'disabled'. Does NOT consume Power from the owning Player.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 18)
})
DefineAction({
  sName = "SquadVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Squad from the map. Squad figures will simply disappear (no 'dying' animation).",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineAction({
  sName = "SquadKill",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Kills the Squad by damaging each Figure so they will die and play their death animation.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineFilterAction({
  sName = "SquadVanishInRange",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Removes Squads of the given ScriptGroup in range.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 10, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadVanish({
        Tag = _ARG_0_.Tag
      }),
      Filters = {
        FilterEntityIsInRange({
          TargetTag = _ARG_0_.TargetTag,
          Range = _ARG_0_.Range
        })
      }
    })
  end
})
DefineAction({
  sName = "ObjectVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Object from the map.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineAction({
  sName = "ObjectDestroy",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Destroys the Object, Object MUST be destructible!",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineAction({
  sName = "BuildingVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Building from the map.",
  sCategory = "Building",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineAction({
  sName = "BuildingDestroy",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Destroys the Building.",
  sCategory = "Building",
  iCreateDate = GetDays(2007, 8, 22)
})
DefineAction({
  sName = "BarrierVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Barrier from the map.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineAction({
  sName = "BarrierDestroy",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Destroys the Barrier.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineAction({
  sName = "EffectVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the effect entity (editor placed effects) from the map.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 3, 19)
})
DefineAction({
  sName = "EntityVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Entity from the map. Works with all supported entity types.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 19)
})
DefineAction({
  sName = "EntityKill",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Kills or destroys the Entity, entity MUST be destructible! Works with all supported entity types.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 3, 19)
})
DefineAction({
  sName = "MonumentVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Monument from the map.",
  sCategory = "Generator & Monument",
  iCreateDate = GetDays(2008, 12, 10)
})
DefineAction({
  sName = "GeneratorVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Generator from the map.",
  sCategory = "Generator & Monument",
  iCreateDate = GetDays(2008, 12, 10)
})
DefineAction({
  sName = "AbilityVanish",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByVanish,
  sDesc = "Removes the Ability from the map. Works with all supported entity types.",
  sCategory = "Ability",
  iCreateDate = GetDays(2008, 11, 4)
})
DefineAction({
  sName = "AbilityDestroy",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  sDesc = "Kills or destroys the Ability regularly.",
  sCategory = "Ability",
  iCreateDate = GetDays(2008, 11, 4)
})
DefineAction({
  sName = "EntityKillInRange",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagTargetParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  ktRangeParam,
  CollectorAllInRange,
  sDesc = "Kills all entities in range. Should not be used! Basis for filtered Actions.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 9, 8)
})
DefineAction({
  sName = "BuildingKillInRange",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagTargetParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  ktRangeParam,
  CollectorBuildingsInRange,
  sDesc = "Kills all Buildings in range.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 9, 8)
})
DefineAction({
  sName = "SquadKillInRange",
  sFunction = "CScriptActionQuantorEntityRemove",
  ktScriptTagTargetParam,
  CScriptActionQuantorEntityRemove.RemoveByKill,
  ktRangeParam,
  CollectorSquadsInRange,
  sDesc = "Kills all Squads in range.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 9, 8)
})
DefineFilterAction({
  sName = "EntityPvETypeKillInRange",
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  sDesc = "Kills entities with specified PvEType in range.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 9, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntityKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "TeamBuildingKillInRange",
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Kills buildings in range.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 9, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      BuildingKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "PlayerBuildingKillInRange",
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Kills buildings in range.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 9, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      BuildingKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "TeamBuildingKillableKillInRange",
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Kills buildings in range, unless it is unkillable.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 9, 11),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      BuildingKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        }),
        FilterEntityHasNotAbilityLine({
          AbilityLineId = AbilityLine.UnKillable
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "PlayerBuildingKillableKillInRange",
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Kills buildings in range, unless it is unkillable.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 9, 11),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      BuildingKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        }),
        FilterEntityHasNotAbilityLine({
          AbilityLineId = AbilityLine.UnKillable
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "TeamEntityPvETypeKillInRange",
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  sDesc = "Kills entities with specified PvEType in range.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 9, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntityKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        }),
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "PlayerEntityPvETypeKillInRange",
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  sDesc = "Kills entities with specified PvEType in range.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 9, 8),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      EntityKillInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        }),
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        })
      }
    })
  end
})
for _FORV_6_, _FORV_7_ in ipairs({
  "Squad",
  "Object",
  "Building"
}) do
  DefineAction({
    sName = _FORV_7_ .. "Respawn",
    sFunction = "CScriptActionQuantorEntityRespawn",
    ktScriptTagParam,
    ktScriptTagSpawnTargetParam,
    ktHealthPercentParam,
    sDesc = "Respawns (recreates) an entity. Entity must be dead and it must have existed before!",
    sCategory = "Squad",
    iCreateDate = GetDays(2007, 8, 28)
  })
end
DefineAction({
  sName = "EntitySetMaxHealthAbsolute",
  sFunction = "CScriptActionQuantorEntitySetMaxHealthAbsolute",
  ktScriptTagTargetParam,
  ktMaxHealthAbsoluteParam,
  sDesc = "Modifies the max health (lifepoints) of the entity in absolute number.",
  sCategory = "Entity",
  iCreateDate = GetDays(2011, 12, 6)
})
DefineAction({
  sName = "EntitySpellAdd",
  sFunction = "CScriptActionQuantorEntitySpellAdd",
  ktScriptTagParam,
  ktSpellIdParam,
  0,
  sDesc = "Adds the Spell to the Entity unless it already exists.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 4)
})
DefineAction({
  sName = "EntitySpellRemove",
  sFunction = "CScriptActionQuantorEntitySpellAdd",
  ktScriptTagParam,
  ktSpellIdParam,
  1,
  sDesc = "Removes the Spell from the Entity if it exists (otherwise command is ignored).",
  sCategory = "Entity",
  iCreateDate = GetDays(2009, 3, 17)
})
DefineAction({
  sName = "EntityTeleport",
  sFunction = "CScriptActionQuantorEntityTeleport",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  0,
  sDesc = "Instantly teleports entity(s) to TargetTag location, no questions asked.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 4)
})
DefineAction({
  sName = "CutsceneEntityTeleport",
  sFunction = "CScriptActionQuantorEntityTeleport",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  1,
  sDesc = "Instantly teleports entity(s) to TargetTag location, no questions asked. Skips 'free position' & blocking checks, allows Figure penetration.",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 10, 4)
})
DefineAction({
  sName = "SquadTeleportInRange",
  sFunction = "CScriptActionQuantorEntityTeleport",
  ktScriptTagParam,
  ktRangeParam,
  CollectorSquadsInRange,
  ktScriptTagTargetParam,
  0,
  sDesc = "Instantly teleports the Squads in range to TargetTag location.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 12, 10)
})
DefineFilterAction({
  sName = "TeamSquadTeleportInRange",
  ktScriptTagParam,
  ktRangeParam,
  ktScriptTagTargetParam,
  ktTeamParam,
  sDesc = "Teleports Squads in Range of the specified Team to the TargetTag location.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 12, 10),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadTeleportInRange({
        Tag = _ARG_0_.Tag,
        Range = _ARG_0_.Range,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "PlayerSquadTeleportInRange",
  ktScriptTagParam,
  ktRangeParam,
  ktScriptTagTargetParam,
  ktPlayerParam,
  sDesc = "Teleports Squads in Range of the specified Player to the TargetTag location.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 12, 10),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadTeleportInRange({
        Tag = _ARG_0_.Tag,
        Range = _ARG_0_.Range,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineAction({
  sName = "MissionCounterShow",
  sFunction = "CScriptActionQuantorGUICounterAdd",
  ktScriptTagPlayerParam,
  ktCounterTagParam,
  ktLocaTagParam,
  ktMaxValueParam,
  sDesc = "Shows a Mission Counter onscreen (eg 'Bosses Killed: 0/3').",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "MissionCounterHide",
  sFunction = "CScriptActionQuantorGUICounterRemove",
  ktScriptTagPlayerParam,
  ktCounterTagParam,
  sDesc = "Hides the Mission Counter from display (the counter's value itself can still be queried/modified).",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "MissionTimerStart",
  sFunction = "CScriptActionQuantorGUITimerAdd",
  ktScriptTagPlayerParam,
  ktTimerTagParam,
  ktLocaTagParam,
  ktSecondsParam,
  ktMinutesParam,
  sDesc = "Adds a Mission Timer displayed onscreen.",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "MissionTimerChange",
  sFunction = "CScriptActionQuantorGUITimerChange",
  ktScriptTagPlayerParam,
  ktTimerTagParam,
  ktTimerTagOldParam,
  ktLocaTagParam,
  sDesc = "Changes the text of an existing Mission Timer without changing the timer.",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 5, 30)
})
DefineAction({
  sName = "MissionTimerStop",
  sFunction = "CScriptActionQuantorGUITimerPauseRemove",
  ktScriptTagPlayerParam,
  ktTimerTagParam,
  TimerRemove,
  sDesc = "Removes the Mission Timer from onscreen display and stops it.",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "MissionTimerPause",
  sFunction = "CScriptActionQuantorGUITimerPauseRemove",
  ktScriptTagPlayerParam,
  ktTimerTagParam,
  TimerPause,
  sDesc = "Pauses the Mission Timer if not already paused. NOT YET IMPLEMENTED - please ask if needed!",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "MissionTimerResume",
  sFunction = "CScriptActionQuantorGUITimerPauseRemove",
  ktScriptTagPlayerParam,
  ktTimerTagParam,
  TimerResume,
  sDesc = "Resumes a Mission Timer if it is currently paused. NOT YET IMPLEMENTED - please ask if needed!",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineAction({
  sName = "PlayerGameOver",
  sFunction = "CScriptActionQuantorPlayerGameOver",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktMessageBoxMessageParam,
  0,
  sDesc = "GameOver man, GameOver!",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "PlayerGameWon",
  sFunction = "CScriptActionQuantorPlayerGameOver",
  ktScriptTagPlayerParam,
  "",
  "",
  1,
  sDesc = "GameOver man, GameOver!",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "PlayerGameOverCheckEnabled",
  sFunction = "CScriptActionQuantorPlayerGameOverCheckEnable",
  ktScriptTagPlayerParam,
  1,
  sDesc = "Enables the GameOver check.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 8, 25)
})
DefineAction({
  sName = "PlayerGameOverCheckDisabled",
  sFunction = "CScriptActionQuantorPlayerGameOverCheckEnable",
  ktScriptTagPlayerParam,
  0,
  sDesc = "Disables the GameOver check.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 8, 25)
})
DefineAction({
  sName = "PlayerPowerGive",
  sFunction = "CScriptActionQuantorPlayerPowerAddRemove",
  ktScriptTagPlayerParam,
  ktAmountParam,
  0,
  sDesc = "Gives Power to or takes Power from a Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 9, 11)
})
DefineAction({
  sName = "PlayerPowerTake",
  sFunction = "CScriptActionQuantorPlayerPowerAddRemove",
  ktScriptTagPlayerParam,
  ktAmountParam,
  1,
  sDesc = "Gives Power to or takes Power from a Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 9, 11)
})
DefineAction({
  sName = "PlayerGeneratorActivate",
  sFunction = "CScriptActionQuantorSlotBuild",
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  1,
  0,
  0,
  sDesc = "Builds a Generator so it generates energy for that Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 6, 26)
})
DefineAction({
  sName = "PlayerMonumentActivate",
  sFunction = "CScriptActionQuantorSlotBuild",
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  1,
  0,
  ktPowerParam,
  sDesc = "Builds an Orb on a Monument for that Player.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 6, 26)
})
DefineAction({
  sName = "PlayerGeneratorDeactivate",
  sFunction = "CScriptActionQuantorSlotBuild",
  ktScriptTagParam,
  "",
  1,
  1,
  0,
  sDesc = "Crushes Generator so it stops producing Energy.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 6, 26)
})
DefineAction({
  sName = "PlayerMonumentDeactivate",
  sFunction = "CScriptActionQuantorSlotBuild",
  ktScriptTagParam,
  "",
  1,
  1,
  0,
  sDesc = "Destroys the Orb of a Monument.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 6, 26)
})
DefineAction({
  sName = "SquadAttack",
  sFunction = "CScriptActionQuantorSquadAttack",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktAttackGroupSizeParam,
  0,
  sDesc = "Squad attacks the specified TargetTag entity (even if it is a friendly entity). TargetTag must be alive and attackable!",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 9, 28)
})
DefineAction({
  sName = "SquadAttackForced",
  sFunction = "CScriptActionQuantorSquadAttack",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktAttackGroupSizeParam,
  1,
  sDesc = "Squad attacks the specified TargetTag entity (even if it is a friendly entity). Uses Forced Goto to get to Target.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 8, 18)
})
DefineAction({
  sName = "PlayerSquadCheer",
  sFunction = "CScriptActionQuantorSquadCheer",
  ktScriptTagPlayerParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Squads in range play their Cheer Anim/FX!",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 6, 13)
})
DefineAction({
  sName = "SquadBarrierMount",
  sFunction = "CScriptActionQuantorSquadEnterExitBarrier",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  0,
  sDesc = "Squad tries to mount TargetTag barrier (module, set or scriptgroup containing modules).",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 13)
})
DefineFilterAction({
  sName = "SquadRangedBarrierMount",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  sDesc = "Only ranged Squads try to mount the TargetTag Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadBarrierMount({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterSquadIsPrimaryRanged({})
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdleBarrierMount",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  sDesc = "Only idle Squads try to mount the TargetTag Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadBarrierMount({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadRangedIdleBarrierMount",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  sDesc = "Only ranged idle Squads try to mount the TargetTag Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadBarrierMount({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterSquadIsIdle({}),
        FilterSquadIsPrimaryRanged({})
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdBarrierMount",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktSquadIdParam,
  sDesc = "Only Squads with specified database ID try to mount the TargetTag Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadBarrierMount({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityHasResId({
          Id = _ARG_0_.SquadId
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdIdleBarrierMount",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktSquadIdParam,
  sDesc = "Only idle Squads with specified database ID try to mount the TargetTag Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadBarrierMount({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityHasResId({
          Id = _ARG_0_.SquadId
        }),
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineAction({
  sName = "SquadBarrierDismount",
  sFunction = "CScriptActionQuantorSquadEnterExitBarrier",
  ktScriptTagParam,
  "",
  1,
  sDesc = "Squad dismounts barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 13)
})
DefineAction({
  sName = "SquadFlee",
  sFunction = "CScriptActionQuantorSquadFlee",
  ktScriptTagParam,
  ktDistanceParam,
  ktSearchRadiusParam,
  sDesc = "Squads flee from nearest enemy entity by using forced Goto in the opposite Direction.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 6, 12)
})
DefineAction({
  sName = "SquadPatrol",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Patrol,
  1,
  0,
  -1,
  0,
  0,
  sDesc = "Squad walks waypoints back and forth continously. Continues patrol after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 23)
})
DefineAction({
  sName = "SquadGoto",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  0,
  ktDirectionParam,
  0,
  0,
  sDesc = "Squad goes towards TargetTag or along TargetTag-ScriptGroup waypoints. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 23)
})
DefineAction({
  sName = "SquadGotoForced",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Force,
  ktWalkModeRunParam,
  0,
  ktDirectionParam,
  0,
  0,
  sDesc = "Same as SquadGoto but Squad does not attack or follow attackers until it has reached its destination. <b>Use with care!</b>",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 23)
})
DefineAction({
  sName = "CutsceneSquadGoto",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  1,
  ktDirectionParam,
  0,
  0,
  sDesc = "Squad goes to target in straight line (stops when blocked, walks through other figures). <b>Use only for Cutscenes!</b>",
  sCategory = "Cutscene",
  iCreateDate = GetDays(2007, 8, 23)
})
DefineAction({
  sName = "SquadGotoFight",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  0,
  ktDirectionParam,
  1,
  0,
  sDesc = "Squad goes to any TargetTag Squad that is in a fight.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 17)
})
DefineFilterAction({
  sName = "SquadIdleGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  ktDirectionParam,
  sDesc = "Only Squads that are currently idle execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGoto({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Direction = _ARG_0_.Direction
      }),
      Filters = {
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoRandom",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  0,
  -1,
  0,
  1,
  sDesc = "Squad goes randomly towards ONE of the TargetTags.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineFilterAction({
  sName = "SquadIdleGotoRandom",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  sDesc = "Only Squads that are currently idle execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoRandom({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Run = _ARG_0_.Run
      }),
      Filters = {
        FilterSquadIsIdle({})
      },
      TargetFilters = {
        FilterEntityIsNotInRange({
          TargetTag = _ARG_0_.Tag or GetScriptTag(),
          Range = 10
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdleGotoFight",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  ktDirectionParam,
  sDesc = "Only Squads that are currently idle execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 17),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoFight({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Direction = _ARG_0_.Direction
      }),
      Filters = {
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadRandomGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  ktDirectionParam,
  ktPercentParam,
  sDesc = "Only a random amount of Squads execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGoto({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Direction = _ARG_0_.Direction
      }),
      Filters = {
        FilterEntityIsAlive({}),
        FilterIsRandomPercent({
          Percent = _ARG_0_.Percent
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdleRandomGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  ktDirectionParam,
  ktPercentParam,
  sDesc = "Only a random amount of Squads that are currently idle execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGoto({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Direction = _ARG_0_.Direction
      }),
      Filters = {
        FilterSquadIsIdle({}),
        FilterIsRandomPercent({
          Percent = _ARG_0_.Percent
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadRandomGotoRandom",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktPercentParam,
  sDesc = "Only randomly picked Squads that are execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoRandom({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Run = _ARG_0_.Run
      }),
      Filters = {
        FilterEntityIsAlive({}),
        FilterIsRandomPercent({
          Percent = _ARG_0_.Percent
        })
      },
      TargetFilters = {
        FilterEntityIsNotInRange({
          TargetTag = _ARG_0_.Tag,
          Range = 10
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdleRandomGotoRandom",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktPercentParam,
  sDesc = "Only randomly picked Squads that are execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 14),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoRandom({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Run = _ARG_0_.Run
      }),
      Filters = {
        FilterSquadIsIdle({}),
        FilterIsRandomPercent({
          Percent = _ARG_0_.Percent
        })
      },
      TargetFilters = {
        FilterEntityIsNotInRange({
          TargetTag = _ARG_0_.Tag or GetScriptTag(),
          Range = 10
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadAmountPatrol",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktAmountParam,
  sDesc = "Only the specified amount of Squads in the ScriptGroup execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadPatrol({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityAmountIsLessOrEqual({
          Amount = _ARG_0_.Amount
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadIdleAmountPatrol",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktAmountParam,
  sDesc = "Only the specified amount of Squads and if they are idle execute the Goto command.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 16),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadPatrol({
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag
      }),
      Filters = {
        FilterEntityAmountIsLessOrEqual({
          Amount = _ARG_0_.Amount
        }),
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoClosestMonumentOrGenerator",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  0,
  CollectorMonumentsAndGeneratorsInRange,
  sDesc = "Squad(s) go towards closest Monument or Generator. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15)
})
DefineFilterAction({
  sName = "SquadGotoClosestPlayerMonumentOrGenerator",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Squad(s) go towards closest M/G owned by Player. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestMonumentOrGenerator({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadGotoClosestTeamMonumentOrGenerator",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktTeamParam,
  sDesc = "Squad(s) go towards closest M/G owned by Team. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestMonumentOrGenerator({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoClosestSquad",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  0,
  CollectorSquadsInRange,
  sDesc = "Squad(s) go towards closest Squad. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15)
})
DefineFilterAction({
  sName = "SquadGotoClosestPlayerSquad",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Squad(s) go towards closest Squad owned by Player. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestSquad({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadGotoClosestTeamSquad",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktTeamParam,
  sDesc = "Squad(s) go towards closest Squad owned by Team. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestSquad({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoClosestBuilding",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  0,
  CollectorBuildingsInRange,
  sDesc = "Squad(s) go towards closest Building. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15)
})
DefineFilterAction({
  sName = "SquadGotoClosestPlayerBuilding",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Squad(s) go towards closest Building owned by Player. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestBuilding({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadGotoClosestTeamBuilding",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktTeamParam,
  sDesc = "Squad(s) go towards closest Building owned by Team. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestBuilding({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoClosestBarrier",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  0,
  CollectorBarriersInRange,
  sDesc = "Squad(s) go towards closest Barrier. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15)
})
DefineFilterAction({
  sName = "SquadGotoClosestPlayerBarrier",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Squad(s) go towards closest Barrier owned by Player. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestBarrier({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadGotoClosestTeamBarrier",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktTeamParam,
  sDesc = "Squad(s) go towards closest Barrier owned by Team. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 15),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadGotoClosestBarrier({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadGotoClosestTarget",
  sFunction = "CScriptActionQuantorSquadGoto",
  ktScriptTagParam,
  tWalkMode.Normal,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  0,
  CollectorAttackableInRange,
  sDesc = "Squad(s) go towards closest attackable Target. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 7, 1)
})
DefineFilterAction({
  sName = "SquadGotoClosestPlayerTarget",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktScriptTagPlayerNoQuantorParam,
  ktIgnoreUnattackableParam,
  sDesc = "Squad(s) go towards closest attackable Target owned by Player. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 7, 1),
  fCommand = function(_ARG_0_, _ARG_1_)
    if _ARG_0_.IgnoreUnattackable == 1 or _ARG_0_.IgnoreUnattackable == true then
      table.insert({
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }, FilterEntityHasNotAbilityLine({AbilityLineId = 9}))
    end
    return CreateFilterCommand({
      SquadGotoClosestTarget({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "SquadGotoClosestTeamTarget",
  ktScriptTagParam,
  ktWalkModeRunParam,
  ktSearchRadiusParam,
  ktTeamParam,
  ktIgnoreUnattackableParam,
  sDesc = "Squad(s) go towards closest attackable Target owned by Team. Continues goto after being attacked.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 7, 1),
  fCommand = function(_ARG_0_, _ARG_1_)
    if _ARG_0_.IgnoreUnattackable == 1 or _ARG_0_.IgnoreUnattackable == true then
      table.insert({
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }, FilterEntityHasNotAbilityLine({AbilityLineId = 9}))
    end
    return CreateFilterCommand({
      SquadGotoClosestTarget({
        Tag = _ARG_0_.Tag,
        Run = _ARG_0_.Run,
        SearchRadius = _ARG_0_.SearchRadius
      }),
      Filters = {},
      TargetFilters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadHoldPosition",
  sFunction = "CScriptActionQuantorSquadHoldPosition",
  ktScriptTagParam,
  sDesc = "Squad holds position, will attack if enemies are in attack range but will not pursue enemies.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 9, 21)
})
DefineAction({
  sName = "SquadGotoMapBorder",
  sFunction = "CScriptActionQuantorSquadMapBorderGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  sDesc = "Squad walks to Target from outside map border. Target must be a ScriptMarker and close to MapBorder.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 9, 26)
})
DefineAction({
  sName = "SquadAnimPlay",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathFigureAnim,
  ktUnitFolderParam,
  ktAnimFileParam,
  0,
  0,
  sDesc = "Plays the animation once for all figures in the Squad.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "SquadAnimPlayLooped",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathFigureAnim,
  ktUnitFolderParam,
  ktAnimFileParam,
  1,
  0,
  sDesc = "Plays the animation continuously for all figures in the Squad.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 2)
})
DefineAction({
  sName = "ObjectAnimPlay",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathObjectAnim,
  "",
  ktAnimFileParam,
  0,
  1,
  sDesc = "Plays the animation once on the Object.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 10, 9)
})
DefineAction({
  sName = "ObjectAnimPlayLooped",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathObjectAnim,
  "",
  ktAnimFileParam,
  1,
  1,
  sDesc = "Plays the animation continuously on the Object.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 10, 9)
})
DefineAction({
  sName = "BuildingAnimPlay",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathBuildingAnim,
  "",
  ktAnimFileParam,
  0,
  1,
  sDesc = "Plays the Building animation once.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 22)
})
DefineAction({
  sName = "BuildingAnimPlayLooped",
  sFunction = "CScriptActionQuantorSquadPlayAnim",
  ktScriptTagParam,
  ksBasePathBuildingAnim,
  "",
  ktAnimFileParam,
  1,
  1,
  sDesc = "Plays the Building animation in a loop.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 22)
})
DefineAction({
  sName = "SquadInRangePushBack",
  sFunction = "CScriptActionQuantorSquadPushBack",
  ktScriptTagTargetParam,
  ktRangeParam,
  CollectorSquadsInRange,
  0,
  math.pi,
  ktPushBackSpeedParam,
  ktPushBackMaxRadiusTypeParam,
  sDesc = "Squad figures are getting pushed back, the epicentre of the pushback is at the TargetTag location.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 23)
})
DefineFilterAction({
  sName = "TeamSquadInRangePushBack",
  ktScriptTagTargetParam,
  0,
  math.pi,
  ktRangeParam,
  ktPushBackSpeedParam,
  ktPushBackMaxRadiusTypeParam,
  ktTeamParam,
  sDesc = "PushBack only for Squads from the specified Team.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 22),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadInRangePushBack({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range,
        Speed = _ARG_0_.Speed,
        MaxRadiusType = _ARG_0_.MaxRadiusType
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineFilterAction({
  sName = "PlayerSquadInRangePushBack",
  ktScriptTagTargetParam,
  0,
  math.pi,
  ktRangeParam,
  ktPushBackSpeedParam,
  ktPushBackMaxRadiusTypeParam,
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "PushBack only for Squads from the specified Player.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 1, 22),
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      SquadInRangePushBack({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range,
        Speed = _ARG_0_.Speed,
        MaxRadiusType = _ARG_0_.MaxRadiusType
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineAction({
  sName = "SquadStop",
  sFunction = "CScriptActionQuantorSquadStop",
  ktScriptTagParam,
  sDesc = "Squad stops movement.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineAction({
  sName = "FogOfWarObserve",
  sFunction = "CScriptActionQuantorUnexploredReveal",
  ktScriptTagTargetParam,
  ktTeamParam,
  ktRangeParam,
  0,
  sDesc = "Starts observing TargetTag position(s), thus completely removing both Unexplored AND Fog of War indefinetely.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 3, 20)
})
DefineAction({
  sName = "FogOfWarGlanceAt",
  sFunction = "CScriptActionQuantorUnexploredReveal",
  ktScriptTagTargetParam,
  ktTeamParam,
  ktRangeParam,
  1,
  sDesc = "Takes a glance (quick look) at the TargetTag(s), thus removing only the Unexplored (targets remain shrouded in Fog of War).",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 3, 20)
})
for _FORV_14_, _FORV_15_ in ipairs({
  "SetTrue",
  "SetFalse",
  "Toggle"
}) do
  local params1 = {
      1,
      0,
      0
    }
  local params2 = {
      Assign,
      Assign,
      Toggle
    }
  local params3 = {
      "Changes the flag accordingly.",
      "Changes the flag accordingly.",
      "Toggles the flag, eg if it is true it will be set to false and vice versa."
    }
  DefineAction({
    sName = "PlayerFlag" .. _FORV_15_,
    sFunction = "CScriptActionQuantorVariableModify",
    ktScriptTagPlayerParam,
    ktPlayerFlagNameParam,
    params1[_FORV_14_],
    params2[_FORV_14_],
    sDesc = params3[_FORV_14_],
    sCategory = "Variable",
    iCreateDate = GetDays(2007, 8, 14)
  })
end
for _FORV_18_, _FORV_19_ in ipairs({
  "Set",
  "Add",
  "Subtract",
  "Increase",
  "Decrease",
  "Randomize"
}) do
  local params1 = {
      ktValueParam,
      ktValueParam,
      ktValueParam,
      1,
      1,
      ktMaxValueParam
    }
  local params2 = {
      Assign,
      Add,
      Subtract,
      Add,
      Subtract
    }
  local params3 = {
      "Changes the value accordingly.",
      "Changes the value accordingly.",
      "Changes the value accordingly.",
      "Increases the value by one, shorthand for Add with Value 1.",
      "Decreases the value by one, shorthand for Subtract with Value 1.",
      "Changes the value accordingly."
    }
  DefineAction({
    sName = "PlayerValue" .. _FORV_19_,
    sFunction = "CScriptActionQuantorVariableModify",
    ktScriptTagPlayerParam,
    ktPlayerValueNameParam,
    params1[_FORV_18_],
    params2[_FORV_18_],
    sDesc = params3[_FORV_18_],
    sCategory = "Variable",
    iCreateDate = GetDays(2007, 8, 14)
  })
end
for _FORV_22_, _FORV_23_ in ipairs({
  "Set",
  "Increase",
  "Decrease"
}) do
  local params1 = {
      ktValueParam,
      1,
      1
    }
  local params2 = {
      Assign,
      Add,
      Subtract
    }
  local params3 = {
      "Changes the value accordingly.",
      "Increases the value by one, shorthand for Add with Value 1.",
      "Decreases the value by one, shorthand for Subtract with Value 1."
    }
  DefineAction({
    sName = "MissionCounter" .. _FORV_23_,
    sFunction = "CScriptActionQuantorVariableModify",
    ktScriptTagPlayerParam,
    ktMissionCounterNameParam,
    params1[_FORV_22_],
    params2[_FORV_22_],
    sDesc = params3[_FORV_22_],
    sCategory = "Mission",
    iCreateDate = GetDays(2008, 3, 7)
  })
end
for _FORV_25_, _FORV_26_ in ipairs({"Start", "Stop"}) do
  local params1 = {TimeStampSet, TimeStampClear}
  DefineAction({
    sName = "PlayerTimer" .. _FORV_26_,
    sFunction = "CScriptActionQuantorVariableModify",
    ktScriptTagPlayerParam,
    ktPlayerTimerNameParam,
    0,
    params1[_FORV_25_],
    sDesc = "Starts/Stops a Timer.",
    sCategory = "Variable",
    iCreateDate = GetDays(2007, 8, 14)
  })
end
DefineAction({
  sName = "MissionCounterSetToHealthPercent",
  sFunction = "CScriptActionQuantorVariableSetToHealthPercent",
  ktScriptTagPlayerParam,
  ktScriptTagParam,
  ktMissionCounterNameParam,
  sDesc = "Sets the Mission Counter to the entity's health percent.",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 5, 6)
})
DefineAction({
  sName = "SquadGroupCheckpointGoto",
  sFunction = "CScriptActionScriptGroupCheckpointGoto",
  ktScriptGroupParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  sDesc = "SquadGroup goes to next Checkpoint (direct line of sight to Checkpoint is recommended).",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 18)
})
DefineAction({
  sName = "SquadCheckpointGoto",
  sFunction = "CScriptActionScriptGroupCheckpointGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktWalkModeRunParam,
  sDesc = "Squad goes to next Checkpoint (direct line of sight to Checkpoint is recommended).",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 18),
  bDocumentationDontShow = true
})
DefineAction({
  sName = "SquadGroupCheckpointGotoStop",
  sFunction = "CScriptActionScriptGroupCheckpointGotoStop",
  ktScriptGroupParam,
  sDesc = "Stops a SquadGroup that is currently on a CheckpointGoto. Group will stop at the next checkpoint they reach.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 3, 25)
})
DefineAction({
  sName = "ScriptGroupClear",
  sFunction = "CScriptActionScriptGroupClear",
  ktScriptGroupParam,
  sDesc = "Removes all tags from the ScriptGroup.",
  sCategory = "ScriptGroup",
  iCreateDate = GetDays(2008, 3, 2),
  bDocumentationDontShow = true
})
DefineAction({
  sName = "ScriptGroupTagAdd",
  sFunction = "CScriptActionScriptGroupTagAddRemove",
  ktScriptGroupParam,
  ktScriptTagParam,
  0,
  sDesc = "Adds the tag to the ScriptGroup.",
  sCategory = "ScriptGroup",
  iCreateDate = GetDays(2008, 3, 2),
  bDocumentationDontShow = true
})
DefineAction({
  sName = "ScriptGroupTagRemove",
  sFunction = "CScriptActionScriptGroupTagAddRemove",
  ktScriptGroupParam,
  ktScriptTagParam,
  1,
  sDesc = "Removes the tag from the ScriptGroup.",
  sCategory = "ScriptGroup",
  iCreateDate = GetDays(2008, 3, 2),
  bDocumentationDontShow = true
})
DefineAction({
  sName = "WeatherSet",
  sFunction = "CScriptActionSetWeather",
  ktWeatherFileParam,
  ktFadeDurationParam,
  sDesc = "Changes the weather. Similar to performing a raindance in real life.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2008, 9, 3)
})
DefineAction({
  sName = "WeatherSetUserGenerated",
  sFunction = "CScriptActionSetWeather",
  ktWeatherFileParamUserGenerated,
  ktFadeDurationParam,
  sDesc = "Changes the weather. Similar to performing a raindance in real life. Uses an absolute path instead of relative from the game directory.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2012, 12, 12)
})
DefineAction({
  sName = "AudioSoundFXPlay",
  sFunction = "CScriptActionSoundPlay",
  ktSoundFileParam,
  ktScriptTagTargetParam,
  sDesc = "Plays the specified Sound FX at the TargetTag position.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 9, 22)
})
DefineAction({
  sName = "SquadGridGoto",
  sFunction = "CScriptActionSquadGridGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktDirectionParam,
  ktGridGotoFlyingSpecialParam,
  WalkModeCrusade,
  sDesc = "Standard Goto that uses the GotoGrid.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 10, 16)
})
DefineAction({
  sName = "SquadGridPatrol",
  sFunction = "CScriptActionSquadGridGoto",
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktDirectionParam,
  0,
  WalkModePatrol,
  sDesc = "Patrol mode on path given by TargetTag (start & end point), uses GotoGrid.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 6, 6)
})
DefineAction({
  sName = "SquadSpawnRandomSelection",
  sFunction = "CScriptActionSquadSpawnRandomSelection",
  ktScriptTagParam,
  ktSquadSpawnRandomSelection,
  ktScriptTagTargetParam,
  ktTeamParam,
  "",
  ktDirectionParam,
  ktHealthPercentParam,
  sDesc = "Spawns a random selection of Squads, depending on the SpawnTable (see description for table syntax).",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "PlayerSquadSpawnRandomSelection",
  sFunction = "CScriptActionSquadSpawnRandomSelection",
  ktScriptTagParam,
  ktSquadSpawnRandomSelection,
  ktScriptTagTargetParam,
  "",
  ktScriptTagPlayerNoQuantorParam,
  ktDirectionParam,
  ktHealthPercentParam,
  sDesc = "Spawns a random selection of Squads, depending on the SpawnTable (see description for table syntax).",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "AudioSoundUIPlay",
  sFunction = "CScriptActionUISoundPlay",
  ktUISoundFileParam,
  1,
  sDesc = "Plays the specified UI Sound at the TargetTag position.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 9, 22)
})
for _FORV_15_, _FORV_16_ in ipairs({"Map", "Entity"}) do
  for _FORV_20_, _FORV_21_ in ipairs({
    "SetTrue",
    "SetFalse",
    "Toggle"
  }) do
    local params1 = {
        1,
        0,
        0
      }
    local params2 = {
        Assign,
        Assign,
        Toggle
      }
    local params3 = {
        "Changes the flag accordingly.",
        "Changes the flag accordingly.",
        "Toggles the flag, eg if it is true it will be set to false and vice versa."
      }
    DefineAction({
      sName = _FORV_16_ .. "Flag" .. _FORV_21_,
      sFunction = "CScriptActionVariableModify",
      _G["kt" .. _FORV_16_ .. "FlagNameParam"],
      params1[_FORV_20_],
      params2[_FORV_20_],
      sDesc = params3[_FORV_20_],
      sCategory = "Variable",
      iCreateDate = GetDays(2007, 8, 14)
    })
  end
end
for _FORV_19_, _FORV_20_ in ipairs({"Map", "Entity"}) do
  for _FORV_24_, _FORV_25_ in ipairs({
    "Set",
    "Add",
    "Subtract",
    "Increase",
    "Decrease",
    "Randomize"
  }) do
    local params1 = {
        ktValueParam,
        ktValueParam,
        ktValueParam,
        1,
        1,
        ktMaxValueParam
      }
    local params2 = {
        Assign,
        Add,
        Subtract,
        Add,
        Subtract,
        Random
      }
    local params3 = {
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Increases the value by one, shorthand for Add with Value 1.",
        "Decreases the value by one, shorthand for Subtract with Value 1.",
        "Changes the value accordingly."
      }
    DefineAction({
      sName = _FORV_20_ .. "Value" .. _FORV_25_,
      sFunction = "CScriptActionVariableModify",
      _G["kt" .. _FORV_20_ .. "ValueNameParam"],
      params1[_FORV_24_],
      params2[_FORV_24_],
      sDesc = params3[_FORV_24_],
      sCategory = "Variable",
      iCreateDate = GetDays(2007, 8, 14)
    })
  end
end
for _FORV_21_, _FORV_22_ in ipairs({"Map", "Entity"}) do
  for _FORV_26_, _FORV_27_ in ipairs({"Start", "Stop"}) do
    local params1 = {TimeStampSet, TimeStampClear}
    DefineAction({
      sName = _FORV_22_ .. "Timer" .. _FORV_27_,
      sFunction = "CScriptActionVariableModify",
      _G["kt" .. _FORV_22_ .. "TimerNameParam"],
      0,
      params1[_FORV_26_],
      sDesc = "Starts/Stops a Timer.",
      sCategory = "Variable",
      iCreateDate = GetDays(2007, 8, 14)
    })
  end
end
for _FORV_24_, _FORV_25_ in ipairs({"Spawn", "Death"}) do
  for _FORV_29_, _FORV_30_ in ipairs({"Reset"}) do
    DefineAction({
      sName = "ScriptGroupSquad" .. _FORV_25_ .. "Counter" .. _FORV_30_,
      sFunction = "CScriptActionVariableModify",
      _G["ktSquad" .. _FORV_25_ .. "CounterNameParam"],
      0,
      Assign,
      sDesc = "Resets the ScriptGroup's Squad" .. _FORV_25_ .. "Counter value (set to 0).",
      sCategory = "ScriptGroup",
      iCreateDate = GetDays(2008, 3, 9)
    })
  end
end
for _FORV_15_, _FORV_16_ in ipairs({"Map", "Entity"}) do
  for _FORV_20_, _FORV_21_ in ipairs({
    "SetTo",
    "Add",
    "Subtract",
    "MultiplyBy",
    "DivideBy"
  }) do
    local params1 = {
        Assign,
        Add,
        Subtract,
        Multiply,
        Divide
      }
    local params2 = {
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Changes the value accordingly.",
        "Changes the value accordingly."
      }
    DefineAction({
      sName = _FORV_16_ .. "Value" .. _FORV_21_ .. "VarValue",
      sFunction = "CScriptActionVariableModifyVariable",
      _G["kt" .. _FORV_16_ .. "Variable1Param"],
      _G["kt" .. _FORV_16_ .. "VariableActionParam"],
      params1[_FORV_20_],
      sDesc = params2[_FORV_20_],
      sCategory = "Variable",
      iCreateDate = GetDays(2008, 3, 1)
    })
  end
end
DefineAction({
  sName = "AudioVoicePlay",
  sFunction = "CScriptActionVoiceStreamPlay",
  ktVoiceFileParam,
  ktVolumeParam,
  sDesc = "Plays the specified Voice Stream.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 6)
})
DefineCondition({
  sName = "AudioAmbientIsPlaying",
  sNegated = "AudioAmbientIsNotPlaying",
  sFunction = "CScriptConditionAudioPlaying",
  0,
  ktAmbientStreamFileParam,
  AudioTypeAmbient,
  sDesc = "Checks if the respective audio file is currently playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineCondition({
  sName = "AudioMusicIsPlaying",
  sNegated = "AudioMusicIsNotPlaying",
  sFunction = "CScriptConditionAudioPlaying",
  0,
  ktMusicFileParam,
  AudioTypeMusic,
  sDesc = "Checks if the respective audio file is currently playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineCondition({
  sName = "AudioSoundFXIsPlaying",
  sNegated = "AudioSoundFXIsNotPlaying",
  sFunction = "CScriptConditionAudioPlaying",
  0,
  ktSoundFileParam,
  AudioTypeSoundFX,
  sDesc = "Checks if the respective audio file is currently playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineCondition({
  sName = "AudioSoundUIIsPlaying",
  sNegated = "AudioSoundUIIsNotPlaying",
  sFunction = "CScriptConditionAudioPlaying",
  0,
  ktUISoundFileParam,
  AudioTypeUISoundFX,
  sDesc = "Checks if the respective audio file is currently playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineCondition({
  sName = "AudioVoiceIsPlaying",
  sNegated = "AudioVoiceIsNotPlaying",
  sFunction = "CScriptConditionAudioPlaying",
  0,
  ktUISoundFileParam,
  AudioTypeVoice,
  sDesc = "Checks if the respective audio file is currently playing.",
  sCategory = "AudioVisual",
  iCreateDate = GetDays(2007, 10, 5)
})
DefineCondition({
  sName = "EntityHealthPercentIsLowest",
  sNegated = "EntityHealthPercentIsNotLowest",
  sFunction = "CScriptConditionEntityLowestHealth",
  0,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  LessOrEqual,
  sDesc = "Checks if the entity has the lowest/greatest health among the other entities.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 4)
})
DefineCondition({
  sName = "EntityHealthPercentIsGreatest",
  sNegated = "EntityHealthPercentIsNotGreatest",
  sFunction = "CScriptConditionEntityLowestHealth",
  0,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  GreaterOrEqual,
  sDesc = "Checks if the entity has the lowest/greatest health among the other entities.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 4)
})
DefineCondition({
  sName = "TutorialEntityPvETypeIsSelected",
  sNegated = "TutorialEntityPvETypeIsNotSelected",
  sFunction = "CScriptConditionEntitySelected",
  0,
  ktPvETypeParam,
  sDesc = "Checks if any entity with the specified PvE Type is in the current selection.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineCondition({
  sName = "TutorialEntityIsSelected",
  sNegated = "TutorialEntityIsNotSelected",
  sFunction = "CScriptConditionEntitySelected",
  0,
  ktScriptTagParam,
  sDesc = "Checks if any entity with the specified ScriptTag is in the current selection.",
  sCategory = "Tutorial",
  iCreateDate = GetDays(2008, 10, 7)
})
for _FORV_6_, _FORV_7_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "MissionDifficultyIs" .. _FORV_7_,
    sNegated = "MissionDifficultyIs" .. params1[_FORV_6_],
    sFunction = "CScriptConditionGameDifficulty",
    0,
    ktDifficultyParam,
    params2[_FORV_6_],
    sDesc = "Checks the game's current difficulty. Note: difficulty NEVER changes during a match!",
    sCategory = "Mission",
    iCreateDate = GetDays(2008, 3, 12)
  })
end
DefineCondition({
  sName = "AND",
  sNegated = "NotAND",
  sFunction = "",
  ktUpdateIntervalParam,
  sDesc = "All/NotAll contained Conditions must be true.",
  sCategory = "Boolean",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 14)
})
DefineCondition({
  sName = "OR",
  sNegated = "NotOR",
  sFunction = "",
  ktUpdateIntervalParam,
  sDesc = "Any/No contained Condition must be true.",
  sCategory = "Boolean",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 14)
})
--[[ -- conflicts with internal GetScriptTag, and GetScriptPath functions
DefineAction({
  sName = "GetScriptTag",
  sFunction = "",
  sDesc = "Returns the ScriptTag of the entity currently executing this script.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "GetScriptPath",
  sFunction = "",
  sDesc = "Returns the path to the current map's 'script' folder, for use in: dofile(GetScriptPath()..'myscript.lua')",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 10)
}) ]]
DefineAction({
  sName = "GetNumPlayers",
  sFunction = "GdsGetNumPlayers",
  sDesc = "Returns the number of Players that are starting the map.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "GetCurrentWaveNumber",
  sFunction = "GetCurrentWaveNumber",
  sDesc = "Returns the current wave number in a SpawnWave 'sp_' script.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 3, 2)
})
DefineAction({
  sName = "GetTotalWaveNumber",
  sFunction = "GetTotalWaveNumber",
  sDesc = "Returns the total number of waves executing this SpawnWave 'sp_' script.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 3, 2)
})
DefineAction({
  sName = "MessageBox",
  sFunction = "ScriptMain:ShowMessage",
  sDesc = "Pops up a MessageBox during script load and displays the message (enter message between brackets as string).",
  sCategory = "!Functions" .. " (DEBUG)",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "MB",
  sFunction = "ScriptMain:ShowMessage",
  sDesc = "Shorthand for MessageBox.",
  sCategory = "!Functions" .. " (DEBUG)",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "ViewTable",
  sFunction = "",
  sDesc = "Stops loading scripts and dumps the given table to disk and opens it in text editor for analysis.",
  sCategory = "!Functions" .. " (DEBUG)",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "VT",
  sFunction = "",
  sDesc = "Shorthand for ViewTable.",
  sCategory = "!Functions" .. " (DEBUG)",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 8, 21)
})
DefineAction({
  sName = "ArenaDummyRandom",
  sFunction = "",
  sDesc = "'Real' random for 1p Arena Map. MUST NOT BE USED FOR PRODUCTION!!! WILL CAUSE DESYNCH IN MULTIPLAYER!!!",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "GdsGetConfigBoolean",
  sFunction = "",
  sDesc = "Tries to read a Boolean value from config.xml. If value does not exist, value is added to config.xml and 'false' is returned. Key is Case-insensitive.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "GdsGetConfigInteger",
  sFunction = "",
  sDesc = "Tries to read an Integer value from config.xml. If value does not exist, value is added to config.xml and '0' is returned. Key is Case-insensitive.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "GdsGetConfigFloat",
  sFunction = "",
  sDesc = "Tries to read a Float value from config.xml. If value does not exist, value is added to config.xml and '0.0' is returned. Key is Case-insensitive.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 7)
})
DefineAction({
  sName = "GdsGetConfigString",
  sFunction = "",
  sDesc = "Tries to read a String value from config.xml. If value does not exist, value is added to config.xml and empty string is returned. Key is Case-insensitive.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 12, 7)
})
DefineCondition({
  sName = "DifficultyIsEqual",
  sNegated = "DifficultyIsNotEqual",
  sDesc = "Checks the mission's current difficulty level.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 11, 15)
})
DefineCondition({
  sName = "DifficultyIsLess",
  sNegated = "DifficultyIsGreaterOrEqual",
  sDesc = "Checks the mission's current difficulty level.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 11, 15)
})
DefineCondition({
  sName = "DifficultyIsGreater",
  sNegated = "DifficultyIsLessOrEqual",
  sDesc = "Checks the mission's current difficulty level.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2007, 11, 15)
})
DefineAction({
  sName = "GetDifficulty",
  sFunction = "",
  sDesc = "Returns the current difficulty (a number).",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 1, 30)
})
DefineAction({
  sName = "SetDifficulty",
  sFunction = "",
  sDesc = "Sets the game's difficulty (a number).",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 6, 19)
})
DefineAction({
  sName = "SpawnWaveTemplate",
  sFunction = "",
  sDesc = "Creates templates of SpawnWaves for later Spawning.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 2, 15)
})
DefineAction({
  sName = "SpawnWaveEmitter",
  sFunction = "",
  sDesc = "Defines a group of spawn waves that can be emitted at a certain location.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 2, 15)
})
DefineAction({
  sName = "RegisterSquadSpawnCounter",
  sFunction = "RegisterSquadSpawnCounter",
  ktScriptGroupParam,
  sDesc = "Counts all Squad spawns in this ScriptGroup. Must be used <b>outside</b> of any State!",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 3, 9)
})
DefineAction({
  sName = "RegisterSquadDeathCounter",
  sFunction = "RegisterSquadDeathCounter",
  ktScriptGroupParam,
  sDesc = "Counts all Squad deaths in this ScriptGroup. Must be used <b>outside</b> of any State!",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 3, 9)
})
DefineAction({
  sName = "UpgradeLevel0",
  sFunction = "UpgradeLevel0",
  sDesc = "Changes the base (Upgrade2) ID of an entity to the corresponding UpgradeLevel ID.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 7, 16)
})
DefineAction({
  sName = "UpgradeLevel1",
  sFunction = "UpgradeLevel1",
  sDesc = "Changes the base (Upgrade2) ID of an entity to the corresponding UpgradeLevel ID.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 7, 16)
})
DefineAction({
  sName = "UpgradeLevel2",
  sFunction = "UpgradeLevel2",
  sDesc = "Changes the base (Upgrade2) ID of an entity to the corresponding UpgradeLevel ID.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 7, 16)
})
DefineAction({
  sName = "UpgradeLevel3",
  sFunction = "UpgradeLevel3",
  sDesc = "Changes the base (Upgrade2) ID of an entity to the corresponding UpgradeLevel ID.",
  sCategory = "!Functions",
  bDocumentationOnly = true,
  iCreateDate = GetDays(2008, 7, 16)
})
DefineAction({
  sName = "GetLastCardID",
  sFunction = "",
  sDesc = "Returns the CardID of the last added (e.g. cloned) card.",
  sCategory = "Card",
  iCreateDate = GetDays(2011, 12, 12),
  bDocumentationOnly = true
})
DefineCondition({
  sName = "BarrierIsPreview",
  sNegated = "BarrierIsBuilt",
  sFunction = "CScriptConditionQuantorBarrierBuildState",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  0,
  sDesc = "Checks if the Barrier is in prebuilt state or not.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 3, 12)
})
DefineCondition({
  sName = "BarrierIsMirrored",
  sNegated = "BarrierIsNotMirrored",
  sFunction = "CScriptConditionQuantorBarrierBuildState",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  1,
  sDesc = "Checks if the Barrier is in mirrored state or not.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 9, 25)
})
DefineCondition({
  sName = "BarrierIsMounted",
  sNegated = "BarrierIsNotMounted",
  sFunction = "CScriptConditionQuantorBarrierMounted",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  "",
  sDesc = "Checks if the Barrier is mounted by any Squad.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 10, 8)
})
DefineCondition({
  sName = "BarrierIsMountedByPlayer",
  sNegated = "BarrierIsNotMountedByPlayer",
  sFunction = "CScriptConditionQuantorBarrierMounted",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  sDesc = "Checks if the Barrier is mounted by a Squad owned by the given Player.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 10, 8)
})
DefineCondition({
  sName = "BuildingWasJustHit",
  sNegated = "BuildingWasNotJustHit",
  sFunction = "CScriptConditionQuantorBuildingHit",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  AttackTypeNone,
  sDesc = "Checks if the Building has just been hit by an attack or any other form of damage.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 5, 20)
})
DefineCondition({
  sName = "EntityIsAlive",
  sNegated = "EntityIsDead",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks living/dead status of Entity, means different things for different Entity types.",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 9, 25)
})
DefineCondition({
  sName = "SquadIsAlive",
  sNegated = "SquadIsDead",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks living/dead status of Squads.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 18)
})
DefineCondition({
  sName = "BuildingIsAlive",
  sNegated = "BuildingIsDestroyed",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks intact/destroyed status of Buildings.",
  sCategory = "Building",
  iCreateDate = GetDays(2007, 8, 18)
})
DefineCondition({
  sName = "BarrierIsAlive",
  sNegated = "BarrierIsDestroyed",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks intact/destroyed status of Barriers.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2007, 10, 1)
})
DefineCondition({
  sName = "ObjectIsOnMap",
  sNegated = "ObjectIsNotOnMap",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks wether Objects are existent on the map.",
  sCategory = "Object",
  iCreateDate = GetDays(2007, 8, 18)
})
DefineCondition({
  sName = "PlayerIsHuman",
  sNegated = "PlayerIsNotHuman",
  sFunction = "CScriptConditionQuantorEntityAlive",
  0,
  0,
  ktScriptTagPlayerParam,
  sDesc = "Checks if Player is a human player (== connected?).",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 9, 5)
})
RegisterEnums("CScriptConditionQuantorEntityGateState", {
  "GateClosed",
  "GateClosing",
  "GateOpen",
  "GateOpening"
})
DefineCondition({
  sName = "BarrierGateIsOpen",
  sNegated = "BarrierGateIsNotOpen",
  sFunction = "CScriptConditionQuantorEntityGateState",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  GateOpen,
  sDesc = "Checks if the BarrierGate is open or not.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 1, 21)
})
DefineCondition({
  sName = "BarrierGateIsClosed",
  sNegated = "BarrierGateIsNotClosed",
  sFunction = "CScriptConditionQuantorEntityGateState",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  GateClosed,
  sDesc = "Checks if the BarrierGate is closed or not.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2008, 1, 21)
})
DefineCondition({
  sName = "SquadIsIdle",
  sNegated = "SquadIsBusy",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasJobIdle,
  0,
  sDesc = "Checks if the Squad is Idle (has nothing to do) or is somehow busy (fighting, walking, etc.).",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 30)
})
DefineFilterCondition({
  sName = "SquadAliveIsIdle",
  sNegated = "SquadAliveIsBusy",
  sCategory = "Squad",
  sDesc = "Checks if the living Squads are idle or busy (avoids trap if one squad of group is dead but check is for all).",
  iCreateDate = GetDays(2008, 3, 14),
  ktForAnyParam,
  ktScriptTagParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsIdle({
        For = _ARG_0_.For,
        Tag = _ARG_0_.Tag
      }),
      Filters = {
        FilterEntityIsAlive({})
      }
    })
  end
})
DefineCondition({
  sName = "SquadIsInGotoMode",
  sNegated = "SquadIsNotInGotoMode",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasJobGoto,
  0,
  sDesc = "Checks if the Squad is in Goto mode, eg on the way to some target location.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineCondition({
  sName = "SquadIsSpellCasting",
  sNegated = "SquadIsNotSpellCasting",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasJobSpellCast,
  0,
  sDesc = "Checks if the Squad is casting a Spell.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineCondition({
  sName = "SquadHasAbility",
  sNegated = "SquadHasNotAbility",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasAbility,
  ktAbilityIdParam,
  sDesc = "Checks if (any member of) the Squad has the given Ability.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 8, 31)
})
DefineCondition({
  sName = "SquadHasAbilityLine",
  sNegated = "SquadHasNotAbilityLine",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasAbilityLine,
  ktAbilityLineIdParam,
  sDesc = "Checks if (any member of) the Squad has an Ability based on the given AbilityLine.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineCondition({
  sName = "SquadHasHate",
  sNegated = "SquadHasNotHate",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  HasHate,
  0,
  sDesc = "Checks if the Squad has Hate (eg. it would like to or is about to attack something).",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 28)
})
DefineCondition({
  sName = "SquadIsFighting",
  sNegated = "SquadIsNotFighting",
  sFunction = "CScriptConditionQuantorEntityHasX",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  IsFighting,
  0,
  sDesc = "Checks if the Squad is fighting.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 28)
})
for _FORV_8_, _FORV_9_ in ipairs({"Greater", "Less"}) do
  local params1 = {
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {Greater, Less}
  DefineCondition({
    sName = "EntityHealthIs" .. _FORV_9_,
    sNegated = "EntityHealthIs" .. params1[_FORV_8_],
    sFunction = "CScriptConditionQuantorEntityHealthCount",
    0,
    ktForAnyParam,
    ktScriptTagParam,
    ktPercentParam,
    params2[_FORV_8_],
    sDesc = "Checks the health (lifepoints) status of entities.",
    sCategory = "Entity",
    iCreateDate = GetDays(2007, 9, 5)
  })
end
DefineCondition({
  sName = "EntityIsInPlayableArea",
  sNegated = "EntityIsNotInPlayableArea",
  sFunction = "CScriptConditionQuantorEntityInPlayVisArea",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  MapAreaPlayable,
  sDesc = "Checks if the entity is inside the map's playable area.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 6, 30)
})
DefineCondition({
  sName = "EntityIsInVisibleArea",
  sNegated = "EntityIsNotInVisibleArea",
  sFunction = "CScriptConditionQuantorEntityInPlayVisArea",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  MapAreaVisible,
  sDesc = "Checks if the entity is inside the map's visible area.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 6, 30)
})
DefineCondition({
  sName = "EntityIsInRange",
  sNegated = "EntityIsNotInRange",
  sFunction = "CScriptConditionQuantorEntityInRange",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Checks if an Entity/Group (with the given Tag) is in Range to (any of) the Target(s).",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 8, 24)
})
DefineFilterCondition({
  sName = "SquadIdleIsInRange",
  sNegated = "SquadIdleIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if an idle Squad is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 1, 16),
  ktForAnyParam,
  ktScriptTagParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      EntityIsInRange({
        For = _ARG_0_.For,
        Tag = _ARG_0_.Tag,
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterSquadIsIdle({})
      }
    })
  end
})
DefineCondition({
  sName = "SquadIsInRange",
  sNegated = "SquadIsNotInRange",
  sFunction = "CScriptConditionQuantorEntityInRange",
  0,
  ANY,
  CollectorSquadsInRange,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Checks if *any* Squad is in range. Used as basis for filtered 'in range' checks, eg. 'PlayerSquadIsInRange'.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 2, 26)
})
DefineFilterCondition({
  sName = "SquadPvETypeIsInRange",
  sNegated = "SquadPvETypeIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad with given PvE Type is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 6, 13),
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "SquadPvETypeAmountIsInRange",
  sNegated = "SquadPvETypeAmountIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if amount Squads with PvE Type are in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 6, 13),
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  ktAmountParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        }),
        FilterEntityAmountIsGreaterOrEqual({
          Amount = _ARG_0_.Amount
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "SquadFlyingIsInRange",
  sNegated = "SquadFlyingIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a flying Squad is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterSquadIsFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "SquadOnGroundIsInRange",
  sNegated = "SquadOnGroundIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad that is NOT flying is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterSquadIsNotFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "PlayerSquadIsInRange",
  sNegated = "PlayerSquadIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Player is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 2, 26),
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "PlayerSquadFlyingIsInRange",
  sNegated = "PlayerSquadFlyingIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Player is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        }),
        FilterSquadIsFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "PlayerSquadOnGroundIsInRange",
  sNegated = "PlayerSquadOnGroundIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Player is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        }),
        FilterSquadIsNotFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "TeamSquadIsInRange",
  sNegated = "TeamSquadIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Team is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 2, 26),
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "TeamSquadFlyingIsInRange",
  sNegated = "TeamSquadFlyingIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Team is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        }),
        FilterSquadIsFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "TeamSquadOnGroundIsInRange",
  sNegated = "TeamSquadOnGroundIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad of a specified Team is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 10, 28),
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        }),
        FilterSquadIsNotFlying({})
      }
    })
  end
})
DefineFilterCondition({
  sName = "SquadIdIsInRange",
  sNegated = "SquadIdIsNotInRange",
  sCategory = "Squad",
  sDesc = "Checks if a Squad with the specified ID is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 3, 1),
  ktSquadIdParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      SquadIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasResId({
          Id = _ARG_0_.SquadId
        })
      }
    })
  end
})
DefineCondition({
  sName = "BuildingIsInRange",
  sNegated = "BuildingIsNotInRange",
  sFunction = "CScriptConditionQuantorEntityInRange",
  0,
  ANY,
  CollectorBuildingsInRange,
  ktScriptTagTargetParam,
  ktRangeParam,
  sDesc = "Checks if *any* Building is in range. Used as basis for filtered 'in range' checks, eg. 'PlayerBuildingIsInRange'.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 2, 26)
})
DefineFilterCondition({
  sName = "PlayerBuildingIsInRange",
  sNegated = "PlayerBuildingIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building of a specified Player is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 2, 26),
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "TeamBuildingIsInRange",
  sNegated = "TeamBuildingIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building of a specified Team is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 2, 26),
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "PlayerBuildingKillableIsInRange",
  sNegated = "PlayerBuildingKillableIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building of a specified Player is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 9, 24),
  ktPlayerNoQuantorParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByPlayer({
          Player = _ARG_0_.Player
        }),
        FilterEntityHasNotAbilityLine({
          AbilityLineId = AbilityLine.UnKillable
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "TeamBuildingKillableIsInRange",
  sNegated = "TeamBuildingKillableIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building of a specified Team is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 9, 24),
  ktTeamParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityIsOwnedByTeam({
          Team = _ARG_0_.Team
        }),
        FilterEntityHasNotAbilityLine({
          AbilityLineId = AbilityLine.UnKillable
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "BuildingIdIsInRange",
  sNegated = "BuildingIdIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building with the specified ID is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 3, 1),
  ktBuildingIdParam,
  ktScriptTagTargetParam,
  ktRangeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasResId({
          Id = _ARG_0_.BuildingId
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "BuildingPvETypeIsInRange",
  sNegated = "BuildingPvETypeIsNotInRange",
  sCategory = "Building",
  sDesc = "Checks if a Building with given PvE Type is in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 8, 24),
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        })
      }
    })
  end
})
DefineFilterCondition({
  sName = "BuildingPvETypeAmountIsInRange",
  sNegated = "BuildingPvETypeAmountIsInRange",
  sCategory = "Building",
  sDesc = "Checks if amount Buildings with PvE Type are in Range to (any of) the Target(s).",
  iCreateDate = GetDays(2008, 9, 5),
  ktScriptTagTargetParam,
  ktRangeParam,
  ktPvETypeParam,
  ktAmountParam,
  fCommand = function(_ARG_0_, _ARG_1_)
    return CreateFilterCommand({
      bNegated = _ARG_1_,
      BuildingIsInRange({
        TargetTag = _ARG_0_.TargetTag,
        Range = _ARG_0_.Range
      }),
      Filters = {
        FilterEntityHasPvEType({
          PvEType = _ARG_0_.PvEType
        }),
        FilterEntityAmountIsGreaterOrEqual({
          Amount = _ARG_0_.Amount
        })
      }
    })
  end
})
DefineCondition({
  sName = "BuildingModeIsActive",
  sNegated = "BuildingModeIsNotActive",
  sFunction = "CScriptConditionQuantorEntityModeActive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktModeIdParam,
  0,
  sDesc = "Checks if the currently active Mode has the specified ModeId.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineCondition({
  sName = "BuildingModeIsEnabled",
  sNegated = "BuildingModeIsDisabled",
  sFunction = "CScriptConditionQuantorEntityModeActive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  0,
  1,
  sDesc = "Checks if the currently active Mode is an 'enabled' or 'disabled' Mode.",
  sCategory = "Building",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineCondition({
  sName = "SquadModeIsActive",
  sNegated = "SquadModeIsNotActive",
  sFunction = "CScriptConditionQuantorEntityModeActive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktModeIdParam,
  0,
  sDesc = "Checks if the currently active Mode has the specified ModeId.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineCondition({
  sName = "SquadModeIsEnabled",
  sNegated = "SquadModeIsDisabled",
  sFunction = "CScriptConditionQuantorEntityModeActive",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  0,
  1,
  sDesc = "Checks if the currently active Mode is an 'enabled' or 'disabled' Mode.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineCondition({
  sName = "EntityIsOwnedByPlayer",
  sNegated = "EntityIsNotOwnedByPlayer",
  sFunction = "CScriptConditionQuantorEntityOwner",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  sDesc = "Checks if the entity is owned by the specified Player.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 2, 25)
})
DefineCondition({
  sName = "EntityIsOwnedByTeam",
  sNegated = "EntityIsNotOwnedByTeam",
  sFunction = "CScriptConditionQuantorEntityOwner",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktTeamParam,
  sDesc = "Checks if the entity is owned by the specified Player.",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 2, 25)
})
DefineCondition({
  sName = "EntitySpellCastIsBlocked",
  sNegated = "EntitySpellCastIsNotBlocked",
  sFunction = "CScriptConditionQuantorEntitySpellBlocked",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktSpellIdParam,
  ktCheckGotoJobParam,
  sDesc = "Checks if casting of this Spell is currently blocked (Recast-Time, already casting Spell, etc.).",
  sCategory = "Entity",
  iCreateDate = GetDays(2007, 10, 3)
})
DefineCondition({
  sName = "EntityHasJustCastedSpell",
  sNegated = "EntityHasNotJustCastedSpell",
  sFunction = "CScriptConditionQuantorEntitySpellResolved",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktSpellIdParam,
  sDesc = "Checks if the Entity has just casted the specified Spell, or any Spell if SpellId = AnySpell",
  sCategory = "Entity",
  iCreateDate = GetDays(2008, 4, 19)
})
DefineCondition({
  sName = "PlayerHasJustPlayedCardId",
  sNegated = "PlayerHasNotJustPlayedCardId",
  sFunction = "CScriptConditionQuantorPlayerCardPlayed",
  0,
  0,
  ktScriptTagPlayerParam,
  ktCardIdParam,
  0,
  "",
  sDesc = "Checks if the Player has just played the specified Card, or any Card if CardId = AnyCard",
  sCategory = "Player",
  iCreateDate = GetDays(2007, 9, 24)
})
DefineCondition({
  sName = "PlayerHasJustPlayedCardPvEType",
  sNegated = "PlayerHasNotJustPlayedCardPvEType",
  sFunction = "CScriptConditionQuantorPlayerCardPlayed",
  0,
  0,
  ktScriptTagPlayerParam,
  0,
  1,
  ktPvETypeParam,
  sDesc = "Checks if the Player has just played a Card whose entity has the specified PvE Type.",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 7, 24)
})
for _FORV_10_, _FORV_11_ in ipairs({
  "Squad",
  "Building",
  "Monument",
  "Generator"
}) do
  for _FORV_16_, _FORV_17_ in ipairs({
    "Equal",
    "Greater",
    "Less"
  }) do
    local params1 = {
        "NotEqual",
        "LessOrEqual",
        "GreaterOrEqual"
      }
    local params2 = {
        Equal,
        Greater,
        Less
      }
    local params3 = {
        TypeSquad,
        TypeBuilding,
        TypeTokenSlot,
        TypePowerSlot
      }
    DefineCondition({
      sName = "Player" .. _FORV_11_ .. "AmountIs" .. _FORV_17_,
      sNegated = "Player" .. _FORV_11_ .. "AmountIs" .. params1[_FORV_16_],
      sFunction = "CScriptConditionQuantorPlayerEntityAmount",
      0,
      0,
      ktScriptTagPlayerParam,
      ktAmountParam,
      0,
      params2[_FORV_16_],
      params3[_FORV_10_],
      PowerNone,
      sDesc = "Checks if the Player(s) own the specified amount of the given entity type.",
      sCategory = "Player",
      iCreateDate = GetDays(2008, 3, 12)
    })
  end
end
for _FORV_13_, _FORV_14_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "PlayerOrbAmountIs" .. _FORV_14_,
    sNegated = "PlayerOrbAmountIs" .. params1[_FORV_13_],
    sFunction = "CScriptConditionQuantorPlayerEntityAmount",
    0,
    0,
    ktScriptTagPlayerParam,
    ktAmountParam,
    0,
    params2[_FORV_13_],
    TypeTokenSlot,
    ktPowerParam,
    sDesc = "Checks the amount of Orbs on Monuments of a specific color. Use PowerAll to count all Power Orbs together.",
    sCategory = "Player",
    iCreateDate = GetDays(2008, 3, 12)
  })
end
for _FORV_16_, _FORV_17_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "PlayerOrbBuildInProgressAmountIs" .. _FORV_17_,
    sNegated = "PlayerOrbBuildInProgressAmountIs" .. params1[_FORV_16_],
    sFunction = "CScriptConditionQuantorPlayerEntityAmount",
    0,
    0,
    ktScriptTagPlayerParam,
    ktAmountParam,
    1,
    params2[_FORV_16_],
    TypeTokenSlot,
    ktPowerParam,
    sDesc = "Checks the amount of Orbs of a specific color that are currently being built. Use PowerAll to count all in progress.",
    sCategory = "Player",
    iCreateDate = GetDays(2008, 9, 25)
  })
end
DefineCondition({
  sName = "PlayerHasGameLost",
  sNegated = "PlayerHasNotGameLost",
  sFunction = "CScriptConditionQuantorPlayerGameOver",
  0,
  0,
  ktScriptTagPlayerParam,
  0,
  sDesc = "Checks if the Player is GameOver man, GameOver!",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 3, 1)
})
DefineCondition({
  sName = "PlayerHasGameWon",
  sNegated = "PlayerHasNotGameWon",
  sFunction = "CScriptConditionQuantorPlayerGameOver",
  0,
  0,
  ktScriptTagPlayerParam,
  1,
  sDesc = "Checks if the Player has won the Game (match).",
  sCategory = "Player",
  iCreateDate = GetDays(2008, 3, 1)
})
for _FORV_8_, _FORV_9_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "PlayerPowerAmountIs" .. _FORV_9_,
    sNegated = "PlayerPowerAmountIs" .. params1[_FORV_8_],
    sFunction = "CScriptConditionQuantorPlayerOwnsPowerAmount",
    0,
    0,
    ktScriptTagPlayerParam,
    ktAmountParam,
    params2[_FORV_8_],
    sDesc = "Checks if the Player(s) own the specified amount of Power.",
    sCategory = "Player",
    iCreateDate = GetDays(2011, 12, 5)
  })
end
for _FORV_8_, _FORV_9_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "PlayerVoidPowerAmountIs" .. _FORV_9_,
    sNegated = "PlayerVoidPowerAmountIs" .. params1[_FORV_8_],
    sFunction = "CScriptConditionQuantorPlayerOwnsVoidPowerAmount",
    0,
    0,
    ktScriptTagPlayerParam,
    ktAmountParam,
    params2[_FORV_8_],
    sDesc = "Checks if the Player(s) own the specified amount of Power.",
    sCategory = "Player",
    iCreateDate = GetDays(2011, 12, 5)
  })
end
DefineCondition({
  sName = "SquadWasJustHit",
  sNegated = "SquadWasNotJustHit",
  sFunction = "CScriptConditionQuantorSquadHit",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  "",
  "",
  AttackTypeNone,
  sDesc = "Checks if the Squad has just been hit by an attack or any other form of damage.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 9, 5)
})
DefineCondition({
  sName = "SquadWasJustHitByPlayer",
  sNegated = "SquadWasNotJustHitByPlayer",
  sFunction = "CScriptConditionQuantorSquadHit",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktPlayerNoQuantorParam,
  "",
  AttackTypeNone,
  sDesc = "Checks if the Squad has just been hit by a specific Player entity's attack/spell.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 9, 5)
})
DefineCondition({
  sName = "SquadWasJustHitByTeam",
  sNegated = "SquadWasNotJustHitByTeam",
  sFunction = "CScriptConditionQuantorSquadHit",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  "",
  ktTeamParam,
  AttackTypeNone,
  sDesc = "Checks if the Squad has just been hit by a specific Team entity's attack/spell.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 9, 5)
})
DefineCondition({
  sName = "SquadIdleTimerIsElapsed",
  sNegated = "SquadIdleTimerIsNotElapsed",
  sFunction = "CScriptConditionQuantorSquadIdleTimer",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktSecondsParam,
  ktMinutesParam,
  ktConsiderDeadAsIdleParam,
  sDesc = "Checks if the Squad was (continuously) idle for (at least) the given time.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 5, 30)
})
DefineCondition({
  sName = "SquadHasMeleeAttackerInRange",
  sNegated = "SquadHasNotMeleeAttackerInRange",
  sFunction = "CScriptConditionQuantorSquadMeleeCombatInRange",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  ktRangeParam,
  sDesc = "Checks if there are any Melee units attacking this Squad in Range.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 10, 2)
})
if not CScriptConditionQuantorSquadMemberAmount then
  CScriptConditionQuantorSquadMemberAmount = {CompareAbsoluteValue = 0, ComparePercentage = 1}
end
DefineCondition({
  sName = "SquadMemberAmountIsAlive",
  sNegated = "SquadMemberAmountIsDead",
  sFunction = "CScriptConditionQuantorSquadMemberAmount",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  CScriptConditionQuantorSquadMemberAmount.CompareAbsoluteValue,
  ktAmountParam,
  sDesc = "Compares the remaining (alive) number of Squad members with the original amount.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 9, 7)
})
DefineCondition({
  sName = "SquadMemberPercentageIsAlive",
  sNegated = "SquadMemberPercentageIsDead",
  sFunction = "CScriptConditionQuantorSquadMemberAmount",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  CScriptConditionQuantorSquadMemberAmount.ComparePercentage,
  ktPercentParam,
  sDesc = "Compares the remaining (alive) number of Squad members with the original amount.",
  sCategory = "Squad",
  iCreateDate = GetDays(2007, 9, 7)
})
DefineCondition({
  sName = "BarrierModuleAmountIsAlive",
  sNegated = "BarrierModuleAmountIsDead",
  sFunction = "CScriptConditionQuantorSquadMemberAmount",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  CScriptConditionQuantorSquadMemberAmount.CompareAbsoluteValue,
  ktAmountParam,
  sDesc = "Compares the remaining (alive) number of Barrier Modules in the BarrierSet.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2007, 10, 6)
})
DefineCondition({
  sName = "BarrierModulePercentageIsAlive",
  sNegated = "BarrierModulePercentageIsDead",
  sFunction = "CScriptConditionQuantorSquadMemberAmount",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  CScriptConditionQuantorSquadMemberAmount.ComparePercentage,
  ktPercentParam,
  sDesc = "Compares the remaining (alive) number of Barrier Modules in the BarrierSet.",
  sCategory = "Barrier",
  iCreateDate = GetDays(2007, 10, 6)
})
DefineCondition({
  sName = "SquadIsMountedOnBarrier",
  sNegated = "SquadIsNotMountedOnBarrier",
  sFunction = "CScriptConditionQuantorSquadMountedBarrier",
  0,
  ktForAnyParam,
  ktScriptTagParam,
  sDesc = "Checks if the Squas has mounted a Barrier.",
  sCategory = "Squad",
  iCreateDate = GetDays(2008, 4, 24)
})
DefineCondition({
  sName = "PlayerFlagIsTrue",
  sNegated = "PlayerFlagIsFalse",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktPlayerFlagNameParam,
  1,
  Equal,
  sDesc = "Checks if the flag has the desired value.",
  sCategory = "Variable",
  iCreateDate = GetDays(2007, 8, 14)
})
for _FORV_10_, _FORV_11_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "PlayerValueIs" .. _FORV_11_,
    sNegated = "PlayerValueIs" .. params1[_FORV_10_],
    sFunction = "CScriptConditionQuantorVariableCompare",
    0,
    0,
    ktScriptTagPlayerParam,
    ktPlayerValueNameParam,
    ktValueParam,
    params2[_FORV_10_],
    sDesc = "Checks if the value satisfies the condition.",
    sCategory = "Variable",
    iCreateDate = GetDays(2007, 8, 14)
  })
end
for _FORV_13_, _FORV_14_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "MissionCounterIs" .. _FORV_14_,
    sNegated = "MissionCounterIs" .. params1[_FORV_13_],
    sFunction = "CScriptConditionQuantorVariableCompare",
    0,
    0,
    ktScriptTagPlayerParam,
    ktMissionCounterNameParam,
    ktValueParam,
    params2[_FORV_13_],
    sDesc = "Checks if the value satisfies the condition.",
    sCategory = "Mission",
    iCreateDate = GetDays(2008, 3, 7)
  })
end
DefineCondition({
  sName = "PlayerTimerIsElapsed",
  sNegated = "PlayerTimerIsNotElapsed",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktPlayerTimerNameParam,
  ktSecondsParam,
  ktMinutesParam,
  TimeElapsed,
  sDesc = "Checks if the given amount of time has passed since Timer was started.",
  sCategory = "Variable",
  iCreateDate = GetDays(2007, 8, 14)
})
DefineCondition({
  sName = "PlayerTimerIsRunning",
  sNegated = "PlayerTimerIsNotRunning",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktPlayerTimerNameParam,
  0,
  NotEqual,
  sDesc = "Checks if the Timer is running or not.",
  sCategory = "Variable",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineCondition({
  sName = "MissionTimerHasRunOut",
  sNegated = "MissionTimerHasNotRunOut",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktMissionTimerNameParam,
  0,
  TimeElapsed,
  sDesc = "Checks if the Mission Timer has run out of time (reached 00:00).",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineCondition({
  sName = "MissionTimerIsElapsed",
  sNegated = "MissionTimerIsNotElapsed",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktMissionTimerNameParam,
  ktSecondsParam,
  ktMinutesParam,
  TimeElapsed,
  sDesc = "Checks if the (forward running) Mission Timer has elapsed.",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineCondition({
  sName = "MissionTimerIsDisplayed",
  sNegated = "MissionTimerIsNotDisplayed",
  sFunction = "CScriptConditionQuantorVariableCompare",
  0,
  0,
  ktScriptTagPlayerParam,
  ktMissionTimerNameParam,
  0,
  Greater,
  sDesc = "Checks if the Mission Timer is displayed (running but could also be paused).",
  sCategory = "Mission",
  iCreateDate = GetDays(2008, 3, 7)
})
DefineCondition({
  sName = "ScriptGroupIsEmpty",
  sNegated = "ScriptGroupIsNotEmpty",
  sFunction = "CScriptConditionScriptGroupEmpty",
  0,
  ktScriptGroupParam,
  sDesc = "Checks if the ScriptGroup contains tags or wether it is empty.",
  sCategory = "ScriptGroup",
  iCreateDate = GetDays(2008, 3, 2),
  bDocumentationDontShow = true
})
for _FORV_8_, _FORV_9_ in ipairs({
  "ScriptGroupAlive",
  "ScriptGroupDead"
}) do
  for _FORV_13_, _FORV_14_ in ipairs({"AmountIs", "PercentIs"}) do
    for _FORV_19_, _FORV_20_ in ipairs({
      "Equal",
      "Greater",
      "Less"
    }) do
      local params1 = {
          "NotEqual",
          "LessOrEqual",
          "GreaterOrEqual"
        }
      local params2 = {
          Equal,
          Greater,
          Less
        }
      DefineCondition({
        sName = _FORV_9_ .. _FORV_14_ .. _FORV_20_,
        sNegated = _FORV_9_ .. _FORV_14_ .. params1[_FORV_19_],
        sFunction = "CScriptConditionScriptGroupMemberAmountAlive",
        0,
        ktScriptGroupParam,
        ktPercentParam,
        _FORV_8_ - 1,
        _FORV_13_ - 1,
        params2[_FORV_19_],
        sDesc = "Checks the amount/percentage of living/dead members of the ScriptGroup.",
        sCategory = "ScriptGroup",
        iCreateDate = GetDays(2008, 3, 9)
      })
    end
  end
end
for _FORV_8_, _FORV_9_ in ipairs({"Map", "Entity"}) do
  DefineCondition({
    sName = _FORV_9_ .. "FlagIsTrue",
    sNegated = _FORV_9_ .. "FlagIsFalse",
    sFunction = "CScriptConditionVariableCompare",
    0,
    _G["kt" .. _FORV_9_ .. "FlagNameParam"],
    1,
    Equal,
    sDesc = "Checks if the flag has the desired value.",
    sCategory = "Variable",
    iCreateDate = GetDays(2007, 8, 14)
  })
end
DefineCondition({
  sName = "NetworkFlagIsTrue",
  sNegated = "NetworkFlagIsFalse",
  sFunction = "CScriptConditionVariableCompare",
  0,
  ktNetworkFlagNameParam,
  1,
  Equal,
  sDesc = "Checks if the flag has the desired value.",
  sCategory = "Variable",
  iCreateDate = GetDays(2008, 9, 1)
})
for _FORV_11_, _FORV_12_ in ipairs({"Map", "Entity"}) do
  for _FORV_16_, _FORV_17_ in ipairs({
    "Equal",
    "Greater",
    "Less"
  }) do
    local params1 = {
        "NotEqual",
        "LessOrEqual",
        "GreaterOrEqual"
      }
    local params2 = {
        Equal,
        Greater,
        Less
      }
    DefineCondition({
      sName = _FORV_12_ .. "ValueIs" .. _FORV_17_,
      sNegated = _FORV_12_ .. "ValueIs" .. params1[_FORV_16_],
      sFunction = "CScriptConditionVariableCompare",
      0,
      _G["kt" .. _FORV_12_ .. "ValueNameParam"],
      ktValueParam,
      params2[_FORV_16_],
      sDesc = "Checks if the value satisfies the condition.",
      sCategory = "Variable",
      iCreateDate = GetDays(2007, 8, 14)
    })
  end
end
for _FORV_11_, _FORV_12_ in ipairs({
  "Equal",
  "Greater",
  "Less"
}) do
  local params1 = {
      "NotEqual",
      "LessOrEqual",
      "GreaterOrEqual"
    }
  local params2 = {
      Equal,
      Greater,
      Less
    }
  DefineCondition({
    sName = "Network" .. "ValueIs" .. _FORV_12_,
    sNegated = "Network" .. "ValueIs" .. params1[_FORV_11_],
    sFunction = "CScriptConditionVariableCompare",
    0,
    _G["kt" .. "Network" .. "ValueNameParam"],
    ktValueParam,
    params2[_FORV_11_],
    sDesc = "Checks if the value satisfies the condition.",
    sCategory = "Variable",
    iCreateDate = GetDays(2008, 9, 1)
  })
end
for _FORV_13_, _FORV_14_ in ipairs({"Map", "Entity"}) do
  for _FORV_18_, _FORV_19_ in ipairs({"IsElapsed"}) do
    
    DefineCondition({
      sName = _FORV_14_ .. "Timer" .. _FORV_19_,
      sNegated = _FORV_14_ .. "Timer" .. "IsNotElapsed",
      sFunction = "CScriptConditionVariableCompare",
      0,
      _G["kt" .. _FORV_14_ .. "TimerNameParam"],
      ktSecondsParam,
      ktMinutesParam,
      TimeElapsed,
      sDesc = "Checks if the given amount of time has passed since Timer was started.",
      sCategory = "Variable",
      iCreateDate = GetDays(2007, 8, 14)
    })
  end
end
for _FORV_15_, _FORV_16_ in ipairs({"Map", "Entity"}) do
  for _FORV_20_, _FORV_21_ in ipairs({"IsRunning"}) do
    DefineCondition({
      sName = _FORV_16_ .. "Timer" .. _FORV_21_,
      sNegated = _FORV_16_ .. "Timer" .. "IsNotRunning",
      sFunction = "CScriptConditionVariableCompare",
      0,
      _G["kt" .. _FORV_16_ .. "TimerNameParam"],
      0,
      NotEqual,
      sDesc = "Checks whether the Timer is running or stopped.",
      sCategory = "Variable",
      iCreateDate = GetDays(2008, 3, 14)
    })
  end
end
DefineCondition({
  sName = "MissionStartTimerIsElapsed",
  sNegated = "MissionStartTimerIsNotElapsed",
  sFunction = "CScriptConditionVariableCompare",
  0,
  "mt_ElapsedTimeSinceMapStart",
  ktSecondsParam,
  ktMinutesParam,
  TimeElapsed,
  sDesc = "Checks if the given amount of time has passed since the Mission started.",
  sCategory = "Mission",
  iCreateDate = GetDays(2007, 10, 29)
})
for _FORV_19_, _FORV_20_ in ipairs({"Spawn", "Death"}) do
  for _FORV_24_, _FORV_25_ in ipairs({"Greater", "Less"}) do
    local params1 = {
        "LessOrEqual",
        "GreaterOrEqual"
      }
    local params2 = {Greater, Less}
    DefineCondition({
      sName = "ScriptGroupSquad" .. _FORV_20_ .. "CounterIs" .. _FORV_25_,
      sNegated = "ScriptGroupSquad" .. _FORV_20_ .. "CounterIs" .. params1[_FORV_24_],
      sFunction = "CScriptConditionVariableCompare",
      0,
      _G["ktSquad" .. _FORV_20_ .. "CounterNameParam"],
      ktValueParam,
      params2[_FORV_24_],
      sDesc = "Checks the ScriptGroups current Squad" .. _FORV_20_ .. "Counter value. Requires that ScriptGroup count has been registered via RegisterSquad" .. _FORV_20_ .. "Counter{Group = ''}",
      sCategory = "ScriptGroup",
      iCreateDate = GetDays(2008, 3, 9)
    })
  end
end
for _FORV_11_, _FORV_12_ in ipairs({"Map", "Entity"}) do
  for _FORV_16_, _FORV_17_ in ipairs({
    "Equal",
    "Greater",
    "Less"
  }) do
    local params1 = {
        "NotEqual",
        "LessOrEqual",
        "GreaterOrEqual"
      }
    local params2 = {
        Equal,
        Greater,
        Less
      }
    DefineCondition({
      sName = _FORV_12_ .. "ValueIs" .. _FORV_17_ .. "ComparedTo",
      sNegated = _FORV_12_ .. "ValueIs" .. params1[_FORV_16_] .. "ComparedTo",
      sFunction = "CScriptConditionVariableCompareVariable",
      0,
      _G["kt" .. _FORV_12_ .. "Variable1Param"],
      _G["kt" .. _FORV_12_ .. "Variable2Param"],
      params2[_FORV_16_],
      sDesc = "Checks if the value satisfies the condition.",
      sCategory = "Variable",
      iCreateDate = GetDays(2008, 3, 1)
    })
  end
end
DefineFilter({
  sName = "FilterEntityHasAbility",
  sNegated = "FilterEntityHasNotAbility",
  sFunction = "CScriptEntityFilterAbility",
  ktAbilityIdParam,
  sDesc = "Filters based on entity's ability.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterEntityHasAbilityLine",
  sNegated = "FilterEntityHasNotAbilityLine",
  sFunction = "CScriptEntityFilterAbilityLine",
  ktAbilityLineIdParam,
  sDesc = "Filters based on entity's abilityline.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterEntityIsAlive",
  sNegated = "FilterEntityIsDead",
  sFunction = "CScriptEntityFilterAlive",
  sDesc = "Filters entity by alive/dead status.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineFilter({
  sName = "FilterEntityAmountIsLess",
  sNegated = "FilterEntityAmountIsGreaterOrEqual",
  sFunction = "CScriptEntityFilterAmount",
  ktAmountParam,
  Less,
  sDesc = "Filters for a specific number of entities.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 1, 15)
})
DefineFilter({
  sName = "FilterEntityAmountIsGreater",
  sNegated = "FilterEntityAmountIsLessOrEqual",
  sFunction = "CScriptEntityFilterAmount",
  ktAmountParam,
  Greater,
  sDesc = "Filters for a specific number of entities.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 1, 15)
})
DefineFilter({
  sName = "FilterEntityAmountIsEqual",
  sNegated = "FilterEntityAmountIsNotEqual",
  sFunction = "CScriptEntityFilterAmount",
  ktAmountParam,
  Equal,
  sDesc = "Filters for a specific number of entities.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 1, 15)
})
DefineFilter({
  sName = "FilterEntityIsAttackable",
  sNegated = "FilterEntityIsNotAttackable",
  sFunction = "CScriptEntityFilterAttackable",
  sDesc = "Filters entity by attackable status (includes check for 'unattackable' abilities).",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 4, 16)
})
DefineFilter({
  sName = "FilterEntityCanProducePower",
  sNegated = "FilterEntityCanNotProducePower",
  sFunction = "CScriptEntityFilterCanProducePower",
  sDesc = "Filters for power production (mostly generators).",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 6, 27)
})
for _FORV_4_, _FORV_5_ in ipairs({
  "Building",
  "Object",
  "Squad"
}) do
  DefineFilter({
    sName = "FilterEntityIs" .. _FORV_5_,
    sNegated = "FilterEntityIsNot" .. _FORV_5_,
    sFunction = "CScriptEntityFilterCategory",
    _G["Type" .. _FORV_5_],
    sDesc = "Filters entity by category " .. _FORV_5_ .. ".",
    sCategory = "Filter",
    iCreateDate = GetDays(2007, 11, 8)
  })
end
DefineFilter({
  sName = "FilterSquadIsFlying",
  sNegated = "FilterSquadIsNotFlying",
  sFunction = "CScriptEntityFilterFlying",
  sDesc = "Filters Squad by flying ability.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 10, 28)
})
DefineFilter({
  sName = "FilterEntityIsInRange",
  sNegated = "FilterEntityIsNotInRange",
  sFunction = "CScriptEntityFilterInRange",
  ktTargetTagDefaultParam,
  ktRangeParam,
  sDesc = "Filters entity wether it is in range to any of the TargetTag entities.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineFilter({
  sName = "FilterSquadIsIdle",
  sNegated = "FilterSquadIsBusy",
  sFunction = "CScriptEntityFilterJobIdle",
  sDesc = "Filters idle squads.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterEntityIsOwnedByPlayer",
  sNegated = "FilterEntityIsNotOwnedByPlayer",
  sFunction = "CScriptEntityFilterOwner",
  ktScriptTagPlayerNoQuantorParam,
  sDesc = "Filters entity by player pwnage.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterSquadIsPrimaryRanged",
  sNegated = "FilterSquadIsMelee",
  sFunction = "CScriptEntityFilterPrimaryRanged",
  sDesc = "Filters Squads based on wether they are Melee or PrimaryRanged.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineFilter({
  sName = "FilterEntityHasPvEType",
  sNegated = "FilterEntityHasNotPvEType",
  sFunction = "CScriptEntityFilterPvEType",
  ktPvETypeParam,
  sDesc = "Filters for PvE Type of an entity.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 6, 13)
})
DefineFilter({
  sName = "FilterIsRandomPercent",
  sNegated = "FilterIsNotRandomPercent",
  sFunction = "CScriptEntityFilterRandom",
  ktPercentParam,
  sDesc = "Filters entities by a random-percent chance.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 3, 14)
})
DefineFilter({
  sName = "FilterEntityHasResId",
  sNegated = "FilterEntityHasNotResId",
  sFunction = "CScriptEntityFilterResID",
  ktResIdParam,
  sDesc = "Filters based on entity ResId.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterEntityIsOwnedByTeam",
  sNegated = "FilterEntityIsNotOwnedByTeam",
  sFunction = "CScriptEntityFilterTeam",
  ktTeamParam,
  sDesc = "Filters entity by team pwnage.",
  sCategory = "Filter",
  iCreateDate = GetDays(2007, 11, 12)
})
DefineFilter({
  sName = "FilterEntityThreatValueIsLess",
  sNegated = "FilterEntityThreatValueIsGreaterOrEqual",
  sFunction = "CScriptEntityFilterThreatValue",
  ktValueParam,
  Less,
  sDesc = "Filters for threatvalue of an entity.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 5, 8)
})
DefineFilter({
  sName = "FilterEntityThreatValueIsGreater",
  sNegated = "FilterEntityThreatValueIsLessOrEqual",
  sFunction = "CScriptEntityFilterThreatValue",
  ktValueParam,
  Greater,
  sDesc = "Filters for threatvalue of an entity.",
  sCategory = "Filter",
  iCreateDate = GetDays(2008, 5, 8)
})
