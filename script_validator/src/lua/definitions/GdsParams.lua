ksPlayerAllowedChars = "[0-9a-zA-Z_-]"
ksAllowedChars = "[0-9a-zA-Z_:]"
ksAllowedCharsAfterPrefix = "[0-9a-zA-Z]"
ksInternalVariablesPrefix = "GDS"
function PropagateConstantValues(_ARG_0_)
  for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_) do
    if type(_FORV_5_) ~= "table" then
      _ARG_0_[_FORV_4_] = {
        Value = _FORV_5_,
        sType = type(_FORV_5_),
        sName = "",
        sDesc = "",
        bConstant = true
      }
    end
  end
end
function RegisterEnums(_ARG_0_, _ARG_1_)
  --if DEBUG then
    assert(type(_ARG_0_) == "string", "RegisterEnums - classname is not a string!")
    assert(type(_ARG_1_) == "table", "RegisterEnums - tEnumNames is not a table!")
  --end
  if not _G[_ARG_0_] then
    _G[_ARG_0_] = {}
  end
  for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_) do
    if _G[_ARG_0_][_FORV_7_] then
      _G[_FORV_7_] = _G[_ARG_0_][_FORV_7_]
    else
      --_G[_FORV_7_] = -1
      _G[_ARG_0_][_FORV_7_] = _FORV_6_
      _G[_FORV_7_] = _G[_ARG_0_][_FORV_7_]
    end
  end
end
--if CScriptMain then
  RegisterEnums("CScriptMain", {
    "TypeNone",
    "TypeFigure",
    "TypeBuilding",
    "TypeObject",
    "TypeScriptGroup",
    "TypeSquad",
    "TypePlayer",
    "TypeWorld",
    "TypeBarrierModule",
    "TypeBarrierSet",
    "TypePowerSlot",
    "TypeTokenSlot",
    "TypeAbilityWorldObject"
  })
  RegisterEnums("CScriptMain", {
    "kGtUpgradeLevelOffset"
  })
  RegisterEnums("CScriptMain", {
    "DifficultyEasy",
    "DifficultyNormal",
    "DifficultyHard"
  })
  DifficultyVeryHard = 4
  RegisterEnums("CScriptMain", {
    "DifficultyStandard",
    "DifficultyAdvanced",
    "DifficultyExpert"
  })
  RegisterEnums("CScriptMain", {
    "AttackTypeNone",
    "AttackTypeMelee",
    "AttackTypeMagic"
  })
  RegisterEnums("CScriptMain", {
    "PowerShadow",
    "PowerNature",
    "PowerFrost",
    "PowerFire",
    "PowerNeutral",
    "PowerNone",
    "PowerAll"
  })
  RegisterEnums("CScriptCondition", {
    "Equal",
    "Greater",
    "Less",
    "TimeElapsed",
    "NotEqual",
    "GreaterOrEqual",
    "LessOrEqual"
  })
  RegisterEnums("CScriptAction", {
    "Assign",
    "Add",
    "Subtract",
    "Divide",
    "Multiply",
    "Toggle",
    "TimeStampSet",
    "TimeStampClear",
    "Random"
  })
  
  RegisterEnums("CScriptEntityCollector", {
    "CollectorAllInRange",
    "CollectorBarriersInRange",
    "CollectorBuildingsInRange",
    "CollectorObjectsInRange",
    "CollectorSquadsInRange",
    "CollectorGeneratorsInRange",
    "CollectorMonumentsInRange",
    "CollectorMonumentsAndGeneratorsInRange",
    "CollectorAttackableInRange"
  })
  
  RegisterEnums("CScriptActionAudioStop", {
    "AudioTypeAmbient",
    "AudioTypeMusic",
    "AudioTypeSoundFX",
    "AudioTypeUISoundFX",
    "AudioTypeVoice"
  })
  RegisterEnums("CScriptActionDebugMessageBox", {
    "IconInfo",
    "IconWarning",
    "IconError"
  })
  RegisterEnums("CScriptActionGUIMiniMapAlert", {
    "AlertDefault",
    "AlertAttack",
    "AlertQuest",
    "AlertOutcry",
    "AlertQuickSelect",
    "AlertDead",
    "AlertNotify",
    "AlertWarning",
    "AlertUserMarker",
    "AlertShort",
    "AlertFollowSquad"
  })
  RegisterEnums("CScriptActionGUITutorialMarker", {
    "MarkerPosNone",
    "MarkerPosLeft",
    "MarkerPosLower",
    "MarkerPosLowerLeft",
    "MarkerPosLowerRight",
    "MarkerPosRight",
    "MarkerPosUpper",
    "MarkerPosUpperLeft",
    "MarkerPosUpperRight"
  })
  RegisterEnums("CScriptActionQuantorSquadGoto", {
    "WalkModeCrusade",
    "WalkModePatrol",
    "WalkModeForce",
    "WalkModePartialForce"
  })
  RegisterEnums("CScriptActionQuantorGUITimerPauseRemove", {
    "TimerRemove",
    "TimerPause",
    "TimerResume"
  })
  RegisterEnums("CScriptConditionQuantorEntityHasX", {
    "HasJobIdle",
    "HasJobGoto",
    "HasJobSpellCast",
    "HasAbility",
    "HasAbilityLine",
    "HasHate",
    "IsFighting"
  })
  RegisterEnums("CScriptConditionQuantorEntityInPlayVisArea", {
    "MapAreaPlayable",
    "MapAreaVisible"
  })
--end
if not CScriptConditionQuantor then
  CScriptConditionQuantor = {
    QuantorCheckAll = 1,
    QuantorCheckAny = 2,
    QuantorCheckNotAll = 3,
    QuantorCheckNone = 4
  }
end
ALL = CScriptConditionQuantor.QuantorCheckAll or "ALL"
ANY = CScriptConditionQuantor.QuantorCheckAny or "ANY"
NOTALL = CScriptConditionQuantor.QuantorCheckNotAll or "NOTALL"
NONE = CScriptConditionQuantor.QuantorCheckNone or "NONE"
if not CScriptActionQuantorSquadGoto then
  CScriptActionQuantorSquadGoto = {}
end
tWalkMode = {
  PartialForce = CScriptActionQuantorSquadGoto.WalkModePartialForce or -1,
  Force = CScriptActionQuantorSquadGoto.WalkModeForce or -1,
  Crusade = CScriptActionQuantorSquadGoto.WalkModeCrusade or -1,
  Scout = CScriptActionQuantorSquadGoto.WalkModeScout or -1,
  Patrol = CScriptActionQuantorSquadGoto.WalkModePatrol or -1,
  Normal = CScriptActionQuantorSquadGoto.WalkModeNormal or -1
}
DespawnHealthPercent = 0
function UpgradeLevel0(id)
  return GetUpgradeLevel(id, 0)
end
function UpgradeLevel1(id)
  return GetUpgradeLevel(id, 1)
end
function UpgradeLevel2(id)
  return GetUpgradeLevel(id, 2)
end
function UpgradeLevel3(id)
  return GetUpgradeLevel(id, 3)
end
function GetUpgradeLevel(id, level)
  if type(id) == "table" then
    id = id[1]
  end
  -- it will be promoted two leevels somewhere
  return id % 1000000 + (level - 2) * 1000000
end
ktStateNameParam = {
  sName = "StateName",
  sType = "string",
  bRequired = true,
  bEventParam = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of a state."
}
ktEventsParam = {
  sName = "Add Events here ...",
  bEventList = true,
  sType = "events",
  sDesc = "The events of that state."
}
ktOutcriesParam = {
  sName = "OutcryList",
  sType = "table",
  bRequired = true,
  sDesc = "Outcries, indexed by names of MapFlags."
}
ktEventNameParam = {
  DefaultValue = "",
  sName = "EventName",
  sType = "string",
  bRequired = false,
  bEventParam = true,
  bAllowEmptyString = true,
  sDesc = "The name of the event (for debugging purposes)."
}
ktGotoStateParam = {
  DefaultValue = "self",
  sName = "GotoState",
  sType = "string",
  bRequired = false,
  bEventParam = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the state."
}
ktFinishStateParam = table.copy(ktGotoStateParam)
ktFinishStateParam.sName = "FinishState"
ktFinishStateParam.sDesc = "If the WP can implicitly 'finish' this is the State/WP it will go to."
ktExitStateParam = table.copy(ktGotoStateParam)
ktExitStateParam.sName = "ExitState"
ktExitStateParam.sDesc = "If Exit conditions are true pattern will change to this State."
ktDeathStateParam = table.copy(ktGotoStateParam)
ktDeathStateParam.sName = "DeathState"
ktDeathStateParam.sDesc = "Change to this State on death. Only use to link with a respawn pattern!"
ktAllowExitBeforeStartParam = {
  DefaultValue = false,
  sName = "AllowExitBeforeStart",
  sType = "boolean",
  bRequired = false,
  bEventParam = true,
  sDesc = "Allows ExitConditions to exit WP before it has even started."
}
ktConsiderDeadAsIdleParam = {
  DefaultValue = false,
  sName = "ConsiderDeadAsIdle",
  sType = "boolean",
  bRequired = false,
  sDesc = "Dead Squads are considered idle. Idle timer is immediately elapsed when unit dies!!!"
}
ktUpdateIntervalEventParam = {
  DefaultValue = 0,
  sName = "UpdateInterval",
  sType = "number",
  bRequired = false,
  bEventParam = true,
  sDesc = "The UpdateInterval of the event."
}
ktConditionsParam = {
  DefaultValue = {},
  sName = "Conditions",
  sType = "table",
  bRequired = false,
  sDesc = "The conditions which need to be satisfied to execute the event.",
  sContentType = "condition"
}
ktStartConditionsParam = table.copy(ktConditionsParam)
ktStartConditionsParam.sName = "StartConditions"
ktStartConditionsParam.sDesc = "These conditions must be satisfied to Start the Event/Pattern."
ktStopConditionsParam = table.copy(ktConditionsParam)
ktStopConditionsParam.sName = "StopConditions"
ktStopConditionsParam.sDesc = "The conditions which have to be true simultaneously to Stop."
ktExitConditionsParam = table.copy(ktConditionsParam)
ktExitConditionsParam.sName = "ExitConditions"
ktExitConditionsParam.sDesc = "If these Conditions are true the WP will exit/change to the WP's GotoState."
ktActionsParam = {
  DefaultValue = {},
  sName = "Actions",
  sType = "table",
  bRequired = false,
  sDesc = "The actions that are to be executed.",
  sContentType = "action"
}
ktGoActionsParam = table.copy(ktActionsParam)
ktGoActionsParam.sName = "GoActions"
ktStartActionsParam = table.copy(ktActionsParam)
ktStartActionsParam.sName = "StartActions"
ktFinishActionsParam = table.copy(ktActionsParam)
ktFinishActionsParam.sName = "FinishActions"
ktFinishActionsParam.sDesc = "FinishActions will be executed if WP can 'complete' its task and has completed it."
ktExitActionsParam = table.copy(ktActionsParam)
ktExitActionsParam.sName = "ExitActions"
ktExitActionsParam.sDesc = "ExitActions will only be executed if ExitConditions are given."
ktDeathActionsParam = {
  DefaultValue = {},
  sName = "OnDeathActions",
  sType = "table",
  bRequired = false,
  sDesc = "These actions will be executed when the entity dies.",
  sContentType = "action"
}
ktStartDespawnedEventParam = {
  DefaultValue = false,
  sName = "StartDespawned",
  sType = "boolean",
  bRequired = false,
  bEventParam = true,
  sDesc = "Determines if entity is to be vanished first (aka SpawnOnlyWhen)."
}
ktHomeActionsParam = {
  DefaultValue = {},
  sName = "HomeActions",
  sType = "table",
  bRequired = false,
  sDesc = "Execute these Actions each time Squad 'returns home'.",
  sContentType = "action"
}
ktOneTimeHomeActionsParam = {
  DefaultValue = {},
  sName = "OneTimeHomeActions",
  sType = "table",
  bRequired = false,
  sDesc = "Execute these Actions the first time Squad 'returns home'.",
  sContentType = "action"
}
ktOnConditionsParam = {
  DefaultValue = {},
  sName = "OnConditions",
  sType = "table",
  bRequired = false,
  sDesc = "The conditions which have to be true simultaneously.",
  sContentType = "condition"
}
ktOnActionsParam = {
  DefaultValue = {},
  sName = "OnActions",
  sType = "table",
  bRequired = false,
  sDesc = "The actions that are to be executed.",
  sContentType = "action"
}
ktOffConditionsParam = {
  DefaultValue = {},
  sName = "OffConditions",
  sType = "table",
  bRequired = false,
  sDesc = "The conditions which have to be true simultaneously.",
  sContentType = "condition"
}
ktOffActionsParam = {
  DefaultValue = {},
  sName = "OffActions",
  sType = "table",
  bRequired = false,
  sDesc = "The actions that are to be executed.",
  sContentType = "action"
}
ktTimerStartConditionsParam = {
  DefaultValue = {},
  sName = "TimerStartConditions",
  sType = "table",
  bRequired = false,
  sDesc = "When these Conditions are satisfied, the timer will be started.",
  sContentType = "condition"
}
ktTimerStartActionsParam = {
  DefaultValue = {},
  sName = "TimerStartActions",
  sType = "table",
  bRequired = false,
  sDesc = "These Actions will be executed when the timer is started.",
  sContentType = "action"
}
ktTimerStopConditionsParam = {
  DefaultValue = {},
  sName = "TimerElapsedConditions",
  sType = "table",
  bRequired = false,
  sDesc = "The timer must be elapsed AND these Conditions must be satisfied to stop the timer.",
  sContentType = "condition"
}
ktTimerStopActionsParam = {
  DefaultValue = {},
  sName = "TimerElapsedActions",
  sType = "table",
  bRequired = false,
  sDesc = "These Actions will be executed when the timer is stopped.",
  sContentType = "action"
}
ktForAnyParam = {
  DefaultValue = "ALL",
  sName = "For",
  sType = "string",
  bRequired = false,
  bModifyValue = true,
  bQuantor = true,
  sAllowedChars = ksPlayerAllowedChars,
  sDesc = "Determines when the condition is satisfied if multiple entities are used."
}
ktSecondsParam = {
  DefaultValue = 0,
  sName = "Seconds",
  sType = "number",
  bRequired = false,
  bModifyValue = true,
  iModifyFactor = 10,
  sDesc = "Number, in Seconds. Fractions are also allowed, in 0.1 increments."
}
ktMinutesParam = {
  DefaultValue = 0,
  sName = "Minutes",
  sType = "number",
  bRequired = false,
  bModifyValue = true,
  iModifyFactor = 600,
  sDesc = "Number, in Minutes. Fractions are also allowed."
}
ktTimerEventSecondsParam = {
  DefaultValue = 3,
  sName = "Seconds",
  sType = "number",
  bRequired = false,
  bEventParam = true,
  sDesc = "How much time must pass before the event fires again."
}
ktTimerEventMinutesParam = {
  DefaultValue = 0,
  sName = "Minutes",
  sType = "number",
  bRequired = false,
  bEventParam = true,
  sDesc = "How much time must pass before the event fires again."
}
ktTimerEventOneTimeConstant = {
  Value = 1,
  sName = "OneTime",
  sType = "number",
  bConstant = true,
  bRequired = false,
  bEventParam = true,
  sDesc = ""
}
ktTimerEventMultipleTimeConstant = {
  Value = 0,
  sName = "OneTime",
  sType = "number",
  bConstant = true,
  bRequired = false,
  bEventParam = true,
  sDesc = ""
}
ktSpawnGroupNameParam = {
  sName = "GroupName",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The base Name of the SpawnGroup."
}
ktSpawnGroupMaxActiveParam = {
  DefaultValue = 1,
  sName = "MaxActive",
  sType = "number",
  bRequired = false,
  sDesc = "How many of this Group can be on the map at once. Script is executed this many times!"
}
ktNetworkFlagNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "nf_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the network variable."
}
ktMapFlagNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "mf_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the map variable."
}
ktPlayerFlagNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "pf_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the player variable."
}
ktEntityFlagNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "ef_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  bAddEntityVarSuffix = true,
  sDesc = "The name of the entity variable."
}
ktNetworkValueNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "nv_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the network variable. Value is limited to 0-255 range!"
}
ktMapValueNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "mv_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the map variable."
}
ktPlayerValueNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "pv_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the player variable."
}
ktEntityValueNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "ev_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  bAddEntityVarSuffix = true,
  sDesc = "The name of the entity variable."
}
ktMapVariable1Param = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "mv_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the map variable."
}
ktMapVariable2Param = table.copy(ktMapVariable1Param)
ktMapVariable2Param.sName = "CompareTo"
ktMapVariable2Param.sDesc = "The name of the map variable to compare with."
ktMapVariableActionParam = table.copy(ktMapVariable1Param)
ktMapVariableActionParam.sName = "VarName"
ktMapVariableActionParam.sDesc = "The name of the map variable to use in the computation."
ktEntityVariable1Param = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "ev_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  bAddEntityVarSuffix = true,
  sDesc = "The name of the entity variable."
}
ktEntityVariable2Param = table.copy(ktEntityVariable1Param)
ktEntityVariable2Param.sName = "CompareTo"
ktEntityVariable2Param.sDesc = "The name of the entity variable to compare with."
ktEntityVariableActionParam = table.copy(ktEntityVariable1Param)
ktEntityVariableActionParam.sName = "VarName"
ktEntityVariableActionParam.sDesc = "The name of the entity variable to use in the computation."
ktPlayerVariableParam = {
  sName = "Variable",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "pv_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the player variable."
}
ktMapTimerNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "mt_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the map timer."
}
ktPlayerTimerNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "pt_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the player timer."
}
ktMissionTimerNameParam = {
  sName = "TimerTag",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  sModifyPrefix = "pt_GUITimer_",
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the Mission Timer."
}
ktMissionCounterNameParam = {
  sName = "CounterTag",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  sModifyPrefix = "pv_GUICounter_",
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the Mission Counter."
}
ktSquadSpawnCounterNameParam = {
  sName = "Group",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  bModifyToUpper = true,
  sModifyPrefix = "mv_ScriptGroupSquadSpawnCounter__",
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the ScriptGroup for which a SquadSpawnCounter is registered."
}
ktSquadDeathCounterNameParam = {
  sName = "Group",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  bModifyToUpper = true,
  sModifyPrefix = "mv_ScriptGroupSquadDeathCounter__",
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of the ScriptGroup for which a SquadDeathCounter is registered."
}
ktEntityTimerNameParam = {
  sName = "Name",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "et_",
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sAllowedChars = ksAllowedChars,
  bAddEntityVarSuffix = true,
  sDesc = "The name of the entity timer."
}
ktValueParam = {
  sName = "Value",
  sType = "number",
  bRequired = true,
  sDesc = "Just a simple value, meaning depends on context."
}
ktDifficultyParam = {
  sName = "Difficulty",
  sType = "number",
  bRequired = true,
  sDesc = "One of: DifficultyStandard, DifficultyAdvanced, DifficultyExpert"
}
ktMaxValueParam = {
  sName = "MaxValue",
  sType = "number",
  bRequired = true,
  sDesc = "The upper limit value the Randomize command will generate."
}
ktAmountParam = {
  sName = "Amount",
  sType = "number",
  bRequired = true,
  sDesc = "Just a simple value, meaning depends on context."
}
ktSpawnAmountParam = {
  DefaultValue = 1,
  sName = "Amount",
  sType = "number",
  bRequired = false,
  sDesc = "Just a simple value, meaning depends on context."
}
ktPercentParam = {
  sName = "Percent",
  sType = "number",
  bRequired = true,
  sDesc = "A percentage from 0-100 (do not write the % sign)."
}
ktHealthPercentParam = {
  DefaultValue = 100,
  sName = "HealthPercent",
  sType = "number",
  bRequired = false,
  sDesc = "The amount of health in percentage of max. health."
}
ktHealthPercentEventParam = table.copy(ktHealthPercentParam)
ktHealthPercentEventParam.bEventParam = true
ktEffectPercentParam = table.copy(ktHealthPercentParam)
ktEffectPercentParam.sName = "Percent"
ktEffectPercentParam.sDesc = "The 'power' percentage of the effect, eg. how big the effect is or which state it is in."
ktMaxHealthAbsoluteParam = {
  sName = "MaxHealthAbsolute",
  sType = "number",
  bRequired = true,
  sDesc = "The amount of max health in absolute number."
}
ktPowerParam = {
  sName = "Power",
  sType = "number",
  bRequired = true,
  sDesc = "Can be any of: PowerShadow, PowerNature, PowerFrost, PowerFire, PowerAll, ..."
}
ktGridGotoFlyingSpecialParam = {
  DefaultValue = false,
  sName = "IgnoreGridIfAllSquadsFly",
  sType = "boolean",
  bRequired = false,
  sDesc = "Ignores grid if (and only if) all Squads of the group are flying units."
}
ktAbilityAddToFiguresParam = {
  DefaultValue = true,
  sName = "ApplyToFigures",
  sType = "boolean",
  bRequired = false,
  sDesc = "Add to/remove from the Squad's figures, not the Squad itself."
}
ktDoDamageParam = {
  DefaultValue = true,
  sName = "DoDamage",
  sType = "boolean",
  bRequired = false,
  sDesc = "Does damage instead of setting health."
}
ktExecuteSpellParam = {
  DefaultValue = true,
  sName = "DoSpell",
  sType = "boolean",
  bRequired = false,
  sDesc = "Wether the CardSquad's Spell should be executed or not."
}
ktIgnoreEmptyGeneratorsParam = {
  DefaultValue = false,
  sName = "IgnoreEmpty",
  sType = "boolean",
  bRequired = false,
  sDesc = "Wether empty (no more energy left) Generators will be ignored."
}
ktIgnoreUnattackableParam = {
  DefaultValue = false,
  sName = "IgnoreUnattackable",
  sType = "boolean",
  bRequired = false,
  sDesc = "Wether targets with 'Unattackable' AbilityLine will be ignored."
}
ktPlayCheerAnimParam = {
  DefaultValue = true,
  sName = "DoCheer",
  sType = "boolean",
  bRequired = false,
  sDesc = "Wether the Squad's cheer animation should be played or not."
}
ktCheckGotoJobParam = {
  DefaultValue = true,
  sName = "DoGotoCheck",
  sType = "boolean",
  bRequired = false,
  sDesc = "Additionally checks if entity is in Goto mode."
}
ktRandomMinValueParam = {
  DefaultValue = 0,
  sName = "MinValue",
  sType = "number",
  bRequired = false,
  sDesc = "Lowest random value that may occur."
}
ktRandomMaxValueParam = {
  DefaultValue = 1,
  sName = "MaxValue",
  sType = "number",
  bRequired = false,
  sDesc = "Highest random value that may occur."
}
ktScriptGroupParam = {
  sDefaultValueGetFunction = "GetScriptTag()",
  sName = "Group",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "A ScriptGroup."
}
ktScriptTagParam = {
  sDefaultValueGetFunction = "GetScriptTag()",
  sName = "Tag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  sDesc = "A ScriptTag or ScriptGroup."
}
ktScriptTagEventParam = table.copy(ktScriptTagParam)
ktScriptTagEventParam.bEventParam = true
ktTimerTagParam = {
  sName = "TimerTag",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "A unique Tag for the Mission Timer."
}
ktTimerTagOldParam = table.copy(ktTimerTagParam)
ktTimerTagOldParam.sName = "TimerTagOld"
ktTimerTagOldParam.sDesc = "The TimerTag of the existing timer whose text should be changed."
ktCounterTagParam = {
  sName = "CounterTag",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "A unique Tag for the Mission Counter."
}
ktLocaTagParam = {
  sName = "LocaTag",
  sType = "string",
  bRequired = true,
  sDesc = "A LocaTag from database, if LocaTag does not exist it's simply the Text displayed."
}
ktScriptTagTargetParam = {
  sName = "TargetTag",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The ScriptTag of the 'target' entity."
}
ktTargetTagParam = ktScriptTagTargetParam
ktScriptTagTargetEventParam = table.copy(ktScriptTagTargetParam)
ktScriptTagTargetEventParam.bEventParam = true
ktAimTagParam = {
  DefaultValue = "",
  sName = "AimTag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  bAllowEmptyString = true,
  sDesc = "Where to aim at (direction). Can be empty if not needed. ScriptGroup not supported."
}
ktCameraTargetTagParam = {
  DefaultValue = "",
  sName = "TargetTag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  bAllowEmptyString = true,
  sDesc = "The TargetTag for relative positioning of the camera. Can be an empty string."
}
ktScriptTagTargetDefaultParam = {
  sDefaultValueGetFunction = "GetScriptTag()",
  sName = "TargetTag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  sDesc = "The ScriptTag of the 'target' entity. (It should exist!)"
}
ktTargetTagDefaultParam = ktScriptTagTargetDefaultParam
ktScriptTagSpawnTargetParam = {
  DefaultValue = "StartPos",
  sName = "TargetTag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  sDesc = "'WhereDied' for position of death or 'StartPos' for startposition."
}
ktScriptTagSpawnTargetEventParam = table.copy(ktScriptTagSpawnTargetParam)
ktScriptTagSpawnTargetEventParam.bEventParam = true
ktScriptTagPlayerParam = {
  DefaultValue = "ALL",
  sName = "Player",
  sType = "string",
  bRequired = false,
  bModifyValue = true,
  bPlayerQuantor = true,
  sAllowedChars = ksPlayerAllowedChars,
  sDesc = "The name of a Player or a ScriptGroup of Players or 'All'/'Any'/etc."
}
ktPlayerParam = ktScriptTagPlayerParam
ktScriptTagPlayerNoQuantorParam = {
  sName = "Player",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "pl_",
  sAllowedChars = ksPlayerAllowedChars,
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sDesc = "The name of a Player. Must not be a ScriptGroup and not 'All'/'Any'/etc."
}
ktPlayerNoQuantorParam = ktScriptTagPlayerNoQuantorParam
ktScriptTagPlayerEventParam = {
  sName = "Player",
  sType = "string",
  bRequired = true,
  bEventParam = true,
  sRequiredPrefix = "pl_",
  sAllowedChars = ksPlayerAllowedChars,
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sDesc = "The name of a Player."
}
ktPlayerEventParam = ktScriptTagPlayerEventParam
ktScriptTagMonumentParam = {
  sDefaultValueGetFunction = "GetScriptTag()",
  sName = "MonumentTag",
  sType = "string",
  bRequired = false,
  sAllowedChars = ksAllowedChars,
  sDesc = "The ScriptTag of a Monument."
}
ktTeamParam = {
  sName = "Team",
  sType = "string",
  bRequired = true,
  sRequiredPrefix = "tm_",
  sAllowedChars = ksAllowedChars,
  sAllowedCharsAfterPrefix = ksAllowedCharsAfterPrefix,
  sDesc = "The name of a Team from the map's TeamSetup."
}
ktPvETypeParam = {
  sName = "PvEType",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The name of a PvE Type."
}
ktUpdateIntervalParam = {
  DefaultValue = 0,
  sName = "UpdateInterval",
  sType = "number",
  bRequired = false,
  sDesc = "How often Condition is updated, default is 0 (== update every GD step). "
}
ktDirectionParam = {
  DefaultValue = -180,
  sName = "Direction",
  sType = "number",
  bRequired = false,
  bModifyValue = true,
  iModifyFactor = math.pi / 180,
  sDesc = "Direction in degrees (0-360°). 0 means 'south' (down), 90 means 'east' (right), etc."
}
ktDirectionEventParam = table.copy(ktDirectionParam)
ktDirectionEventParam.bEventParam = true
ktVariationParam = table.copy(ktDirectionParam)
ktVariationParam.sName = "Variation"
ktHeightParam = {
  DefaultValue = 0,
  sName = "Height",
  sType = "number",
  bRequired = false,
  sDesc = "Height in meters. Decimal places and negative values are ok, 0 == on the ground"
}
ktScalingParam = {
  DefaultValue = 1,
  sName = "Scaling",
  sType = "number",
  bRequired = false,
  sDesc = "Scaling Factor, 1.0 is default scaling (100%)."
}
ktDistanceParam = {
  sName = "Distance",
  sType = "number",
  bRequired = true,
  sDesc = "Distance in meters. Decimal places are allowed."
}
ktBlastRadiusParam = table.copy(ktDistanceParam)
ktBlastRadiusParam.sName = "BlastRadius"
ktPushBackSpeedParam = {
  sName = "Speed",
  sType = "number",
  bRequired = true,
  sDesc = "Speed in meters per second. Decimal places allowed."
}
RadiusType_S = 1
RadiusType_M = 2
RadiusType_L = 3
RadiusType_XL = 4
RadiusType_XXL = 5
RadiusType_XXXL = 6
ktPushBackMaxRadiusTypeParam = {
  sName = "MaxRadiusType",
  sType = "number",
  bRequired = true,
  sDesc = "Can be: RadiusType_S, .._M, .._L, .._XL (see BEE)."
}
ktResIdParam = {
  sName = "Id",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of an entity."
}
ktModeIdParam = {
  sName = "ModeId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a Mode"
}
ktSquadIdParam = {
  sName = "SquadId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a Squad (2000000 is automatically added to the number!)"
}
ktCardSquadIdParam = {
  sName = "CardSquadId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a CardSquad"
}
ktCardSpellIdParam = {
  sName = "CardSpellId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a CardSpell"
}
ktCardBuildingIdParam = {
  sName = "CardBuildingIdParam",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a CardBuilding"
}
ktObjectIdParam = {
  sName = "ObjectId",
  sType = "number",
  bRequired = true,
  sDesc = "The DB ID of an Object"
}
ktBuildingIdParam = {
  sName = "BuildingId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a Building"
}
ktAbilityIdParam = {
  sName = "AbilityId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of an Ability"
}
ktAbilityLineIdParam = {
  sName = "AbilityLineId",
  sType = "number",
  bRequired = true,
  sDesc = "The DB ID of an AbilityLine"
}
AnyCard = 0
ktCardIdParam = {
  sName = "CardId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  sDesc = "The DB ID of a Card"
}
AnySpell = 0
ktSpellIdParam = {
  sName = "SpellId",
  sType = "number",
  bRequired = true,
  bModifyValue = true,
  bModifyOnlyIfNotZero = true,
  iModifyAddValue = 1 * 2,
  sDesc = "The DB ID of a Spell"
}
ktVolumeParam = {
  DefaultValue = 1,
  sName = "Volume",
  sType = "number",
  bRequired = false,
  sDesc = "The Voice Volume, 1.0 is default, higher means louder."
}
ktVoiceFileParam = {
  sName = "Voice",
  sType = "string",
  bRequired = true,
  bAllowEmptyString = true,
  sDesc = "file relative to '" .. "bf1/sound/streams/" .. "', no extension!"
}
ktMusicFileParam = {
  sName = "Music",
  sType = "string",
  bRequired = true,
  bAllowEmptyString = true,
  sDesc = "file relative to '" .. "bf1/sound/streams/" .. "', no extension!"
}
ktSoundFileParam = {
  sName = "Sound",
  sType = "string",
  bRequired = true,
  sDesc = "file relative to '" .. "bf1/sound/ram/" .. "', no extension!"
}
ktAmbientStreamFileParam = {
  sName = "Ambient",
  sType = "string",
  bRequired = true,
  sDesc = "file relative to '" .. "bf1/sound/streams/" .. "', no extension!"
}
ktUISoundFileParam = {
  sName = "Sound",
  sType = "string",
  bRequired = true,
  sDesc = "file relative to '" .. "bf1/sound/ram/" .. "', no extension!"
}
ktEffectFileParam = {
  sName = "Effect",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  sModifyPath = "bf1/gfx/effects/",
  sModifyExtension = ".fxb",
  sDesc = "file relative to '" .. "bf1/gfx/effects/" .. "', no extension!"
}
ksBasePathObjectAnim = "bf1/gfx/test/buildings/october_portal/"
ksBasePathFigureAnim = "bf1/gfx/units/"
ksBasePathBuildingAnim = "bf1/gfx/buildings/quest/"
ktWeatherFileParam = {
  sName = "Weather",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  sModifyPath = "bf1/map/lighting/",
  sModifyExtension = ".xml",
  sDesc = "RenderSettings file from '" .. "bf1/map/lighting/" .. "' folder, w/o extension!"
}
ktWeatherFileParamUserGenerated = {
  sName = "Weather",
  sType = "string",
  bRequired = true,
  bModifyValue = true,
  sModifyPath = "",
  sModifyExtension = ".xml",
  sDesc = "RenderSettings file absolute path, w/o extension!"
}
ktAnimFileParam = {
  sName = "Anim",
  sType = "string",
  bRequired = true,
  sDesc = "Name of the Animation (may include subfolders with forward slashes, eg: 'folder1/folder2/my-anim')."
}
ktUnitFolderParam = {
  sName = "Unit",
  sType = "string",
  bRequired = true,
  sDesc = "A subfolder from the '" .. ksBasePathFigureAnim .. "' folder."
}
ktCameraPathParam = {
  sDefaultValueGetFunction = "GetCameraPath()",
  sName = "CameraPath",
  sType = "string",
  bRequired = false,
  bConstant = true,
  sDesc = "Camera path."
}
ktCameraTakesParam = {
  sName = "Takes",
  sType = "table",
  bRequired = true,
  sContentType = "string",
  sDesc = "Camera takes from the 'camera' folder of the map (without .TAK)."
}
ktCameraParam = {
  sName = "Camera",
  sType = "string",
  bRequired = true,
  sContentType = "string",
  bModifyValue = true,
  sModifyPath = "",
  sModifyExtension = ".cs",
  sDesc = "Camera takes from the 'camera' folder of the map (without extension '.CS')."
}
ktRangeParam = {
  DefaultValue = 10,
  sName = "Range",
  sType = "number",
  bRequired = false,
  sDesc = "The Radius around the TargetTag(s) that is considered to be 'in range'."
}
ktSearchRadiusParam = {
  sName = "SearchRadius",
  sType = "number",
  bRequired = true,
  sDesc = "The (max.) Radius around the Tag that should be searched."
}
ktRangeEventParam = {
  DefaultValue = 8,
  sName = "HomeRange",
  sType = "number",
  bRequired = false,
  bEventParam = true,
  sDesc = "If Squad is not within HomeRange to Target it will return."
}
ktAreaParam = {
  sName = "Area",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The named area that has been defined in the map editor."
}
ktMessageBoxMessageParam = {
  DefaultValue = "<no message specified by scripter>",
  sName = "Message",
  sType = "string",
  bRequired = true,
  bAllowEmptyString = true,
  sDesc = "An important Message from you to whom it may concern."
}
ktShowMessageBoxParam = {
  DefaultValue = false,
  sName = "Popup",
  sType = "boolean",
  bRequired = false,
  sDesc = "If true, message will also popup in a windows Message Box."
}
ktRespawnDelaySecondsEventParam = {
  DefaultValue = 0,
  sName = "RespawnDelaySeconds",
  sType = "number",
  bEventParam = true,
  bRequired = false,
  sDesc = "Wait at least this many seconds before respawning."
}
ktRespawnDelayMinutesEventParam = {
  DefaultValue = 0,
  sName = "RespawnDelayMinutes",
  sType = "number",
  bEventParam = true,
  bRequired = false,
  sDesc = "Wait at least this many minutes before respawning."
}
ktSquadSpawnRandomSelection = {
  sName = "SpawnTable",
  sType = "table",
  bRequired = true,
  bPassTableToCode = true,
  sDesc = "Must be a table with specific syntax, see command description."
}
ktWalkModeRunParam = {
  DefaultValue = true,
  sName = "Run",
  sType = "boolean",
  bRequired = false,
  sDesc = "Changes WalkSpeed, running is default. Walking may look dull for large figures."
}
ktWalkModeRunEventParam = table.copy(ktWalkModeRunParam)
ktWalkModeRunEventParam.bEventParam = true
ktTextTagParam = {
  sName = "TextTag",
  sType = "string",
  bRequired = true,
  bAllowEmptyString = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The Tag of a Text from database."
}
ktTextParam = {
  sName = "Text",
  sType = "string",
  bRequired = true,
  sDesc = "The text to show if the TextTag text does not exist."
}
ktShortTextParam = {
  sName = "Summary",
  sType = "string",
  bRequired = true,
  sDesc = "A short summary for the MissionGoal or EventQueue message."
}
ktDetailedTextParam = {
  DefaultValue = "<no description>",
  sName = "Description",
  sType = "string",
  bRequired = false,
  sDesc = "The detailed text for the MissionGoal or EventQueue message."
}
ktMissionTaskTagParam = {
  sName = "TaskTag",
  sType = "string",
  bRequired = true,
  sDesc = "The 'tag' of the MissionTask to be modified."
}
ktEventQueueTypeParam = {
  sName = "EventType",
  sType = "number",
  bRequired = true,
  sDesc = "one of: EventNotify, EventWarning, EventSuccess, EventFailed"
}
ktMissionTaskTypeParam = {
  sName = "TaskType",
  sType = "number",
  bRequired = true,
  sDesc = "The type of the MissionTask."
}
ktMiniMapAlertTypeParam = {
  sName = "AlertType",
  sType = "number",
  bRequired = true,
  sDesc = "The type of a MiniMap Alert (determines gfx & color)."
}
ktRepeatCountParam = {
  DefaultValue = 1,
  sName = "RepeatCount",
  sType = "number",
  bRequired = false,
  sDesc = "How often this sound should be repeated, default is once."
}
ktKeyComboEventParam = {
  sName = "Key",
  sType = "string",
  bRequired = true,
  bEventParam = true,
  sDesc = "The type of key as string."
}
ktKeyShiftEventParam = {
  DefaultValue = false,
  sName = "Shift",
  sType = "boolean",
  bRequired = false,
  bEventParam = true,
  sDesc = "Wether Shift key must also be pressed."
}
ktKeyCtrlEventParam = {
  DefaultValue = false,
  sName = "Ctrl",
  sType = "boolean",
  bRequired = false,
  bEventParam = true,
  sDesc = "Wether Ctrl key must also be pressed."
}
ktKeyAltEventParam = {
  DefaultValue = false,
  sName = "Alt",
  sType = "boolean",
  bRequired = false,
  bEventParam = true,
  sDesc = "Wether Alt key must also be pressed."
}
ktFigureRenderOnlyParam = {
  DefaultValue = "",
  sName = "RenderOnly",
  sType = "string",
  bRequired = false,
  bAllowEmptyString = true,
  sDesc = "ScriptTag/Group with Squads that should be rendered during Cutscene."
}
ktAttackGroupSizeParam = {
  DefaultValue = 0,
  sName = "AttackGroupSize",
  sType = "number",
  bRequired = false,
  sDesc = "Squads will attack all available Targets in Groups of this size."
}
ktDurationSecondsParam = {
  DefaultValue = 5,
  sName = "DurationSeconds",
  sType = "number",
  bRequired = false,
  sDesc = "Duration for the outcry in integer seconds, default is 5."
}
ktFadeDurationParam = {
  DefaultValue = 3,
  sName = "FadeDuration",
  sType = "number",
  bRequired = false,
  sDesc = "Duration in Seconds for Fading between two Weather/Render Settings."
}
ktPortraitFileNameParam = {
  DefaultValue = "",
  sName = "PortraitFileName",
  sType = "string",
  bRequired = false,
  bModifyValue = true,
  bAllowEmptyString = true,
  sModifyPath = "bf1/gfx/ui/ingame/outcry_camera/outcry_portraits/",
  sModifyExtension = ".tga",
  sDesc = "File name number of the portrait."
}
ktAttackGroupSizeEventParam = table.copy(ktAttackGroupSizeParam)
ktAttackGroupSizeEventParam.bEventParam = true
ktTutorialMarkerTagParam = {
  sName = "MarkerTag",
  sType = "string",
  bRequired = true,
  sAllowedChars = ksAllowedChars,
  sDesc = "The tag of this UI Marker, used to properly (re)move it later."
}
ktWidgetNameParam = {
  sName = "Widget",
  sType = "string",
  bRequired = true,
  sDesc = "The name (reference string) of a Widget."
}
ktMarkerFileParam = {
  sName = "MarkerFileName",
  sType = "string",
  bModifyValue = true,
  sModifyPath = "bf1/ui/",
  sModifyExtension = ".xml",
  bRequired = true,
  sDesc = "The template file of the marker. Folder: " .. "bf1/ui/"
}
ktDetailFileParam = {
  DefaultValue = "",
  sName = "DetailFileName",
  sType = "string",
  bModifyValue = true,
  sModifyPath = "bf1/ui/",
  sModifyExtension = ".xml",
  bRequired = false,
  bAllowEmptyString = true,
  sDesc = "The template file of the detail stuff (optional). Folder: " .. "bf1/ui/"
}
ktTutorialMarkerPositionParam = {
  DefaultValue = MarkerPosNone,
  sName = "Position",
  sType = "number",
  bRequired = false,
  sDesc = "The position of the tutorial marker, or MarkerPosNone for default."
}
ktSlotParam = {
  sName = "Slot",
  sType = "number",
  bRequired = true,
  sDesc = "The position of the Card in the Deck, starting with 0."
}
