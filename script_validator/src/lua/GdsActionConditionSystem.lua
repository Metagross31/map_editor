function DefineAction(_ARG_0_)
  _ARG_0_.sType = "action"
  _ARG_0_.bAction = true
  DefineActionCondition(_ARG_0_)
end
function DefineCondition(_ARG_0_)
  _ARG_0_.sType = "condition"
  _ARG_0_.bCondition = true
  DefineActionCondition(_ARG_0_)
end
function DefineFilter(_ARG_0_)
  _ARG_0_.sType = "filter"
  _ARG_0_.bFilter = true
  DefineActionCondition(_ARG_0_)
end
function DefineFilterCommand(_ARG_0_)
  _ARG_0_.bDocumentationOnly = true
  _ARG_0_.sFunction = _ARG_0_.sName
  _G[_ARG_0_.sName] = _ARG_0_.fCommand
  if _ARG_0_.sNegated then
    _G[_ARG_0_.sNegated] = function(l_1_0)
      return (_G[_ARG_0_.sName](l_1_0, true))
    end
  end
end
function DefineFilterAction(_ARG_0_)
  DefineFilterCommand(_ARG_0_)
  DefineAction(_ARG_0_)
end
function DefineFilterCondition(_ARG_0_)
  DefineFilterCommand(_ARG_0_)
  DefineCondition(_ARG_0_)
end
function DefineActionCondition(_ARG_0_)
  if not _ARG_0_.bDocumentationOnly then
    PropagateConstantValues(_ARG_0_)
    table.insert(tActionConditionSpecs, _ARG_0_)
  end
end
function DefineActionsLibrary(_ARG_0_)
end
function DefineConditionsLibrary(_ARG_0_)
end
function RegisterActionsConditions()
  table.foreachi(tActionConditionSpecs, function(_ARG_0_, _ARG_1_)
    RegisterActionConditionFunction(_ARG_0_, _ARG_1_.sName, false)
    if type(_ARG_1_.sNegated) == "string" then
      RegisterActionConditionFunction(_ARG_0_, _ARG_1_.sNegated, true)
    end
  end)
end
function RegisterActionConditionFunction(_ARG_0_, _ARG_1_, _ARG_2_)
  if _ARG_2_ then
  end
  dostring((string.gsub(string.gsub(string.gsub([[
-- see complete dostring() for '_sFuncName' below:
	function _sFuncName (_tParams)
		local _tCheckedParams = CheckParams('_sFuncName', tActionConditionSpecs[_iIndex], _tParams, _bNegated)
		PostProcessParams(_tCheckedParams)
		CreateExecuteString(_tCheckedParams)
		return _tCheckedParams
	end]], "_sFuncName", _ARG_1_), "_iIndex", tostring(_ARG_0_)), "_bNegated", "true")))
end
function CreateExecuteString(param_0)
  if param_0.bLuaFunction then
    tParams = param_0
    param_0.sExecute = "return " .. param_0.sFunction .. "(tParams)"
    param_0 = dostring(param_0.sExecute)
    tParams = nil
  else
    local l_12_1 = {}
    table.insert(l_12_1, param_0.sFunction)
    table.insert(l_12_1, "(")
    param_0.tCheckedParams = {}
    table.foreachi(param_0, function(_ARG_0_, _ARG_1_)
      if type(_ARG_1_) == "string" then
        _ARG_1_ = string.format("[=[%s]=]", _ARG_1_)
      elseif type(_ARG_1_) == "table" then
        tStateMachine.iTempTableCount = tStateMachine.iTempTableCount + 1
        table.insert(tStateMachine._TempTables, _ARG_1_)
        _ARG_1_ = "tStateMachine._TempTables[" .. tStateMachine.iTempTableCount .. "]"
      end
      if _ARG_0_ == 1 then
        table.insert(l_12_1, tostring(_ARG_1_))
      else
        table.insert(l_12_1, ", ")
        table.insert(l_12_1, tostring(_ARG_1_))
      end
      param_0.tCheckedParams[_ARG_0_] = param_0[_ARG_0_]
      param_0[_ARG_0_] = nil
    end)
    table.insert(l_12_1, ")")
    param_0.sExecute = table.concat(l_12_1, "")
  end
end
function CreateParamString(_ARG_0_)
  if not FINAL then
    if _ARG_0_.tLogicalConditions then
      _UPVALUE0_ = _UPVALUE0_ + 1
      for _FORV_7_, _FORV_8_ in ipairs(_ARG_0_.tLogicalConditions) do
        table.insert({
          _ARG_0_.sName,
          string.rep("    ", _UPVALUE0_) .. "{"
        }, string.rep("    ", _UPVALUE0_) .. CreateParamString(_FORV_8_))
      end
      _UPVALUE0_ = _UPVALUE0_ - 1
      table.insert({
        _ARG_0_.sName,
        string.rep("    ", _UPVALUE0_) .. "{"
      }, string.rep("    ", _UPVALUE0_) .. "}" .. "    ")
      return table.concat({
        _ARG_0_.sName,
        string.rep("    ", _UPVALUE0_) .. "{"
      }, "\n")
    else
      table.sort((table.keys(_ARG_0_.tUserParams)))
      table.foreach(table.keys(_ARG_0_.tUserParams), function(_ARG_0_, _ARG_1_)
        if type(_UPVALUE0_.tUserParams[_ARG_1_]) == "string" then
        end
        if _UPVALUE1_ then
          _UPVALUE1_ = false
          _UPVALUE2_ = string.format("%s%s = %s", _UPVALUE2_, _ARG_1_, tostring((string.format("\"%s\"", _UPVALUE0_.tUserParams[_ARG_1_]))))
        else
          _UPVALUE2_ = string.format("%s, %s = %s", _UPVALUE2_, _ARG_1_, tostring((string.format("\"%s\"", _UPVALUE0_.tUserParams[_ARG_1_]))))
        end
      end)
      return string.format("%s}", (string.format("%s {", _ARG_0_.sName)))
    end
  end
  return ""
end
function PostProcessParams(_ARG_0_)
  if string.find(_ARG_0_.sName, "Elapsed", 1, 1) then
    if string.find(_ARG_0_.sName, "Player", 1, 1) or string.find(_ARG_0_.sName, "Mission", 1, 1) then
      AddAndMergeParameters(_ARG_0_, 5, 6)
    elseif string.find(_ARG_0_.sName, "SquadIdleTimer", 1, 1) then
      AddAndMergeParameters(_ARG_0_, 4, 5)
    else
      AddAndMergeParameters(_ARG_0_, 3, 4)
    end
  elseif string.find(_ARG_0_.sName, "MissionTimerStart", 1, 1) then
    AddAndMergeParameters(_ARG_0_, 4, 5)
  end
end
function AddAndMergeParameters(_ARG_0_, _ARG_1_, _ARG_2_)
  _ARG_0_.ParamsArray[_ARG_1_] = _ARG_0_[_ARG_1_].iModifyFactor * _ARG_0_.ParamsArray[_ARG_1_] + _ARG_0_[_ARG_2_].iModifyFactor * _ARG_0_.ParamsArray[_ARG_2_]
  table.remove(_ARG_0_, _ARG_2_)
  table.remove(_ARG_0_.ParamsArray, _ARG_2_)
end
function LogicalCombination(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
  if not tStateMachine.tLogicalConditions then
    tStateMachine.tLogicalConditions = {}
  end
  _ARG_0_.UpdateInterval = _ARG_0_.UpdateInterval or 0
  return {
    sExecute = string.format("CScriptConditionLogical(%i, %i)", _ARG_0_.UpdateInterval, _ARG_1_),
    sFunction = "CScriptConditionLogical",
    sName = _ARG_3_,
    sNegated = _ARG_3_,
    bNegated = _ARG_2_,
    sType = "condition",
    tLogicalConditions = _ARG_0_
  }
end
function AND(_ARG_0_)
  return LogicalCombination(_ARG_0_, CScriptConditionLogical.LogicAND, false, "AND")
end
function OR(_ARG_0_)
  return LogicalCombination(_ARG_0_, CScriptConditionLogical.LogicOR, false, "OR")
end
function NotAND(_ARG_0_)
  return LogicalCombination(_ARG_0_, CScriptConditionLogical.LogicAND, true, "NotAND")
end
function NotOR(_ARG_0_)
  return LogicalCombination(_ARG_0_, CScriptConditionLogical.LogicOR, true, "NotOR")
end
function CreateFilterCommand(_ARG_0_)
  if DEBUG then
    assert(type(_ARG_0_) == "table", "Filter must be a table!")
    assert(type(_ARG_0_[1]) == "table", "Filter command must be a table!")
    assert(not _ARG_0_[1].tFilters, "huh? filters already exist!")
    assert(not _ARG_0_[1].tTargetFilters, "huh? target filters already exist!")
  end
  _ARG_0_[1].tFilters = _ARG_0_.Filters or {}
  _ARG_0_[1].tTargetFilters = _ARG_0_.TargetFilters or {}
  if _ARG_0_.bNegated then
    _ARG_0_[1].bNegated = _ARG_0_.bNegated
  end
  return _ARG_0_[1]
end
function FilterCondition(_ARG_0_)
  return CreateFilterCommand(_ARG_0_, "Condition")
end
function FilterAction(_ARG_0_)
  return CreateFilterCommand(_ARG_0_, "Action")
end
